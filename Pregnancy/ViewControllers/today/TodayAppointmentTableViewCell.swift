//
//  TodayAppointmentTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class TodayAppointmentTableViewCell: UITableViewCell {

    @IBOutlet weak var uvAppointmentBox: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uvAppointmentBox.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
