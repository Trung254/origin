//
//  TodayDueDateTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class TodayDueDateTableViewCell: UITableViewCell {

    @IBOutlet weak var uvDueDateBox: UIView!
    @IBOutlet weak var uvDueDateIcon: UIView!
    @IBOutlet weak var mlblDueDate: UILabel!
    @IBOutlet weak var mlblSetup: UILabel!
    @IBOutlet weak var mlblMonth: UILabel!
    @IBOutlet weak var mlblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uvDueDateBox.layer.cornerRadius = 5
        uvDueDateIcon.layer.cornerRadius = uvDueDateIcon.layer.frame.size.width / 2

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
