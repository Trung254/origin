//
//  TodayTaskTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class TodayTaskTableViewCell: UITableViewCell {

    @IBOutlet weak var uvTaskBox: UIView!
    @IBOutlet weak var mlblNumberTodo: UILabel!
    @IBOutlet weak var mlblNameTodo: UILabel!
    @IBOutlet weak var mViewCount: UIView!
    @IBOutlet weak var mlblCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uvTaskBox.layer.cornerRadius = 5
        mViewCount.layer.cornerRadius = mViewCount.size.width/2
        mViewCount.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
