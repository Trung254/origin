
//
//  AppointmentListViewController.swift
//  Reusable Template
//
//  Created by mai kim tai  on 12/11/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit
import FontAwesome_swift
import Cosmos
import SwiftyJSON
import SafariServices

class AppointmentListViewController: BaseViewController {
    @IBOutlet weak var mCollectionView: UICollectionView!
    var vnpay: VNPay = VNPay()
    var products: Array<JSON> = []
    var old_price:String = ""
    var cur_price:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mCollectionView.register(UINib(nibName: "OtherServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OtherServiceCell")
        self.vnpay.delegate = self
        self.setTitle(title: "Gói khám")
        self.mIsShowRightButton = false
        self.addBackgroundImage("more-background-blur")
        
        
        if (DatabaseManager.sharedManager.isGuest()) {
            
            self.prompt("Thông báo", message: "Bạn cần phải đăng nhập. Bạn có muốn đăng nhập ngay bây giờ không", okTitle: "Đăng nhập", okHandler: { (okAction) in
                DatabaseManager.sharedManager.logout {}
            }, cancelTitle: "Không") { (cancelAction) in
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            self.loadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func loadData() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAppointmentProducts(id: "18") { (data, message, isSuccess) in
            if (isSuccess) {
                if let data = data as? JSON {
                    if let value = data.dictionary {
                        if let products = value["Products"]?.array {
                            self.products = products
                            
                            self.products.sort{$0["Id"].intValue < $1["Id"].intValue}
                            
                            self.mCollectionView.reloadData()
                            self.hideHud()
                            return
                        }
                    }
                }
            } else {
                self.hideHud()
                self.alert("Vui lòng thử lại")
            }
        }
    }
    
}

extension AppointmentListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width, column:2)
        if IS_IPHONE_5{
            let value = Int(screenSizeHeight/2.40)
            return CGSize(width: itemWidth, height: value)
        }else{
            let value = Int(screenSizeHeight/2.78)
            return CGSize(width: itemWidth, height: value)
        }
    }
    
    func getItemWidth(boundWidth: CGFloat, column:CGFloat) -> Int {
        let totalWidth = boundWidth - (Constant.offset + Constant.offset) - (column - 1) * Constant.minItemSpacing
        return Int(totalWidth / column)
    }
    
}

extension AppointmentListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherServiceCell", for: indexPath) as! OtherServiceCollectionViewCell
        
        cell.backgroundColor = UIColor.white
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.contentView.layer.borderWidth = 1.0
        
        DispatchQueue.main.async(execute: {
            if self.products.count > indexPath.row {
                let product = self.products[indexPath.row]
                
                cell.lblServiceName.text = product["Name"].stringValue
                cell.lblServiceName.font = UIFont.systemFont(ofSize: 18)
                if IS_IPHONE_5{
                    cell.lblServiceName.font = UIFont.systemFont(ofSize: 15)
                }
                cell.lblServiceName.lineBreakMode = .byWordWrapping
                cell.lblServiceName.numberOfLines = 2
                
                cell.imageThumb.contentMode = .scaleToFill
                let imageModel = product["DefaultPictureModel"].dictionaryValue
                
                if let image = imageModel["ImageUrl"]?.string {
                    let url = image
                    if url.count > 0 {
                        cell.imageThumb.loadImageUsingUrlString(urlString:url )
                    }
                }
                let productPrice = product["ProductPrice"].dictionaryValue
                
                
                if let priceStr = productPrice["Price"]?.stringValue {
                    self.cur_price = priceStr
                    let price = self.doubleFromPrice(string: priceStr)
                    var oldPrice = price
                    var oldPriceStr = priceStr
                    if let _oldPriceStr = productPrice["OldPrice"]?.string {
                        self.old_price = _oldPriceStr
                        oldPrice = self.doubleFromPrice(string: _oldPriceStr)
                        oldPriceStr = _oldPriceStr
                    }else {
                        self.old_price = oldPriceStr
                    }
                    let percent = (oldPrice - price)/oldPrice  * 100
                    
                    if IS_IPHONE_5{
                        cell.lblCost.font = UIFont.boldSystemFont(ofSize: 15.0)
                    }
                    cell.lblCost.text = "\(formatString(value: Float(price)))"
                    
                    if priceStr != oldPriceStr {
                        cell.lblSaleOff.isHidden = false
                        cell.lblSaleOff.font = UIFont.fontAwesome(ofSize: 13.0)
                        cell.lblSaleOff.text = String.fontAwesomeIcon(name: .caretDown) + " " + String(format: "%.f", percent) + "%"
                        cell.lblDisCount.font = UIFont.fontAwesome(ofSize: 16.0)
                        let result = formatAttributeString(value:formatString(value: Float(oldPrice)))
                        cell.lblDisCount.attributedText = result
                    }else {
                        cell.lblSaleOff.isHidden = true
                    }
                    
                    if let review = product["ReviewOverviewModel"].dictionary {
                        
                        let total = review["TotalReviews"]?.intValue
                        var rating = Double(0)
                        if let sum = review["RatingSum"]?.int, let total = review["TotalReviews"]?.int, total > 0 {
                            rating = Double(sum) / Double(total)
                        }
                        cell.cosmosRateStar.rating = Double(rating)
                        if IS_IPHONE_5{
                            cell.lblNumberComment.textAlignment = NSTextAlignment.right
                        }
                        cell.lblNumberComment.text = "(\(total ?? 0))"
                    }
                }
            }
        })
        
        return cell
    }
    
    func doubleFromPrice(string: String) -> Double {
        
        let rawPrice = string.replacingOccurrences(of: "[^0-9,]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
        let price = Double(rawPrice.trimmingCharacters(in: .whitespacesAndNewlines).split(separator: ",").first ?? "0")
        return price ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let sb = UIStoryboard.init(name: "More", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "DetailPackageViewController") as! DetailPackageViewController
        vc.titleDetail = self.products[indexPath.row]["Name"].stringValue
        vc.id = self.products[indexPath.row]["Id"].stringValue
        vc.products = self.products
        vc.mIgnoredIndex = indexPath.row
        vc.old_price = old_price
        vc.cur_price = cur_price
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension AppointmentListViewController: VNPayDelegate {
    func vnpay(_ vnpay: VNPay, dismiss viewController: VNPayViewController, withTransactionResult transactionIsSuccess: Bool) {
        viewController.dismiss(animated: true) {
            
        }
    }
    
    func vnpay(_ vnpay: VNPay, present vnPayViewController: VNPayViewController) {
        self.present(vnPayViewController, animated: true) {
            
        }
    }
    
    
    func vnpay(_ vnpay: VNPay, present safariViewController: SFSafariViewController) {
        self.present(safariViewController, animated: true) {
            
        }
    }
}
