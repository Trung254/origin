//
//  BabyNameViewController.swift
//  Reusable Template
//
//  Created by mai kim tai  on 12/12/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit
import SwiftyJSON

var user_id: String?

class BabyListNameViewController: BaseViewController {
    
    @IBOutlet weak var mtblBabyName: UITableView!
    @IBOutlet weak var switchOne: UISwitch!
    @IBOutlet weak var switchTwo: UISwitch!
    @IBOutlet weak var uvBoxTop: UIView!
    
    var check: Dictionary = [String:Any]()
    var babynames: [JSON] = []
    var gender_id: String = "1"
    var order_by: String = "position"
    var userId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uvBoxTop.layer.masksToBounds = false
//        self.uvBoxTop.layer.shadowColor = UIColor.white.cgColor
//        self.uvBoxTop.layer.shadowOpacity = 1
//        self.uvBoxTop.layer.shadowOffset = CGSize(width: -1, height: 2)
//        self.uvBoxTop.layer.shadowRadius = 1
        self.addBackgroundImage("more-background-blur")
//        self.getUserIdProfile()
        userId = DatabaseManager.sharedManager.getLocalUserInfo()?.id ?? 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
        self.loadData(genderId: gender_id, orderBy: order_by)
    }
        
    @IBAction func backBabyNameListViewConTroller(segue:UIStoryboardSegue){}
    
    @IBAction func saveBabyName(segue:UIStoryboardSegue){}
    
    @IBAction func btnBoyOrGirl(_ sender: Any) {
        if switchOne.isOn {
            gender_id = "2"
        }
        else {
            gender_id = "1"
        }
        loadData(genderId: gender_id, orderBy: order_by)
        mtblBabyName.reloadData()
    }
    
    @IBAction func btnSort(_ sender: Any) {
        if switchTwo.isOn {
//            order_by = "alphabet"
            loadData(genderId: gender_id, orderBy: "alphabet")
        }
        else {
//            order_by = "position"
            self.babynames = self.babynames.sorted { $0["id"].intValue < $1["id"].intValue }
            self.babynames = self.babynames.sorted { $0["order"].intValue < $1["order"].intValue }
        }
//        loadData(genderId: gender_id, orderBy: order_by)
        mtblBabyName.reloadData()
    }
    
    private func loadData(genderId: String, orderBy: String) {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllBabyName(countryId: 1){ (data, message, isSuccess) in
            if isSuccess {
                if let data = data as? JSON {
                    self.babynames.removeAll()
                    DispatchQueue.main.async {
                        self.mtblBabyName.reloadData()
                    }
                    var deffaultBabynames = data.arrayValue
                    let vn = Locale(identifier: "vi_VI")
                    deffaultBabynames = deffaultBabynames.sorted {
                        $0["name"].stringValue.compare($1["name"].stringValue, locale: vn) == .orderedAscending
                    }
                    for item in deffaultBabynames {
                        if item["gender_id"].stringValue == genderId &&
                            item["custom_baby_name_by_user_id"].stringValue == "" {
                            self.babynames.append(item)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.mtblBabyName.reloadData()
                        self.hideHud()
                    }
                }
            } else {
                self.alert("Vui lòng thử lại")
            }
        }
    }
    
    private func getUserIdProfile() {
        NetworkManager.shareInstance.apiGetProfile(){
            (data, message, isSuccess) in
            let data = data as? JSON
            user_id = data!["id"].stringValue
        }
    }
}


extension BabyListNameViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.babynames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib.init(nibName: "BabyNameTableViewCell", bundle: nil), forCellReuseIdentifier: "BabyNameCell")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BabyNameCell", for: indexPath) as! BabyNameTableViewCell
    
        cell.lblName.text = "\(indexPath.row + 1).  \(self.babynames[indexPath.row]["name"].stringValue)"
        
        if gender_id == "2" {
            cell.lblName.textColor = #colorLiteral(red: 0.9960784314, green: 0.5529411765, blue: 0.7254901961, alpha: 1)
        }
        else {
            cell.lblName.textColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
        }
        
        if babynames[indexPath.row]["user_id"].stringValue != "" {
            if gender_id == "1" {
                cell.imgCheck.image = UIImage(named: "did-like")
            }
            else {
                cell.imgCheck.image = UIImage(named: "did-like-girl")
            }
        }
        else {
            cell.imgCheck.image = UIImage(named: "like")
        }

        cell.selectionStyle = .none
        return cell
    }
    
}

extension BabyListNameViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = babynames[indexPath.row]["id"].intValue
        let userId = babynames[indexPath.row]["user_id"].intValue
        
        if userId == 0 {
            NetworkManager.shareInstance.apiPostUserBabyName(userId: userId, babyNameId: id)
            { (data, messge, isSuccess) in
                DispatchQueue.global(qos: .background).async {
                    self.loadData(genderId: self.gender_id, orderBy: self.order_by)
                }
            }
        }
        else {
            NetworkManager.shareInstance.apiDeleteUserBabyNamne(babyNameId: id)
            { (data, messge, isSuccess) in
                DispatchQueue.global(qos: .background).async {
                    self.loadData(genderId: self.gender_id, orderBy: self.order_by)
                }
            }
        }
        
        tableView.reloadData()
        
    }
    
}
