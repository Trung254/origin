//
//  UnitViewController.swift
//  Pregnancy
//
//  Created by dady on 3/25/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class DoctorsCell: UICollectionViewCell {
    @IBOutlet weak var mDoctorAva: UIImageView!
    @IBOutlet weak var mDoctorName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mDoctorAva.layer.cornerRadius = 30
        mDoctorAva.layer.masksToBounds = true
    }
}

class UnitViewController: BaseViewController {

    @IBOutlet weak var mtblUnit: UITableView!
    @IBOutlet weak var mIconMore: UIBarButtonItem!
    @IBOutlet weak var mScrollView: UIScrollView!
    @IBOutlet weak var mHeight: NSLayoutConstraint!
    @IBOutlet weak var mCollectionView: UICollectionView!
    @IBOutlet weak var mHeightTable: NSLayoutConstraint!
    
    //headerview
    @IBOutlet weak var mImage: UIImageView!
    @IBOutlet weak var mGMap: UIButton!
    
    //four buttons view
    @IBOutlet weak var mbtnLove: UIButton!
    @IBOutlet weak var mbtnSchedule: UIButton!
    @IBOutlet weak var mbtnContact: UIButton!
    @IBOutlet weak var mbtnReview: UIButton!
    @IBOutlet weak var mLove: UILabel!
    
    //icon
    @IBOutlet weak var mAddress: UILabel!
    
    // variable
    var doctors: [JSON] = []
    var mProductRatingData: Array<RatingInfo> = []
    var numberOfShowingComment = 5
    var urlImage: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mIconMore.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 20)], for: .normal)
        mIconMore.title = String.fontAwesomeIcon(name: .addressBook)
        mAddress.font = UIFont.fontAwesome(ofSize: 20)
        mAddress.text = String.fontAwesomeIcon(name: .mapMarker)
        
        // open ggmap
        mGMap.addTarget(self, action: #selector(moveToMap), for: .touchUpInside)
        
        // action four buttons
        mbtnLove.addTarget(self, action: #selector(likeUnit), for: .touchUpInside)
        mbtnSchedule.addTarget(self, action: #selector(moveToAppoinment), for: .touchUpInside)
        mbtnContact.addTarget(self, action: #selector(contactUs), for: .touchUpInside)
        mbtnReview.addTarget(self, action: #selector(reviewHospital), for: .touchUpInside)
        
        //load product by
        loadCategories()
        
        // load review hospital
        loadProductReviews(product_id: "57")
        
        //load Manufacture
        loadManufacture()
        
        // setup cell
        mtblUnit.register(UINib(nibName: "CommentServicePackageCell", bundle: nil), forCellReuseIdentifier: "CommentServicePackageCell")
        
        hideNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // transperance navigation
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dialogView.isHidden = true
    }
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        let btndots = UIButton(type: .custom)
        btndots.titleLabel?.font = UIFont.fontAwesome(ofSize: 20)
        btndots.titleLabel?.textAlignment = .right
        btndots.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
        let titleBtn = String.fontAwesomeIcon(name: .ellipsisV)
        btndots.setTitle(titleBtn, for: .normal)
        btndots.addTarget(self, action: #selector(openDialog), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btndots)
        return [barButton]
    }
    
    let btnView: UIButton = {
        let btn = UIButton()
        btn.isUserInteractionEnabled = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let dialogView : UIView = {
        let uv = UIView()
        uv.backgroundColor = .white
        uv.layer.cornerRadius = 5
        uv.translatesAutoresizingMaskIntoConstraints = false
        return uv
    }()
    
    let btnFirst: UIButton = {
        let btn = UIButton()
        btn.setTitle("   Báo cáo", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.contentHorizontalAlignment = .left
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let btnSecond: UIButton = {
        let btn = UIButton()
        btn.setTitle("   Đề xuất chỉnh sửa", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.contentHorizontalAlignment = .left
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    @objc func openDialog() {
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        currentWindow?.addSubview(btnView)
        currentWindow?.addSubview(dialogView)
        dialogView.addSubview(btnFirst)
        dialogView.addSubview(btnSecond)
        btnView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        dialogView.frame = CGRect(x: view.frame.width - 210, y: 40, width: 200, height: 80)
        btnFirst.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        btnSecond.frame = CGRect(x: 0, y: 40, width: 200, height: 40)
        
        btnView.addTarget(self, action: #selector(hideSubview), for: .touchUpInside)
    }
    
    @objc func hideSubview() {
        btnView.removeFromSuperview()
        dialogView.removeFromSuperview()
    }
    
    fileprivate func hideNavigation() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    fileprivate func showNavigation() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
    }
    
    fileprivate func loadManufacture() {
        self.showHud()
        NetworkManager.shareInstance.apiGetManufacture { (data, message, isSuccess) in
            if isSuccess {
                let data = JSON(data)
                self.urlImage = data[0]["PictureModel"]["ImageUrl"].stringValue
                if let url = URL(string: self.urlImage) {
                    if let dataImg = try? Data(contentsOf: url) {
                        self.mImage.image = UIImage(data: dataImg)
                    }
                }
                
                DispatchQueue.main.async {
                    self.hideHud()
                    self.mtblUnit.reloadData()
                }
            } else {
                self.hideHud()
                self.alert("Vui lòng thử lại")
            }
        }
    }
    
    fileprivate func loadCategories() {
        self.showHud()
        // get products follow category
        NetworkManager.shareInstance.apiGetCategories { (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                let result = data["categories"].arrayValue.filter({ $0["name"] == "Bác sĩ"})
                let category_id = result[0]["id"].stringValue
                
                NetworkManager.shareInstance.apiGetProductByCategory(categoryId: category_id, callBack: { (data, message, isSuccess) in
                    if isSuccess {
                        let data = data as! JSON
                        self.doctors = data["Products"].arrayValue
                        self.mCollectionView.reloadData()
                    }
                    
                    DispatchQueue.main.async {
                        self.hideHud()
                    }
                })
            } else {
                self.alert("Vui lòng thử lại")
                self.hideHud()
            }
        }
    }
    
    fileprivate func loadProductReviews(product_id: String) {
        self.showHud()
        NetworkManager.shareInstance.apiGetProductReviews(product_id: product_id) { (data, message, isSuccess) in
            if (isSuccess) {
                let data = JSON(data)
                for item in data["Items"].arrayValue {
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy HH:mm a"
                    dateFormatter.locale = Locale(identifier: "vi_VI")
                    let date = dateFormatter.date(from: item["WrittenOnStr"].stringValue)
                    
                    let rate = RatingInfo(guid: item["CustomerGuid"].stringValue, cusid:item["CustomerId"].stringValue, username: item["CustomerName"].stringValue, date: date ?? Date(), comment: item["ReviewText"].stringValue, rate: item["Rating"].intValue)
                    self.mProductRatingData.append(rate)
                }
                
                self.customHeightTable()
                
                DispatchQueue.main.async {
                    self.hideHud()
                    self.mtblUnit.reloadData()
                }
            } else {
                self.alert("Vui lòng thử lại")
                self.hideHud()
            }
        }
    }
    
    fileprivate func customHeightTable() {
        let numberRate = self.mProductRatingData.count
        if numberRate > 0 {
            if numberRate > 5{
                if numberRate > self.numberOfShowingComment {
                    mHeightTable.constant = 128*CGFloat(self.numberOfShowingComment) + 237.0
                }else {
                    mHeightTable.constant = 128*CGFloat(numberRate) + 237.0
                }
            }else{
                mHeightTable.constant = 128*CGFloat(numberRate) + 202.0
            }
        }else{
            mHeightTable.constant = 202
        }
        
        mHeight.constant = mHeight.constant + mHeightTable.constant - 90
        self.mScrollView.reloadInputViews()
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let contentOffsetY = mScrollView.contentOffset.y
        
        if contentOffsetY > 100 {
            self.title = "Bệnh viện đa khoa MEDLATEC"
            let alpha = contentOffsetY/250
            let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: alpha)]
            navigationController?.navigationBar.titleTextAttributes = textAttributes
        }else {
            self.title = ""
        }
        
        if contentOffsetY > 150 {
            UIView.animate(withDuration: 0.5, animations: {
                self.mImage.image = nil
                self.mImage.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
                self.mGMap.alpha = 0
            })
            
            if contentOffsetY > 200 {
                UIView.animate(withDuration: 0.5, animations: {
                    self.showNavigation()
                })
            }
        } else {
            UIView.animate(withDuration: 1, animations: {
                self.mImage.backgroundColor = .clear
                if let url = URL(string: self.urlImage) {
                    if let dataImg = try? Data(contentsOf: url) {
                        self.mImage.image = UIImage(data: dataImg)
                    }
                }
            })
            
            UIView.animate(withDuration: 0.5, animations: {
                self.hideNavigation()
                self.mGMap.alpha = 1
            })
        }
    }
    
    @objc func moveToMap() {
        guard let url = URL(string: "https://www.google.com/maps/dir/57+Ngh%C4%A9a+D%C5%A9ng,+Ph%C3%BAc+X%C3%A1,+Ba+%C4%90%C3%ACnh,+Hanoi/B%E1%BB%87nh+vi%E1%BB%87n+qu%E1%BB%91c+t%E1%BA%BF,+42+Ngh%C4%A9a+D%C5%A9ng,+Ph%C3%BAc+x%C3%A1,+Ba+%C4%90%C3%ACnh,+H%C3%A0+N%E1%BB%99i,+Vietnam/@21.0481644,105.8445186,17z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x3135abb6a561cdd9:0xcc9204a5bac19773!2m2!1d105.847381!2d21.047695!1m5!1m1!1s0x3135ab7f306c24f7:0x166f69f5f53e13b8!2m2!1d105.8460335!2d21.0485622") else {
            return 
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func likeUnit(_ sender: UIButton) {
        if sender.tag == 0 {
            mbtnLove.tag = 1
            mLove.textColor = .red
        } else {
            mbtnLove.tag = 0
            mLove.textColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
        }
    }
    
    @objc func moveToAppoinment() {
        showNavigation()
    }
    
    @objc func contactUs() {
        if let url = URL(string: "tel://1900565656") {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func reviewHospital() {
        let vc = PopupReviewUnitViewController(nibName: "PopupReviewUnitViewController", bundle: nil)
        vc.modalPresentationStyle = .custom
//        vc.product_id = self.id
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func moveToTotalReviews() {
        let vc = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "ReviewsViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension UnitViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return min(doctors.count, 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DoctorsCell", for:
            indexPath) as! DoctorsCell
        
        if doctors.count != 0 {
            var index = doctors[indexPath.row]
            cell.mDoctorName.text = index["Name"].stringValue
            if let url = URL(string: index["DefaultPictureModel"]["ImageUrl"].stringValue) {
                if let data = try? Data(contentsOf: url) {
                    cell.mDoctorAva.image = UIImage(data: data)
                }
            }
        }
        
        return cell
    }
}

extension UnitViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if mHeightTable.constant != 0 {
            return mHeightTable.constant
        }
        
        return UITableView.automaticDimension
        //        return 500
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentServicePackageCell", for: indexPath) as! CommentServicePackageCell
        
        DatabaseManager.sharedManager.getUserInfo { (pregUser, mnessage, isSuccess) in
            if pregUser != nil {
                if pregUser?.avatar != "" {
                    var avatar = NetworkManager.rootDomain
                    if pregUser!.avatar.hasPrefix("http") {
                        avatar = pregUser!.avatar
                    } else {
                        avatar.append(pregUser!.avatar)
                    }
                    
                    if let url = URL(string: avatar) {
                        if let data = try? Data(contentsOf: url) {
                            cell.myAvatar.image = UIImage(data: data)
                        }
                    }
                }
                    
                else {
                    cell.myAvatar.image = UIImage(named: "user_placeholder")
                }
            }
        }
        
        cell.mRatingList = self.mProductRatingData
        let numberRate = self.mProductRatingData.count
        if numberRate > 5 {
            let height = 128*CGFloat(numberRate) + 235.0
            cell.setUpTable(cellHeightAuto: Int(height))
        }else {
            let height = 128*CGFloat(numberRate) + 200.0
            cell.setUpTable(cellHeightAuto: Int(height))
        }

        cell.myTableView?.isScrollEnabled = false
        cell.myTableView?.bounces = false
        cell.myTableView?.alwaysBounceVertical = false
        cell.myTableView?.reloadData()
        cell.btnRate.isUserInteractionEnabled = true
        cell.btnRate.addTarget(self, action: #selector(reviewHospital), for: .touchUpInside)
        
        cell.btnMore.backgroundColor = .white
        cell.btnMore.setTitle("Các bài đăng khác", for: .normal)
        cell.btnMore.setTitleColor(#colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1), for: .normal)
        cell.btnMore.addTarget(self, action: #selector(moveToTotalReviews), for: UIControl.Event.touchUpInside)
        
        return cell
    }
}
