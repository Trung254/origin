//
//  VNOrderResultViewController.swift
//  Pregnancy
//
//  Created by Trung Duc on 3/3/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift

class VNOrderResultViewController: BaseViewController {
    
    @IBOutlet weak var orderID: UILabel?
    var order_id: Int = 0
    var order_id_Note: Int = 0
    
    var presenter: BaseViewController?
    
    @IBAction func backhomefail(_ sender: Any) {
        self.backToHome()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let orderId = self.orderID{
            orderId.text = "\(order_id)"
        }
        self.createorderNote();
    }
    
    
    func createorderNote() {
        let realm = try! Realm()
        try! realm.write {
            let order = DatabaseManager.sharedManager.getLocalUserInfo()
            order?.OrderId = "\(order_id_Note)"
            if (order?.type == "datlichbacsi") {
                NetworkManager.shareInstance.apiPostOrdersNote(OrderId: order!.OrderId, tenBV: order!.tenBV, anh: "", idPro: order!.idPro, ngayDat: order!.ngayDat, ngayKham: order!.ngayKham, hoTen: order!.hoTen, sdt: order!.sdt, datLich: order!.datLich, diaChi: order!.diaChi, gioiTinh: order!.gioiTinh, namSinh: order!.namSinh, trieuChung: order!.trieuChung, khungGio: order!.khungGio, email: order!.email, noiKham: order!.noiKham, kham: order!.kham, type: order!.type, callBack: { (data, message, isSuccess) in
                    if isSuccess {
                        self.alert("Bạn đã đặt lịch thành công")
                        DispatchQueue.main.async {
                            self.hideHud()
                        }
                    }
                    else {
                        self.alert("Vui lòng thử lại")
                        DispatchQueue.main.async {
                            self.hideHud()
                        }
                    }
                })
            }
            else if (order?.type == "datlich") {
                NetworkManager.shareInstance.apiPostOrdersNote(OrderId: order!.OrderId, tenBV: order!.tenBV, anh: "", idPro: order!.idPro, ngayDat: order!.ngayDat, ngayKham: order!.ngayKham, hoTen: order!.hoTen, sdt: order!.sdt, datLich: order!.datLich, diaChi: order!.diaChi, gioiTinh: order!.gioiTinh, namSinh: order!.namSinh, trieuChung: order!.trieuChung, khungGio: order!.khungGio, email: order!.email, noiKham: order!.noiKham, kham: order!.kham, type: order!.type, callBack: { (data, message, isSuccess) in
                    if isSuccess {
                        self.alert("Bạn đã đặt lịch thành công")
                        DispatchQueue.main.async {
                            self.hideHud()
                        }
                    }
                    else {
                        self.alert("Vui lòng thử lại")
                        DispatchQueue.main.async {
                            self.hideHud()
                        }
                    }
                })
            }
            else {
                NetworkManager.shareInstance.apiPostOrdersNote(OrderId: order!.OrderId, tenBV: order!.tenBV, anh: "", idPro: order!.idPro, ngayDat: order!.ngayDat, ngayKham: order!.ngayKham, hoTen: order!.hoTen, sdt: order!.sdt, datLich: order!.datLich, diaChi: order!.diaChi, gioiTinh: order!.gioiTinh, namSinh: order!.namSinh, trieuChung: order!.trieuChung, khungGio: order!.khungGio, email: order!.email, noiKham: order!.noiKham, kham: order!.kham, type: order!.type, callBack: { (data, message, isSuccess) in
                    if isSuccess {
                        self.alert("Bạn đã đặt lịch thành công")
                        DispatchQueue.main.async {
                            self.hideHud()
                        }
                    }
                    else {
                        self.alert("Vui lòng thử lại")
                        DispatchQueue.main.async {
                            self.hideHud()
                        }
                    }
                })
            }
        }
    }
    
    func backToHome() {
        if let persenter = self.presenter {
            self.dismiss(animated: true) {
                persenter.dismiss()
                if let BackToHomPage = ViewManager.getRootTabbarController(){
                    BackToHomPage.selectedIndex = 0
                }
            }
        }
        
        
    }
    func backToHistoryOrder() {
        if let persenter = self.presenter {
            persenter.tabBarController?.selectedIndex = 3
            self.dismiss(animated: true) {
                persenter.dismiss()
                let sb = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "AppointmentsHistoryViewController") as! AppointmentsHistoryViewController
                let tabbar = ViewManager.getRootTabbarController()
                tabbar?.showViewController(viewController: sb, at: 3, backTo: 3)
            }
            
        }
        
    }
    
    
    @IBAction func backtohome(_ sender: Any) {
        self.backToHome()
    }
    
    @IBAction func back_to_history_order(_ sender: Any) {
        self.backToHistoryOrder()
    }
}
