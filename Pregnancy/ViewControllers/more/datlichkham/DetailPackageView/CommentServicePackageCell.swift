//
//  CommentServicePackageCell.swift
//  iCNM
//
//  Created by Mac osx on 11/9/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos
protocol CommentServicePackageCellDelegate {
    func pushMoreCommentService()
}
class CommentServicePackageCell: UITableViewCell, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myAvatar: UIImageView!
    @IBOutlet weak var numberPeopleRate: UILabel!
    @IBOutlet weak var numberOfRate: UILabel!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var cosmosRate: CosmosView!
    @IBOutlet weak var mHeaderView: UIView!
    @IBOutlet weak var mHeightHeader: NSLayoutConstraint!
    
    
    var mRatingList: Array<RatingInfo> = []
    var delegate:CommentServicePackageCellDelegate?
    var numberReview: Int = 5
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style , reuseIdentifier: reuseIdentifier)
        //setUpTable()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //setUpTable()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //setUpTable()
    }
    
    func setUpTable(cellHeightAuto:Int){
        myAvatar.layer.cornerRadius = 30
        myAvatar.layer.masksToBounds = true
//        myAvatar.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
//        myAvatar.layer.borderWidth = 2.0
        myAvatar.image = UIImage(named: "user")
//        myAvatar.image = UIImage.fontAwesomeIcon(name: .user, textColor: .lightGray, size: CGSize(width: 90, height: 90))
        
        numberPeopleRate.text = "Nói cho mọi người biết trải nghiệm của bạn!"
        let numberRate = self.mRatingList.count
        if numberRate > numberReview{
            btnMore.isHidden = false
        }else{
            btnMore.isHidden = true
        }
        
        if numberRate == 0 {
            numberPeopleRate.text = "Bạn hãy là người đầu tiên đánh giá gói khám này."
            numberOfRate.text = ""
            cosmosRate.rating = 0
        }
        
//        cosmosRate.rating = Double(self.mRatingList.first!.rate)
//        numberOfRate.text = "\(self.mRatingList.first!.rate)/5"
//        numberPeopleRate.text = self.mRatingList.first!.comment
        
        numberOfRate.textColor = UIColor(red: 1, green: 149/255, blue: 0, alpha: 1)
        
        myTableView?.delegate = self
        myTableView?.dataSource = self
        myTableView?.estimatedRowHeight = 200
        myTableView?.rowHeight = UITableView.automaticDimension
        let nib = UINib(nibName: "CommentServiceCell", bundle: nil)
        myTableView?.register(nib, forCellReuseIdentifier: "CommentServiceCell")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mRatingList.count != 0{
            return min(self.mRatingList.count, numberReview)
        }else{
            return 0
        }
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentServiceCell", for: indexPath) as! CommentServiceCell
        tableView.separatorStyle = .none
        
        if self.mRatingList.count != 0{
            let serviceRatingObj = self.mRatingList[indexPath.row]
            cell.userAvatar.layer.cornerRadius = 30
            cell.userAvatar.layer.masksToBounds = true
            if serviceRatingObj.username != "" {
                cell.lblUserName.text = serviceRatingObj.username
            }
            
            cell.userAvatar.image = UIImage(named: "user_placeholder")
            cell.lblDescription.text = serviceRatingObj.comment
            cell.lblDescription.lineBreakMode = .byWordWrapping
            cell.lblDescription.numberOfLines = 0
            cell.lblDateTime.text = serviceRatingObj.date.timeAgoSinceNow()
            cell.cosmosRate.rating = Double(serviceRatingObj.rate)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func btnMoreComment(_ sender: Any) {
        self.numberReview += 5
    }

    //    func handleTapTestNameself(sender: UITapGestureRecognizer) {
//        //let tag = (sender.view as? UILabel)?.tag
//        let testCode = (sender.view as? UILabel)?.accessibilityHint
//        delegate?.getInforDetailTestName(testCode: testCode!)
//    }
}
