//
//  AnalysisTableViewCell.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 3/3/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class AnalysisTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var mContentView: UIView!
    
    @IBOutlet weak var mWhiteBackground: UIView!
    @IBOutlet weak var mLeftTitle: UILabel!
    @IBOutlet weak var mRightTitle: UILabel!
    @IBOutlet weak var mNoticeView: UIView!
    @IBOutlet weak var mNoticeLbl: UILabel!
    @IBOutlet weak var mViewBoder: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code1
//
//        self.mNoticeView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        self.mNoticeView.layer.borderWidth = 1
        self.mWhiteBackground.backgroundColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        self.mLeftTitle.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.mRightTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
