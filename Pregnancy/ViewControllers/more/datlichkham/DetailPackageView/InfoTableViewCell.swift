//
//  InfoTableViewCell.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 2/28/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import WebKit
import Cosmos

class InfoTableViewCell: UITableViewCell {


    @IBOutlet weak var heightWebView: NSLayoutConstraint!
    @IBOutlet weak var webView: UIView!
    @IBOutlet weak var mLblTitle: UILabel!
    @IBOutlet weak var mLblCost: UILabel!
    @IBOutlet weak var mLblDiscount: UILabel!
    @IBOutlet weak var mLblSaleOff: UILabel!
    @IBOutlet weak var mLblRate: UILabel!
    @IBOutlet weak var mLblNumberPeopleRate: UILabel!
    @IBOutlet weak var cosmosRateStar: CosmosView!
    @IBOutlet weak var mbtnDown: UIButton!
    
    var heightConstant: CGFloat = 0
    var mTableview: UITableView?
    var checkIsExpand = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.mbtnDown.setTitle(String.fontAwesomeIcon(name: .angleDown) , for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func AnimateBackgroundHeight() {
//        UIView.animate(withDuration: 1, animations: {
//            if self.mbtnDown.tag == 1 {
//                self.heightWebView.constant = self.heightConstant
//                if let tbl = self.mTableview {
//                    tbl.reloadData()
//                }
//            }
//            else {
//
//                let indexPath = IndexPath(row: 0, section: 0)
//
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                    if let tbl = self.mTableview {
//                        tbl.scrollToRow(at: indexPath, at: .top, animated: false)
//                        tbl.reloadSections(IndexSet(integersIn: 1...1), with: .top)
//                    }
//                }
//
//                UIView.animate(withDuration: 0.75, animations: {
//                    self.heightWebView.constant = 80
//                    self.mTableview?.contentOffset.y = 140
//                })
//            }
//
//        })
//    }
    
    @IBAction func showText(_ sender: UIButton) {
        if self.checkIsExpand == false {
            //Đang là đóng -> mở to ra
            if let tbl = self.mTableview {
                self.checkIsExpand = true
                self.heightWebView.constant = self.heightConstant
                self.mbtnDown.setTitle(String.fontAwesomeIcon(name: .angleUp) , for: .normal)
                tbl.reloadData()
            }
            
        } else {
            if let tbl = self.mTableview {
                self.checkIsExpand = false
                self.heightWebView.constant = 80
                self.mbtnDown.setTitle(String.fontAwesomeIcon(name: .angleDown) , for: .normal)
                tbl.reloadData()
            }
        }
//        if sender.tag == 0 {
//            sender.tag = 1
//            self.mbtnDown.setTitle(String.fontAwesomeIcon(name: .angleUp) , for: .normal)
//        }else {
//            sender.tag = 0
//            self.mbtnDown.setTitle(String.fontAwesomeIcon(name: .angleDown) , for: .normal)
//        }
//        AnimateBackgroundHeight()
    }
    
}

extension InfoTableViewCell: WKUIDelegate {
    
}

extension InfoTableViewCell: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                
                webView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    if error == nil {
                        let height: CGFloat = height as! CGFloat
                        if self.heightWebView.constant != height {
//                            self.heightWebView.constant = height
//                            if let tbl = self.mTableview {
//                                tbl.reloadData()
//                            }
                            self.heightConstant = height
                        }
                        
//                        if height > 80 {
//                            self.mbtnDown.isHidden = false
//                        }else {
//                            self.mbtnDown.isHidden = true
//                        }
                    }
                    
                })
            }
            
        })
    }
    
}
