//
//  CustomHeaderServiceDetail.swift
//  iCNM
//
//  Created by Mac osx on 8/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
class CustomHeaderServiceDetail: UITableViewHeaderFooterView
{
    @IBOutlet weak var lblSectionName: UILabel!
    @IBOutlet weak var lblSectionName2: UILabel!
    @IBOutlet weak var mBackground: UIView!
    var detailPackageViewController: DetailPackageViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CustomMedicalBodyHeader.tapHeader(_:))))
    }
    
    @IBAction func selectTab(_ sender: UIButton) {
        
        if let detailVC = self.detailPackageViewController {
            detailVC.didChangeDetailTag(sender.tag)
        }
    }
    
}
