//
//  PopupReviewUnitViewController.swift
//  Pregnancy
//
//  Created by dady on 3/28/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import Cosmos
import SwiftyJSON

class PopupReviewUnitViewController: BaseViewController, UITextViewDelegate {

    @IBOutlet weak var mTextview: UITextView!
    @IBOutlet weak var mRateFirst: CosmosView!
    @IBOutlet weak var mRateSecond: CosmosView!
    @IBOutlet weak var mRateThỉrd: CosmosView!
    @IBOutlet weak var mErrorMessage: UILabel!
    
    var product_id: String = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        //  your code here
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        mTextview.text = String()
        mTextview.font = UIFont.systemFont(ofSize: 14)
        mTextview.textColor = UIColor.black
    }
    
    
    @IBAction func exitForm(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendReview(_ sender: Any) {
//        let reviewText = mTextview.text
//        let rate = "\(Int(mRateFirst.rating))"
//        
//        if reviewText != "" , reviewText != "Nội dung đánh giá"{
//            self.showHud()
//            
//            NetworkManager.shareInstance.apiGetUserProfile() { (data, message, isSuccess) in
//                if isSuccess {
//                    let result = data as! JSON
//                    if result.arrayValue.isEmpty == false {
//                        let nopcustomer_id = result[0]["nopcustomer_id"].intValue
//                        
//                        NetworkManager.shareInstance.apiGetAppointmentCustomersById(nopcustomer_id: nopcustomer_id) { (data, messge, isSuccess) in
//                            if(isSuccess) {
//                                let result = JSON(data)
//                                
//                                let cusGUID = result["customers"].arrayValue[0]["customer_guid"].stringValue
//                                
//                                NetworkManager.shareInstance.apiPostProductReviews(product_id: self.product_id, ReviewText: reviewText!, Rating: rate, CustomerGUID: cusGUID) { (data, message, isSuccess) in
//                                    if isSuccess {
//                                        self.prompt("Thông báo", message: "Bạn đã đánh giá thành công", okHandler: { (alert) in
//                                            self.navigationController?.popViewController(animated: true)
//                                            self.dismiss(animated: true, completion: nil)
//                                        }, cancelHandler: {
//                                            (alert) in
//                                            self.navigationController?.popViewController(animated: true)
//                                            self.dismiss(animated: true, completion: nil)
//                                        })
//                                    }else {
//                                        self.alert("Vui lòng thử lại")
//                                    }
//                                    
//                                    DispatchQueue.main.async {
//                                        self.hideHud()
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            
//            
//        }else {
//            self.mErrorMessage.isHidden = false
//        }
    }
}
