//
//  OtherServiceCollectionViewCell.swift
//  iCNM
//
//  Created by Quang Hung on 7/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos

class OtherServiceCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageThumb: CustomImageView!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblSaleOff: UILabel!
    @IBOutlet weak var lblDisCount: UILabel!
    @IBOutlet weak var cosmosRateStar: CosmosView!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblNumberComment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

let imageCache = NSCache<AnyObject, AnyObject>()
class CustomImageView: UIImageView {
    
    var imageUrlString: String?
    
    func loadImageUsingUrlString(urlString: String) {
        
        imageUrlString = urlString
        
        let url = NSURL(string: urlString)
        if url == nil{
            return
        }
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        URLSession.shared.dataTask(with: url! as URL, completionHandler: { (data, respones, error) in
            
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async(execute: {
                
                let imageToCache = UIImage(data: data!)
                
                if self.imageUrlString == urlString {
                    self.image = imageToCache
                }
                
                if imageToCache != nil{
                    imageCache.setObject(imageToCache!, forKey: urlString as AnyObject)
                }else{
                    self.image = UIImage(named: "default-thumbnail")
                }
                
            })
            
        }).resume()
    }
}
