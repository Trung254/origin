//
//  ReviewsViewController.swift
//  Pregnancy
//
//  Created by dady on 4/1/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class ReviewsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var mTableView: UITableView!
    var mProductRatingData: Array<RatingInfo> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableview()
        
        self.title = "Tất cả bài đánh giá"
        
        loadProductReviews(product_id: "57")
        
    }
    
    fileprivate func setupTableview() {
        mTableView.translatesAutoresizingMaskIntoConstraints = false
        mTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        mTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        mTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        mTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        mTableView.register(UINib(nibName: "CommentServicePackageCell", bundle: nil), forCellReuseIdentifier: "CommentServicePackageCell")
    }
    
    func loadProductReviews(product_id: String) {
        self.showHud()
        NetworkManager.shareInstance.apiGetProductReviews(product_id: product_id) { (data, message, isSuccess) in
            if (isSuccess) {
                let data = JSON(data)
                for item in data["Items"].arrayValue {
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy HH:mm a"
                    dateFormatter.locale = Locale(identifier: "vi_VI")
                    let date = dateFormatter.date(from: item["WrittenOnStr"].stringValue)
                    
                    let rate = RatingInfo(guid: item["CustomerGuid"].stringValue, cusid:item["CustomerId"].stringValue, username: item["CustomerName"].stringValue, date: date ?? Date(), comment: item["ReviewText"].stringValue, rate: item["Rating"].intValue)
                    self.mProductRatingData.append(rate)
                }
                
                DispatchQueue.main.async {
                    self.hideHud()
                    self.mTableView.reloadData()
                }
            } else {
                self.alert("Vui lòng thử lại")
                self.hideHud()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let numberRate = self.mProductRatingData.count
        if numberRate > 0 {
            return 128*CGFloat(numberRate) + 5
        }else{
            return 180.0
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentServicePackageCell", for: indexPath) as! CommentServicePackageCell
        
        
        cell.mRatingList = self.mProductRatingData
        let numberRate = self.mProductRatingData.count
        let height = 128*CGFloat(numberRate) + 215.0
        cell.setUpTable(cellHeightAuto: Int(height))
        cell.numberReview = numberRate
        cell.mHeightHeader.constant = 0
        
        cell.myTableView?.isScrollEnabled = false
        cell.myTableView?.bounces = false
        cell.myTableView?.alwaysBounceVertical = false
        cell.myTableView?.reloadData()
        cell.mHeaderView.isHidden = true
        cell.btnMore.isHidden = true
        return cell
    }
}
