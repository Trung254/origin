//
//  DetailPackegeViewController.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 2/28/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import WebKit

class RatingInfo {
    
    var username: String = ""
    var date: Date = Date()
    var dateStr: String = ""
    var comment: String = ""
    var rate = 0
    var customerGuid: String = ""
    var cusId : String = ""
    
    
    convenience init(guid:String, cusid:String, username:String, date: Date = Date(), comment:String = "", rate: Int = 5) {
        self.init()
        self.customerGuid = guid
        self.cusId = cusid
        self.username = username
        self.date = date
        self.comment = comment
        self.rate = rate
    }
    
}

class DetailPackageViewController: BaseViewController, UIWebViewDelegate {
    
    @IBOutlet weak var mtbDetailPackage: UITableView!
    @IBOutlet weak var mbtnPopup: UIButton!
    @IBOutlet weak var mOrderNow: UIButton!
    @IBOutlet weak var mIconOrder: UIBarButtonItem!
    
    var titleDetail = ""
    var currentTag = 1
    var id = ""
    var listHeader = ["", "", "", "CHI TIẾT", "   GÓI KHÁM KHÁC"]
    var old_price:String = ""
    var cur_price:String = ""
    
    
    var infoProducts = JSON()
    var manufacture = JSON()
    var storedOffsets = [Int: CGFloat]()
    var products: Array<JSON> = []
    var mIgnoredIndex: Int!
    var mProductRatingData: Array<RatingInfo> = []
    var viewController : UIViewController!
    var dataListTest: Array<JSON> = []
    var numberOfShowingComment = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mIsShowRightButton = false
        
        self.mtbDetailPackage.estimatedRowHeight = 300
        self.mtbDetailPackage.rowHeight = UITableView.automaticDimension
        
        self.setTitle(title: titleDetail)
        self.setupCell()
        self.mOrderNow.titleLabel?.font = UIFont.fontAwesome(ofSize: 20.0)
        let titleBtn = String.fontAwesomeIcon(name: .calendarPlusO) +  " Đặt lịch hẹn ngay"
        self.mOrderNow.setTitle(titleBtn, for: .normal)
        
        self.mIconOrder.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 20)], for: .normal)
        self.mIconOrder.title = String.fontAwesomeIcon(name: .calendarPlusO)
        
        if (DatabaseManager.sharedManager.isGuest()) {
            
            self.prompt("Thông báo", message: "Bạn cần phải đăng nhập. Bạn có muốn đăng nhập ngay bây giờ không", okTitle: "Đăng nhập", okHandler: { (okAction) in
                DatabaseManager.sharedManager.logout {}
            }, cancelTitle: "Thoát") { (cancelAction) in
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            self.loadDataManufacture()
            self.loadDataProductById(productID: self.id)
            self.loadProductReviews(product_id: self.id)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavigation()
    }
    
    func setupCell() {
        mtbDetailPackage.register(UINib.init(nibName: "CustomSlideTableViewCell", bundle: nil), forCellReuseIdentifier: "customSlideTableViewCell")
        mtbDetailPackage.register(UINib.init(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "infoTableViewCell")
        mtbDetailPackage.register(UINib.init(nibName: "LocationTableViewCell", bundle: nil), forCellReuseIdentifier: "locationTableViewCell")
        mtbDetailPackage.register(UINib.init(nibName: "TableListTableViewCell", bundle: nil), forCellReuseIdentifier: "tableListTableViewCell")
        mtbDetailPackage.register(UINib.init(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "commentTableViewCell")
        mtbDetailPackage.register(UINib.init(nibName: "SubDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "subDescriptionTableViewCell")
        mtbDetailPackage.register(UINib.init(nibName: "CustomTableViewCell", bundle: nil), forCellReuseIdentifier: "customTableViewCell")
        
        mtbDetailPackage.register(UINib(nibName: "CustomHeaderServiceDetail", bundle: nil), forHeaderFooterViewReuseIdentifier: "headerServiceDetailCell")
        mtbDetailPackage.register(UINib(nibName: "AnalysisTableViewCell", bundle: nil), forCellReuseIdentifier: "AnalysisTableViewCell")
        mtbDetailPackage.register(UINib(nibName: "CommentServicePackageCell", bundle: nil), forCellReuseIdentifier: "CommentServicePackageCell")
        mtbDetailPackage.register((UINib(nibName: "CustomTableViewCell", bundle: nil)), forCellReuseIdentifier: "CustomTableViewCell")
        
    }
    
    fileprivate func showNavigation() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
    }
    
    func loadDataProductById(productID: String) {
        self.showHud()
        NetworkManager.shareInstance.apiGetProductById(productId: productID) { (data, message, isSuccess) in
            if (isSuccess) {
                let result = JSON(data)
                self.infoProducts  = result["products"].arrayValue.first!
                for item in self.infoProducts["attributes"].arrayValue {
                    if item["text_prompt"] == "Danh mục xét nghiệm" {
                        self.dataListTest.append(item)
                    }
                }
                
                DispatchQueue.main.async {
                    self.mtbDetailPackage.reloadData()
                    self.hideHud()
                }
            } else {
                self.hideHud()
                self.alert("Vui lòng thử lại")
            }
        }
    }
    
    func loadDataManufacture() {
        NetworkManager.shareInstance.apiGetManufacture { (data, message, isSucess) in
            if (isSucess) {
                self.manufacture = JSON(data)
                DispatchQueue.main.async {
                    self.mtbDetailPackage.reloadData()
                }
            } else {
                self.alert("Vui lòng thử lại")
            }
        }
    }
    
    func loadProductReviews(product_id: String) {
        NetworkManager.shareInstance.apiGetProductReviews(product_id: product_id) { (data, message, isSuccess) in
            if (isSuccess) {
                let data = JSON(data)
                for item in data["Items"].arrayValue {

                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy HH:mm a"
                    dateFormatter.locale = Locale(identifier: "vi_VI")
                    let date = dateFormatter.date(from: item["WrittenOnStr"].stringValue)
                    
                    let rate = RatingInfo(guid: item["CustomerGuid"].stringValue, cusid:item["CustomerId"].stringValue, username: item["CustomerName"].stringValue, date: date ?? Date(), comment: item["ReviewText"].stringValue, rate: item["Rating"].intValue)
                    self.mProductRatingData.append(rate)
                }
                
                DispatchQueue.main.async {
                    self.mtbDetailPackage.reloadData()
                }
            } else {
                self.alert("Vui lòng thử lại")
            }
        }
    }
    
    
    @IBAction func showPopup(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FormPackageViewController") as! FormPackageViewController
        
        vc.dataPackage.id = infoProducts["id"].intValue
        if infoProducts["images"].arrayValue.isEmpty == false {
            vc.dataPackage.url_image = infoProducts["images"].arrayValue.first!["src"].stringValue
        }
        
        vc.dataPackage.name = infoProducts["name"].stringValue
        vc.dataPackage.city = "Hà Nội"
        vc.dataPackage.price_package = infoProducts["price"].stringValue

        vc.modalPresentationStyle = .custom
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func didChangeDetailTag(_ tag: Int) -> Void {
        self.currentTag = tag
        self.mtbDetailPackage.reloadSections([3], with: UITableView.RowAnimation.fade)
    }
    
    
    func doubleFromPrice(string: String) -> Double {
        
        let rawPrice = string.replacingOccurrences(of: "[^0-9,]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
        let price = Double(rawPrice.trimmingCharacters(in: .whitespacesAndNewlines).split(separator: ",").first ?? "0")
        return price ?? 0
        
    }
    
    @objc func performRatingAction(_ sender: Any) {
        let vc = PopupReviewViewController(nibName: "PopupReviewViewController", bundle: nil)
        vc.modalPresentationStyle = .custom
        vc.product_id = self.id
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func loadMore(_ sender : UIButton){
        self.numberOfShowingComment += 5
        self.mtbDetailPackage.reloadData()
        
    }
    
    @objc func moveToUnitView(_ sender : UIButton){
        self.performSegue(withIdentifier: "detail_package_to_unit", sender: self)
    }
}


extension DetailPackageViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        let sectionName = listHeader[section]
        if !sectionName.isEmpty{
            return 50
        }else{
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerServiceDetailCell") as! CustomHeaderServiceDetail
        headerView.lblSectionName.tag = 0
        headerView.mBackground.backgroundColor = UIColor.white
        headerView.isUserInteractionEnabled = false
        
        if section == 3 {
            headerView.isUserInteractionEnabled = true
            headerView.detailPackageViewController = self
            headerView.mBackground.backgroundColor = UIColor.clear
            headerView.lblSectionName2.isHidden = false
            if self.currentTag == 2{
                headerView.lblSectionName2.textColor =  #colorLiteral(red: 0.0002389721922, green: 0.6639527082, blue: 1, alpha: 1)
                headerView.lblSectionName.textColor = UIColor.lightGray
            }else{
                headerView.lblSectionName.textColor =  #colorLiteral(red: 0.0002389721922, green: 0.6639527082, blue: 1, alpha: 1)
                headerView.lblSectionName2.textColor = UIColor.lightGray
            }
            headerView.lblSectionName.font = UIFont.fontAwesome(ofSize: 16.0)
            let infoTitle = String.fontAwesomeIcon(name: .list)
            headerView.lblSectionName.textAlignment = .center
            headerView.lblSectionName.text = " " + infoTitle + "  CHI TIẾT"
            
            headerView.lblSectionName2.font = UIFont.fontAwesome(ofSize: 16.0)
            let commentTitle = String.fontAwesomeIcon(name: .comment)
            headerView.lblSectionName2.text = commentTitle + "  ĐÁNH GIÁ (\(self.mProductRatingData.count))"
            headerView.lblSectionName2.tag = 1
            
        } else {
            
            headerView.lblSectionName.text = self.listHeader[section]
            headerView.lblSectionName2.backgroundColor = UIColor.clear
            headerView.lblSectionName.layer.borderWidth = 0
            headerView.lblSectionName2.isHidden = true
            headerView.lblSectionName.textColor = UIColor.lightGray
            headerView.lblSectionName.textAlignment = NSTextAlignment.left
            
            if section == 2{
                headerView.lblSectionName.backgroundColor = UIColor.clear
                headerView.lblSectionName2.backgroundColor = UIColor.white
            }
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            
            return self.view.height / 3
            
        } else if indexPath.section == 3 && self.currentTag == 2 {
            let numberRate = self.mProductRatingData.count
            if numberRate > 0 {
                if numberRate > 5{
                    if numberRate > self.numberOfShowingComment {
                        return 128*CGFloat(self.numberOfShowingComment) + 237.0
                    }else {
                        return 128*CGFloat(numberRate) + 237.0
                    }
                }else{
                    return 128*CGFloat(numberRate) + 202.0
                }
            }else{
                return 180.0
            }
        } else if indexPath.section == 4 {
            let productCount = self.products.count - 1
            if productCount > 0 {
                let value = Int(screenSizeHeight/2.78)
                if IS_IPAD{
                    if productCount % 2 == 0{
                        return CGFloat(240 + value*(productCount / 2 - 1) + 300)
                    }else{
                        return CGFloat(240 + value*(productCount / 2) + 300)
                    }
                }else{
                    if IS_IPHONE_5{
                        if productCount % 2 == 0{
                            return CGFloat(240 + value*(productCount / 2 - 1) + 175)
                        }else{
                            return CGFloat(240 + value*(productCount / 2) + 175)
                        }
                    }else{
                        if productCount % 2 == 0{
                            if IS_IPHONE_X{
                                return CGFloat(240 + value*(productCount / 2 - 1) + 105)
                            }else{
                                return CGFloat(240 + value*(productCount / 2 - 1) + 75)
                            }
                        }else{
                            if IS_IPHONE_X{
                                return CGFloat(240 + value*(productCount / 2) + 105)
                            }else{
                                return CGFloat(240 + value*(productCount / 2) + 120)
                            }
                        }
                        //return CGFloat(240 + value*(services.count/2)+10*(services.count/2+1)+40)
                    }
                }
            }
            
        }
        return UITableView.automaticDimension
        //        return 500
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 3 {
            if self.currentTag == 1 {
                if self.dataListTest.count != 0 {
                    return (self.dataListTest.count + 2)
                }else {
                    return 2
                }
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, analysisCellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnalysisTableViewCell", for: indexPath) as! AnalysisTableViewCell
        cell.mLeftTitle.font = UIFont.systemFont(ofSize: 16)
        cell.mWhiteBackground.isHidden = false
        cell.mNoticeView.isHidden = true
        cell.mNoticeLbl.text = ""
        cell.mLeftTitle.text = ""
        cell.mRightTitle.text = ""
        if (indexPath.row == 0) {
            cell.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
            cell.mWhiteBackground.isHidden = true
            cell.mLeftTitle.text = "Danh mục xét nghiệm"
            cell.mLeftTitle.font = UIFont.boldSystemFont(ofSize: 16)
            cell.mRightTitle.text = "Chi phí"
            cell.mRightTitle.font = UIFont.boldSystemFont(ofSize: 16)
            cell.mRightTitle.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else if (indexPath.row == self.dataListTest.count + 1) { /// last index
            cell.mNoticeView.isHidden = false
            cell.mNoticeView.backgroundColor = UIColor.clear
            cell.mWhiteBackground.isHidden = true
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.mNoticeLbl.text = "Lưu ý: Nhấn vào tên xét nghiệm để biết thêm ý nghĩa xét nghiệm"
            cell.mNoticeLbl.sizeToFit()
            cell.mViewBoder.isHidden = true
        } else {
            if self.dataListTest.count != 0 {
                cell.mLeftTitle.text = "\(self.dataListTest[indexPath.row - 1]["product_attribute_name"].stringValue)"
                cell.mRightTitle.isHidden = false
                cell.mRightTitle.text = formatString(value: Float(self.dataListTest[indexPath.row - 1]["attribute_values"][0]["cost"].intValue))
                cell.mRightTitle.font = UIFont.boldSystemFont(ofSize: 14)
                
            }
            else {
                cell.mRightTitle.isHidden = true
            }
            
            cell.mRightTitle.textColor = UIColor.black
            cell.backgroundColor = UIColor.darkGray
            cell.mViewBoder.isHidden = false
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, serviceCommentCellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentServicePackageCell", for: indexPath) as! CommentServicePackageCell
        
        cell.delegate = self
        cell.mRatingList = self.mProductRatingData
        let numberRate = self.mProductRatingData.count
        if numberRate > 5 {
            let height = 128*CGFloat(numberRate) + 215.0
            cell.setUpTable(cellHeightAuto: Int(height))
        }else {
            let height = 128*CGFloat(numberRate) + 180.0
            cell.setUpTable(cellHeightAuto: Int(height))
        }
        
        cell.myTableView?.isScrollEnabled = false
        cell.myTableView?.bounces = false
        cell.myTableView?.alwaysBounceVertical = false
        cell.myTableView?.reloadData()
        cell.btnRate.isUserInteractionEnabled = true
        cell.btnRate.addTarget(self, action: #selector(DetailPackageViewController.performRatingAction(_:)), for: UIControl.Event.touchUpInside)
        cell.btnMore.addTarget(self, action: #selector(loadMore(_:)), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "customSlideTableViewCell", for: indexPath) as!
            CustomSlideTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            cell.images = []
            
            for item in infoProducts["images"].arrayValue {
                cell.images.append(item["src"].stringValue)
            }
            
            cell.namePackage = infoProducts["name"].stringValue
            
            if cell.images.count != 0 {
                cell.slidesCollectionView.reloadData()
            }
            
            return cell
            
        } else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "infoTableViewCell", for: indexPath) as! InfoTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            if infoProducts.isEmpty == false {
                
                cell.mLblTitle.text = infoProducts["name"].stringValue

                let priceStr = formatString(value: infoProducts["price"].floatValue)
                let price = self.doubleFromPrice(string: priceStr)
                var oldPrice = price
                
                let _oldPriceStr = formatString(value: infoProducts["old_price"].floatValue)
                if _oldPriceStr != "" {
                    oldPrice = self.doubleFromPrice(string: _oldPriceStr)
                }
                
                if IS_IPHONE_5{
                    cell.mLblCost.font = UIFont.boldSystemFont(ofSize: 15.0)
                    
                }
                cell.mLblCost.text = "\(priceStr)"
                let percent = (oldPrice - price)/oldPrice  * 100
                
                if percent != 0 , oldPrice != 0 {
                    cell.mLblSaleOff.isHidden = false
                    
                    cell.mLblSaleOff.font = UIFont.fontAwesome(ofSize: 13.0)
                    cell.mLblSaleOff.text = String.fontAwesomeIcon(name: .caretDown) + " " + String(format: "%.f", percent) + "%"
                    
                    cell.mLblDiscount.font = UIFont.boldSystemFont(ofSize: 18.0)
                    let result = formatAttributeString(value: "\(_oldPriceStr)")
                    cell.mLblDiscount.attributedText = result
                }else {
                    cell.mLblSaleOff.isHidden = true
                }
                
                var rating = Double(0)
                let sum = infoProducts["approved_rating_sum"].intValue
                let total = infoProducts["approved_total_reviews"].intValue
                if total > 0 {
                    rating = Double(sum) / Double(total)
                }
                
                cell.cosmosRateStar.rating = rating
                cell.mLblRate.text = "\(String(format: "%.2f", rating))/5"
                if IS_IPHONE_5{
                    cell.mLblNumberPeopleRate.textAlignment = NSTextAlignment.right
                }
                cell.mLblNumberPeopleRate.text = "(\(total))"
                
                
                let urlStr = infoProducts["full_description"].stringValue
                var cellWebView: WKWebView
                if let webView  = cell.webView.viewWithTag(100) as? WKWebView {
                    cellWebView = webView
                } else {
                    let webConfiguration = WKWebViewConfiguration()
                    let webView = WKWebView(frame: .zero, configuration: webConfiguration)
                    webView.uiDelegate = cell
                    webView.navigationDelegate = cell
                    
                    webView.isUserInteractionEnabled = false
                    webView.tag = 100
                    
                    cell.webView.addSubview(webView)
                    webView.translatesAutoresizingMaskIntoConstraints = false
                    webView.snp.makeConstraints { (maker) in
                        maker.top.equalToSuperview()
                        maker.leading.equalToSuperview()
                        maker.bottom.equalToSuperview()
                        maker.trailing.equalToSuperview()
                    }
                    cellWebView = webView
                }
                cell.mTableview = tableView
                cellWebView.loadHTMLString("""
                    <html><head> <meta name="viewport" content="width=device-width, initial-scale=1"> <style>*{font-family: "Helvetica";}img{margin: 0;}</style> <meta charset="utf-8"></head><body>\(urlStr.htmlDecoded)</body></html>
                    """, baseURL: nil)
                
                
            }
            
            return cell
            
            
        } else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "locationTableViewCell", for: indexPath) as! LocationTableViewCell
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.mbtnUnit.addTarget(self, action: #selector(moveToUnitView), for: .touchUpInside)
            self.addBoxShadow(uiview: cell.mViewHospital)
            
            if (manufacture.isEmpty == false) && (infoProducts.isEmpty == false) {
                for manuId in manufacture.arrayValue {
                    if infoProducts["manufacturer_ids"].arrayValue.contains(JSON(manuId["Id"].intValue)) {
                        
//                        let url = URL(string: manuId["PictureModel"]["ThumbImageUrl"].stringValue)
//                        cell.mImgAvatar.sd_setImage(with: url, completed: nil)
                        
                        cell.mLblName.text = manuId["Name"].stringValue
//                        cell.mLblName.layer.borderWidth = 0.5
//                        cell.mLblName.layer.borderColor = UIColor.greenColor().CGColor
                        
                        let urlStr = manuId["Description"].stringValue
                        
                        var cellWebView: UIWebView
                        if let webView = cell.mWebviewDescription.viewWithTag(100) as? UIWebView {
                            cellWebView = webView
                        } else {
                            let webView = UIWebView(frame: CGRect(x: 0, y: 0, width: cell.mWebviewDescription.bounds.width, height: cell.mWebviewDescription.bounds.height))
                            webView.tag = 100
                            
                            webView.isUserInteractionEnabled = false
                            webView.scrollView.isUserInteractionEnabled = false
                            
                            cell.mWebviewDescription.addSubview(webView)
                            cellWebView = webView
                        }
                        cellWebView.loadHTMLString((urlStr.htmlAttributedString?.string)!, baseURL: nil)
                    }
                }
            }
            
            return cell
            
        } else if indexPath.section == 3{
            
            if currentTag == 1 {
                return self.tableView(tableView, analysisCellForRowAt: indexPath)
            } else {
                return self.tableView(tableView, serviceCommentCellForRowAt:indexPath)
            }
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as! CustomTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.collectionView.register(UINib(nibName: "OtherServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OtherServiceCell")
            cell.collectionView.isScrollEnabled = false
            cell.collectionView.bounces = false
            
            if let layout = cell.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
                layout.scrollDirection = .vertical
            }
            return cell
            
        }
    }
}

extension DetailPackageViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 4{
            guard let tableViewCell = cell as? CustomTableViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 6{
            guard let tableViewCell = cell as? CustomTableViewCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 3 && self.currentTag == 1 && 0 < indexPath.row && indexPath.row < dataListTest.count + 1) {
            let descLookUpResultVC = DescLookUpResultVC(nibName: "DescLookUpResultVC", bundle: nil)
//            descLookUpResultVC.testName = "test name"
            
            for item in self.dataListTest[indexPath.row - 1]["attribute_values"].arrayValue {
                if item["display_order"].intValue == 0 {
                    descLookUpResultVC.meaning1 = item["name"].stringValue
                    descLookUpResultVC.testName = item["name"].stringValue
//                    descLookUpResultVC.testName = "\(self.dataListTest[indexPath.row - 1]["product_attribute_name"].stringValue)"
                }
                
                if  item["display_order"].intValue == 1 {
                    descLookUpResultVC.meaning2 = item["name"].stringValue
//                    descLookUpResultVC.testName = item["name"].stringValue
                    descLookUpResultVC.testName = "\(self.dataListTest[indexPath.row - 1]["product_attribute_name"].stringValue)"
                }
            }
            
            descLookUpResultVC.modalPresentationStyle = .custom
            self.present(descLookUpResultVC, animated: true, completion: nil)
        }
    }
}

extension DetailPackageViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width, column:2)
        if IS_IPHONE_5{
            let value = Int(screenSizeHeight/2.40)
            return CGSize(width: itemWidth, height: value)
        }else{
            let value = Int(screenSizeHeight/2.78)
            return CGSize(width: itemWidth, height: value)
        }
    }
    
    func getItemWidth(boundWidth: CGFloat, column:CGFloat) -> Int {
        let totalWidth = boundWidth - (Constant.offset + Constant.offset) - (column - 1) * Constant.minItemSpacing
        return Int(totalWidth / column)
    }
    
}

extension DetailPackageViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count - 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherServiceCell", for: indexPath) as! OtherServiceCollectionViewCell
        
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.contentView.layer.borderWidth = 1.0
        
        DispatchQueue.main.async(execute: {
            if self.products.count > indexPath.item  {
                var productIndex: Int
                var product: JSON
                if indexPath.item < self.mIgnoredIndex {
                    productIndex = indexPath.item
                } else {
                    productIndex = indexPath.item + 1
                }
                product = self.products[productIndex]
                
                cell.lblServiceName.text = product["Name"].stringValue
                cell.lblServiceName.font = UIFont.systemFont(ofSize: 18)
                if IS_IPHONE_5{
                    cell.lblServiceName.font = UIFont.systemFont(ofSize: 15)
                }
                cell.lblServiceName.lineBreakMode = .byWordWrapping
                cell.lblServiceName.numberOfLines = 2
                
                cell.imageThumb.contentMode = .scaleToFill
                let imageModel = product["DefaultPictureModel"].dictionaryValue
                
                if let image = imageModel["ImageUrl"]?.string {
                    let url = image
                    if url.count > 0 {
                        cell.imageThumb.loadImageUsingUrlString(urlString:url )
                    }
                }
                let productPrice = product["ProductPrice"].dictionaryValue
                
                
                if let priceStr = productPrice["Price"]?.stringValue {
                    let price = self.doubleFromPrice(string: priceStr)
                    var oldPrice = price
                    var oldPriceStr = priceStr
                    if let _oldPriceStr = productPrice["OldPrice"]?.string {
                        oldPrice = self.doubleFromPrice(string: _oldPriceStr)
                        oldPriceStr = _oldPriceStr
                    }else {
                        self.old_price = oldPriceStr
                    }
                    let percent = (oldPrice - price)/oldPrice  * 100
                    
                    if IS_IPHONE_5{
                        cell.lblCost.font = UIFont.boldSystemFont(ofSize: 15.0)
                    }
                    cell.lblCost.text = "\(priceStr)"
                    
                    if priceStr != oldPriceStr {
                        cell.lblSaleOff.isHidden = false
                        cell.lblSaleOff.font = UIFont.fontAwesome(ofSize: 13.0)
                        cell.lblSaleOff.text = String.fontAwesomeIcon(name: .caretDown) + " " + String(format: "%.f", percent) + "%"
                        cell.lblDisCount.font = UIFont.fontAwesome(ofSize: 16.0)
                        let result = formatAttributeString(value:oldPriceStr)
                        cell.lblDisCount.attributedText = result
                    }else {
                        cell.lblSaleOff.isHidden = true
                    }
                    
                    if let review = product["ReviewOverviewModel"].dictionary {
                        
                        let total = review["TotalReviews"]?.intValue
                        var rating = Double(0)
                        if let sum = review["RatingSum"]?.int, let total = review["TotalReviews"]?.int, total > 0 {
                            rating = Double(sum) / Double(total)
                        }
                        cell.cosmosRateStar.rating = Double(rating)
                        if IS_IPHONE_5{
                            cell.lblNumberComment.textAlignment = NSTextAlignment.right
                        }
                        
                        cell.lblNumberComment.text = "(\(total ?? 0))"
                    }
                }
            }
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let sb = UIStoryboard.init(name: "More", bundle: nil)
//        let vc = sb.instantiateViewController(withIdentifier: "DetailPackegeViewController") as! DetailPackageViewController
        var productIndex: Int
        var product: JSON
        if indexPath.item < self.mIgnoredIndex {
            productIndex = indexPath.item
        } else {
            productIndex = self.mIgnoredIndex + 1
        }
        product = self.products[productIndex]
//        vc.titleDetail = product["Name"].stringValue
//        vc.id = product["Id"].stringValue
//        vc.products = self.products
//        vc.mIgnoredIndex = productIndex
//        self.navigationController?.pushViewController(vc, animated: true)
        
        
        if (DatabaseManager.sharedManager.isGuest()) {
            
            self.prompt("Thông báo", message: "Bạn cần phải đăng nhập. Bạn có muốn đăng nhập ngay bây giờ không", okTitle: "Đăng nhập", okHandler: { (okAction) in
                DatabaseManager.sharedManager.logout {}
            }, cancelTitle: "Thoát") { (cancelAction) in
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            self.setTitle(title: product["Name"].stringValue)
            self.loadDataManufacture()
            self.loadDataProductById(productID: product["Id"].stringValue)
            self.loadProductReviews(product_id: product["Id"].stringValue)
            
            
            self.mtbDetailPackage.contentOffset = CGPoint(x: 0, y: 0)
        }
        
    }
}


extension String {
    /// Converts HTML string to a `NSAttributedString`
    
    var htmlAttributedString: NSAttributedString? {
        return try? NSAttributedString(data: Data(utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
    }
}

extension DetailPackageViewController: CommentServicePackageCellDelegate {
    
    func pushMoreCommentService() {
        
    }
    
    
}
