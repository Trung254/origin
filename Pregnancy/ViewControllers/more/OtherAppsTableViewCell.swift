//
//  OtherAppsTableViewCell.swift
//  Reusable Template
//
//  Created by mai kim tai  on 12/11/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit

class OtherAppsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgOtherApps: UIImageView!
    
    @IBOutlet weak var lblOtherApps: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgOtherApps.clipsToBounds = true
        imgOtherApps.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func showUrl(_ sender: Any) {
        if let url = URL(string: "https://itunes.apple.com/app/id1132002156?mt=8") {
            if (UIApplication.shared.canOpenURL(url)) {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
