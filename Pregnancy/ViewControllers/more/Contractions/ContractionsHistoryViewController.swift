//
//  ContractionsHistoryViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/3/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class dataContractionsHistory {
    public var lblDateContractionsHistory : String
    public var lblDurationHistory : String
    public var lblIntervalHistory : String
    
    init(lblDate: String, lblDuration: String, lblInterval: String) {
        self.lblDateContractionsHistory = lblDate
        self.lblDurationHistory = lblDuration
        self.lblIntervalHistory = lblInterval
    }
}

class ContractionsHistoryViewController: BaseViewController {
    
    var dataTableContractionsHistory : [dataContractionsHistory] = [
        dataContractionsHistory(lblDate: "03-01-19 08:44", lblDuration: "15:16", lblInterval: "00:10"),
        dataContractionsHistory(lblDate: "03-01-19 09:30", lblDuration: "01:16", lblInterval: "01:10"),
        dataContractionsHistory(lblDate: "03-01-19 09:54", lblDuration: "05:16", lblInterval: "05:10"),
    ]
    
    @IBOutlet weak var mTableViewHistory: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    private func setupUI() {
        setupTablieView()
        addNavigationButton()
    }
    
    private func setupTablieView() {
        let nib = UINib(nibName: "KickHistoryTableViewCell", bundle: Bundle.main)
        mTableViewHistory.register(nib, forCellReuseIdentifier: "KickHistoryCell")
        mTableViewHistory.delegate = self
        mTableViewHistory.dataSource = self
        self.mTableViewHistory.tableFooterView = UIView(frame: CGRect.zero)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            dataTableContractionsHistory.remove(at: indexPath.row)
            mTableViewHistory.reloadData()
        }
    }
    
    func addNavigationButton(){
        let addButton = UIButton(type: .custom)
        addButton.setImage(UIImage(named: "delete-button"), for: .normal)
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        addButton.addTarget(self, action: #selector(addButtonAction), for: .touchUpInside)
        let infoButton = UIButton(type: .custom)
        infoButton.setImage(UIImage(named: "info (1)"), for: .normal)
        infoButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        infoButton.addTarget(self, action: #selector(handleNavigationIcon), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: addButton)
        let barButton2 = UIBarButtonItem(customView: infoButton)
        
        self.navigationItem.rightBarButtonItems = [barButton,barButton2]
    }
    
    @objc func handleNavigationIcon(){
        performSegue(withIdentifier: "History_To_Info", sender: self)
        
    }
    @objc func addButtonAction(){
        self.prompt("Message", message: "Do you want to delete ALL the recorded contractions? This action cannot be undone", okHandler: { (alert) in
            self.dataTableContractionsHistory.removeAll()
            self.mTableViewHistory.reloadData()
            self.alert("All previous contractions have been deleted", title: "Message", handler: nil)
        }, cancelHandler: nil)
    }
    
}


    extension ContractionsHistoryViewController : UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return dataTableContractionsHistory.count
        }
    }

    extension ContractionsHistoryViewController : UITableViewDelegate {
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "KickHistoryCell", for: indexPath) as! KickHistoryTableViewCell
            cell.backgroundColor = UIColor.clear
            cell.mLblDateKickHistory.text = dataTableContractionsHistory[indexPath.row].lblDateContractionsHistory
            cell.mLblDurationKickHistory.text = dataTableContractionsHistory[indexPath.row].lblDurationHistory
            cell.mLblKickResultKickHistory.text = dataTableContractionsHistory[indexPath.row].lblIntervalHistory
            return cell
        }
    }
