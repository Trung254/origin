//
//  ContractionsViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/25/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
enum CoutingStatus: Int {
    case firstStopping
    case firstStarting
    case stopping
    case starting
}

class ContractionsViewController: BaseViewController {

   
    @IBOutlet weak var mViewOverview: UIView!
    @IBOutlet weak var mBtnOverview: UIButton!
    @IBOutlet weak var mKickBtn: UIButton!
    @IBOutlet weak var mKickView: UIView!
    @IBOutlet weak var mDeleteView: UIView!
    @IBOutlet weak var mDeleteBtn: UIButton!
    @IBOutlet weak var mUndoView: UIView!
    @IBOutlet weak var mUndoBtn: UIButton!
    @IBOutlet weak var mCountUpLbl: UILabel!
    @IBOutlet weak var mTimerLbl: UILabel!
    @IBOutlet weak var mLastDurationLbl: UILabel!
    @IBOutlet weak var mLastIntervalLbl: UILabel!
    @IBOutlet weak var mLbDoKick: UILabel!
    
    var coutingStatus: CoutingStatus = .firstStopping
    var mIsFirstStop: Bool = true
    var mTimer: Timer = Timer()
    var mTimerTotal: Timer = Timer()
    var mTimerLast : TimeInterval?
    var mStartDate:Date?
    var mTotalStartDate: Date?
    var canStartTotalTimer: Bool = true
    var mIsCounting: Bool = false
    
    //var stopImages: [UIImage] = []
    
    var image1:UIImage = UIImage(named: "stop-0")!
    var image2:UIImage = UIImage(named: "stop-1")!
    var image3:UIImage = UIImage(named: "stop-2")!
    var image4:UIImage = UIImage(named: "stop-3")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mKickView.layer.borderColor = UIColor(red: 139/255.0, green: 97/255.0, blue: 97/255.0, alpha: 1.0).cgColor
        mKickView.layer.borderWidth = 3
        mKickView.layer.cornerRadius = 90
        mKickView.backgroundColor = UIColor.clear
        mKickBtn.layer.cornerRadius = mKickBtn.frame.size.width / 2
        
        mViewOverview.layer.borderColor = UIColor(red: 139/255.0, green: 97/255.0, blue: 97/255.0, alpha: 1.0).cgColor
        mViewOverview.layer.borderWidth = 2
        mViewOverview.layer.cornerRadius = 26
        mViewOverview.backgroundColor = UIColor.clear
        mBtnOverview.layer.cornerRadius = mBtnOverview.frame.size.width / 2
        
        mDeleteView.layer.borderColor = UIColor(red: 139/255.0, green: 97/255.0, blue: 97/255.0, alpha: 1.0).cgColor
        mDeleteView.layer.borderWidth = 2
        mDeleteView.layer.cornerRadius = 26
        mDeleteView.backgroundColor = UIColor.clear
        mDeleteBtn.layer.cornerRadius = mDeleteBtn.frame.size.width / 2
        
        mUndoView.layer.borderColor = UIColor(red: 139/255.0, green: 97/255.0, blue: 97/255.0, alpha: 1.0).cgColor
        mUndoView.layer.borderWidth = 2
        mUndoView.layer.cornerRadius = 26
        mUndoView.backgroundColor = UIColor.clear
        mUndoBtn.layer.cornerRadius = mUndoBtn.frame.size.width / 2
        
        self.mDeleteBtn.isEnabled = false
        self.mUndoBtn.isEnabled = false
        self.addNavigationButton()
        
        //stopImages = createImageArray(total: 3, imagePrefix: "stop")
    }
   
//    func createImageArray(total: Int, imagePrefix: String) -> [UIImage] {
//
//        var imageArray: [UIImage] = []
//
//        for imageCount in 0..<total {
//            let imageName = "\(imagePrefix)-\(imageCount).png"
//            let image = UIImage(named: imageName)!
//
//            imageArray.append(image)
//        }
//        return imageArray
//    }
//
//    func animate(imageView: UIImageView, images: [UIImage]) {
//        imageView.animationImages = images
//        imageView.animationDuration = 1.0
//        imageView.animationRepeatCount = 1
//        imageView.startAnimating()
//    }
    
    @IBAction func doKick(_ sender: Any) {
        if(self.mIsCounting) {
            self.stopCounting()
        } else {
            self.startCountUpTotal()
            switch coutingStatus {
            case .firstStopping:
                print("firstStoping")
//                mKickBtn.setTitle("Stop", for: UIControl.State.normal)
//                mKickBtn.backgroundColor = UIColor.red
                
//                animate(imageView: mKickBtn.imageView!, images: stopImages)
                
                mKickBtn.setImage(image1, for: UIControl.State.normal)
                mKickBtn.imageView!.animationImages = [image1, image2, image3, image4]
                mKickBtn.imageView!.animationDuration = 4.0
                mKickBtn.imageView!.startAnimating()
                
                
                self.startCountUp()
                coutingStatus = .firstStarting
                self.mLbDoKick.text = "Current duration"
                self.mDeleteBtn.isEnabled = true
                break
            case .firstStarting:
                print("firstStarting")
                //self.mLastDurationLbl.text = self.mCountUpLbl.text
                
                mTimerLast = Date.init().timeIntervalSince(self.mStartDate!)
                let times = Int(mTimerLast!)
                let hour = times / 3600
                let seconds = times % 60
                let minutes = (times / 60) % 60
                if times == 1 {
                    self.mLastDurationLbl.text = "\(seconds) sec"
                } else if times < 60 {
                    self.mLastDurationLbl.text = "\(seconds) secs"
                } else if times < 120 {
                    self.mLastDurationLbl.text = "\(minutes) min \(seconds) sec"
                } else if times < 3600 {
                    self.mLastDurationLbl.text = "\(minutes) mins \(seconds) sec"
                } else {
                    self.mLastDurationLbl.text = "\(hour) hour \(minutes) min \(seconds) sec"
                }
                
                self.mLbDoKick.text = "Current interval"
                self.alert("Contraction has been saved", title: nil) { (alert) in
                    self.mKickBtn.setImage(nil, for: UIControl.State.normal)
                    self.mKickBtn.backgroundColor = #colorLiteral(red: 0.3333333333, green: 0.7843137255, blue: 0.7921568627, alpha: 1)
                    self.mKickBtn.setTitle("Start", for: UIControl.State.normal)
                    self.coutingStatus = .stopping
                    self.mUndoBtn.isEnabled = true
                }
                break
            case .stopping:
                print("stopping")
                coutingStatus = .starting
//                mKickBtn.setTitle("Stop", for: UIControl.State.normal)
//                mKickBtn.backgroundColor = UIColor.red
                
                mKickBtn.setImage(image1, for: UIControl.State.normal)
                mKickBtn.imageView!.animationImages = [image1, image2, image3, image4]
                mKickBtn.imageView!.animationDuration = 4.0
                mKickBtn.imageView!.startAnimating()
                
                mTimerLast = Date.init().timeIntervalSince(self.mStartDate!)
                let times = Int(mTimerLast!)
                let hour = times / 3600
                let seconds = times % 60
                let minutes = (times / 60) % 60
                if times == 1 {
                    self.mLastIntervalLbl.text = "\(seconds) sec"
                } else if times < 60 {
                    self.mLastIntervalLbl.text = "\(seconds) secs"
                } else if times < 120 {
                    self.mLastIntervalLbl.text = "\(minutes) min \(seconds) sec"
                } else if times < 3600 {
                    self.mLastIntervalLbl.text = "\(minutes) mins \(seconds) sec"
                } else {
                    self.mLastIntervalLbl.text = "\(hour) hour \(minutes) min \(seconds) sec"
                }
                
                self.startCountUp()
                self.mUndoBtn.isEnabled = false
                self.mLbDoKick.text = "Current duration"
                //self.mLastIntervalLbl.text = self.mCountUpLbl.text
                
                break
            case .starting:
                print("starting")
                coutingStatus = .stopping
                self.mKickBtn.setImage(nil, for: UIControl.State.normal)
                self.mKickBtn.backgroundColor = #colorLiteral(red: 0.3333333333, green: 0.7843137255, blue: 0.7921568627, alpha: 1)
                self.mKickBtn.setTitle("Start", for: UIControl.State.normal)
                self.mUndoBtn.isEnabled = true
                self.mLbDoKick.text = "Current interval"
                //self.mLastDurationLbl.text = self.mCountUpLbl.text
                
                mTimerLast = Date.init().timeIntervalSince(self.mStartDate!)
                let times = Int(mTimerLast!)
                let hour = times / 3600
                let seconds = times % 60
                let minutes = (times / 60) % 60
                if times == 1 {
                    self.mLastDurationLbl.text = "\(seconds) sec"
                } else if times < 60 {
                    self.mLastDurationLbl.text = "\(seconds) secs"
                } else if times < 120 {
                    self.mLastDurationLbl.text = "\(minutes) min \(seconds) sec"
                } else if times < 3600 {
                    self.mLastDurationLbl.text = "\(minutes) mins \(seconds) sec"
                } else {
                    self.mLastDurationLbl.text = "\(hour) hour \(minutes) min \(seconds) sec"
                }
                
                break
            }
        }
        UIDevice.vibrate()
    }
    
    func startCountUpTotal() -> Void {
        if canStartTotalTimer == true  {
            self.mTotalStartDate = Date.init()
            self.mDeleteBtn.isEnabled = true
            self.mTimerLbl.text = "< 1 min"
            self.mTimerTotal.invalidate()
            self.mTimerTotal = Timer.scheduledTimer(timeInterval: 60,
                                               target: self,
                                               selector: #selector(checkCounterTotal),
                                               userInfo:nil,
                                               repeats: true)

            canStartTotalTimer = false
        }
    }
    
    @objc func checkCounterTotal() -> Void {
        let time = Date.init().timeIntervalSince(self.mTotalStartDate!)
        let times = Int(time)
        let minutes = (times / 60) % 60
        //print("Time: \(time)")
        //self.setCountUptime(time)
        if time <= 120 {
            self.mTimerLbl.text = " 1 min"
        } else if time <= 3600  {
            self.mTimerLbl.text = "\(minutes) mins"
        } else {
            self.mTimer.invalidate()
            self.mTimerTotal.invalidate()
            self.alert("Interval exceeds 1 hour, the session will now end", title: "Message") { (alert) in
                self.mKickBtn.backgroundColor = #colorLiteral(red: 0.3333333333, green: 0.7843137255, blue: 0.7921568627, alpha: 1)
                self.mKickBtn.setTitle("Start", for: UIControl.State.normal)
                self.coutingStatus = .stopping
                self.canStartTotalTimer = true
            }
        }
    }
    
    func startCountUp() -> Void {
        self.mStartDate = Date.init()
        self.mDeleteBtn.isEnabled = true
        self.mTimer.invalidate()
        self.mTimer = Timer.scheduledTimer(timeInterval: 1,
                                            target: self,
                                           selector: #selector(checkCounter),
                                           userInfo:nil,
                                           repeats: true)
    }
    
    @objc func checkCounter() -> Void {
        let time = Date.init().timeIntervalSince(self.mStartDate!)
        self.setCountUptime(time)
    }
    
    func setCountUptime(_ timeInterval:TimeInterval) -> Void {
        let timeString = self.stringFromTimeInterval(interval: timeInterval)
        self.mCountUpLbl.text = timeString
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval)
        if interval == 3600 {
            return String("60:00")
        } else {
            let seconds = interval % 60
            let minutes = (interval / 60) % 60
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }
    
    func stopCounting() {
        self.mTimer.invalidate()
        self.mTimerTotal.invalidate()
        
        self.mIsCounting = false
        self.setCountUptime(0)
        self.mDeleteBtn.isEnabled = false
        self.mUndoBtn.isEnabled = false
        self.mLastDurationLbl.text = ""
        self.mLastIntervalLbl.text = ""
        self.mTimerLbl.text = ""
        self.coutingStatus = .firstStopping
        self.canStartTotalTimer = true
        //mKickBtn.setImage(image1, for: UIControl.State.normal)
    }
    
    @IBAction func mDeleteBtn(_ sender: Any) {
        self.prompt("Thông báo", message: "Bạn có chắc chắn muốn kết thúc phiên này?", okHandler: { (alert) in
            self.prompt("Thông báo", message: "Do you want to delete ALL the recorded contractions? This action cannot be undone", okHandler: { (alert) in
                self.mKickBtn.setImage(nil, for: UIControl.State.normal)
                self.mKickBtn.backgroundColor = #colorLiteral(red: 0.3333333333, green: 0.7843137255, blue: 0.7921568627, alpha: 1)
                self.mKickBtn.setTitle("Start", for: UIControl.State.normal)
                self.stopCounting()
                self.alert("All previous contractions have been deleted", title: "Message", handler: nil)
            }, cancelHandler: nil)
        }, cancelHandler: nil)
    }
    @IBAction func mUndoBtn(_ sender: Any) {
    }
    
    func addNavigationButton(){
        let addButton = UIButton(type: .custom)
        addButton.setImage(UIImage(named: "redo"), for: .normal)
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        addButton.addTarget(self, action: #selector(addButtonAction), for: .touchUpInside)
        let infoButton = UIButton(type: .custom)
        infoButton.setImage(UIImage(named: "info (1)"), for: .normal)
        infoButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        infoButton.addTarget(self, action: #selector(handleNavigationIcon), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: addButton)
        let barButton2 = UIBarButtonItem(customView: infoButton)
        
        self.navigationItem.rightBarButtonItems = [barButton,barButton2]
    }
    
    @objc func handleNavigationIcon(){
        performSegue(withIdentifier: "Contractions_To_Info", sender: self)
        
    }
    @objc func addButtonAction(){
        performSegue(withIdentifier: "Contractions_To_History", sender: self)
    }
    
}
