//
//  BabyNameTableViewCell.swift
//  Reusable Template
//
//  Created by mai kim tai  on 12/11/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit

class BabyNameTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var imgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
