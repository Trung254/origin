//
//  SubShoppingTableViewCell.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/13/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

protocol SubShoppingCellDelegate : class {
    func btnToBuyTapped(_ send: SubShoppingTableViewCell)
    func btnGotItTapped(_ send: SubShoppingTableViewCell)
}

//protocol SubShoppingDelegate : class {
//    func didPressButton(_ send: String)
//}

class SubShoppingTableViewCell: UITableViewCell {

    @IBOutlet weak var mToBuyButton: UIButton!
    @IBOutlet weak var mGotItButton: UIButton!
    
    weak var delegate : SubShoppingCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   

    @IBOutlet weak var mLabel: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func toBuyTapped(_ sender: UIButton) {
        
        delegate?.btnToBuyTapped(self)
        UIView.animate(withDuration: 0.05, delay: 0, options: .curveLinear, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
           sender.isSelected = !sender.isSelected
        }) { (success) in
            UIView.animate(withDuration: 0.05, delay: 0, options: .curveLinear, animations: {
                sender.transform = .identity
            }, completion: nil)
        }
        
        if mToBuyButton.isSelected == true {
            mGotItButton.isSelected = false
        }
    }
    
    @IBAction func gotItTapped(_ sender: UIButton) {
        delegate?.btnGotItTapped(self)
        UIView.animate(withDuration: 0.05, delay: 0, options: .curveLinear, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            sender.isSelected = !sender.isSelected
        }) { (success) in
            UIView.animate(withDuration: 0.05, delay: 0, options: .curveLinear, animations: {
                sender.transform = .identity
            }, completion: nil)
        }
        
        if mGotItButton.isSelected == true {
            mToBuyButton.isSelected = false
        }
//        if isPressCheck == false {
//            isPressCheck = true
//            mGotItButton.setImage(UIImage(named: "Checkbox"), for: .normal)
//        }
//        else{
//            isPressCheck = false
//            mGotItButton.setImage(UIImage(named: "UnCheckbox"), for: .normal)
//
//        }
//        mToBuyButton.setImage(UIImage(named: "shopping-bag"), for: .normal)
//        isPressBag = false
//    }
}
}
