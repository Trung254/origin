//
//  FirstCellShoppingTableViewCell.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/13/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class FirstCellShoppingTableViewCell: UITableViewCell {

    @IBOutlet weak var mFirstImage: UIImageView!
    @IBOutlet weak var mFirstLabel: UILabel!
    @IBOutlet weak var mSecondLabel: UILabel!
    @IBOutlet weak var mThirdLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
