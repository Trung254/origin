//
//  AddLabelTableViewCell.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/14/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class AddLabelTableViewCell: UITableViewCell {

    @IBOutlet weak var mAddLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
