//
//  ShoppingTableViewCell.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/17/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class ShoppingTableViewCell: UITableViewCell {
    @IBOutlet weak var mImageView: UIImageView!
    @IBOutlet weak var NumberView: UIView!
    @IBOutlet weak var LabelView: UILabel!
    @IBOutlet weak var NumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NumberView.layer.cornerRadius = NumberView.frame.width/2
        NumberLabel.textColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
