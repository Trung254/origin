//
//  AddToBuyTableViewCell.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/14/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class AddToBuyTableViewCell: UITableViewCell {

    @IBOutlet weak var mAddToBuyTF: UITextField!
    
    var item:Array<String> = [""]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
}
