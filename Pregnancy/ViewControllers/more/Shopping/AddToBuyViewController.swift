//
//  AddToBuyViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/13/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddToBuyViewController: BaseViewController, UITextFieldDelegate{
    
    
    @IBOutlet weak var mAddToBuyTableView: UITableView!
    @IBOutlet weak var mPickerView: UIPickerView!
    
    var item:Array<String> = ["Breastfeeding"]
    var name:String = ""
    var passedItem:String = ""
    var pickerItem:Array<String> = []
     var selectedPicker:String = ""
    
    var arrDataShoppingCate = [PregShoppingCategorys]()
    var dataShoppingItem: [JSON] = []
    var dataShoppingCart: [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mAddToBuyTableView.register(UINib.init(nibName: "AddToBuyTableViewCell", bundle: nil), forCellReuseIdentifier: "AddToBuyCell")
        mAddToBuyTableView.register(UINib.init(nibName: "AddLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "AddLabel")
        mAddToBuyTableView.rowHeight = 50
        mAddToBuyTableView.estimatedRowHeight = 60.5
        mAddToBuyTableView.tableFooterView = UIView()
        self.addNavigationItem()
        self.addNavigationTitle()
        self.setItemPicker()
    }
    
    // set title of label inside picker
    func setItemPicker(){
        for item in pickerItem {
            if item == passedItem {
                selectedPicker = item
            }
        }
    }
    
    private func pushShoppingCart(_ shoppingitemid: Int) {
        NetworkManager.shareInstance.apiPostShoppingCart(shoppingItemId: shoppingitemid) {
            (data, message, isSuccess) in
        }
    }
    
    private func loadDataFromAPI() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllShoppingCategorys { (shoppingDataArray, message, isSuccess) in
//            self.arrDataShoppingCate = shoppingDataArray
            for item in shoppingDataArray {
                self.pickerItem.append(item.title)
            }
            DispatchQueue.main.async {
//                self.loadItemShopping()
            }
        }
    }
    
    private func loadItemShopping() {
        NetworkManager.shareInstance.apiGetShoppingItems() {
            (data, message, isSuccess) in
            if isSuccess {
                self.dataShoppingItem = (data as? JSON)!.arrayValue
            }
        }
    }
    
    func addNavigationItem(){
        let checkButton = UIButton(type: .custom)
        checkButton.setImage(UIImage(named: "checked"), for: .normal)
        checkButton.addTarget(self, action: #selector(pressCheck), for: .touchUpInside)
        checkButton.frame = CGRect(x: self.view.frame.size.width, y: 0, width: 50, height: 44)
        let barButton = UIBarButtonItem(customView: checkButton)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func addNavigationTitle(){
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
        lbl.text = "Add item"
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(25.0)
        lbl.sizeToFit()
        navigationItem.titleView = lbl
    }
    
  
    @objc func pressCheck(){
        let viewController = self.navigationController?.viewControllers.first as! SubShoppingViewController
        viewController.passedData = "passed"
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("name at tf \(name)")
        item.append(name)
        passedItem.append(name)
        let indexPatch:IndexPath = IndexPath(row: item.count - 1, section: 0)
        mAddToBuyTableView.insertRows(at: [indexPatch], with: .automatic)
        mAddToBuyTableView.reloadData()
        textField.text = ""
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        let nameAtTF:String = updatedString ?? ""
        name = nameAtTF
        return true

    }

    
    
    func createPicker(){
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let pickerView = UIPickerView()
        pickerView.frame = CGRect(x: 0, y: mAddToBuyTableView.frame.height - 100 , width: mAddToBuyTableView.frame.width, height: 200)
        pickerView.showsSelectionIndicator = true
        pickerView.delegate = self
        pickerView.dataSource = self
        self.view.addSubview(pickerView)
        // set default item when press picker
        let indexItem = pickerItem.firstIndex(of: selectedPicker)
        pickerView.selectRow(indexItem!, inComponent: 0, animated: true)
    }
}

extension AddToBuyViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item.count + 1
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let totalRow = mAddToBuyTableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == totalRow - 1 {
            
            let cell:AddToBuyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddToBuyCell", for: indexPath) as! AddToBuyTableViewCell
            cell.mAddToBuyTF.delegate = self
            return cell
            
        }
        else if indexPath.row == 0 {
            let cell3:AddLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddLabel", for: indexPath) as! AddLabelTableViewCell
            cell3.mAddLabel.textColor = #colorLiteral(red: 0.2392156863, green: 0.4509803922, blue: 0.9490196078, alpha: 1)
            cell3.mAddLabel.font = cell3.mAddLabel.font.withSize(18)
            cell3.mAddLabel.text = selectedPicker
            return cell3
        }
        else{
            let cell2:AddLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddLabel", for: indexPath) as! AddLabelTableViewCell
            cell2.mAddLabel.text = item[indexPath.row]
            
            return cell2
        }
        
    }
}

extension AddToBuyViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let totalRow = mAddToBuyTableView.numberOfRows(inSection: indexPath.section)
        indexGuide = indexPath.row
        mAddToBuyTableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row > 0 && indexPath.row < totalRow - 1 {
            let alert = UIAlertController(title: "", message: "Do you want to delete item?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                self.item.remove(at: indexPath.row)
                self.mAddToBuyTableView.reloadData()
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else if indexPath.row == 0 {
            createPicker()
        }
        
    }
}

extension AddToBuyViewController:UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedPicker = pickerItem[row]
        print("selected \(selectedPicker)")
        mAddToBuyTableView.reloadData()
        pickerView.isHidden = true
    }
    
    
}

extension AddToBuyViewController:UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerItem.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerItem[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
}
