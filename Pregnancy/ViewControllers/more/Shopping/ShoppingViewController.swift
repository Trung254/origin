//
//  ShoppingViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/12/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class ShoppingViewController: BaseViewController {
    @IBOutlet weak var shoppingTableView: UITableView!
    
    var arrDataShoppingCate = [PregShoppingCategorys]()
    var dataShoppingItem: [JSON] = []
    var dataShoppingCart: [JSON] = []
    var titlePass:String = ""
    var selectedIndex:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = .white
        shoppingTableView.rowHeight = UITableView.automaticDimension
        shoppingTableView.estimatedRowHeight = 60.5
        self.setNavigationTitle()
        setupTableView()
        loadDataFromAPI()
    }
    
    private func setupTableView() {
        let nib = UINib(nibName: "ShoppingTableViewCell", bundle: nil)
        shoppingTableView.register(nib, forCellReuseIdentifier: "shoppingCell")
        shoppingTableView.delegate = self
        shoppingTableView.dataSource = self
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 240.0))

        let image: UIImage = UIImage(named: "baby-mom")!
        let headerImageView = UIImageView(image: image)
        headerImageView.frame = CGRect.init(x: 0, y: 0, width: headerView.bounds.width, height: headerView.bounds.height)
        headerView.addSubview(headerImageView)
        shoppingTableView.tableHeaderView = headerView
        shoppingTableView.backgroundView = UIImageView(image: UIImage(named: "me-background-blur"))
    }
    
    private func loadDataFromAPI() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllShoppingCategorys { (shoppingDataArray, message, isSuccess) in
            self.arrDataShoppingCate = shoppingDataArray
            DispatchQueue.main.async {
                self.loadItemShopping()
            }
        }
    }
    
    private func loadItemShopping() {
        NetworkManager.shareInstance.apiGetShoppingItems() {
            (data, message, isSuccess) in
            if isSuccess {
                self.dataShoppingItem = (data as? JSON)!.arrayValue
            }
            
            DispatchQueue.main.async {
                self.loadShoppingCart()
            }
        }
    }
    
    private func loadShoppingCart() {
        NetworkManager.shareInstance.apiGetShoppingCart() {
            (data, message, isSuccess) in
            self.dataShoppingCart = []
            if isSuccess {
                let data = data as? JSON
                self.dataShoppingCart = (data?.arrayValue)!
            }
            
            DispatchQueue.main.async {
                self.shoppingTableView.reloadData()
                self.hideHud()
            }
        }
    }
    
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        let rightButton = UIButton(type: .custom)
        rightButton.setImage(UIImage(named: "share-1"), for: .normal)
        rightButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        rightButton.addTarget(self, action: #selector(handleNavigationIcon), for: .touchUpInside)
        let rightButton2 = UIButton(type: .custom)
        rightButton2.setImage(UIImage(named: "info (1)"), for: .normal)
        rightButton2.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        rightButton2.addTarget(self, action: #selector(handleNavigationIcon), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: rightButton2)
        let barButton2 = UIBarButtonItem(customView: rightButton)
        return [barButton, barButton2]
    }
    
    func setNavigationTitle(){
        
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
        lbl.text = "Mua sắm"
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(25.0)
        navigationItem.titleView = lbl
    }
    
    @objc func handleNavigationIcon(){
        print("shopping navigation item")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "shopping_to_subview" {
            let viewController = segue.destination as! SubShoppingViewController
            viewController.passedTitle = titlePass
            viewController.passedIndex = selectedIndex
        }
    }
}

extension ShoppingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        titlePass = arrDataShoppingCate[indexPath.row].title
        selectedIndex = arrDataShoppingCate[indexPath.row].id
        self.performSegue(withIdentifier: "shopping_to_subview", sender: self)

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ShoppingViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDataShoppingCate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ShoppingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "shoppingCell", for: indexPath) as! ShoppingTableViewCell
        cell.LabelView.text = arrDataShoppingCate[indexPath.row].title
        
       if dataShoppingCart.isEmpty == false && dataShoppingItem.isEmpty == false {
            var count = 0
            for item in dataShoppingCart {
                for j in dataShoppingItem {
                    if item["shopping_item_id"].intValue == j["id"].intValue {
                        if j["category_id"].intValue == indexPath.row {
                            count += 1
                        }
                    }
                }
            }
            if count > 0 {
                cell.NumberLabel.isHidden = false
                cell.NumberView.isHidden = false
                cell.NumberLabel.text = "\(count)"
            }
            else {
                cell.NumberView.isHidden = true
                cell.NumberLabel.isHidden = true
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
}
