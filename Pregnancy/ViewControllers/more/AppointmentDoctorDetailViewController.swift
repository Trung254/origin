//
//  AppointmentDoctorDetailViewController.swift
//  Pregnancy
//
//  Created by dady on 2/28/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import WebKit
import RealmSwift


class AppointmentDoctorDetailViewController: BaseViewController, WKNavigationDelegate {

    @IBOutlet weak var mNameDoctors: customTextField!
    @IBOutlet weak var mAddressHospital: customTextField!
    @IBOutlet weak var mTimeSchedule: customTextField!
    @IBOutlet weak var mPrice: customTextField!
    @IBOutlet weak var mViewAvatar: UIView!
    @IBOutlet weak var mImgAvatar: UIImageView!
    @IBOutlet weak var mUserName: customTextField!
    @IBOutlet weak var mPhoneNumber: customTextField!
    @IBOutlet weak var mAddress: customTextField!
    @IBOutlet weak var mGender: customTextField!
    @IBOutlet weak var mBirthday: customTextField!
    @IBOutlet weak var mRadioMale: UIButton!
    @IBOutlet weak var mRadioFemale: UIButton!
    @IBOutlet weak var mPickerView: UIPickerView!
    @IBOutlet weak var mPickerbox: UIView!
    @IBOutlet weak var mbtnPerson: customButton!
    @IBOutlet weak var mReason: UITextView!
    @IBOutlet weak var mReasonTitleLbl: UILabel!
    @IBOutlet weak var mScrollView: UIScrollView!
    
    private var currentTextField : UITextField?
    private var currentTextView : UITextView?
    
    
    var dataPersonAppointment: Array<String> = ["Tôi", "Người khác"]
    var user_id: Int = 0
    let dateFormatter = DateFormatter()
    var hospital_id:Int = 0
    var date_appoint: String = ""
    var dataAppointment  = [keyValues]()
    var dataProducts: Array<JSON> = []
    var arrDic = Array<Dictionary<String, Any>>()
    var order_id: Int = 0
    var InfoUserTyped: [String] = []
    var checkBirthDay = false

    var last_name:String = ""
    var nopcustomer_id: Int?
    var customer_email: String = "example@example.com"
    var location: String = ""
    var vnpay: VNPay = VNPay()
    
    var webView = WKWebView()
    let getUrlAtDocumentStartScript = "GetUrlAtDocumentStart"
    let getUrlAtDocumentEndScript = "GetUrlAtDocumentEnd"

    
    var tap: UITapGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mIsShowRightButton = false
        self.setAvatar()
        self.mReason.layer.cornerRadius = 5
        self.mReason.layer.borderColor = UIColor.lightGray.cgColor
        self.mReason.layer.borderWidth = 0.7
//        self.checkStatusGender()
        self.vnpay.delegate = self
        
        let config = WKWebViewConfiguration()
        let contentController = WKUserContentController()
        let scriptSource = "document.body.style.backgroundColor = `red`;"
        let script = WKUserScript(source: scriptSource, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        contentController.addUserScript(script)
        config.userContentController = contentController
        let webView = WKWebView(frame: .zero, configuration: config)
        
        self.setupDelegateTextFiled()
        
        self.registerForNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getProducts()
        self.mPickerbox.isHidden = true
        self.mNameDoctors.text = self.dataAppointment.last?.value
        self.mAddressHospital.text = self.dataAppointment.first?.value
        self.mTimeSchedule.text = "\(self.dataAppointment[1].value) lúc \(self.dataAppointment[3].value)"
        self.mPrice.text = self.dataAppointment[2].value
        
        DatabaseManager.sharedManager.getUserInfo { (pregUser, mnessage, isSuccess) in
            if pregUser != nil {
                self.mUserName.text = pregUser?.first_name
                self.mPhoneNumber.text = pregUser?.phone
                self.user_id = (pregUser?.id)!
                
                if pregUser?.avatar != "" {
                    var avatar = NetworkManager.rootDomain
                    if pregUser!.avatar.hasPrefix("http") {
                        avatar = pregUser!.avatar
                    } else {
                        avatar.append(pregUser!.avatar)
                    }
                    
                    if let url = URL(string: avatar) {
                        if let data = try? Data(contentsOf: url) {
                            self.mImgAvatar.image = UIImage(data: data)
                        }
                    }
                }
                    
                else {
                    self.mImgAvatar.image = UIImage(named: "user_placeholder")
                }
            }
        }
        
        self.getProducts()
        
//        self.tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
//        if tap != nil {
//            view.addGestureRecognizer(tap!)
//        }
        
        self.mBirthday.addTarget(self, action: #selector(birthdayChange), for: .editingChanged)
    }
    
    @objc func birthdayChange() {
        if let birthDay = Int(self.mBirthday.text!) {
            if birthDay > 1900 && birthDay <= Calendar.current.component(.year, from: Date()) {
                self.mBirthday.errorMessage = ""
                self.checkBirthDay = true
            } else {
                self.mBirthday.errorMessage = "* Năm sinh"
                self.checkBirthDay = false
            }
        }
    }
    
    func setupDelegateTextFiled() {
        self.mUserName.delegate = self
        self.mPhoneNumber.delegate = self
        self.mAddress.delegate = self
        self.mBirthday.delegate = self
        self.mGender.delegate = self
        
        self.mReason.delegate = self
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func chooseMale(_ sender: Any) {
        self.mRadioFemale.tag = 0
        self.mRadioMale.tag = 1
        self.mGender.errorMessage = ""
        self.checkStatusGender()
    }
    
    @IBAction func chooseFemale(_ sender: Any) {
        self.mRadioMale.tag = 0
        self.mRadioFemale.tag = 1
        self.mGender.errorMessage = ""
        self.checkStatusGender()
    }
    
//    @IBAction func chooseGender(_ sender: Any) {
//        if mRadioMale.tag == 1 {
//            self.mRadioMale.tag = 0
//            self.mRadioFemale.tag = 1
//            self.mGender.errorMessage = ""
//        }
//        else {
//            self.mRadioFemale.tag = 0
//            self.mRadioMale.tag = 1
//            self.mGender.errorMessage = ""
//        }
//
//        self.checkStatusGender()
//    }
    
    @IBAction func openPicker(_ sender: AnyObject) {
        self.mPickerView.reloadAllComponents()
        self.mPickerbox.isHidden = false
    }
    
    @IBAction func closePicker(_ sender: Any) {
        self.mPickerbox.isHidden = true
    }
    
    private func setAvatar() {
        self.mViewAvatar.layer.cornerRadius = 35
        self.mViewAvatar.layer.borderColor = UIColor(red:0/255, green:150/255, blue:255/255, alpha: 1).cgColor
        self.mViewAvatar.layer.borderWidth = 2
        
        self.mImgAvatar.layer.masksToBounds = true
        self.mImgAvatar.layer.cornerRadius = 30
    }
    
    private func checkStatusGender() {
        if mRadioMale.tag == 1 {
            self.mRadioFemale.tag = 0
            self.mRadioMale.setImage(UIImage(named:"radio-button"), for: .normal)
            self.mRadioFemale.setImage(UIImage(named:"untick"), for: .normal)
            self.mGender.text = "Nam"
        }
        if mRadioFemale.tag == 1 {
            self.mRadioMale.tag = 0
            self.mRadioFemale.setImage(UIImage(named:"radio-button"), for: .normal)
            self.mRadioMale.setImage(UIImage(named:"untick"), for: .normal)
            self.mGender.text = "Nữ"
        }
        if mRadioMale.tag == 0, mRadioFemale.tag == 0 {
            self.mRadioFemale.setImage(UIImage(named:"untick"), for: .normal)
            self.mRadioMale.setImage(UIImage(named:"untick"), for: .normal)
            self.mGender.text = ""
        }
    }
    
    private func cleanTextField() {
        self.saveDataTyped()
        self.mUserName.text = ""
        self.mPhoneNumber.text = ""
        self.mAddress.text = ""
        self.mBirthday.text = ""
        self.mRadioMale.tag = 0
        self.mRadioFemale.tag = 0
        self.checkStatusGender()
        self.mReason.text = ""
    }
    
    private func saveDataTyped() {
        self.InfoUserTyped = [self.mUserName.text ?? "",
                              self.mPhoneNumber.text ?? "",
                              self.mAddress.text ?? "",
                              self.mBirthday.text ?? "",
                              self.mReason.text ?? "",
                              self.mGender.text ?? ""]
    }
    
    private func revertDataType() {
        self.mUserName.text = InfoUserTyped[0]
        self.mPhoneNumber.text = InfoUserTyped[1]
        self.mAddress.text = InfoUserTyped[2]
        self.mBirthday.text = InfoUserTyped[3]
        self.mReason.text = InfoUserTyped[4]
        
        if InfoUserTyped[5] == "Nam" {
            self.mRadioMale.tag = 1
        }
        if InfoUserTyped[5] == "Nữ" {
            self.mRadioFemale.tag = 1
        }
        if InfoUserTyped[5] == ""  {
            self.mRadioMale.tag = 0
            self.mRadioFemale.tag = 0
        }
        self.checkStatusGender()
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let should = super.gestureRecognizer(gestureRecognizer, shouldReceive: touch)
        
        if should != true, let view = touch.view {
            if !view.isFirstResponder {
                self.view.endEditing(true)
            }
        }
        
        return should
    }
    
    private func getProducts() {
        NetworkManager.shareInstance.apiGetUserProfile() { (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                self.nopcustomer_id = data.arrayValue[0]["nopcustomer_id"].intValue
                if data.arrayValue[0]["email"].stringValue != "" {
                    self.customer_email = data.arrayValue[0]["email"].stringValue
                }
                
                if data.arrayValue[0]["last_name"].stringValue != "" {
                    self.last_name = data.arrayValue[0]["last_name"].stringValue
                }
                else {
                    self.last_name = data.arrayValue[0]["first_name"].stringValue
                }
                
                if data.arrayValue[0]["location"].stringValue != "" {
                    self.location = data.arrayValue[0]["location"].stringValue
                }
            }
        }
        
        if let id_product = self.dataAppointment.last?.key {
            NetworkManager.shareInstance.apiGetProducts(id_product: id_product, callBack: { (data, message, isSuccess) in
                if isSuccess {
                    let data = data as! JSON
                    self.dataProducts.removeAll()
                    self.dataProducts = [[data.dictionaryValue][0]["products"]![0]["attributes"]]
                }
            })
        }
    }

    
    @IBAction func sendAppointment(_ sender: Any) {
        
        let customer_name = self.mUserName.text!
        let customer_phone = self.mPhoneNumber.text!
        let customer_address = self.mAddress.text!
        let customer_birthday = self.mBirthday.text!
        let bill_address = self.dataAppointment.first?.value
//        var addressAppointment: Int = 243
        let person_appoint = self.mbtnPerson.titleLabel?.text
        let reason_appoint = self.mReason.text

        var customer_gender = "Nữ"
        if self.mRadioMale.tag == 1{
            customer_gender = "Nam"
        }
        self.arrDic.removeAll()
        let id_product = self.dataAppointment.last?.key

        if customer_name.isEmpty == false, customer_phone.isEmpty == false, customer_address.isEmpty == false,
            reason_appoint?.isEmpty == false, customer_birthday.isEmpty == false, self.checkBirthDay == true {
            
            for i in 0..<self.dataProducts[0].count {
                var item = dataProducts[0][i]
                // khung gio
                if item["product_attribute_id"] == 58 {
                    let attr : Dictionary = ["id": self.dataAppointment[3].key, "value" : self.dataAppointment[3].value] as Dictionary
                    self.arrDic.append(attr)
                }
                
                // dat lich cho
                else if item["product_attribute_id"] == 68 {
                    for j in 0..<item["attribute_values"].count {
                        var str = item["attribute_values"][j]["name"].stringValue
                        str = self.subStringAppointment(str)
                        if str == person_appoint! {
                            let attr : Dictionary = ["id": item["id"].intValue, "value" : item["attribute_values"][j]["id"].stringValue] as Dictionary
                            self.arrDic.append(attr)
                        }
                    }
                }
                
                    // so dien thoai
                else if item["product_attribute_id"] == 60 {
                    let attr : Dictionary = ["id": item["id"].intValue, "value" : customer_phone] as Dictionary
                    self.arrDic.append(attr)
                }
                
                    // Gioi tinh
                else if item["product_attribute_id"] == 11 {
                    for j in 0..<item["attribute_values"].count {
                        if item["attribute_values"][j]["name"].stringValue == customer_gender {
                            let attr : Dictionary = ["id": item["id"].intValue, "value" : item["attribute_values"][j]["id"].stringValue] as Dictionary
                            self.arrDic.append(attr)
                        }
                    }
                }
                
                    //nam sinh
                else if item["product_attribute_id"] == 12 {
                    if customer_birthday != "" {
                        let attr : Dictionary = ["id": item["id"].intValue, "value" : customer_birthday] as Dictionary
                        self.arrDic.append(attr)
                    }
                }
                
                    //ly do kham
                else if item["product_attribute_id"] == 67 {
                    if customer_birthday != "" {
                        let attr : Dictionary = ["id": item["id"].intValue, "value" : reason_appoint!] as Dictionary
                        self.arrDic.append(attr)
                    }
                }
                    
                    // noi kham
                else if item["product_attribute_id"] == 64 {
                    for j in 0..<item["attribute_values"].count {
                        var str = item["attribute_values"][j]["name"].stringValue
                        str = self.subStringAppointment(str)
                        if str == (self.dataAppointment.first?.value)! {
                            let attr : Dictionary = ["id": item["id"].intValue, "value" : item["attribute_values"][j]["id"].stringValue] as Dictionary
                            self.arrDic.append(attr)
                        }
                    }
                }
                
                    //  trieu chung
                else if item["product_attribute_id"] == 66 {
                    let attr : Dictionary = ["id": item["id"].intValue, "value" : reason_appoint!] as Dictionary
                    self.arrDic.append(attr)
                }
                    
                    // dia chi
                else if item["product_attribute_id"] == 62 {
                    let attr : Dictionary = ["id": item["id"].intValue, "value" : customer_address] as Dictionary
                    self.arrDic.append(attr)
                }
                    
                   //  ngay kham
                else if item["product_attribute_id"] == 57 {
                    
                    let dateAppoint = self.dateFormatter.date(from: self.dataAppointment[1].value)
                    self.dateFormatter.dateFormat = "dd-MM-yyyy"
                    
                    let dateTransStr = self.dateFormatter.string(from: dateAppoint ?? Date())
                    
                    
                    let attr : Dictionary = ["id": item["id"].intValue, "value" : dateTransStr] as Dictionary
                    self.arrDic.append(attr)
                }
                else {
                    print("end")
                }
            }
            
            self.showHud()
            NetworkManager.shareInstance.apiPostShoppingCartItemProduct(arrDic: self.arrDic, product_id: id_product!, user_id: nopcustomer_id ?? 0) {
                (data, message, isSuccess) in
                if isSuccess {
                    
                    self.dateFormatter.dateFormat = "dd-MM-yyyy"
                    let dateCreated = self.dateFormatter.string(from: Date())
                    
                    NetworkManager.shareInstance.apiPostOrders(city: self.location, email: self.customer_email, ship_address: customer_address, first_name: customer_name, last_name: self.last_name, phone_number: customer_phone, zip_postal_code: "7546547997", bill_address: bill_address!, created_on_utc: dateCreated, customer_id: self.nopcustomer_id ?? 0, customer_gender: customer_gender ?? "", customer_birthday: customer_birthday, callBack: {
                        (data, message, isSuccess) in
                        if isSuccess {
                            self.order_id = (data as! JSON)["orders"][0]["id"].intValue

                            var order_price = self.dataAppointment[2].value
                            if let endIndex = order_price.range(of: ",")?.lowerBound {
                                order_price = (String(order_price[..<endIndex]))
                            }
                            order_price = order_price.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil)

//                            let ref = arc4random_uniform(90000000) + 10000000;

                            let data = VNPayOrderInfo(
                                amount: "\(Int(order_price)! * 100)",
                                orderInfo: "\(self.order_id)",
                                ref: "\(self.order_id + 100000000)"
                            )
                            if !self.vnpay.vnpay(makeOrderForData: data) {
                                self.alert("Khởi tạo thanh toán thất bại")
                            }
                        }
                    })
                } else {
                    self.prompt("Thông báo", message: "Đã xảy ra lỗi, vui lòng liên hệ với hệ với chúng tôi để được trợ giúp", okHandler: { (alert) in
                        let activityController = UIActivityViewController(activityItems: [""], applicationActivities: nil)
                        activityController.setValue("Phản ảnh lại với chúng tôi", forKey: "subject")
                        self.present(activityController, animated: true, completion: nil)
                    }, cancelHandler: { (alert) in
                        self.hideHud()
                    })
                }
            }
            
            DispatchQueue.main.async {
                self.hideHud()
            }

        }else {

            if (self.mUserName.text?.isEmpty)! {
                self.mUserName.errorMessage = "* Họ tên"
            }
            if (self.mAddress.text?.isEmpty)! {
                self.mAddress.errorMessage = "* Địa chỉ"
            }
            if (self.mPhoneNumber.text?.isEmpty)! {
                self.mPhoneNumber.errorMessage = "* Số điện thoại"
            }
            if (self.mGender.text?.isEmpty)! {
                self.mGender.errorMessage = "* Giới tính"
            }
            if (self.mBirthday.text?.isEmpty)! || (self.checkBirthDay != true){
                self.mBirthday.errorMessage = "* Năm sinh"
            }
            if (self.mReason.text.isEmpty) {
                self.mReasonTitleLbl.textColor = UIColor.red
                self.mReasonTitleLbl.text = "* Lý do khám"
            }
            
            let alert = UIAlertController(title: "Cảnh báo", message: "Bạn cần nhập đủ thông tin", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                
                
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        let realm = try! Realm()
        try! realm.write {
            let order = DatabaseManager.sharedManager.getLocalUserInfo()
            order?.hoTen = self.mUserName.text!
            order?.tenBV = self.dataAppointment.first!.value
            order?.diaChi = self.mAddress.text!
            order?.noiKham = self.dataAppointment.first!.value
            order?.namSinh = self.mBirthday.text!
            order?.sdt = self.mPhoneNumber.text!
            order?.ngayKham = self.dataAppointment[1].value
            order?.khungGio = self.dataAppointment[3].value
            order?.idPro = self.dataAppointment.last!.key
            order?.datLich = self.mbtnPerson.titleLabel!.text!
            order?.trieuChung = self.mReason.text
            order?.gioiTinh = customer_gender
            order?.type = "datlichbacsi"
            order?.ngayDat = ""
            order?.kham = ""
            realm.add(order!)
        }
        
    }
}

extension AppointmentDoctorDetailViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

         return dataPersonAppointment.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return dataPersonAppointment[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.mbtnPerson.setTitle(dataPersonAppointment[row], for: .normal)
        
        if row == 1 {
            self.cleanTextField()
        }
        else {
            self.revertDataType()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
}

extension AppointmentDoctorDetailViewController: VNPayDelegate {
    func vnpay(_ vnpay: VNPay, dismiss viewController: VNPayViewController, withTransactionResult transactionIsSuccess: Bool) {
        self.showHud()
        viewController.dismiss(animated: true) {
            var vc: VNOrderResultViewController
            if transactionIsSuccess {
                vc = self.storyboard?.instantiateViewController(withIdentifier: "VNOrderResultViewController") as! VNOrderResultViewController
            } else {
                vc = self.storyboard?.instantiateViewController(withIdentifier: "VNOrderResultViewControllerfail") as! VNOrderResultViewController
            }
            
            vc.order_id = self.order_id + 1000000
            vc.order_id_Note = self.order_id
            vc.presenter = self
            self.present(vc, animated: true)
            self.hideHud()
        }
    }
    
    func vnpay(_ vnpay: VNPay, present vnPayViewController: VNPayViewController) {
        self.present(vnPayViewController) {
            
        }
    }
    
    func vnpay(_ vnpay: VNPay, present WKWebViewController: WKWebView) {
        
    }
    
    
}

extension AppointmentDoctorDetailViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.mAddress {
            self.mAddress.errorMessage = ""
        }
        if textField == self.mUserName {
            self.mUserName.errorMessage = ""
        }
        if textField == self.mPhoneNumber {
            self.mPhoneNumber.errorMessage = ""
        }
        if textField == self.mBirthday {
            self.mBirthday.errorMessage = ""
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.currentTextField = textField
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
}

extension AppointmentDoctorDetailViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == self.mReason {
            self.mReasonTitleLbl.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
            self.mReasonTitleLbl.text = "Lý do khám"
        }
    }

    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.currentTextView = textView
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        self.currentTextView = nil
        return true
    }
}

extension AppointmentDoctorDetailViewController {
    private func registerForNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = self.mScrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            self.mScrollView.contentInset = contentInset
            self.mScrollView.scrollIndicatorInsets = contentInset
            if let rect = curTextField.superview?.convert(curTextField.frame, to: curTextField.superview?.superview) {
                let test = view.bounds.height - (rect.size.height + rect.origin.y + self.mScrollView.contentInset.top) - 60.0
                let different = keyboardFrame.size.height - test
                if different > 0 {
                    self.mScrollView.contentOffset.y = different - self.mScrollView.contentInset.top
                }
            }
        } else if let curTextView = currentTextView {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = mScrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            mScrollView.contentInset = contentInset
            mScrollView.scrollIndicatorInsets = contentInset
            if let rect = curTextView.superview?.convert(curTextView.frame, to: curTextView.superview?.superview) {
                let test = view.bounds.height - (rect.size.height + rect.origin.y + mScrollView.contentInset.top) - 60.0
                let different = keyboardFrame.size.height - test
                if different > 0 {
                    self.mScrollView.contentOffset.y = different - self.mScrollView.contentInset.top
                }
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = self.mScrollView.contentInset
        contentInset.bottom = 0
        self.mScrollView.contentInset = contentInset
        self.mScrollView.scrollIndicatorInsets = contentInset
    }
}
