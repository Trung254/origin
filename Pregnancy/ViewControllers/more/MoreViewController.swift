//
//  MoreViewController.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/11/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class MoreViewController: MenuCardViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addPadLockButton()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        
        super.viewWillAppear(animated)
        showNavigation()
    }
    
    fileprivate func showNavigation() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
    }
    
    // setup menu items
    override func setupMenuItems() {

        let name = ["Gói khám", "Đặt lịch khám", "Danh bạ điện thoại", "Tên em bé", "Mua sắm", "Đồ dùng y tế"]
        let nameImage = ["Other Apps", "Contractions", "Phone", "Baby names", "Shopping", "Hospital Bag"]
        for i in (0...name.count - 1) {
                menuItems.append(MenuItem.init(name: name[i], imageName: nameImage[i] ))
        }
    }
    
    override func getViewTitle() -> String {
        return "Xem thêm"
    }
    
    override func backgroundImageName() -> String {
        return "more-background"
    }
    
    override func segueForMenuItemAtIndex(_ index: Int) -> String {
        var segue_id = ""
        switch index {
        case 0:
            segue_id = "more_to_otherApp"
//            alert("Chức năng đang xây dựng")
            break
        case 1:
            if (DatabaseManager.sharedManager.isGuest()) {
                
                self.prompt("Thông báo", message: "Bạn cần phải đăng nhập", okTitle: "Đăng nhập", okHandler: { (action) in
                    DatabaseManager.sharedManager.logout {}
                }, cancelTitle: "Thoát") { (action) in
                    
                }
//                self.prompt("Thông báo", message: "Bạn cần phải đăng nhập", okHandler: { (alert) in
//                    DatabaseManager.sharedManager.logout {}
//                }) { (alertAction) in
////                    self.navigationController?.popViewController(animated: true)
//                }
            } else {
                
                let actionSheet = UIAlertController(title: "Tuỳ chọn", message: "", preferredStyle: .actionSheet)
                
                actionSheet.addAction(UIAlertAction(title: "Đặt lịch  ", style: .default, handler: { (action) -> Void in
                    self.performSegue(withIdentifier: "more_to_appointment", sender: self)
                }))
                
                actionSheet.addAction(UIAlertAction(title: "Đặt lịch với bác sỹ", style: .default, handler: { (action) -> Void in
                    self.performSegue(withIdentifier: "more_appointment_doctors", sender: self)
                }))
                
                actionSheet.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: nil))
                
                self.present(actionSheet, animated: true, completion: nil)
            }
            
//            alert("Chức năng đang xây dựng")
        case 2:
            segue_id = "more_to_phone_numbers"
            break
        case 3:
            segue_id = "more_to_babyName"
            break
        case 4:
//            segue_id = "more_to_shopping"
            alert("Chức năng đang xây dựng")
            break
        case 5:
            segue_id = "more_to_bag"
            break
//        case 5:
//            segue_id = "more_to_contractions"
//            break
        default:
            NSLog("Selected : \(index)")
        }
        return segue_id
    }
    
    override func handleLeftMenu() {
        self.showMainMenu()
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
