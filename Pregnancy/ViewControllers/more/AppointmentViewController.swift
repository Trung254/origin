//
//  AppointmentViewController.swift
//  Pregnancy
//
//  Created by dady on 2/25/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import FontAwesome_swift
import SwiftyJSON
import SkyFloatingLabelTextField
import RealmSwift

class AppointmentViewController: BaseViewController,PickerDateViewDelegate,
UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet weak var mViewAvatar: UIView!
    @IBOutlet weak var mImgAvatar: UIImageView!
    @IBOutlet weak var mIDDoctors: customTextField!
    @IBOutlet weak var mUserName: customTextField!
    @IBOutlet weak var mPhoneNumber: customTextField!
    @IBOutlet weak var mAddress: customTextField!
    @IBOutlet weak var mGender: customTextField!
    @IBOutlet weak var mBirthday: customTextField!
    @IBOutlet weak var mRadioMale: UIButton!
    @IBOutlet weak var mRadioFemale: UIButton!
    @IBOutlet weak var mPickerView: UIPickerView!
    @IBOutlet weak var mPickerbox: UIView!
    @IBOutlet weak var mbtnDate: customButton!
    @IBOutlet weak var mbtnAddress: UIButton!
    @IBOutlet weak var mSegment: UISegmentedControl!
    @IBOutlet weak var mbtnPerson: customButton!
    @IBOutlet weak var mbtnTime: customButton!
    @IBOutlet weak var mSympton: customTextView!
    @IBOutlet weak var mSymptomLbl: UILabel!
    @IBOutlet weak var mDatelbl: UILabel!
    @IBOutlet weak var mTimelbl: UILabel!
    @IBOutlet weak var mScrollView: UIScrollView!
    
    
    private var currentTextField:UITextField?
    private var currentTextView:UITextView?
    
    
    var dataPersonAppointment: Array<String> = ["Tôi", "Người khác"]
    var dataAdrAppointment: [JSON] = []
    var dataTimeSchedule: [JSON] = []
    var dataTimeFoursHours: [String] = []
    var atributes: [JSON] = []
    var atributesUpdate = Array<Dictionary<String, Any>>()
    var InfoUserTyped: Array<String> = []
    var checkBirthDay = false
    
    var user_id: Int = 0
    var dateAppointment = Date()
    let dateFormatter = DateFormatter()
    var last_name:String = ""
    var tag_button:Int = 1
    var nopcustomer_id: Int?
    var customer_email: String = "example@example.com"
    var location: String = ""
    var time_appoint_id: Int = 0
    var person_appoint_id = 245
    var tap: UITapGestureRecognizer?
    var product_id:Int = 0
    var curDate: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mIsShowRightButton = false
        self.mPickerbox.isHidden = true
        self.setAvatar()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.curDate = dateFormatter.string(from: Date())
        self.mbtnDate.setTitle("--Chọn ngày--", for: .normal)
        self.disableAutoCorrect()

        self.addDelegateTextFiled()
        self.registerForNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DatabaseManager.sharedManager.getUserInfo { (pregUser, mnessage, isSuccess) in
            if pregUser != nil {
                self.mUserName.text = pregUser?.first_name
                self.mPhoneNumber.text = pregUser?.phone
                self.user_id = pregUser?.id ?? 0

                if pregUser?.avatar != "" {
                    var avatar = NetworkManager.rootDomain
                    if pregUser!.avatar.hasPrefix("http") {
                        avatar = pregUser!.avatar
                    } else {
                        avatar.append(pregUser!.avatar)
                    }
                    
                    let url = URL(string: avatar)
                    let data = try? Data(contentsOf: url!)
                    if data?.isEmpty == false {
                        self.mImgAvatar.image = UIImage(data: data!)
                    }
                }
                    
                else {
                    self.mImgAvatar.image = UIImage(named: "user_placeholder")
                }
            }
        }
        
        self.getAllApi()
        self.mBirthday.addTarget(self, action: #selector(birthdayChange), for: .editingChanged)
    }
    
    @objc func birthdayChange() {
        if let birthDay = Int(self.mBirthday.text!) {
            if birthDay > 1900 && birthDay <= Calendar.current.component(.year, from: Date()) {
                self.mBirthday.errorMessage = ""
                self.checkBirthDay = true
            } else {
                self.mBirthday.errorMessage = "Năm sinh"
                self.checkBirthDay = false
            }
        }
    }
    
    
    func addDelegateTextFiled() {
        self.mIDDoctors.delegate = self
        self.mUserName.delegate = self
        self.mPhoneNumber.delegate = self
        self.mAddress.delegate = self
        self.mBirthday.delegate = self
        self.mGender.delegate = self
        self.mSympton.delegate = self
    }
    
    func disableAutoCorrect() {
        self.mIDDoctors.autocorrectionType = .no
        self.mUserName.autocorrectionType = .no
        self.mPhoneNumber.autocorrectionType = .no
        self.mAddress.autocorrectionType = .no
        self.mGender.autocorrectionType = .no
        self.mBirthday.autocorrectionType = .no
        self.mSympton.autocorrectionType = .no
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    @IBAction func chooseMale(_ sender: Any) {
        self.mRadioFemale.tag = 0
        self.mRadioMale.tag = 1
        self.mGender.errorMessage = ""
        self.checkStatusGender()
    }
    @IBAction func chooseFemale(_ sender: Any) {
        self.mRadioMale.tag = 0
        self.mRadioFemale.tag = 1
        self.mGender.errorMessage = ""
        self.checkStatusGender()
    }
    
    @IBAction func openPicker(_ sender: AnyObject) {
        self.checkTimebeforeFourHours()
        self.tag_button = sender.tag
        self.mPickerView.reloadAllComponents()
        self.mPickerbox.isHidden = false
    }
    
    @IBAction func closePicker(_ sender: Any) {
        self.mPickerbox.isHidden = true
    }
    
    @IBAction func chooseDateAppointment(_ sender: Any) {
        let pickerDate = PickerDateView.initView()
        pickerDate.delegate = self
        self.navigationController?.view.addSubview(pickerDate)
        pickerDate.datePicker.locale = Locale.init(identifier: "vi")
        pickerDate.datePicker.minimumDate = Date()
    }
    
    @IBAction func sendAppointment(_ sender: Any) {
    
        let customer_doctor_id = self.mIDDoctors.text
        let customer_name = self.mUserName.text!
        let customer_phone = self.mPhoneNumber.text!
        let customer_address = self.mAddress.text!
        
        var customer_birthday = ""
        if self.checkBirthDay == true {
            if let mBirthday = self.mBirthday.text {
                customer_birthday = mBirthday
            }
        }
        
        var bill_address = self.mbtnAddress.titleLabel?.text
        let customer_symptom = self.mSympton.text
        let customer_person = self.mbtnPerson.titleLabel?.text
        let customer_gender = self.mGender.text
        var addressAppointment = "Khám tại viện"
        if mSegment.selectedSegmentIndex == 1 {
            addressAppointment = "Khám tại nhà"
            bill_address = customer_address
        }
        let addressHos = self.mbtnAddress.titleLabel?.text
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date_of_appoint = dateFormatter.string(from: dateAppointment)
        
        let customer_date = self.mbtnDate.titleLabel?.text
        let customer_time = self.mbtnTime.titleLabel?.text
        var id_schedule_time = 0
        
        if customer_name.isEmpty == false, customer_phone.isEmpty == false, customer_address.isEmpty == false, customer_symptom?.isEmpty == false , customer_birthday.isEmpty == false , customer_date != "--Chọn ngày--", customer_time != "Hết giờ khám" {
            
            let result = self.atributes.filter { $0["product_attribute_name"] == "Khung giờ" }
            for item in result[0]["attribute_values"] {
                if customer_date == item.1["name"].stringValue {
                    id_schedule_time = item.1["id"].intValue
                }
            }
            
            // append data atribute to arrDic
            for item in self.atributes {
    
                
                let id =  item["id"].intValue
                // dat lich tai
//                if item["product_attribute_name"] == "Đặt lịch tại nhà" {
//                    let result = item["attribute_values"].arrayValue.filter { $0["name"].stringValue == addressAppointment }
//                    let attr : Dictionary = ["id": id, "value" : result[0]["id"].intValue] as Dictionary
//                    self.atributesUpdate.append(attr)
//                }

                    // dat lich cho
                if item["product_attribute_name"] == "Đặt lịch cho" {
                    let result = item["attribute_values"].arrayValue.filter { $0["name"].stringValue == customer_person }
                    
                    let attr : Dictionary = ["id": id, "value" : result[0]["id"].intValue] as Dictionary
                    self.atributesUpdate.append(attr)
                }
                    
                    // Gioi tinh
//                else if item["product_attribute_name"] == "Giới tính" {
//                    let result = item["attribute_values"].arrayValue.filter { $0["name"].stringValue == customer_gender }
//
//                    let attr : Dictionary = ["id": id, "value" : result[0]["id"].intValue] as Dictionary
//                    self.atributesUpdate.append(attr)
//                }

                    // Khung giờ
                else if item["product_attribute_name"] == "Khung giờ" {
                    
                    let attr : Dictionary = ["id": id, "value" : id_schedule_time ] as Dictionary
                    self.atributesUpdate.append(attr)
                }
                    
                    // Noi kham
//                else if item["product_attribute_name"] == "Nơi khám" {
//                    let result = item["attribute_values"].arrayValue.filter { $0["name"].stringValue == addressHos }
//
//                    let attr : Dictionary = ["id": id, "value" : result[0]["id"].intValue] as Dictionary
//                    self.atributesUpdate.append(attr)
//                }
                    
                    //ma bac si
//                else if item["product_attribute_name"] == "Mã bác sĩ" {
//                    if customer_doctor_id != "" {
//                        let attr : Dictionary = ["id": id, "value" : customer_doctor_id] as Dictionary
//                        self.atributesUpdate.append(attr)
//                    }
//                }
                    
                    //ho ten
//                else if item["product_attribute_name"] == "Họ và tên" {
//                    let attr : Dictionary = ["id": id, "value" : customer_name] as Dictionary
//                    self.atributesUpdate.append(attr)
//                }

                    //nam sinh
//                else if item["product_attribute_name"] == "Năm sinh" {
//                    if customer_birthday != "" {
//                        let attr : Dictionary = ["id": id, "value" : customer_birthday] as Dictionary
//                        self.atributesUpdate.append(attr)
//                    }
//                }
                    
                    //so dien thoai
//                else if item["product_attribute_name"] == "Số điện thoại" {
//                    let attr : Dictionary = ["id": id, "value" : customer_phone] as Dictionary
//                    self.atributesUpdate.append(attr)
//                }
                    
                    //dia chi
//                else if item["product_attribute_name"] == "Địa chỉ" {
//                    let attr : Dictionary = ["id": id, "value" : customer_address] as Dictionary
//                    self.atributesUpdate.append(attr)
//                }

                    //trieu chung
                else if item["product_attribute_name"] == "Triệu chứng" {
                    let attr : Dictionary = ["id": id, "value" : customer_symptom!] as Dictionary
                    self.atributesUpdate.append(attr)
                }
                    
                    //  ngay kham
//                else if item["product_attribute_name"] == "Ngày khám" {
//                    let attr : Dictionary = ["id": id, "value" : date_of_appoint] as Dictionary
//                    self.atributesUpdate.append(attr)
//                }
                else {
                    print("end")
                }   
            }
            
            self.showHud()
            NetworkManager.shareInstance.apiPostShoppingCartItemProduct(arrDic: self.atributesUpdate, product_id: self.product_id, user_id: self.nopcustomer_id ?? 0) { (data, message, isSuccess) in
                if isSuccess {
                    NetworkManager.shareInstance.apiPostOrders(city: self.location, email: self.customer_email, ship_address: customer_address, first_name: customer_name, last_name: self.last_name, phone_number: customer_phone, zip_postal_code: "7546547997", bill_address: bill_address!, created_on_utc: "\(Date())", customer_id: self.nopcustomer_id ?? 0, customer_gender: customer_gender ?? "", customer_birthday: customer_birthday,callBack: {
                        (data, message, isSuccess) in
                        if isSuccess {
                            self.alert("Bạn đã đặt lịch thành công")
                            self.cleanTextField()
                            DispatchQueue.main.async {
                                self.hideHud()
                            }
                        }
                        else {
                            self.alert("Vui lòng thử lại")
                            DispatchQueue.main.async {
                                self.hideHud()
                            }
                        }
                    })
                } else {
                    self.prompt("Thông báo", message: "Đã xảy ra lỗi, vui lòng liên hệ với hệ với chúng tôi để được trợ giúp", okHandler: { (alert) in
                        let activityController = UIActivityViewController(activityItems: [""], applicationActivities: nil)
                        activityController.setValue("Phản ảnh lại với chúng tôi", forKey: "subject")
                        self.present(activityController, animated: true, completion: nil)
                    }, cancelHandler: { (alert) in
                        self.hideHud()
                    })
                }
            }
        
        }else {

            // Hiển thị thông báo cho người dùng biết cần nhập những chỗ nào
            if (self.mAddress.text?.isEmpty)! {
                self.mAddress.errorMessage = "* Địa chỉ"
            }
            if (self.mUserName.text?.isEmpty)! {
                self.mUserName.errorMessage = "* Họ tên"
            }
            if (self.mPhoneNumber.text?.isEmpty)! {
                self.mPhoneNumber.errorMessage = "* Số điện thoại"
            }
            if (self.mGender.text?.isEmpty)! {
                self.mGender.errorMessage = "* Giới tính"
            }
            if (self.mBirthday.text?.isEmpty)! {
                self.mBirthday.errorMessage = "* Năm sinh"
            }
            if (self.mSympton.text?.isEmpty)! {
                self.mSymptomLbl.textColor = .red
                self.mSymptomLbl.text = "  * Triệu chứng"
            }
            if customer_time == "Hết giờ khám" {
                self.mTimelbl.textColor = .red
            }
            if customer_date == "--Chọn ngày--" {
                self.mDatelbl.textColor = .red
            }
            
            let alert = UIAlertController(title: "Cảnh báo", message: "Bạn cần nhập đủ thông tin", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        let realm = try! Realm()
        try! realm.write {
            let order = DatabaseManager.sharedManager.getLocalUserInfo()
            order?.hoTen = self.mUserName.text!
            order?.tenBV = self.mbtnAddress.titleLabel!.text!
            order?.diaChi = self.mAddress.text!
            order?.noiKham = self.mbtnAddress.titleLabel!.text!
            order?.namSinh = self.mBirthday.text!
            order?.sdt = self.mPhoneNumber.text!
            order?.trieuChung = self.mSympton.text!
            order?.gioiTinh = self.mGender.text!
            order?.type = "datlich"
            realm.add(order!)
        }
    }
    
    func didSelectedDateString(dateString: String) {
        if curDate != dateString {
            self.dataTimeFoursHours.removeAll()
            self.mbtnTime.setTitle(self.dataTimeSchedule[0]["name"].stringValue, for: .normal)
            self.mbtnTime.isEnabled = true
        }
        else {
            self.checkTimebeforeFourHours()
        }
        
        self.mDatelbl.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.mbtnDate.setTitle(dateString, for: .normal)
        dateAppointment = self.convertStringToDate(dateString)
    }
    
    func doneTapped(_ sender: PickerDateView) {}
    
    private func checkTimebeforeFourHours() {
        self.dataTimeFoursHours.removeAll()
        
        let result = self.atributes.filter { $0["product_attribute_name"] == "Khung giờ" }
        for item in result[0]["attribute_values"] {
            let time_schedule = item.1["weight_adjustment"].floatValue
            
            dateFormatter.dateFormat = "HH"
            let hours = dateFormatter.string(from: Date())
            dateFormatter.dateFormat = "mm"
            let minutes = dateFormatter.string(from: Date())
            
            let cur_time = Float("\(hours).\(minutes)")
            
            if (time_schedule - cur_time!) >= 4 {
                self.dataTimeFoursHours.append(item.1["name"].stringValue)
                self.mTimelbl.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                self.mbtnTime.isEnabled = true
            } else {
                self.mbtnTime.setTitle("Hết giờ khám", for: .normal)
                self.mbtnTime.isEnabled = false
            }
        }
    }
    
    private func setAvatar() {
        self.mViewAvatar.layer.cornerRadius = 35
        self.mViewAvatar.layer.borderColor = UIColor(red:0/255, green:150/255, blue:255/255, alpha: 1).cgColor
        self.mViewAvatar.layer.borderWidth = 2
        
        self.mImgAvatar.layer.masksToBounds = true
        self.mImgAvatar.layer.cornerRadius = 30
    }
    
    private func checkStatusGender() {
        if mRadioMale.tag == 1 {
            self.mRadioFemale.tag = 0
            self.mRadioMale.setImage(UIImage(named:"radio-button"), for: .normal)
            self.mRadioFemale.setImage(UIImage(named:"untick"), for: .normal)
            self.mGender.text = "Nam"
        }
        if mRadioFemale.tag == 1 {
            self.mRadioMale.tag = 0
            self.mRadioFemale.setImage(UIImage(named:"radio-button"), for: .normal)
            self.mRadioMale.setImage(UIImage(named:"untick"), for: .normal)
            self.mGender.text = "Nữ"
        }
        if mRadioMale.tag == 0, mRadioFemale.tag == 0 {
            self.mRadioFemale.setImage(UIImage(named:"untick"), for: .normal)
            self.mRadioMale.setImage(UIImage(named:"untick"), for: .normal)
            self.mGender.text = ""
        }
    }
    
    private func getAllApi() {
        
        self.showHud()
        
        NetworkManager.shareInstance.apiGetUserProfile() { (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                self.nopcustomer_id = data.arrayValue[0]["nopcustomer_id"].intValue
                if data.arrayValue[0]["email"].stringValue != "" {
                    self.customer_email = data.arrayValue[0]["email"].stringValue
                }
                
                if data.arrayValue[0]["last_name"].stringValue != "" {
                    self.last_name = data.arrayValue[0]["last_name"].stringValue
                }
                else {
                    self.last_name = data.arrayValue[0]["first_name"].stringValue
                }
                
                if data.arrayValue[0]["location"].stringValue != "" {
                    self.location = data.arrayValue[0]["location"].stringValue
                }
            }
            
            DispatchQueue.main.async {
                self.mPickerView.reloadAllComponents()
                self.hideHud()
            }
        }
        
        // lấy thông tin nơi khám bênh và set lịch khám trước 4h
        self.showHud()
        NetworkManager.shareInstance.apiGetCategories { (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                let result =  data["categories"].arrayValue.filter({ $0["name"] == "Đặt lịch"})
                let category_id = result[0]["id"].stringValue
                
                NetworkManager.shareInstance.apiGetProductByCategory(categoryId: category_id, callBack: { (data, message, isSuccess) in
                    if isSuccess {
                        let data = data as! JSON
                        self.product_id = data["Products"][0]["Id"].intValue
                        
                        NetworkManager.shareInstance.apiGetProducts(id_product: self.product_id, callBack: { (data, message, isSuccess) in
                            if isSuccess {
                                let data = data as! JSON
                                let allProducts = data["products"][0]
                                self.atributes = (allProducts["attributes"]).arrayValue
                                let results = self.atributes.filter { $0["product_attribute_name"] == "Nơi khám" }
                                
                                self.dataAdrAppointment = results[0]["attribute_values"].arrayValue
                                self.mbtnAddress.setTitle(self.dataAdrAppointment[0]["name"].stringValue, for: .normal)
                                
                                let timeSchedule = self.atributes.filter { $0["product_attribute_name"] == "Khung giờ" }
                                
                                self.dataTimeSchedule = timeSchedule[0]["attribute_values"].arrayValue
                                
                                self.mbtnTime.setTitle(self.dataTimeSchedule[0]["name"].stringValue, for: .normal)
                            }
                            
                            DispatchQueue.main.async {
                                self.mPickerView.reloadAllComponents()
                                self.hideHud()
                            }
                        })
                    }
                })
            } else {
                DispatchQueue.main.async {
                    self.alert("")
                    self.hideHud()
                }
            }
        }
    }
    
    private func cleanTextField() {
        self.saveDataTyped()
        self.mIDDoctors.text = ""
        self.mUserName.text = ""
        self.mPhoneNumber.text = ""
        self.mAddress.text = ""
        self.mBirthday.text = ""
        self.mSympton.text = ""
        self.mRadioMale.tag = 0
        self.mRadioFemale.tag = 0
        self.checkStatusGender()
    }
    
    private func saveDataTyped() {
        self.InfoUserTyped = [self.mIDDoctors.text ?? "",
                              self.mUserName.text ?? "",
                              self.mPhoneNumber.text ?? "",
                              self.mAddress.text ?? "",
                              self.mBirthday.text ?? "",
                              self.mSympton.text ?? "",
                              self.mGender.text ?? ""]
    }
    
    private func revertDataType() {
        self.mIDDoctors.text = InfoUserTyped[0]
        self.mUserName.text = InfoUserTyped[1]
        self.mPhoneNumber.text = InfoUserTyped[2]
        self.mAddress.text = InfoUserTyped[3]
        self.mBirthday.text = InfoUserTyped[4]
        self.mSympton.text = InfoUserTyped[5]
        
        if InfoUserTyped[6] == "Nam" {
            self.mRadioMale.tag = 1
        }
        if InfoUserTyped[6] == "Nữ" {
            self.mRadioFemale.tag = 1
        }
        if InfoUserTyped[6] == ""  {
            self.mRadioMale.tag = 0
            self.mRadioFemale.tag = 0
        }
        self.checkStatusGender()
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let should = super.gestureRecognizer(gestureRecognizer, shouldReceive: touch)
        
        if should != true, let view = touch.view {
            if !view.isFirstResponder {
                self.view.endEditing(true)
            }
        }
        
        return should
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if tag_button == 1 {
            return dataPersonAppointment.count
        }
        else if tag_button == 2 {
            return dataAdrAppointment.count
        }
        else {
            if dataTimeFoursHours.count != 0 {
                return dataTimeFoursHours.count
            }
            else {
                return dataTimeSchedule.count
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if tag_button == 1 {
            return dataPersonAppointment[row]
        }
        else if tag_button == 2 {
            return dataAdrAppointment[row]["name"].stringValue
        }
        else {
            if dataTimeFoursHours.count != 0 {
                 return dataTimeFoursHours[row]
            }else {
                 return dataTimeSchedule[row]["name"].stringValue
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if tag_button == 1 {
            self.mbtnPerson.setTitle(dataPersonAppointment[row], for: .normal)
            
            if row == 1 {
                self.cleanTextField()
            }
            else {
                self.revertDataType()
            }
        }
        else if tag_button == 2{
            self.mbtnAddress.setTitle(dataAdrAppointment[row]["name"].stringValue, for: .normal)
        }
        else {
            if dataTimeFoursHours.count != 0 {
                self.mbtnTime.setTitle(dataTimeFoursHours[row], for: .normal)
            }else {
                self.mbtnTime.setTitle(dataTimeSchedule[row]["name"].stringValue, for: .normal)
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
}

class customTextField: SkyFloatingLabelTextField{
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit(){
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

class customTextView: UITextView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.lightGray.cgColor
        layer.cornerRadius = 7.0
        clipsToBounds = true
    }
}

class customButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.lightGray.cgColor
        layer.cornerRadius = 7.0
        clipsToBounds = true
    }
}

extension AppointmentViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.mAddress {
            self.mAddress.errorMessage = ""
        }
        if textField == self.mUserName {
            self.mUserName.errorMessage = ""
        }
        if textField == self.mPhoneNumber {
            self.mPhoneNumber.errorMessage = ""
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
}

extension AppointmentViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == self.mSympton {
            self.mSymptomLbl.textColor = #colorLiteral(red: 0.458770752, green: 0.4588538408, blue: 0.4587655067, alpha: 1)
            self.mSymptomLbl.text = "    Triệu chứng"
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        currentTextView = textView
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        currentTextView = nil
        return true
    }
}
extension AppointmentViewController {
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = self.mScrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            self.mScrollView.contentInset = contentInset
            self.mScrollView.scrollIndicatorInsets = contentInset
            if let rect = curTextField.superview?.convert(curTextField.frame, to: curTextField.superview?.superview) {
                let test = view.bounds.height - (rect.size.height + rect.origin.y + self.mScrollView.contentInset.top) - 60.0
                let different = keyboardFrame.size.height - test
                if different > 0 {
                    self.mScrollView.contentOffset.y = different - self.mScrollView.contentInset.top
                }
            }
        } else if let curTextView = currentTextView {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = mScrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            mScrollView.contentInset = contentInset
            mScrollView.scrollIndicatorInsets = contentInset
            if let rect = curTextView.superview?.convert(curTextView.frame, to: curTextView.superview?.superview) {
                let test = view.bounds.height - (rect.size.height + rect.origin.y + mScrollView.contentInset.top) - 60.0
                let different = keyboardFrame.size.height - test
                if different > 0 {
                    self.mScrollView.contentOffset.y = different - self.mScrollView.contentInset.top
                }
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = self.mScrollView.contentInset
        contentInset.bottom = 0
        self.mScrollView.contentInset = contentInset
        self.mScrollView.scrollIndicatorInsets = contentInset
    }
}

