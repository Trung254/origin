//
//  HospitalBagDetailViewController.swift
//  Pregnancy
//
//  Created by mai kim tai  on 1/10/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class BagDetailShareViewCell: UITableViewCell {
    @IBOutlet weak var mShare: UIButton!
}

class BagDetailListViewCell: UITableViewCell {
    @IBOutlet weak var mContentDetail: UILabel!
    @IBOutlet weak var mStatusImg: UIImageView!
}

class HospitalBagDetailViewController: BaseViewController {

    @IBOutlet weak var mtblBagDetail: UITableView!
    var tagBag = 0
    var curIndex = -1
    var dataItemBag: [JSON] = []
    var dataUserItemBag: [JSON] = []
    var userId = 0
    var typeBag = 1
    var yourListBag: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addBackgroundImage("more-background-blur.png")

        self.mtblBagDetail.tableFooterView = UIView()
        userId = DatabaseManager.sharedManager.getLocalUserInfo()?.id ?? 0
    }
    
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        
        var barItems = super.createRightBarButtonItems()
        
        let addButton = UIButton(type: .custom)
        addButton.setImage(UIImage(named: "add"), for: .normal)
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        addButton.addTarget(self, action: #selector(addButtonAction), for: .touchUpInside)
        let barButton2 = UIBarButtonItem(customView: addButton)
        
        barItems.insert(barButton2, at: 0)
        return barItems
    }
    
    @objc func addButtonAction() {
        performSegue(withIdentifier: "hospital_bag_to_listbagnew", sender: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
        self.loadDataHospital()
        self.checkConnection()
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "hospital_bag_to_listbagnew" {
            let vc = segue.destination as! AddNewItemBagViewController
            vc.tagBag = tagBag
        }
    }
    
    private func checkConnection() {
        if !Connectivity.isConnectedToInternet() {
            let alert = UIAlertController(title: "", message: "Có sự cố về mạng, vui lòng kiểm tra lại", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                //                DatabaseManager.sharedManager.logout {
                //
                //                }
                self.hideHud()
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            self.hideHud()
        }
    }
    @objc func applicationDidBecomeActive(_ notification: Notification) {
        self.checkConnection()
    }
    
    private func loadDataHospital() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllHospitalBagItems() {
            (data, message, isSuccess) in
            self.dataItemBag = []
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    if item["type"].intValue == self.tagBag && (item["custom_item_by_user_id"].intValue == self.userId || item["custom_item_by_user_id"].intValue == 0){
                        self.dataItemBag.append(item)
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.loadDataUserHospital()
            }
        }
    }
    
    private func loadDataUserHospital() {
        NetworkManager.shareInstance.apiGetAllUserHospitalBagItems() {
            (data, message, isSuccess) in
            self.dataUserItemBag = []
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    if item["user_id"].intValue == self.userId && self.userId != 0 {
                        self.dataUserItemBag.append(item)
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.mtblBagDetail.reloadData()
                self.hideHud()
            }
        }
    }
    
    private func pushUserHospital(hospitalBagItemId: Int) {
        NetworkManager.shareInstance.apiPostUserHospitalBagItems(hospitalBagItemId: hospitalBagItemId) {
            (data, message, isSuccess) in
            DispatchQueue.main.async {
                self.loadDataHospital()
            }
        }
    }
    
    private func deleteUserHopitalBag(_ hosId: Int) {
        NetworkManager.shareInstance.apiDeleteUserHospitalBagItems(hospitalBagItemId: hosId) {
            (data, message, isSuccess) in
            DispatchQueue.main.async {
                self.loadDataHospital()
            }
        }
    }
    
    private func deleteData(hospitalBagItemId: Int) {
        NetworkManager.shareInstance.apiDeleteHospitalBagItems(hospitalBagItemId: hospitalBagItemId) {
            (data, message, isSuccess) in
            DispatchQueue.main.async {
                self.loadDataHospital()
            }
        }
    }
    
    @objc func shareBags() {
        var shareText = "Danh sách bao gồm: \n"
        yourListBag.removeAll()
        for item in dataUserItemBag {
            for j in dataItemBag {
                if j["id"].intValue == item["hospital_bag_item_id"].intValue ,
                    j["type"].intValue == tagBag {
                    yourListBag.append(j["name"].stringValue)
                }
            }
        }
        
        if yourListBag.isEmpty == false {
            for item in yourListBag {
                shareText += "- \(item)\n"
            }
        } else {
            shareText += "Không có dữ liệu"
        }
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        vc.setValue("Danh sách đồ dùng của bạn", forKey: "Subject")
        present(vc, animated: true, completion: nil)
    }
}

extension HospitalBagDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dataItemBag.count + 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: "defaultCell")
        
        let index = indexPath.row
        
        if index == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "BagDetailShareViewCell", for: indexPath) as! BagDetailShareViewCell
            
            (cell as! BagDetailShareViewCell).mShare.addTarget(self, action: #selector(shareBags), for: .touchUpInside)
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: "BagDetailListViewCell", for: indexPath) as! BagDetailListViewCell
            cell.selectionStyle = .none
            
            (cell as! BagDetailListViewCell).mStatusImg.image = UIImage(named: "untick")
            (cell as! BagDetailListViewCell).mContentDetail.text = dataItemBag[index - 1]["name"].stringValue
            for item in dataUserItemBag {
                if dataItemBag[index - 1]["id"].intValue == item["hospital_bag_item_id"].intValue {
                    (cell as! BagDetailListViewCell).mStatusImg.image = UIImage(named: "tick-3")
                }
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
}


extension HospitalBagDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            var checkBag = false
            let hospitalbagId = dataItemBag[indexPath.row - 1]["id"].intValue
            for item in dataUserItemBag {
                if hospitalbagId == item["hospital_bag_item_id"].intValue {
                    checkBag = true
                }
            }
            if checkBag == true {
                self.deleteUserHopitalBag(hospitalbagId)
            }
            else {
                self.pushUserHospital(hospitalBagItemId: dataItemBag[indexPath.row - 1]["id"].intValue)
            }
        }
    }
}
