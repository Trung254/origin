//
//  HospitalBagViewController.swift
//  Pregnancy
//
//  Created by mai kim tai  on 1/10/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

struct HospitalBag {
    var image: String
    var name: String
    var value: Int
    
    init(img: String, name: String, value: Int) {
        self.image = img
        self.name  = name
        self.value = value
    }
}

struct listBagShare {
    var name: String
    var type: Int
}

class HospitalBagViewCell: UITableViewCell {
    @IBOutlet weak var mBagImg: UIImageView!
    @IBOutlet weak var mBagName: UILabel!
    @IBOutlet weak var mBagValue: UILabel!
}

class HospitalBagViewController: BaseViewController {

    @IBOutlet weak var mtblHospital: UITableView!
    var indexRow = -1
    var count: [Int] = [0,0,0]
    var dataItemBag: [JSON] = []
    var userId = 0
    var yourListBag = [listBagShare]()
    
    var listHosBag:[HospitalBag] = [HospitalBag(img: "woman",name: "Đồ dùng thiết yếu cho bé",value: 0),
                                    HospitalBag(img: "woman",name: "Đồ dùng chăm sóc sức khỏe em bé",value: 0),
                                    HospitalBag(img: "woman",name: "Đồ dùng chăm sóc sức khỏe mẹ",value: 0)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackgroundImage("more-background-blur.png")
        self.mtblHospital.tableFooterView = UIView()
        userId = DatabaseManager.sharedManager.getLocalUserInfo()?.id ?? 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadDataHospital()
        self.checkConnection()
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        var barItems = super.createRightBarButtonItems()
        
        let shareButton = UIButton(type: .custom)
//        shareButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 30)
        shareButton.setImage(UIImage(named: "share-1"), for: .normal)
//        let titleBtn = String.fontAwesomeIcon(name: .shareAlt)
//        shareButton.setTitle(titleBtn, for: .normal)
        shareButton.addTarget(self, action: #selector(shareBag), for: .touchUpInside)
        let barButton2 = UIBarButtonItem(customView: shareButton)
        
        barItems.insert(barButton2, at: 0)
        return barItems
    }
    
    @objc func shareBag() {
        var shareText = "Danh sách của tôi\n"

        var bagTypeFirst = "Đồ dùng thiết yếu cho bé: \n"
        var bagTypeSecond = "Đồ dùng chăm sóc sức khỏe em bé: \n"
        var bagTypeThird = "Đồ dùng chăm sóc sức khỏe mẹ: \n"
        if yourListBag.isEmpty == false {
            for item in yourListBag {
                if item.type == 1{
                    bagTypeFirst += "- \(item.name)\n"
                } else if item.type == 2{
                    bagTypeSecond += "- \(item.name)\n"
                } else {
                    bagTypeThird += "- \(item.name)\n"
                }
            }
            
            if bagTypeFirst != "Đồ dùng thiết yếu cho bé: \n" {
                shareText += bagTypeFirst
            }
            if bagTypeSecond != "Đồ dùng chăm sóc sức khỏe em bé: \n" {
                shareText += bagTypeSecond
            }
            if bagTypeThird != "Đồ dùng chăm sóc sức khỏe mẹ: \n" {
                shareText += bagTypeThird
            }
        } else {
            shareText += "Không có dữ liệu"
        }
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        vc.setValue("Danh sách đồ dùng y tế", forKey: "Subject")
        present(vc, animated: true, completion: nil)
    }
    
    private func checkConnection() {
        if !Connectivity.isConnectedToInternet() {
            let alert = UIAlertController(title: "", message: "Có sự cố về mạng, vui lòng kiểm tra lại", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
//                DatabaseManager.sharedManager.logout {
//
//                }
                self.hideHud()
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else {
              self.hideHud()
        }
    }
    @objc func applicationDidBecomeActive(_ notification: Notification) {
        self.checkConnection()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "hospital_bag_to_listbag" {
            let vc = segue.destination as! HospitalBagDetailViewController
            vc.tagBag = self.indexRow + 1
        }
    }
    
    private func loadDataHospital() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllHospitalBagItems() {
            (data, message, isSuccess) in
            self.dataItemBag = []
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    if item["custom_item_by_user_id"].intValue == 0 || item["custom_item_by_user_id"].intValue == self.userId{
                        self.dataItemBag.append(item)
                    }
                }
                
                DispatchQueue.main.async {
                    self.loadDataUserHospital()
                }
            }
        }
    }
    
    private func loadDataUserHospital() {
        NetworkManager.shareInstance.apiGetAllUserHospitalBagItems() {
            (data, message, isSuccess) in
            self.count = [0,0,0]
            self.yourListBag.removeAll()
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    for j in self.dataItemBag {
                        if item["hospital_bag_item_id"].intValue == j["id"].intValue {
                            if j["type"].intValue == 1 {
                                self.count[0] += 1
                                self.yourListBag.append(listBagShare(name: j["name"].stringValue,type: 1))
                            }
                            else if j["type"].intValue == 2 {
                                self.count[1] += 1
                                self.yourListBag.append(listBagShare(name: j["name"].stringValue,type: 2))
                            }
                            else {
                                self.count[2] += 1
                                self.yourListBag.append(listBagShare(name: j["name"].stringValue,type: 3))
                            }
                        }
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.mtblHospital.reloadData()
                self.hideHud()
            }
        }
    }
}

extension HospitalBagViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listHosBag.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HospitalBagViewCell", for: indexPath) as! HospitalBagViewCell
        
        cell.selectionStyle = .none
        cell.mBagImg.image = UIImage(named: listHosBag[indexPath.row].image)
        cell.mBagName.text = listHosBag[indexPath.row].name
        cell.mBagValue.text = "\(count[indexPath.row])"
        
        return cell
    }
}

extension HospitalBagViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexRow = indexPath.row
        performSegue(withIdentifier: "hospital_bag_to_listbag", sender: self)
    }
}
