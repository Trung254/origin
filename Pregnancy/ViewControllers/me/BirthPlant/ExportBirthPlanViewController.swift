//
//  ExportBirthPlanViewController.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 3/14/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage

struct ItemCheck {
    var content = ""
    var idItem = 0
    var custom_item_by_user_id = 0
}

struct DataExport {
    var id = 0
    var title = ""
    var icon = ""
    var data = [ItemCheck]()
}

class ExportBirthPlanViewController: BaseViewController {
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var mExportTableView: UITableView!
    
    //    var dataExport = [DataExport]()
    var dataExportFilter = [DataExport]()
    
    
    //    var birthPlanData = [PregMyBirthPlanType]()
    //    var bithPlansCheckbox = [BirthPlansCheck]()
    //    var birthPlansItems = [BirthPlansItem]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let shareIcon = UIImage(named: "share")
        addIconToButton(btn: shareBtn , img: shareIcon!)
        shareBtn.setTitle("Xuất sang email", for: .normal)
        //        self.setTitle(title: "Kế hoạch sinh con của tôi")
        shareBtn.contentHorizontalAlignment = .left
        shareBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 60, bottom: 0, right: 0)
        
        self.title = "Kế hoạch sinh con của tôi"
        let navigationFont = UIFont.systemFont(ofSize: 19)
        let navigationColor =  UIColor.white
        var attrs = [NSAttributedString.Key: Any]();
        attrs[.font] = navigationFont
        attrs[.foregroundColor] = navigationColor
        self.navigationController?.navigationBar.titleTextAttributes = attrs
        
        self.setupTableView()
        //        self.setupData()
    }
    
    func addIconToButton(btn: UIButton, img : UIImage){
        let imgQuestionView = UIImageView(frame: CGRect(x: 15, y: 15, width: 30, height: 30))
        imgQuestionView.image = img
        btn.addSubview(imgQuestionView)
    }
    
    private func setupTableView() {
        self.mExportTableView.rowHeight = UITableView.automaticDimension
        self.mExportTableView.estimatedRowHeight = 80
        self.mExportTableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    
    //    func setupData() {
    //        for i in 0..<self.birthPlanData.count {
    //            var data = DataExport()
    //            data.id = self.birthPlanData[i].id
    //            data.title = self.birthPlanData[i].type
    //            data.icon = self.birthPlanData[i].type_icon
    //            self.dataExport.append(data)
    //        }
    //        for planItem in birthPlansItems {
    //            for planCheckbox in bithPlansCheckbox {
    //                if planItem.id == planCheckbox.birth_plan_item_id {
    //                    // Lấy ra thằng check
    //                    for i in 0..<self.dataExport.count {
    //                        if self.dataExport[i].id == planItem.my_birth_plan_type_id {
    //
    //                            var item = ItemCheck()
    //                            item.content = planItem.item_content
    //                            item.idItem = planItem.id
    //
    //                            self.dataExport[i].data.append(item)
    //                            break
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        self.dataExportFilter = self.dataExport.filter { (item) -> Bool in
    //            item.data.isEmpty == false
    //        }
    //
    //        self.mExportTableView.reloadData()
    //    }
    
    
    
    @IBAction func sharePlans(_ sender: Any) {
        print(self.dataExportFilter)
        var textShare = ""
        for itemShare in self.dataExportFilter {
            textShare += itemShare.title + "\n"
            for i in 0..<itemShare.data.count {
                textShare += "   - \(itemShare.data[i].content)\n"
            }
        }
        let activityController = UIActivityViewController(activityItems: [textShare], applicationActivities: nil)
        activityController.setValue("Kế hoạch sinh con", forKey: "subject")
        
        UIApplication.shared.keyWindow?.rootViewController?.present(activityController, animated: true, completion: nil)
        
    }
    
}
extension ExportBirthPlanViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerExportBirthPlanCell") as? HeaderExportBirthPlanCell
        
        let header = Bundle.main.loadNibNamed("HeaderExportBirthPlanCell", owner: self, options: nil)?.first as! HeaderExportBirthPlanCell
        
        //        let domain = NetworkManager.rootDomain
        let url = URL(string: NetworkManager.rootDomain + self.dataExportFilter[section].icon)
        header.mIconLbl.sd_setImage(with: url, completed: nil)
        header.mPlanTitle.text = self.dataExportFilter[section].title
        header.mPlanTitle.textColor = .black
        
        //        return header
        return header.contentView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataExportFilter.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataExportFilter[section].data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "exportBirthPlanCell", for: indexPath) as! ExportBirthPlanViewCell
        let section = indexPath.section
        let row = indexPath.row
        
        cell.mPlanLbl.text = self.dataExportFilter[section].data[row].content
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.showHud()
            let birthPlantItemId = self.dataExportFilter[indexPath.section].data[indexPath.row].idItem
            // xóa tick vào item birth plant
            NetworkManager.shareInstance.apiDeleteMyBirthPlans(itemId: birthPlantItemId) { (data, message, isSuccess) in
                if (isSuccess) {
                    // Check xem nếu đó là kế hoạch của người dùng -> xóa
                    if self.dataExportFilter[indexPath.section].data[indexPath.row].custom_item_by_user_id != 0 {
                        // Thành công xóa birth plant item
                        NetworkManager.shareInstance.apiDeleteMybirthPlanItems(myBirthPlanItemId: birthPlantItemId, callBack: { (data, message, isSuccess) in
                            if (isSuccess) {
                                self.dataExportFilter[indexPath.section].data.remove(at: indexPath.row)
                                if (self.dataExportFilter[indexPath.section].data.isEmpty) {
                                    self.dataExportFilter.remove(at: indexPath.section)
                                }
                                self.mExportTableView.reloadData()
                                self.hideHud()
                            } else {
                                self.hideHud()
                                self.alert("Không xóa được kế hoạch. Xin vui lòng thử lại")
                                self.mExportTableView.reloadData()
                            }
                        })
                    } else {
                        self.dataExportFilter[indexPath.section].data.remove(at: indexPath.row)
                        if (self.dataExportFilter[indexPath.section].data.isEmpty) {
                            self.dataExportFilter.remove(at: indexPath.section)
                        }
                        self.mExportTableView.reloadData()
                        self.hideHud()
                    }
                    
                    
                    
                    
                } else {
                    self.alert("Xóa kế hoạch không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                        self.mExportTableView.reloadData()
                        self.hideHud()
                    })
                }
            }
        }
    }
}

class ExportBirthPlanViewCell : UITableViewCell {
    @IBOutlet weak var mPlanLbl: UILabel!
    
}

