//
//  AddWishViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/20/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddWishViewController: BaseViewController {

    @IBOutlet weak var mTitleBtn: addWishButton!
    @IBOutlet weak var mAddWishTbv: UITableView!
    @IBOutlet weak var mViewBouncePicker: UIView!
    @IBOutlet weak var mPickerView: UIPickerView!
    @IBOutlet weak var mDoneBtn: UIButton!
    
    @IBOutlet weak var mTextFieldAddWish: UITextField!
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    
    
    var passedTitle = ""
    var birthPlanData : [PregMyBirthPlanType] = [PregMyBirthPlanType]()
    
    var wishDataPost = [String]() // Dữ liệu content để post lên
    var birthPlantTypeID : Int?
    var listIDPostSuccess = [Int]() // Khi bắn dữ liệu lên -> được list ID  của nó để tạo check
    var defaultSelectPicker = 0 // mặc định của pickerview
    
    
    var countPostItem = 0 //Đếm số lần post item lên, nếu post xong hết -> post plan
    var countPostPlan = 0 // Đếm số lần post plan lên, nếu xong hết -> pop lại view trước

    override func viewDidLoad() {
        super.viewDidLoad()
        self.birthPlanData.remove(at: 0)
        self.birthPlanData.remove(at: 0)
        self.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func setupView() {
        
//        self.mTitleBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        self.mTitleBtn.setTitle(self.passedTitle, for: .normal)
        self.mViewBouncePicker.isHidden = true
        //setup tableview
        self.mAddWishTbv.register(UINib.init(nibName: "AddWishTableViewCell", bundle: nil), forCellReuseIdentifier: "addWishTableViewCell")
        self.mAddWishTbv.delegate = self
        self.mAddWishTbv.dataSource = self
        self.mAddWishTbv.tableFooterView = UIView(frame: .zero)
        self.heightTableView.constant = 0
        self.mAddWishTbv.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.mAddWishTbv.isScrollEnabled = false
        self.mTitleBtn.titleLabel?.textColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        
        
        // Setup delegate pickerView
        self.mPickerView.delegate = self
        
        //Set delegate textFiled
        self.mTextFieldAddWish.delegate = self
        
        //check xem đang là row nào để hiển thị
        for i in 0..<self.birthPlanData.count {
            if self.birthPlanData[i].type == self.passedTitle {
                self.mPickerView.selectRow(i, inComponent: 0, animated: true)
                self.defaultSelectPicker = i
                self.birthPlantTypeID = self.birthPlanData[i].id
            }
        }
    }
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        
        let acceptButton = UIButton(type: .custom)
        acceptButton.setImage(UIImage(named: "checked"), for: .normal)
        acceptButton.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
        acceptButton.addTarget(self, action: #selector(doneAccept), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: acceptButton)
        return [barButton]
    }
    
    // Nhấn nút chấp nhận post lên server
    @objc func doneAccept() {
        self.insertToTabelView()
        self.view.endEditing(true)
        self.mViewBouncePicker.isHidden = true
        print(self.birthPlantTypeID)
        
        // Bắn lên tại đây
        if wishDataPost.isEmpty == false {
            // Có dữ liệu -> bắn lên
            self.countPostItem = 0
            for i in 0..<self.wishDataPost.count {
                self.showHud()
                self.postMyBirthPlantItem(birthPlanTypeID: self.birthPlantTypeID!, content: self.wishDataPost[i])
            }
            
            
        } else {
            // Hỏi xem muốn thoát không nếu ko có dữ liệu nào được lưu
            self.prompt("Thông báo", message: "Không có kế hoạch nào được thêm. Bạn có muốn thoát không?", okTitle: "Thoát", okHandler: { (action) in
                self.navigationController?.popViewController(animated: true)
            }, cancelTitle: "Không") { (action) in
                
            }
        }
    }
    
    // Thêm wish người dùng nhập vào tabelview
    func insertToTabelView() {
        if let textInsert = self.mTextFieldAddWish.text?.trimmingCharacters(in: .whitespaces), textInsert.isEmpty == false {
            self.wishDataPost.append(textInsert)
            
            let indexpath = IndexPath(row: self.wishDataPost.count - 1, section: 0)
            
            self.mAddWishTbv.beginUpdates()
            self.mAddWishTbv.insertRows(at: [indexpath], with: .automatic)
            self.mAddWishTbv.endUpdates()
            
            self.mTextFieldAddWish.text = ""
            
            self.heightTableView.constant = CGFloat(50 * self.wishDataPost.count)
            
            self.view.endEditing(true)
            self.view.layoutIfNeeded()
        }
        
    }
 
    
    @IBAction func click_Title_Btn(_ sender: Any) {
        //Kiểm tra xem nó hiện chưa, nếu chưa thì hiện
        if self.mViewBouncePicker.isHidden == true {
            self.mViewBouncePicker.isHidden = false
        }
        view.endEditing(true)
    }
    @IBAction func click_Done_Btn(_ sender: Any) {
        // Bấm nút xong -> ẩn combobox picker view
        self.mViewBouncePicker.isHidden = true
    }
}


// Hàm gọi networking post birthplant item và birthplant
extension AddWishViewController {
    func postMyBirthPlantItem(birthPlanTypeID: Int, content: String) {
        if let pregnancy = DatabaseManager.sharedManager.getLocalUserInfo() {
            NetworkManager.shareInstance.apiPostMyBirthPlansItems(userID: pregnancy.id, my_birth_plan_type_id: birthPlanTypeID, item_content: content) { (data, message, isSuccess) in
                //Server đã trả về thông
                if (isSuccess) {
                    self.countPostItem += 1
                    let result = JSON(data)
                    self.listIDPostSuccess.append(result["id"].intValue)
                    if self.countPostItem == self.wishDataPost.count {
                        // Khi Biến đếm bằng với count của data post -> post item lên
                        self.countPostPlan = 0
                        for i in 0..<self.listIDPostSuccess.count {
                            self.postMyBirthPlant(my_birth_plan_item_id: self.listIDPostSuccess[i])
                        }
                    }
                } else {
                    self.countPostItem += 1
                    self.alert("Không thêm được việc cần làm \(content). Xin vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                    })
                }
            }
        }
    }
    
    func postMyBirthPlant(my_birth_plan_item_id : Int) {
        NetworkManager.shareInstance.apiPostMyBirthPlans(my_birth_plan_item_id: my_birth_plan_item_id) { (data, message, isSuccess) in
            self.countPostPlan += 1
            if self.countPostPlan == self.listIDPostSuccess.count {
                self.hideHud()
                self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
}

extension AddWishViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.wishDataPost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addWishTableViewCell", for: indexPath) as! AddWishTableViewCell
        cell.mWishLabel.text = self.wishDataPost[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let totalRow = mAddWishTbv.numberOfRows(inSection: indexPath.section)
        indexGuide = indexPath.row
        mAddWishTbv.deselectRow(at: indexPath, animated: true)
        if indexPath.row >= 0 {
            let alert = UIAlertController(title: "", message: "Bạn có muốn xóa kế hoạch này?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                self.wishDataPost.remove(at: indexPath.row)
                self.mAddWishTbv.reloadData()
                self.heightTableView.constant = CGFloat(50 * self.wishDataPost.count)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else if indexPath.row == 0 {
            view.endEditing(true)
        }
    }
    
}
extension AddWishViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.birthPlanData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.birthPlanData[row].type
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //Cho title hiển thị khi chọn pickerview
        self.mTitleBtn.setTitle(self.birthPlanData[row].type, for: .normal)
        self.birthPlantTypeID = self.birthPlanData[row].id
    }
}

extension AddWishViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
        // khi bắt đầu edit -> ẩn pickerview
        self.mViewBouncePicker.isHidden = true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("textFieldDidBeginEditing")
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        self.insertToTabelView()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
}

class addWishButton : UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit(){
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.height - width, width: UIScreen.main.bounds.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
    

