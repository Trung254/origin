//
//  AddWishTableViewCell.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 4/10/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class AddWishTableViewCell: UITableViewCell {

    
    @IBOutlet weak var mWishLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
