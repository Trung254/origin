//
//  HeaderExportBirthPlanCell.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 3/14/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class HeaderExportBirthPlanCell: UITableViewCell {

    @IBOutlet weak var mPlanTitle: UILabel!
    @IBOutlet weak var mIconLbl: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
