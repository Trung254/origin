//
//  BirthPlanViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/18/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyJSON

struct BirthPlansCheck {
    var birth_plan_item_id = 0
    var userId = 0
}
struct BirthPlansItem {
    var id = 0
    var item_content = ""
    var my_birth_plan_type_id = 0
    var custom_item_by_user_id = 0
}

class BirthPlanViewController: BaseViewController {
    
    
    @IBOutlet weak var mTableViewBirthPlan: UITableView!
    
    
    var birthPlanData = [PregMyBirthPlanType]()
    
    var bithPlansCheckbox = [BirthPlansCheck]()
    var birthPlansItems = [BirthPlansItem]()
    
    // Dữ liệu để export sang mail
    var dataExport = [DataExport]()
    var dataExportFilter = [DataExport]()
    //

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        self.setTitle(title: "Kế hoạch sinh con")
        self.addBackgroundImage("me-background-blur.png")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if birthPlanData.isEmpty == false {
            birthPlanData.removeAll()
        }
        if bithPlansCheckbox.isEmpty == false {
            bithPlansCheckbox.removeAll()
        }
        if birthPlansItems.isEmpty == false {
            birthPlansItems.removeAll()
        }
        if self.dataExport.isEmpty == false {
            self.dataExport.removeAll()
        }
        if self.dataExportFilter.isEmpty == false {
            self.dataExportFilter.removeAll()
        }
        
        
        self.loadBirthPlansType()
        
//        self.loadBirthPlansItems()
//        self.loadBirthPlans()
    }
    override func isUpdateTabBarWhenScrolling() -> Bool {
        return true
    }

    private func loadBirthPlansType() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllMyBirthPlansType { (birthPlanTypeArray, message, isSuccess) in
            if (isSuccess) {
                self.birthPlanData = birthPlanTypeArray
                self.loadBirthPlansItems()
            } else {
                if (message == "Authorization has been denied for this request."){
                    let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        DatabaseManager.sharedManager.logout {
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                        self.hideHud()
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                
                self.alert("Không tải được dữ liệu. Xin vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                    
                })
                    
                }
            }
            
        }
        self.hideHud()
    }
    
    

    // Lấy thông tin plant item
    private func loadBirthPlansItems() {
        

        if let pregnacy = DatabaseManager.sharedManager.getLocalUserInfo() {
            NetworkManager.shareInstance.apiGetAllMyBirthPlansItems { (data, message, isSuccess) in
                if (isSuccess) {
                    let result = JSON(data)
                    for items in result.arrayValue {
                        var item = BirthPlansItem()
                        item.id = items["id"].intValue
                        item.my_birth_plan_type_id = items["my_birth_plan_type_id"].intValue
                        item.item_content = items["item_content"].stringValue
                        item.custom_item_by_user_id = items["custom_item_by_user_id"].intValue
                        self.birthPlansItems.append(item)
                    }
                    self.loadBirthPlans()
                } else {
                    self.alert("Không tải được dữ liệu. Xin vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                        
                    })
                }
            }
        } else {
            self.alert("Không lấy được dữ liệu người dùng. Vui lòng đăng nhập lại", title: "Thông báo") { (action) in
                DatabaseManager.sharedManager.logout {
                    
                }
            }
        }
        
    }
    
    // Lấy thông tin plant item mà người dùng đã tick
    private func loadBirthPlans() {
        NetworkManager.shareInstance.apiGetMyBirthPlans { (data, message, isSuccess) in
            if(isSuccess) {
                let result = JSON(data)
                for items in result.arrayValue {
                    var item  = BirthPlansCheck()
                    item.birth_plan_item_id = items["my_birth_plan_item_id"].intValue
                    item.userId = items["user_id"].intValue
                    self.bithPlansCheckbox.append(item)
                }
                DispatchQueue.main.async {
                    self.setupData()
                    self.mTableViewBirthPlan.reloadData()
                    self.hideHud()
                }
            } else {
                DispatchQueue.main.async {
                    self.mTableViewBirthPlan.reloadData()
                    self.hideHud()
                }
            }
        }
    }
    
    
    
    private func setupTableView() {
        let nib = UINib(nibName: "BirthPlanTableViewCell", bundle: Bundle.main)
        mTableViewBirthPlan.register(nib, forCellReuseIdentifier: "cellbirthplan")
 
        mTableViewBirthPlan.delegate = self
        mTableViewBirthPlan.dataSource = self
        
        self.mTableViewBirthPlan.separatorStyle = .none
        mTableViewBirthPlan.tableFooterView = UIView(frame: CGRect.zero)
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 240.0))
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapRecognizer.delegate = self
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        headerView.addGestureRecognizer(tapRecognizer)
        
        let image: UIImage = UIImage(named: "imgBirthPlan")!
        let headerImageView = UIImageView(image: image)
        headerImageView.frame = CGRect.init(x: 0, y: 0, width: headerView.bounds.width, height: headerView.bounds.height)
        headerView.addSubview(headerImageView)
        mTableViewBirthPlan.tableHeaderView = headerView
    }
    
    @objc func handleTap(gestureRecognizer: UIGestureRecognizer) {
        let sb = UIStoryboard(name: "Me", bundle: nil)
        let mBirthPlanView = sb.instantiateViewController(withIdentifier: "WhatIsABirthPlan") as! WhatIsABirthPlanViewController
        self.navigationController?.pushViewController(mBirthPlanView, animated: true)
    }
    
    
    // Tạo right bar button, info và share
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        
        var barItems = super.createRightBarButtonItems()
        
        let addButton = UIButton(type: .custom)
        addButton.setImage(UIImage(named: "share-1"), for: .normal)
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        addButton.addTarget(self, action: #selector(addButtonAction), for: .touchUpInside)
        let barButton2 = UIBarButtonItem(customView: addButton)
        
        barItems.insert(barButton2, at: 0)
        return barItems
    }
    
    func setupData() {
        for i in 0..<self.birthPlanData.count {
            var data = DataExport()
            data.id = self.birthPlanData[i].id
            data.title = self.birthPlanData[i].type
            data.icon = self.birthPlanData[i].type_icon
            self.dataExport.append(data)
        }
        for planItem in birthPlansItems {
            for planCheckbox in bithPlansCheckbox {
                if planItem.id == planCheckbox.birth_plan_item_id {
                    // Lấy ra thằng check
                    for i in 0..<self.dataExport.count {
                        if self.dataExport[i].id == planItem.my_birth_plan_type_id {
                            
                            var item = ItemCheck()
                            item.content = planItem.item_content
                            item.idItem = planItem.id
                            item.custom_item_by_user_id = planItem.custom_item_by_user_id
                            
                            self.dataExport[i].data.append(item)
                            break
                        }
                    }
                }
            }
        }
        self.dataExportFilter = self.dataExport.filter { (item) -> Bool in
            item.data.isEmpty == false
        }
    }
    
    @objc func addButtonAction(){
        // Export email tại đây
        var textShare = ""
        for itemShare in self.dataExportFilter {
            textShare += itemShare.title + "\n"
            for i in 0..<itemShare.data.count {
                textShare += "   - \(itemShare.data[i].content)\n"
            }
        }
        let activityController = UIActivityViewController(activityItems: [textShare], applicationActivities: nil)
        activityController.setValue("Kế hoạch sinh con", forKey: "subject")
        
        UIApplication.shared.keyWindow?.rootViewController?.present(activityController, animated: true, completion: nil)
    }
    
}

extension BirthPlanViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return birthPlanData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellbirthplan", for: indexPath) as! BirthPlanTableViewCell
        
        let domain = NetworkManager.rootDomain
        let urlString = domain + birthPlanData[indexPath.row].type_icon
        cell.mImgBirthPlanCell.sd_cancelCurrentImageLoad()
        if let url = URL(string: urlString) {
            cell.mImgBirthPlanCell.sd_setImage(with: url) { (image, error, cacheType, url) in
                cell.mImgBirthPlanCell.image = image
            }
        }
        
        cell.mLbBirthPlanCell.text = birthPlanData[indexPath.row].type
        cell.mSubLbBirthPlanCell.text = "0"
        
        var count = 0
        
        for item in birthPlansItems {
            if item.my_birth_plan_type_id == birthPlanData[indexPath.row].id {
                for plan in bithPlansCheckbox {
                    if item.id == plan.birth_plan_item_id {
                        count += 1
                    }
                }
            }
        }
        cell.mSubLbBirthPlanCell.text = String(count)
        
        
        
        
        if indexPath.row == 0 {
            cell.mBackground.isHidden = false
            cell.mBackground.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            cell.mLbBirthPlanCell.textColor = #colorLiteral(red: 0.0002389721922, green: 0.6639527082, blue: 1, alpha: 1)
            cell.mLineBottom.isHidden = true
            
            cell.mSubLbBirthPlanCell.isHidden = true
            
        } else if indexPath.row == 1 {
            cell.mBackground.isHidden = false
            cell.mBackground.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            cell.mLbBirthPlanCell.textColor = #colorLiteral(red: 0.0002389721922, green: 0.6639527082, blue: 1, alpha: 1)
            cell.mSubLbBirthPlanCell.isHidden = true
        } else {
            cell.mBackground.isHidden = true
            cell.mLbBirthPlanCell.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.mLineBottom.isHidden = false
            cell.mSubLbBirthPlanCell.isHidden = false
        }
        cell.selectionStyle = .none
        return cell
    }
}

extension BirthPlanViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if birthPlanData.isEmpty == false {
            if indexPath.row == 0 {
                let sb = UIStoryboard(name: "Me", bundle: nil)
                let mBirthPlanView = sb.instantiateViewController(withIdentifier: "WhatIsABirthPlan") as! WhatIsABirthPlanViewController
                self.navigationController?.pushViewController(mBirthPlanView, animated: true)
            } else if indexPath.row == 1 {
                // export Email
                let sb = UIStoryboard(name: "Me", bundle: nil)
                let mBirthPlanView = sb.instantiateViewController(withIdentifier: "ExportBirthPlanViewController") as! ExportBirthPlanViewController
                //              mBirthPlanView.birthPlansItems = self.birthPlansItems
                //              mBirthPlanView.bithPlansCheckbox = self.bithPlansCheckbox
                //              mBirthPlanView.birthPlanData = self.birthPlanData
                mBirthPlanView.dataExportFilter = self.dataExportFilter
                
                self.navigationController?.pushViewController(mBirthPlanView, animated: true)
            } else {
                let sb = UIStoryboard(name: "Me", bundle: nil)
                let mSubBirthPlanView = sb.instantiateViewController(withIdentifier: "SubBirthPlan") as! SubBirthPlanViewController
                
                mSubBirthPlanView.brithPlantTypeID = birthPlanData[indexPath.row].id
                mSubBirthPlanView.titleBar = birthPlanData[indexPath.row].type
                mSubBirthPlanView.birthPlanData = self.birthPlanData
                
                self.navigationController?.pushViewController(mSubBirthPlanView, animated: true)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = UIColor(white: 1, alpha: 0.5)
    }
}
