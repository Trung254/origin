//
//  GuideTableViewCell.swift
//  Reusable Template
//
//  Created by mai kim tai  on 12/4/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit

class GuideTableViewCell: UITableViewCell {

    @IBOutlet weak var mImageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var labelLeadingToContainViewConstraint: NSLayoutConstraint?
    @IBOutlet weak var labelLeadingToImageConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mTextLeadingCons: NSLayoutConstraint?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(_ data: MenuData) {
        if (self.mTextLeadingCons != nil) {
            self.contentView.removeConstraint(self.mTextLeadingCons!)
            self.mTextLeadingCons = nil
        }
        lblTitle.text = data.name
        mImageView.isHidden = false
        labelLeadingToContainViewConstraint?.priority = UILayoutPriority(rawValue: 250)
        lblTitle.textAlignment = .left
        mImageView.image = UIImage(named: data.image)
        mImageView.contentMode = .scaleToFill
        
        switch data.type {
        case "no image":
            mImageView.isHidden = true
            labelLeadingToContainViewConstraint?.priority = UILayoutPriority(rawValue: 750)
            lblTitle.textAlignment = .center
            self.backgroundColor = #colorLiteral(red: 0.2862745098, green: 0.7254901961, blue: 0.7803921569, alpha: 1)
            self.mImageView.isHidden = true
            self.lblTitle.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        case "google":
            self.lblTitle.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
            mImageView.contentMode = .center
        case "twitter":
            self.backgroundColor = #colorLiteral(red: 0, green: 0.6745098039, blue: 0.9333333333, alpha: 1)
            self.lblTitle.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        case "facebook":
            self.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.3529411765, blue: 0.6, alpha: 1)
            self.lblTitle.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            mImageView.contentMode = .center
        default:
            print("no selected")
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
