//
//  SizeGuideViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 10/12/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage

class SizeGuideViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableSizeGuide: UITableView!
    
    
    var mySizeGuidesItems : [PregSizeGuide] = [PregSizeGuide]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTitle(title: "Kích cỡ em bé")
        let nib = UINib(nibName: "SizeGuideTableViewCell", bundle: Bundle.main)
        tableSizeGuide.register(nib, forCellReuseIdentifier: "CellSizeGuide")
        tableSizeGuide.delegate = self
        tableSizeGuide.dataSource = self
        tableSizeGuide.backgroundView = UIImageView(image: UIImage(named: "baby-background-blur"))
        //addBackBT()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    private func loadWeek() {
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
            if let pregnancy = pregnancy {
                var components = Calendar.current.dateComponents([ .day], from: Date(), to:pregnancy.due_date.addingTimeInterval(86400))
                if var day = components.day {
                    if (day < 0 ) {
                        day = 0
                    } else {
                        day = 280 - day
                    }
                    
                    self.weeks = day / 7
                    if pregnancy.show_week == 1 {
                        self.weeks = day / 7 + 1
                    }
                    self.days = day - (self.weeks! * 7 )
                    if self.weeks == 0 || pregnancy.baby_already_born == 1 {
                        self.weeks = 1
                    }
                } else {
                    self.days = 0
                    self.weeks = 1
                }
                
                if(self.mySizeGuidesItems.count > 0) {
                    for i in 0..<self.mySizeGuidesItems.count {
                        let data = self.mySizeGuidesItems[i]
                        if (data.week_id == self.weeks! + 1) {
                            self.tableSizeGuide.reloadData()
                            self.tableSizeGuide.scrollToRow(at: IndexPath(row: i-1, section: 0), at: UITableView.ScrollPosition.top, animated: false)
                            return
                        }
                    }
                }
            }
        }
    }
    
    override func isUpdateTabBarWhenScrolling() -> Bool {
        return false
    }
    
    private func loadData() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllSizeguides { (items, message, isSuccess) in
            if (isSuccess) {
                self.hideHud()
                self.mySizeGuidesItems = items
                self.tableSizeGuide.reloadData()
                self.loadWeek()
//                if var week = self.weeks {
//
//                    if week > 40 {
//                        week = 40
//                    }
//
//                    for i in 0..<self.mySizeGuidesItems.count {
//                        let data = self.mySizeGuidesItems[i]
//                        if (data.week_id == week + 1) {
//                            self.hideHud()
//                            self.tableSizeGuide.scrollToRow(at: IndexPath(row: i-1, section: 0), at: UITableView.ScrollPosition.top, animated: false)
//                            return
//                        }
//                    }
//                    self.hideHud()
//                } else {
//                    self.hideHud()
//
//                }
                
            } else {
                self.hideHud()
                self.alert("Không load được kích cỡ em bé. Vui lòng thử lại sau", title: "Thông báo", handler: nil)
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mySizeGuidesItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellSizeGuide", for: indexPath) as! SizeGuideTableViewCell
        if (mySizeGuidesItems[indexPath.row].week_id == 4){
        cell.lblSizeGuide.text = "Tuần 3 & 4 "
        }
        else if (mySizeGuidesItems[indexPath.row].week_id == 41){
            cell.lblSizeGuide.text = "Tuần 41 & 42 "
        }
        else {
            cell.lblSizeGuide.text = "Tuần \(mySizeGuidesItems[indexPath.row].week_id)"
        }
        cell.lbl1SizeGuide.text = mySizeGuidesItems[indexPath.row].title
        let weight = String(mySizeGuidesItems[indexPath.row].weight)
        let length = String(mySizeGuidesItems[indexPath.row].length)
        cell.lbl2SizeGuide.text =  "Cân nặng: \(weight) gram"
        cell.lbl3SizeGuide.text =  "Chiều dài: \(length) cm"
        if (mySizeGuidesItems[indexPath.row].week_id == 41){
            cell.lbl2SizeGuide.text =  "Cân nặng: 3500 - 3800 gram"
        }else if (mySizeGuidesItems[indexPath.row].week_id == 14){
            cell.lbl3SizeGuide.text =  "Chiều dài: 8.7 cm"
        }

        let urlString = NetworkManager.rootDomain + mySizeGuidesItems[indexPath.row].image
        cell.imgSizeGuide.sd_cancelCurrentImageLoad()
        if let url = URL(string: urlString) {
            cell.imgSizeGuide.sd_setImage(with: url, placeholderImage: UIImage(named:""), options: SDWebImageOptions.highPriority, completed: nil)
        }

        
        if indexPath.row == 0 {
            cell.noiTren.isHidden = true
            cell.noiDuoi.isHidden = false
        } else {
            if indexPath.row == self.mySizeGuidesItems.count - 1 {
                cell.noiDuoi.isHidden = true
                cell.noiTren.isHidden = false
            } else {
                cell.noiDuoi.isHidden = false
                cell.noiTren.isHidden = false
            }
        }
        
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
}
