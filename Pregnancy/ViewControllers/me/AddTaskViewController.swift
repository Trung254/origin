//
//  AddTaskViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 06/12/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddTaskViewController: BaseViewController {

//    @IBOutlet weak var typeHereTf: UITextView!
    @IBOutlet weak var tableViewTask: UITableView!
    @IBOutlet var mViewAddTask: UIView!
    @IBOutlet weak var typeHereTf: UITextField!
    
    var check: Dictionary = [String:Any]()
    var dataTodos: [JSON] = []
    var dataUserTodos: [JSON] = []
    var count = 0
    var userId = 0
    var todo_id: Int?
    var isSaveCustomTask = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "TodoTableViewCell", bundle: Bundle.main)
        tableViewTask.register(nib, forCellReuseIdentifier: "TodoCell")
//        typeHereTf.leftView = UIView.init(frame:CGRect(x: 0, y: 0, width: 15, height: typeHereTf.frame.size.height))
//        typeHereTf.leftViewMode = .always
        tableViewTask.rowHeight = UITableView.automaticDimension
        tableViewTask.estimatedRowHeight = 50
        mViewAddTask.backgroundColor = UIColor(patternImage: UIImage(named: "me-background-blur")!)
        userId = DatabaseManager.sharedManager.getLocalUserInfo()!.id
        self.loadDataTodos()
        self.tableViewTask.tableFooterView = UIView()
//        typeHereTf.textColor = .lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
    }
    
    @IBAction func saveNewTask(_ sender: Any) {
//        let mbackmyAppoint = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "TodoListViewController") as! TodoListViewController
        let lblText = self.typeHereTf.text
        
        let lblTextSend = lblText?.trimmingCharacters(in: .whitespaces)
        
        if lblTextSend?.isEmpty == false {
            self.pushNewDataToUserTodo(String(lblTextSend!), userId)
        }
        else {
//            self.alert("Thêm công việc mới không được để trống. Xin vui lòng thử lại", title: "Thông báo") { (action) in
//                self.typeHereTf.text = ""
//            }
            self.navigationController?.popViewController(animated: true)
        }
//        self.navigationController?.popViewController(animated: true)
    }
    
    private func pushNewDataToUserTodo(_ title: String,_ customTaskByUserId: Int) {
        self.showHud()
        NetworkManager.shareInstance.apiPostToDos(title: title, customTaskByUserId: customTaskByUserId) {
            (data, message, isSuccess) in
            if isSuccess {
                NetworkManager.shareInstance.apiGetAllToDos() {
                    (data, message, isSuccess) in
                    if isSuccess {
                        let data = data as! JSON
                        var arrTitle: [Int] = []
                        for item in data.arrayValue {
                            if item["custom_task_by_user_id"].intValue == self.userId && self.typeHereTf.text?.trimmingCharacters(in: .whitespaces) == item["title"].stringValue {
                                arrTitle.append(item["id"].intValue)
                            }
                        }
                        
                        self.todo_id = arrTitle.last
                        NetworkManager.shareInstance.apiPostUserToDos(todoId: self.todo_id! , status: 0) {
                            (data, message, isSuccess) in
                            
                            DispatchQueue.main.async {
                                self.hideHud()
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    } else if (isSuccess == false) {
                        let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            DatabaseManager.sharedManager.logout {
                            }
                        }))
                        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                            self.hideHud()
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                }
//                self.hideHud()
//                self.navigationController?.popViewController(animated: true)
            } else {
                self.alert("Thông báo", title: "Thêm việc cần làm không thành công. Vui lòng thử lại sau", handler: { (action) in
                    self.hideHud()
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    private func loadDataTodos() {
        NetworkManager.shareInstance.apiGetAllToDos() {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    if item["custom_task_by_user_id"].intValue == 0 {
                        self.dataTodos.append(item)
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.loadUserTodos()
            }
        }
    }
    
    private func pushDataUserTodos(_ todoId: Int,_ status: Int) {
        NetworkManager.shareInstance.apiPostUserToDos(todoId: todoId, status: status) {
            (data, message, isSuccess) in
            DispatchQueue.main.async {
                self.loadUserTodos()
            }
        }
    }
    
    private func pushDataTodos(_ title: String,_ customTaskByUserId: Int) {
        NetworkManager.shareInstance.apiPostToDos(title: title, customTaskByUserId: customTaskByUserId) {
            (data, message, isSuccess) in
            if isSuccess {
                self.loadUserTodos()
            }
        }
    }

    
    private func deleteDataUserTodos(_ todoId: Int) {
        NetworkManager.shareInstance.apiDeleteUserToDos(todoId: todoId) {
            (data, message, isSuccess) in
            DispatchQueue.main.async {
                self.loadUserTodos()
            }
        }
    }
    
    private func loadUserTodos() {
        self.showHud()
        self.dataUserTodos = []
        NetworkManager.shareInstance.apiGetUserToDos() {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                self.dataUserTodos = data.arrayValue
            }
            
            DispatchQueue.main.async {
                self.hideHud()
                self.tableViewTask.reloadData()
            }
        }
    }
}

extension AddTaskViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return (Int(dataTodos.count/7) + 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sections = Int(dataTodos.count/7) + 1
        var days = 0
        
        for i in 0..<sections {
            if section == i {
                for j in 0...6 {
                    if i*7 + j < dataTodos.count  {
                        if dataTodos[i*7 + j]["title"].stringValue != "" {
                            days += 1
                        }
                    }
                }
            }
        }

        return days
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath) as! TodoTableViewCell
        
        cell.toDolabel.text = ""
        cell.toDoImage.image = UIImage(named: "untick")
        if dataTodos != [] {
            cell.toDolabel.text = dataTodos[(indexPath.section * 7 ) + (indexPath.row ) ]["title"].stringValue
            for item in dataUserTodos {
                if dataTodos[(indexPath.section * 7 ) + (indexPath.row ) ]["id"].intValue == item["todo_id"].intValue {
                    cell.toDoImage.image = UIImage(named: "tick-3")
                }
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Tuần thứ \(section+1)"
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath) as! TodoTableViewCell
        
        let todoId = dataTodos[(indexPath.section * 7 ) + (indexPath.row ) ]["id"].intValue
        var check = false
        for item in dataUserTodos {
            if dataTodos[(indexPath.section * 7 ) + (indexPath.row ) ]["id"].intValue == item["todo_id"].intValue {
                check = true
            }
        }
        
        if check == true {
            self.deleteDataUserTodos(todoId)
        }
        
        else {
            self.pushDataUserTodos(todoId, 0)
        }

        tableView.reloadData()
    }
}

extension AddTaskViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Nhập ở đây..." && textView.textColor == .lightGray)
        {
            textView.text = ""
            textView.textColor = .black
            self.isSaveCustomTask = true
        }
        textView.becomeFirstResponder() //Optional
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = "Nhập ở đây..."
            textView.textColor = .lightGray
            self.isSaveCustomTask = false
        } else {
            self.isSaveCustomTask = true
        }
        textView.resignFirstResponder()
    }
    
}
