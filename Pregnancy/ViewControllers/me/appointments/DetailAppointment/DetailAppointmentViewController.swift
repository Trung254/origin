//
//  DetailAppointmentViewController.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 2/25/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import WebKit

class DetailAppointmentViewController: BaseViewController, UIWebViewDelegate {

    
    @IBOutlet weak var mDetailAppointTableView: UITableView!
    
    var orderItemSingle : Array<OrderItems> = []
    var manufacturer : Array<Manufacturer> = []
    
    
    var sectionGet : Array<String> = []
//    var sectionFilter : Array<String> = []

    struct SectionFilter {
        var title = ""
        var data : Array<OrderItems> = []
    }
    
    var sectionFilter : Array<SectionFilter> = [] // Mảng các section
    
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        self.setTitle(title: "Chi tiết lịch hẹn khám")

        self.mIsShowRightButton = false
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

        if self.orderItemSingle.isEmpty {
            self.alert("Bạn không có lịch hẹn. Vui lòng thử lại sau", title: "Thông báo") { (action) in
                self.navigationController?.popViewController(animated: true)
            }
            
        } else {
            self.filterSection()
        }
        
        

    }
    
    func setupTableView() {
        let nib = UINib(nibName: "DetailAppointmentTableViewCell", bundle: nil)
        self.mDetailAppointTableView.register(nib, forCellReuseIdentifier: "DetailAppointmentCell")
        self.mDetailAppointTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.mDetailAppointTableView.estimatedRowHeight = 100
        self.mDetailAppointTableView.rowHeight = UITableView.automaticDimension
        
        let nib_header = UINib(nibName: "KhaosatCustomHeader", bundle: nil)
        self.mDetailAppointTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "KhaosatHeader")
    }
    
    func filterSection() {
        
        var sectionTitle = [String]()
        for i in 0..<self.orderItemSingle.count {
            sectionTitle.append(orderItemSingle[i].dateExamine)
        }
        
        let sectionTitleFilter = Array(Set(sectionTitle))
        
        for i in 0..<sectionTitleFilter.count {
            var item = SectionFilter()
            item.title = sectionTitleFilter[i]
            sectionFilter.append(item)
        }
        
        for i in 0..<sectionFilter.count {
            for item in orderItemSingle {
                if sectionFilter[i].title == item.dateExamine {
                    sectionFilter[i].data.append(item)
                }
            }
        }
        
        self.sectionFilter.sort { (date, nextDate) -> Bool in
            
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let date1 = dateFormatter.date(from: date.title)
            let date2 = dateFormatter.date(from: nextDate.title)
            
            if date1 != nil && date2 != nil {
                return date1?.compare(date2!) != .orderedDescending
            } else {
                return false
            }
        }
        
        self.mDetailAppointTableView.reloadData()
    }
}

extension DetailAppointmentViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension DetailAppointmentViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionFilter.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sectionFilter[section].data.count
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "KhaosatHeader") as? KhaosatCustomHeader
        
            headerView?.lblTitle.text = self.sectionFilter[section].title
            headerView?.lblTitle.backgroundColor = UIColor.green
            headerView?.lblTitle.layer.cornerRadius = 10

        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailAppointmentCell", for: indexPath) as! DetailAppointmentTableViewCell
        cell.selectionStyle = .none
        
        let section = indexPath.section
        let row = indexPath.row
        
        if self.sectionFilter[section].data[row].product.name == "Đặt lịch" {
            // Trường hợp đặt lịch
            cell.mTitle.text = self.sectionFilter[section].data[row].product.name
            
//            let url = URL(string: self.sectionFilter[section].data[row].product.image)
//            cell.mImage.sd_setImage(with: url, completed: nil)
            // Dữ liệu người dùng nhập
            
            for item1 in self.sectionFilter[section].data[row].product_attributes {
                for item2 in self.sectionFilter[section].data[row].product.attributes {
                    
                    if item2.product_attribute_name == "Nơi khám" {
                        for atributesValue in item2.attribute_values {
                            if atributesValue.id == Int(item1.value) {
                                // toi, nguoi khac
                                let urlStr = atributesValue.name
                                var cellWebView: WKWebView
                                if let webView  = cell.webView.viewWithTag(110) as? WKWebView {
                                    cellWebView = webView
                                } else {
                                    let webConfiguration = WKWebViewConfiguration()
                                    let webView = WKWebView(frame: .zero, configuration: webConfiguration)
                                    webView.uiDelegate = cell
                                    webView.navigationDelegate = cell
                                    
                                    webView.isUserInteractionEnabled = false
                                    webView.tag = 110
                                    
                                    cell.webView.addSubview(webView)
                                    webView.translatesAutoresizingMaskIntoConstraints = false
                                    webView.snp.makeConstraints { (maker) in
                                        maker.top.equalToSuperview()
                                        maker.leading.equalToSuperview()
                                        maker.bottom.equalToSuperview()
                                        maker.trailing.equalToSuperview()
                                    }
                                    cellWebView = webView
                                }
                                cell.mTableview = tableView
                                cellWebView.loadHTMLString("""
                                    <html><head> <meta name="viewport" content="width=device-width, initial-scale=1"> <style>*{font-family: "Helvetica";}img{margin: 0;}</style> <meta charset="utf-8"></head><body>\(urlStr.htmlDecoded)</body></html>
                                    """, baseURL: nil)
                            }
                        }
                    }
                    
                    
                    if item2.product_attribute_name == "Đặt lịch cho" {
                        for atributesValue in item2.attribute_values {
                            if atributesValue.id == Int(item1.value) {
                                // toi, nguoi khac
                                cell.mPeopleAppointment.text = "Đặt lịch khám cho: \(atributesValue.name)"
                            }
                        }
                    }
                    
                    
                    if item2.product_attribute_name == "Họ và tên" {
                        if item2.id == item1.id {
                            cell.mFullName.text = "Họ và tên: \(item1.value)"
                        }
                    }
                    
                    if item2.product_attribute_name == "Số điện thoại" {
                        if item2.id == item1.id {
                            cell.mPhonePeople.text = "Số điện thoại: \(item1.value)"
                        }
                    }
                    
                    if item2.product_attribute_name == "Triệu chứng" {
                        if item2.id == item1.id {
                            cell.mSymptom.text = "Triệu chứng: \(item1.value)"
                        }
                    }
                    
                    if item2.product_attribute_name == "Khung giờ" {
                        for atributesValue in item2.attribute_values {
                            if atributesValue.id == Int(item1.value) {
                                
                                //                            cell.mPeopleAppointment.text = "Email: \(atributesValue.name)"
                                let timeStr = atributesValue.name
                                
                                cell.mTimeAppointment.text = timeStr.substring(fromIndex: 0, toIndex: 5)
                            }
                        }
                    }
                    
                }
            }
        } else {
            cell.mTitle.text = self.sectionFilter[section].data[row].product.name
            
            let url = URL(string: self.sectionFilter[section].data[row].product.image)
            cell.mImage.sd_setImage(with: url, completed: nil)
            
            // Địa chỉ
            let urlStr = self.sectionFilter[section].data[row].product.full_description
            var cellWebView: WKWebView
            if let webView  = cell.webView.viewWithTag(110) as? WKWebView {
                cellWebView = webView
            } else {
                let webConfiguration = WKWebViewConfiguration()
                let webView = WKWebView(frame: .zero, configuration: webConfiguration)
                webView.uiDelegate = cell
                webView.navigationDelegate = cell
                
                webView.isUserInteractionEnabled = false
                webView.tag = 110
                
                cell.webView.addSubview(webView)
                webView.translatesAutoresizingMaskIntoConstraints = false
                webView.snp.makeConstraints { (maker) in
                    maker.top.equalToSuperview()
                    maker.leading.equalToSuperview()
                    maker.bottom.equalToSuperview()
                    maker.trailing.equalToSuperview()
                }
                cellWebView = webView
            }
            cell.mTableview = tableView
            cellWebView.loadHTMLString("""
                <html><head> <meta name="viewport" content="width=device-width, initial-scale=1"> <style>*{font-family: "Helvetica";}img{margin: 0;}</style> <meta charset="utf-8"></head><body>\(urlStr.htmlDecoded)</body></html>
                """, baseURL: nil)
            
            
            // Dữ liệu người dùng nhập
            
            for item1 in self.sectionFilter[section].data[row].product_attributes {
                for item2 in self.sectionFilter[section].data[row].product.attributes {
                    
                    if item2.product_attribute_name == "Đặt lịch cho" {
                        for atributesValue in item2.attribute_values {
                            if atributesValue.id == Int(item1.value) {
                                // toi, nguoi khac
                                cell.mPeopleAppointment.text = "Đặt lịch khám cho: \(atributesValue.name)"
                            }
                        }
                    }
                    
                    
                    if item2.product_attribute_name == "Họ và tên" {
                        if item2.id == item1.id {
                            cell.mFullName.text = "Họ và tên: \(item1.value)"
                        }
                    }
                    
                    if item2.product_attribute_name == "Số điện thoại" {
                        if item2.id == item1.id {
                            cell.mPhonePeople.text = "Số điện thoại: \(item1.value)"
                        }
                    }
                    
                    if item2.product_attribute_name == "Triệu chứng" {
                        if item2.id == item1.id {
                            cell.mSymptom.text = "Triệu chứng: \(item1.value)"
                        }
                    }
                    
                    if item2.product_attribute_name == "Khung giờ" {
                        for atributesValue in item2.attribute_values {
                            if atributesValue.id == Int(item1.value) {
                                
                                //                            cell.mPeopleAppointment.text = "Email: \(atributesValue.name)"
                                let timeStr = atributesValue.name
                                
                                cell.mTimeAppointment.text = timeStr.substring(fromIndex: 0, toIndex: 5)
                            }
                        }
                    }
                    
                }
            }
        }
        return cell
    }
    
}
