//
//  DetailAppointmentTableViewCell.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 2/25/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import WebKit

class DetailAppointmentTableViewCell: UITableViewCell {

    @IBOutlet weak var heightWebView: NSLayoutConstraint!
    @IBOutlet weak var webView: UIView!
    @IBOutlet weak var viewInfomation: UIView!
    @IBOutlet weak var mImage: UIImageView!
    @IBOutlet weak var mTimeAppointment: UILabel!
    @IBOutlet weak var mTitle: UILabel!
//    @IBOutlet weak var mAddress: UILabel!
//    @IBOutlet weak var mPhoneSupport: UILabel!
    @IBOutlet weak var mPeopleAppointment: UILabel!
    @IBOutlet weak var mFullName: UILabel!
    @IBOutlet weak var mPhonePeople: UILabel!
    @IBOutlet weak var mSymptom: UILabel!
    var mTableview: UITableView?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupView() {
        self.mImage.layer.cornerRadius = self.mImage.frame.size.width / 2
        self.viewInfomation.layer.borderWidth = 1
        self.viewInfomation.layer.borderColor = UIColor.black.cgColor
        

        
//        self.mTimeAppointment.backgroundColor = .clear
        self.mTimeAppointment.layer.masksToBounds = true
        self.mTimeAppointment.layer.cornerRadius = 10
        self.mTimeAppointment.text = "09:30"
        
        
        // Tạo hình oval
//        let shapeLayer = CAShapeLayer()
//        let circlePath = UIBezierPath(ovalIn: self.mTimeAppointment.bounds)
//        shapeLayer.path = circlePath.cgPath
//        shapeLayer.fillColor = UIColor.red.cgColor
//        shapeLayer.strokeColor = UIColor.red.cgColor
//        shapeLayer.lineWidth = 1
//
//        self.mTimeAppointment.layer.addSublayer(shapeLayer)
//
    }
    
}
extension DetailAppointmentTableViewCell: WKUIDelegate {
    
}
extension DetailAppointmentTableViewCell: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                
                webView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    if error == nil {
                        let height: CGFloat = height as! CGFloat
                        if self.heightWebView.constant != height {
                            self.heightWebView.constant = height
                            if let tbl = self.mTableview {
                                tbl.reloadData()
                            }
                        }
                    }
                    
                })
            }
            
        })
    }
    
}
