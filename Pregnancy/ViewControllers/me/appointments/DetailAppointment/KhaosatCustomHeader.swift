//
//  KhaosatCustomHeader.swift
//  iCNM
//
//  Created by Mac osx on 12/4/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class KhaosatCustomHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        let f = contentView.frame
//        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(10, 5, 5, 5))
//        contentView.frame = fr
//        addShadow(cell: self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewHeaderFooterView) {
        cell.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height:0)
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.1
    }
}
