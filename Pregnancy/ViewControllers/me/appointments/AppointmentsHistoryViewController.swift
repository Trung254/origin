//
//  AppointmentsViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/18/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

struct OrderItems {
    var dateExamine = ""
    var created_on_utc = ""
    var customer_id = 0
    var customer_FirstName = ""
    var customer_LastName = ""
    var product_id = 0
    var id = 0
    var product_attributes : Array<ProductAttributes> = []
    var product = Product()

}

struct Product {
    var id = 0
    var name = ""
    var short_description = ""
    var full_description = ""
    var manufacturer_ids = 0
    var image = ""
    var dayExamination = ""
//    var manufacturer = Manufacturer()
    var attributes : Array<Atributes> = []
}

struct ProductAttributes {
    var value = ""
    var id = 0
    

}
struct Atributes {
    var id = 0
    var product_attribute_name = ""
    var attribute_values : Array<AtributesValue> = []
}
struct AtributesValue {
    var name = ""
    var id = 0
}


struct Manufacturer {
    var id = 0
    var name = ""
    var description = ""
    var ImageUrl = ""
}

struct Catogories {
    var name = ""
    var id = 0
}
struct ProductByCategories {
    var nameCate = ""
    var id = 0
    var nameProduct = ""
}


/*
 category :
 - 18 : Gói khám
 - 19 : Bác sĩ
 - 20 : Đặt lịch
 
 productByCategory:
 
 - với ID = 18 :  có các product sau: (Khám theo gói khám)
    -  57 : GÓI KHÁM THAI LẦN 2
    - 58 : GÓI KHÁM THAI LẦN
    - 59 : GÓI KHÁM THAI LẦN 4
    - 60 : GÓI KHÁM THAI LẦN 5
    - 64 : GÓI KHÁM THAI LẦN 6
    - 56 : GÓI KHÁM THAI đầu
 
 - Id = 19 có các product sau (Khám theo bác sĩ)
    - 61: BS. Dương Ngọc Văn
    - 65 : BS. Nguyễn Duy Phương
    - 62 : BS. Nguyễn Thị Hiền
    - 63 : BS. Nguyễn Thị Thu
    - 66 : BS. Thân Ngọc Tuấn
 
 - id = 20: có các product sau (Đặt lịch khám)
    - 67 : Đặt lịch
 */

class AppointmentsHistoryViewController: BaseViewController {
    
    
    @IBOutlet weak var mAppointTableView: UITableView!
    

    
    let idCustomerFix = 2189
    var customerGUID = "36409f09-2f2a-4ac2-a90e-2957dbaff0ee"
    
    
    var orderItems : Array<OrderItems> = []
    var orderItemsPacket : Array<OrderItems> = [] //Filter kiểu khám theo gói
    var orderItemSingle : Array<OrderItems> = [] //Khám bác sĩ hoặc tự khám
    var manufacturer : Array<Manufacturer> = []
    
    var countArr = 0
    var countCheck = 0 // Check load hết dữ liệu rồi mới sắp xếp
    var countCate = 0
    var countCateCheck = 0
    
    let dateFormatter = DateFormatter()
    
    
    
    
    var listCatogories : Array<Catogories> = []
    var listProductByCategories : Array<ProductByCategories> = []
    //Số điện thoại
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.setTitle(title: "Quản lý lịch hẹn")
        
        
        self.mIsShowRightButton = false
        
        if (DatabaseManager.sharedManager.isGuest()) {
            
            self.prompt("Thông báo", message: "Bạn cần phải đăng nhập. Bạn có muốn đăng nhập ngay bây giờ không", okTitle: "Đăng nhập", okHandler: { (okAction) in
                DatabaseManager.sharedManager.logout {}
            }, cancelTitle: "Không") { (cancelAction) in
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            self.getProductListID()
        }

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if countCheck != 0 {
//            countCheck = 0
//        }
//        if countArr != 0 {
//            countArr  = 0
//        }
//
//        if self.orderItems.isEmpty == false {
//            self.orderItems.removeAll()
//        }
//
//        if self.orderItemsPacket.isEmpty == false {
//            self.orderItemsPacket.removeAll()
//        }
//
//        if self.orderItemSingle.isEmpty == false {
//            self.orderItemSingle.removeAll()
//        }
//
//        if self.manufacturer.isEmpty == false {
//            self.manufacturer.removeAll()
//        }

        
//        self.getProductListID()
//        self.getManufacture()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setupTableView(){
        let nib = UINib(nibName: "AppointmentsTableViewCell", bundle: nil)
        self.mAppointTableView.register(nib, forCellReuseIdentifier: "AppointmentsCell")

        self.mAppointTableView.tableFooterView = UIView(frame: CGRect.zero)
//        self.mAppointTableView.rowHeight = UITableView.automaticDimension
//        self.mAppointTableView.estimatedRowHeight = 100
    }
    
    func getProductListID() {
        self.showHud()
        NetworkManager.shareInstance.apiGetCategories { (data, message, isSuccess) in
            if (isSuccess) {
                let result = JSON(data)
                
                self.countCate = result["categories"].arrayValue.count
                
                for categories in result["categories"].arrayValue {
                    var category = Catogories()
                    category.name = categories["name"].stringValue
                    category.id = categories["id"].intValue
                    self.listCatogories.append(category)
                    self.getListProductIDByCatrgory(categoriID: categories["id"].stringValue)
                }
            }
            else {
                self.hideHud()
                self.alert("Không có Categories. Xin vui lòng thử lại", title: "Thông Báo", handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    func getListProductIDByCatrgory(categoriID : String) {
        NetworkManager.shareInstance.apiGetProductByCategory(categoryId: categoriID) { (data, message, isSuccess) in
            if(isSuccess) {
                
                self.countCateCheck += 1
                
                let result = JSON(data)
                
                var list = ProductByCategories()
                list.nameCate = result["Name"].stringValue
                for product in result["Products"].arrayValue {
                    list.id = product["Id"].intValue
                    list.nameProduct = product["Name"].stringValue
                    self.listProductByCategories.append(list)
                }
                
                if self.countCateCheck == self.countCate {
                    self.getCustomer()
//                    self.mAppointTableView.reloadData()
                }
            }
            else {
                self.hideHud()
                self.alert("Không có Product. Xin vui lòng thử lại", title: "Thông Báo", handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    
    
    func getCustomer() {
        NetworkManager.shareInstance.apiGetUserProfile() { (data, message, isSuccess) in
            if isSuccess {
                let result = data as! JSON
                if result.arrayValue.isEmpty == false {
                    self.getGuidCustomer(nopcustomer_id: result[0]["nopcustomer_id"].intValue)
                }
            } else {
                self.hideHud()
                self.alert("Không có dữ liệu người dùng. Xin vui lòng thử lại", title: "Thông Báo", handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
            
        }
    }
    
    func getGuidCustomer(nopcustomer_id: Int) {
        NetworkManager.shareInstance.apiGetAppointmentCustomersById(nopcustomer_id: nopcustomer_id) { (data, messge, isSuccess) in
            if(isSuccess) {
                let result = JSON(data)
                
                let cusGUID = result["customers"].arrayValue[0]["customer_guid"].stringValue
                self.customerGUID = cusGUID
                self.loadOrderIdByCustomer(customerGUID: cusGUID)
            } else {
                self.hideHud()
                self.alert("Không có GUID người dùng. Xin vui lòng thử lại sau", title: "Thông Báo", handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
            
        }
    }
    
    func loadOrderIdByCustomer(customerGUID : String) {
        NetworkManager.shareInstance.apiGetOrderIdByCustommerId(apiSecretKey: "m117z119u100l110e122w115p122i120", customerGUID: customerGUID, storeId: "1", languageId: "1") { (data, message, isSuccess) in
            if (isSuccess) {
                
                
                let orderIDItems = JSON(data)
                
                self.countArr = orderIDItems.count
                
                for item in orderIDItems.arrayValue {
                    self.checkOrder(idOrder: item["OrderId"].intValue)
                }
            } else {
//                self.hideHud()
//                self.mAppointTableView.reloadData()
                self.hideHud()
                self.alert("Không có lịch hẹn/gói khám. Vui lòng thử lại sau", title: "Thông Báo", handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    func checkOrder(idOrder: Int) {
        NetworkManager.shareInstance.apiGetOrderByID(orderId: idOrder) { (data, message, isSucess) in
            if (isSucess) {
                
                self.countCheck += 1
                
                
                let result = JSON(data)
                for items in result["orders"].arrayValue {
                    var orderItem = OrderItems()
                    orderItem.created_on_utc = items["created_on_utc"].stringValue
                    orderItem.customer_id = items["customer_id"].intValue
                    
                    orderItem.customer_FirstName = items["customer"]["first_name"].stringValue
                    orderItem.customer_LastName = items["customer"]["last_name"].stringValue
                    
                    
                    for itemOrder in items["order_items"].arrayValue {
                        orderItem.product_id = itemOrder["product_id"].intValue
                        
                        for product_attributes in itemOrder["product_attributes"].arrayValue {
                            var product = ProductAttributes()
                            product.id = product_attributes["id"].intValue
                            product.value = product_attributes["value"].stringValue
                            
                            orderItem.product_attributes.append(product)
                        }
                        
                        // lấy ra các product attributes
                        for attributes in itemOrder["product"]["attributes"].arrayValue {
                            var attri = Atributes()
                            attri.id = attributes["id"].intValue
                            attri.product_attribute_name = attributes["product_attribute_name"].stringValue
                            
                            if attributes["product_attribute_name"].stringValue == "Ngày khám" {
                                for product_attributes in orderItem.product_attributes {
                                    if product_attributes.id == attributes["id"].intValue {
                                        orderItem.dateExamine = product_attributes.value
                                    }
                                }
                            }
                            
                            
                            for attValue in attributes["attribute_values"].arrayValue {
                                var attValu = AtributesValue()
                                attValu.name = attValue["name"].stringValue
                                attValu.id = attValue["id"].intValue
                                attri.attribute_values.append(attValu)
                            }
                            orderItem.product.attributes.append(attri)
                        }
                        

                        // Get product
                        orderItem.id = itemOrder["product"]["Id"].intValue
                        orderItem.product.name = itemOrder["product"]["name"].stringValue
                        orderItem.product.short_description = itemOrder["product"]["short_description"].stringValue
                        orderItem.product.full_description = itemOrder["product"]["full_description"].stringValue
                        
                        if itemOrder["product"]["images"].arrayValue.isEmpty == false {
                            orderItem.product.image = itemOrder["product"]["images"].arrayValue.first!["src"].stringValue
                        }
                        
                        if itemOrder["product"]["manufacturer_ids"].arrayValue.isEmpty == false {
                            orderItem.product.manufacturer_ids = itemOrder["product"]["manufacturer_ids"].arrayValue.first!.intValue
                            
                            // Lấy dữ liệu manufacture và add vào mảng
                            
                        }

                        
                        
                        self.orderItems.append(orderItem)
                    }
                }
                
                if self.countCheck >= self.countArr {
                    self.filterOrderItem()
                }
            } else {
                self.hideHud()
                self.alert("Không lấy được Order. Vui lòng thử lại sau", title: "Thông Báo", handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    func filterOrderItem() {
        
        if self.orderItemsPacket.isEmpty == false {
            self.orderItemsPacket.removeAll()
        }

        if self.orderItemSingle.isEmpty == false {
            self.orderItemSingle.removeAll()
        }
        
        for item in self.orderItems {
            if self.checkProductId(product_id: item.product_id) == true {
                self.orderItemsPacket.append(item)
            } else {
                self.orderItemSingle.append(item)
            }
        }
//        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        if self.orderItemsPacket.count > 1 {
            self.orderItemsPacket.sort(by: { (date, nexDate) -> Bool in
                
                

                let date1 = dateFormatter.date(from: date.dateExamine)
                let date2 = dateFormatter.date(from: nexDate.dateExamine)
                
                if date1 != nil && date2 != nil {
                    return date1!.compare(date2!) != .orderedDescending
                } else {
                    return false
                }
                
            })
        }

        if self.orderItemSingle.count > 1 {
            self.orderItemSingle.sort(by: { (date, nexDate) -> Bool in
                
                let date1 = dateFormatter.date(from: date.dateExamine)
                let date2 = dateFormatter.date(from: nexDate.dateExamine)
                
                if date1 != nil && date2 != nil {
                    return date1!.compare(date2!) != .orderedDescending
                } else {
                    return false
                }

            })
        }
        
        self.getManufacture()
    }
    
    //Lấy thông tin manufacture của bác sĩ hay nơi khám về
    func getManufacture() {
        NetworkManager.shareInstance.apiGetAppointmentManufacturer(customerGUID: customerGUID) { (data, message, isSucess) in
    
            if(isSucess) {
                let result = JSON(data)
                for manufacture in result.arrayValue {
                    var manuItem = Manufacturer()
                    manuItem.id = manufacture["Id"].intValue
                    manuItem.description = manufacture["Description"].stringValue
                    manuItem.name = manufacture["Name"].stringValue
                    manuItem.ImageUrl = manufacture["PictureModel"]["ImageUrl"].stringValue
                    self.manufacturer.append(manuItem)
                }
                DispatchQueue.main.async {
                    self.hideHud()
                    self.mAppointTableView.reloadData()
                }
            } else {
                DispatchQueue.main.async {
                    self.hideHud()
                    self.mAppointTableView.reloadData()
                }
            }
        }
    }

}

extension AppointmentsHistoryViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 80
    }

}

extension AppointmentsHistoryViewController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentsCell", for: indexPath) as! AppointmentsTableViewCell
        cell.selectionStyle = .none
        
        
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        
        
        switch indexPath.row {
        case 0:
            cell.mAppointment.text = "Lịch hẹn khám"
            cell.mIcon.image = UIImage(named: "LichHenKham")
            cell.mNumberOfAppointment.text = String(self.orderItemSingle.count)
            
            if self.orderItemSingle.isEmpty == false {
                
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let dateSingle = dateFormatter.date(from: (self.orderItemSingle.first?.dateExamine)!)
                
                dateFormatter.dateFormat = "HH:mm' ngày 'dd/MM/yyyy"
                
                if dateSingle != nil {
                    let dateSingleStr = dateFormatter.string(from: dateSingle!)
                    cell.mTimeApointment.text = "Lịch hẹn gần nhất lúc \(dateSingleStr)"
                }
                
            }
            break
        case 1:
            cell.mAppointment.text = "Gói khám/Dịch vụ"
            cell.mIcon.image = UIImage(named: "GoiKham")
            cell.mNumberOfAppointment.text = String(self.orderItemsPacket.count)
            
            if self.orderItemsPacket.isEmpty == false {
//                let dateSingle = dateFormatter.date(from: (self.orderItemsPacket.first?.created_on_utc)!)
//
//                dateFormatter.locale = Locale.init(identifier: "vi")
//                dateFormatter.dateFormat = "HH:mm EEEE' ngày 'dd/MM/yyyy"
//
//                if dateSingle != nil {
//                    let dateSingleStr = dateFormatter.string(from: dateSingle!)
//                    cell.mTimeApointment.text = "Lịch hẹn gần nhất lúc \(dateSingleStr)"
//                }
                cell.mTimeApointment.text = self.orderItemsPacket.first?.product.name
            }
            
            
            break
        default:
            break
        }

        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        switch indexPath.row {
        case 0:
            let sb = UIStoryboard.init(name: "Me", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "DetailAppointmentViewController") as! DetailAppointmentViewController
            vc.orderItemSingle = self.orderItemSingle
//            vc.symptomListId = self.symptomListId
            vc.manufacturer = self.manufacturer
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            break
        case 1:
            let sb = UIStoryboard.init(name: "Me", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "DetailPacketAppointmentViewController") as! DetailPacketAppointmentViewController
            vc.orderItemsPacket = self.orderItemsPacket
//            vc.symptomListId = self.symptomListId
            vc.manufacturer = self.manufacturer
            
            self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            break
        }
    }

}

// Tiện ích tính toán cho appointment
extension AppointmentsHistoryViewController {
    
    
    func checkProductId(product_id : Int) -> Bool { //Nếu là gói khám thì ném xuống dưới

        let check = self.listProductByCategories.filter { (item) -> Bool in
            (item.nameCate == "Gói Khám") && (item.id == product_id)
        }
        
        if check.isEmpty {
            return false
        } else {
            return true
        }
    }
}



