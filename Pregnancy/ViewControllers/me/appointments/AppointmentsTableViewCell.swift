//
//  AppointmentsTableViewCell.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 2/25/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class AppointmentsTableViewCell: UITableViewCell {

    @IBOutlet weak var mNumberOfAppointment: UILabel!
    @IBOutlet weak var mTimeApointment: UILabel!
    @IBOutlet weak var mAppointment: UILabel!
    @IBOutlet weak var mIcon: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mNumberOfAppointment.layer.masksToBounds = true
        self.mNumberOfAppointment.layer.cornerRadius = self.mNumberOfAppointment.frame.size.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
}
