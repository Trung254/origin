//
//  AddWeightViewController.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/17/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddWeightViewController: BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var btnStartDate: UIButton!
    @IBOutlet weak var lblStartDateTitle: UILabel!
    @IBOutlet weak var lblPreWeight: UILabel!
    @IBOutlet weak var lblCurWeight: UILabel!
    @IBOutlet weak var lblCurWeightTitle: UILabel!
    @IBOutlet weak var btnPreWeight: UIButton!
    @IBOutlet weak var btnCurrentWeight: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var uiPickerWeight: UIPickerView!
    @IBOutlet weak var pvPickerDate: UIDatePicker!
    @IBOutlet weak var uiViewWeight: UIView!
    @IBOutlet weak var preWeightView: UIView!
    @IBOutlet weak var uvSelectDate: UIView!
    @IBOutlet weak var mbtnDate: UIButton!
    @IBOutlet weak var mHeightPreWei: NSLayoutConstraint!
    
    var dayChoise = ""
    
    var parrentAddVC : UIViewController?
//    var parrentVC : UIViewController = BaseViewController()
    
    var weightFromTableWeight : Float = 0.0
    var dayFromTableWeight : Date?
    var myWeightIdFromTableWeight : Int?
    
    let firstWeight = Array(0...1000)
    let secondWeight = Array(0...9)
//    var actualSegmentedIndex: Int?
    var indexRow: Int?
    var dataCurWei: Float = 50.0
    var dataStartWei: Float = 50.0
//    var dataDate = Date()
    var unitString: String?
    
    var checkFirstPush = 0
    var start_date = Date()
    var dataMyWeight : [PregMyWeight] = []
    
    var checkView = 0 // Bằng 1 nếu từ view Cân nặng sang -> popview lại
                        // Bằng 2 nếu từ view Bảng cân nặng sang -> push view Cân nặng và dimiss view kia
    
    var crrWeightPicker0 = 50
    var crrWeightPicker1 = 0
    var preWeightPicker0 = 50
    var preWeightPicker1 = 0
    
    var secondsFromGMT: Int { return TimeZone.current.secondsFromGMT() }
    
    let currentDate = Date.init().addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        print("View truyen data qua : \(parrentAddVC)")
        
        
        self.pvPickerDate.locale = Locale.init(identifier: "vi")
        
        self.setTitle(title: "Thêm cân nặng")
        // Config picker view weight
        uiPickerWeight.selectRow(50, inComponent: 0, animated: true)
        pvPickerDate.timeZone = TimeZone.init(abbreviation: "GMT+0:00")

        uiViewWeight.isHidden = true
//        btnDelete.isHidden = true
        uvSelectDate.isHidden = true
        if let titleLabel = btnStartDate.titleLabel {
            titleLabel.numberOfLines = 1
            titleLabel.adjustsFontSizeToFitWidth = true
            titleLabel.lineBreakMode = .byClipping
            titleLabel.minimumScaleFactor = 0.1
        }
        
        if self.myWeightIdFromTableWeight != nil {
            btnDelete.isHidden = false
        } else {
            btnDelete.isHidden = true
        }
        
        self.LoadDataFromAPI()
    }
    func LoadDataFromAPI() {
        self.showHud()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
            if let pregnancy = pregnancy {
                NetworkManager.shareInstance.apiGetAllMyWeights { (data, message, isSuccess) in
                    if(isSuccess) {
                        
                        let resutl = JSON(data)
                        for items in resutl.arrayValue {
                            let item = PregMyWeight()
                            item.my_weight_type_id = items["my_weight_type_id"].intValue
                            item.current_weight = items["current_weight"].floatValue
                            item.week_id = items["week_id"].intValue
                            item.id = items["id"].intValue
                            item.start_date = pregnancy.start_date
                            
                            let crrDate = items["current_date"].stringValue
                            if crrDate.isEmpty == false {
                                if let date = dateFormatter.date(from: crrDate) {
                                    if(date.compare(pregnancy.due_date.addingTimeInterval(86399)) == .orderedDescending ||
                                        date.compare(pregnancy.start_date.addingTimeInterval(-86399)) == .orderedAscending){
                                        continue
                                    }
                                    item.current_date  = date
                                }
                            }
                            
                            self.dataMyWeight.append(item)
                        }
                        if(self.dataMyWeight.count > 0) {
//                            self.transferDate(date: self.dayFromTableWeight)
                            self.transferDate(date: self.dayFromTableWeight ?? self.currentDate, startDate: pregnancy.start_date)
                            if self.weightFromTableWeight != 0 {
                                self.btnCurrentWeight.setTitle(String(format: "%.1f", self.weightFromTableWeight), for: .normal)
                                self.crrWeightPicker0 = Int(self.weightFromTableWeight)
                                self.crrWeightPicker1 = Int((self.weightFromTableWeight - Float(self.crrWeightPicker0)) * 10)
                            }
                            
                            self.checkFirstPush = 2
                            // Có giá trị -> Ẩn
                            self.mHeightPreWei.constant = 0
                            self.preWeightView.isHidden = true
                            //                self.unitWeightView.isHidden = true
                            self.lblStartDateTitle.text = "Ngày"
                            
                            self.start_date = pregnancy.start_date
                            
                            self.pvPickerDate.maximumDate = pregnancy.due_date
                            self.pvPickerDate.minimumDate = pregnancy.start_date
                        } else {
                            self.setAddStartValue(pregnancy)
                        }
                    } else {
                        self.setAddStartValue(pregnancy)
                        
                    }
                    
                    self.hideHud()
                }
            } else {
                self.hideHud()
            }
        }
        
    }
    
    func setAddStartValue(_ pregnancy: PregPregnancy) {
        self.checkFirstPush = 1
//        self.transferDate(date: pregnancy.start_date, startDate: pregnancy.start_date)
//        self.transferDate(date: pregnancy.start_date)
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        formatter.dateFormat = "EEEE, d MMM yyyy"
        formatter.locale = Locale(identifier: "vi_VI")
        self.dayChoise = formatter.string(from: pregnancy.start_date)
//        self.btnStartDate.setTitle(self.dayChoise + " - Tháng thứ 0", for: .normal)
        self.btnStartDate.setTitle(self.dayChoise, for: .normal)
        
        self.start_date = pregnancy.start_date
        self.btnStartDate.isEnabled = false
        self.btnPreWeight.setTitle("50.0", for: .normal)
        self.btnCurrentWeight.setTitle("50.0", for: .normal)
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        self.pvPickerDate.maximumDate = pregnancy.due_date
        self.pvPickerDate.minimumDate = pregnancy.start_date
        
        //Kiểm tra xem ngày bắt đầu có phải ngày hiện tại
        let calDay = Calendar.current.dateComponents([.day], from: pregnancy.start_date, to: self.currentDate)
        if let calDay = calDay.day {
            if calDay <= 0 {
                self.preWeightView.isHidden = true
                self.mHeightPreWei.constant = 0
            }
        }
    }
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        let infoButton = UIButton(type: .custom)
        infoButton.setImage(UIImage(named: "checked"), for: .normal)
        infoButton.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
        infoButton.addTarget(self, action: #selector(donePushData), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: infoButton)
        return [barButton]
    }
    
    @objc func donePushData() {
        print("push to server")
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        formatter.dateFormat = "EEEE, d MMM yyyy"
        formatter.locale = Locale(identifier: "vi_VI")
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        dateFormatter.locale = Locale(identifier: "vi_VI")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

        let startDateStr = dateFormatter.string(from: self.start_date)
        
        if checkFirstPush == 1 {
            // Truyền cân nặng đầu tiên lên
            
            let preWeight = btnPreWeight.titleLabel?.text // Cân nặng trước khi mang thai
            let preWei = Float(preWeight!)
            // Truyền cân nặng ngày hiện tại
            
            let crrWeight = btnCurrentWeight.titleLabel?.text // Cân nặng Hiện tại
            let crrWei = Float(crrWeight!)
            
            let component = Calendar.current.dateComponents([.weekOfYear], from: self.start_date, to: self.currentDate)
            var week = component.weekOfYear!
            
            if week > 40 {
                week = 40
            } else if week < 1 {
                week = 1
            }
            
            let date = dateFormatter.string(from: self.currentDate)
            
            let day = Calendar.current.dateComponents([.day], from: self.start_date, to: self.currentDate)
            
            // trường hợp start date và ngày hiện tại không trùng nhau thì post 2 dữ liệu lên
            if day.day != 0 {
                self.showHud()
                NetworkManager.shareInstance.apiPostMyWeights(myWeightTypeId: 1, startDate: startDateStr, currentDate: startDateStr, currentWeight: preWei!, weekID: 1) { (data, message, isSuccess) in
                    if (isSuccess) {
                        NetworkManager.shareInstance.apiPostMyWeights(myWeightTypeId: 1, startDate:  startDateStr, currentDate: date, currentWeight: crrWei!, weekID: week) { (data, message, isSuccess) in
                            if (isSuccess) {
                                DatabaseManager.sharedManager.putPregnancy(showWeek: nil, babyGender: nil, dueDate: nil, startDate: nil, dateOfBirth: nil, babyAlreadyBorn: nil, weightBeforePregnant: preWei!) { (pregUser, isTemp, isSuccess) in
                                    if (isSuccess == false) {
                                        // put ko thành công lên server
                                        self.alert("Có lỗi xảy ra. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                                            DatabaseManager.sharedManager.logout {}
                                        })
                                    } else {
                                        self.hideHud()
                                        self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)
                                    }
                                }
                            } else {
                                self.hideHud()
                                self.alert("Thêm cân nặng không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                                })
                            }
                        }
                    } else {
                        self.hideHud()
                        self.alert("Thêm cân nặng không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                        })
                    }
                }
            } else {
                // Trường hợp start date trùng với ngày hiện tại -> post 1 lên
                self.showHud()
                NetworkManager.shareInstance.apiPostMyWeights(myWeightTypeId: 1, startDate: startDateStr, currentDate: startDateStr, currentWeight: crrWei!, weekID: week) { (data, message, isSuccess) in
//                    print(message)
                    if (isSuccess) {
                        DatabaseManager.sharedManager.putPregnancy(showWeek: nil, babyGender: nil, dueDate: nil, startDate: nil, dateOfBirth: nil, babyAlreadyBorn: nil, weightBeforePregnant: crrWei!) { (pregUser, isTemp, isSuccess) in
                            if (isSuccess == false) {
                                // put ko thành công lên server
                                self.alert("Có lỗi xảy ra. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                                    DatabaseManager.sharedManager.logout {}
                                })
                            } else {
                                self.hideHud()
                                self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)
                            }
                        }
                    } else {
                        self.hideHud()
                        self.alert("Thêm cân nặng không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                        })
                    }
                }
            }
  
            
        
        } else if checkFirstPush == 2 {
            
            let crrDate = self.dayChoise
            let crrDateStr = formatter.date(from: crrDate)
            let dontCrrDateStr = dateFormatter.string(from: crrDateStr!) // Ngày hiện tại
            
            let crrWeight = btnCurrentWeight.titleLabel?.text // Cân nặng Hiện tại
            let crrWei = Float(crrWeight!)
            
            let component = Calendar.current.dateComponents([.weekOfYear], from: self.start_date, to: crrDateStr!) // Tuần hiện
            let week = component.weekOfYear! + 1
            
            let dataSort = dataMyWeight.sorted { (item1, item2) -> Bool in
                item1.current_date < item2.current_date
            }
            
            if self.dayFromTableWeight == nil { // Khác thì làm như bt
                for i in 0..<dataSort.count {
                    let dayCount = Calendar.current.dateComponents([.day], from: dataSort[i].current_date, to: crrDateStr!)
                    
                    if dayCount.day! < 0 {
                        // post len mang va break
                        self.showHud()
                        NetworkManager.shareInstance.apiPostMyWeights(myWeightTypeId: 1, startDate:  startDateStr, currentDate: dontCrrDateStr, currentWeight: crrWei!, weekID: week) { (data, message, isSuccess) in
                            //                        print(message)
                            if (isSuccess) {
                                self.hideHud()
                                self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)
                            } else {
                                self.hideHud()
                                self.alert("Thêm cân nặng không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                                })
                            }
                        }
                        break
                    } else if dayCount.day == 0 { // Trùng ngày trong mảng -> thay đổi, push lên
                        self.showHud()
                        NetworkManager.shareInstance.apiPutMyWeights(myWeightId: dataSort[i].id, myWeightTypeId: nil, currentDate: nil, curWeight: crrWei) { (data, message, isSuccess) in
                            //                        print("put data")
                            if (isSuccess) {
                                self.hideHud()
                                self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)
                            } else {
                                self.hideHud()
                                self.alert("Thêm cân nặng không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                                })
                            }
                        }
                        break
                    }
                    let dayCheck = Calendar.current.dateComponents([.day], from: dataSort.last!.current_date, to: crrDateStr!)
                    if dayCheck.day! > 0 {
                        self.showHud()
                        NetworkManager.shareInstance.apiPostMyWeights(myWeightTypeId: 1, startDate:  startDateStr, currentDate: dontCrrDateStr, currentWeight: crrWei!, weekID: week) { (data, message, isSuccess) in
                            if (isSuccess) {
                                self.hideHud()
                                self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)
                            } else {
                                self.hideHud()
                                self.alert("Thêm cân nặng không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                                })
                            }
                        }
                        break
                    }
                }
            } else {
                // gửi từ bên table view sang -> update bảng thôi :D
                if let myWeightID = self.myWeightIdFromTableWeight {
                    self.showHud()
                    NetworkManager.shareInstance.apiPutMyWeights(myWeightId: myWeightID, myWeightTypeId: nil, currentDate: dontCrrDateStr, curWeight: crrWei!) { (data, message, isSuccess) in
                        if (isSuccess) {
                            self.hideHud()
                            self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)
                        } else {
                            self.hideHud()
                            self.alert("Chỉnh sửa cân nặng không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                                self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)
                            })
                        }
                    }
                }
                
            }
            
            
        }
        

//            self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)

    }
    

    
    func transferDate(date: Date, startDate : Date) {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        formatter.dateFormat = "EEEE, d MMM yyyy"
        formatter.locale = Locale(identifier: "vi_VI")
        self.dayChoise = formatter.string(from: date)
        
        let month = Calendar.current.dateComponents([.month], from: startDate, to: date)
        
        if let month = month.month {
            if month >= 0 {
                btnStartDate.setTitle(self.dayChoise + " - Tháng thứ \(min(month + 0, 9))", for: .normal)
            } else {
                btnStartDate.setTitle(self.dayChoise, for: .normal)
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return firstWeight.count
        }
        else {
            return secondWeight.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return String(firstWeight[row])
        } else {
            return String(secondWeight[row])
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let selectedRowFirst = uiPickerWeight.selectedRow(inComponent: 0)
        let selectedRowSecond = uiPickerWeight.selectedRow(inComponent: 1)
        let IntegerWei = firstWeight[selectedRowFirst]
        let DecimalWei = secondWeight[selectedRowSecond]
        
        
        if uiPickerWeight.tag == 1 {
            self.preWeightPicker0 = IntegerWei
            self.preWeightPicker1 = DecimalWei
            dataStartWei = Float("\(IntegerWei).\(DecimalWei)")!
            btnPreWeight.setTitle("\(dataStartWei)", for: .normal)
        }
        if uiPickerWeight.tag == 2 {
            self.crrWeightPicker0 = IntegerWei
            self.crrWeightPicker1 = DecimalWei
            dataCurWei = Float("\(IntegerWei).\(DecimalWei)")!
            btnCurrentWeight.setTitle("\(dataCurWei)", for: .normal)
        }
    }
    
    @IBAction func btnShowPreWeight(_ sender: Any) { // Hiển thị picker cân nặng trước khi mang thai để chọn
        
        btnDelete.isHidden = true;
        uiViewWeight.isHidden = false
        uvSelectDate.isHidden = true
        uiPickerWeight.selectRow(self.preWeightPicker0, inComponent: 0, animated: true)
        uiPickerWeight.selectRow(self.preWeightPicker1, inComponent: 1, animated: true)
        uiPickerWeight.tag = 1
    }    
    
    @IBAction func btnShowCurrentWeight(_ sender: Any) { // Hiển thị cân nặng hiện tại
        
        
        btnDelete.isHidden = true;
        uiViewWeight.isHidden = false
        uvSelectDate.isHidden = true
        uiPickerWeight.selectRow(self.crrWeightPicker0, inComponent: 0, animated: true)
        uiPickerWeight.selectRow(self.crrWeightPicker1, inComponent: 1, animated: true)
        uiPickerWeight.tag = 2
    }
    
    @IBAction func btnDisablePicker(_ sender: Any) { 
        uiViewWeight.isHidden = true
        if self.myWeightIdFromTableWeight != nil {
            btnDelete.isHidden = false
        } else {
            btnDelete.isHidden = true
        }
    }
    
    @IBAction func btnShowPickerDate(_ sender: Any) { // Show picker Date
        
        uvSelectDate.isHidden = false
        uiViewWeight.isHidden = true
        pvPickerDate.setDate(self.dayFromTableWeight ?? self.currentDate, animated: true)
    }
    
    @IBAction func btnHideDateView(_ sender: Any) { // Nút để
        uvSelectDate.isHidden = true
    }
    
    
    @IBAction func datePickerChanged(sender: UIDatePicker) {
        transferDate(date: sender.date, startDate: self.start_date)
//        transferDate(date: sender.date)
//        self.dayFromTableWeight = sender.date // Biến này dùng để gửi dữ liệu lên
    }
    
   
    @IBAction func saveMyWeight(_ sender: Any) {

    }
    
    @IBAction func deleteMyWeight(_ sender: Any) {
        // Xóa cân nặng
        if let myWeightIdFromTableWeight = self.myWeightIdFromTableWeight {
            self.showHud()
            NetworkManager.shareInstance.apiDeleteMyWeights(myWeightId: myWeightIdFromTableWeight) {
                (data, message, isSuccess) in
                if (isSuccess) {
                    self.hideHud()
                    self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)
                } else {
                    self.hideHud()
                    self.alert("Có lỗi xảy ra. Xin vui lòng thử lại sau", title: "Thông báo") { (action) in
                        self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)
                    }
                }
                
            }
        } else {
            self.hideHud()
            self.alert("Có lỗi xảy ra. Xin vui lòng thử lại sau", title: "Thông báo") { (action) in
                self.navigationController?.popToViewController(self.parrentAddVC!, animated: true)
            }
        }
        
    }
}


