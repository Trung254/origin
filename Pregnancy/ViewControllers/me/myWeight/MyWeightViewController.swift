//
//  MyWeightViewController.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/17/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import Charts
import RealmSwift

class MyWeightViewController: BaseViewController, ChartViewDelegate {
    @IBOutlet weak var imgRotate: UIImageView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var btnStartWeight: UIButton!
    @IBOutlet weak var btnCurWeight: UIButton!
    @IBOutlet weak var lblChange: UILabel!
    @IBOutlet weak var lblCurWeight: UILabel!
    @IBOutlet weak var imgWeiChange: UIImageView!
    @IBOutlet weak var mlblStartWei: UILabel!
    @IBOutlet weak var lineChartView: LineChartView!
    
    //Setup Monthly Weight Gain
    @IBOutlet weak var month1View: UIView!
    @IBOutlet weak var month2View: UIView!
    @IBOutlet weak var month3View: UIView!
    @IBOutlet weak var month4View: UIView!
    @IBOutlet weak var month5View: UIView!
    @IBOutlet weak var month6View: UIView!
    @IBOutlet weak var month7View: UIView!
    @IBOutlet weak var month8View: UIView!
    @IBOutlet weak var month9View: UIView!
    @IBOutlet weak var m1Gainlbl: UILabel!
    @IBOutlet weak var m2Gainlbl: UILabel!
    @IBOutlet weak var m3Gainlbl: UILabel!
    @IBOutlet weak var m4Gainlbl: UILabel!
    @IBOutlet weak var m5Gainlbl: UILabel!
    @IBOutlet weak var m6Gainlbl: UILabel!
    @IBOutlet weak var m7Gainlbl: UILabel!
    @IBOutlet weak var m8Gainlbl: UILabel!
    @IBOutlet weak var m9Gainlbl: UILabel!
    @IBOutlet weak var subViewM1: UIView!
    @IBOutlet weak var subViewM2: UIView!
    @IBOutlet weak var subViewM3: UIView!
    @IBOutlet weak var subViewM4: UIView!
    @IBOutlet weak var subViewM5: UIView!
    @IBOutlet weak var subViewM6: UIView!
    @IBOutlet weak var subViewM7: UIView!
    @IBOutlet weak var subViewM8: UIView!
    @IBOutlet weak var subViewM9: UIView!
    @IBOutlet weak var mCurrentweight: UILabel!
    
    var weightUnit: Int?
    var userId: String?
    var startWeight: Float?
    var currentWeight: Float?
    var dataSettingUser: [JSON] = []
    var ratioWei: Float?
    var unitString: String?
    
    var checkType = 1
    var dataMyWeight : [PregMyWeight] = []
    var monthViews : Array<UIView>!
    var subMonthViews : Array<UIView>!
    var arrLblGain : Array<UILabel>!

    var dataWeightHard : Array<Float> = Array(repeating: 0, count: 9)
    
    var weightGainPerMonth : Array<Float> = Array(repeating: 0, count: 9)
    //--------------------------------------

    var checkDataFromToday = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitle(title: "Cân Nặng")
        
        if self.checkDataFromToday == true {
            self.checkDataFromToday = false
            self.secondView.isHidden = false
            self.firstView.isHidden = true
        }
        
        
        self.lineChartView.delegate = self
        
        self.imgRotate.transform = CGAffineTransform(rotationAngle: (-10.0 * .pi) / 180.0)
        self.imgRotate.layer.borderWidth = 10
        self.imgRotate.layer.borderColor = (UIColor.white.cgColor)
        self.secondView.isHidden = false
        self.userId = "9"
        self.setupLineChart()
        self.setupWeightGainView()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (self.monthViews == nil) {
            self.monthViews = [month1View, month2View, month3View, month4View, month5View, month6View, month7View, month8View, month9View]
        }
        
        if (self.subMonthViews == nil) {
            self.subMonthViews = [subViewM1, subViewM2, subViewM3, subViewM4, subViewM5, subViewM6, subViewM7, subViewM8, subViewM9]
        }
        
        for subView in self.subMonthViews {
            subView.translatesAutoresizingMaskIntoConstraints = false
            subView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        if (self.arrLblGain == nil) {
            self.arrLblGain = [m1Gainlbl, m2Gainlbl, m3Gainlbl, m4Gainlbl, m5Gainlbl, m6Gainlbl, m7Gainlbl, m8Gainlbl, m9Gainlbl]
        }
        for lbl in self.arrLblGain {
            lbl.font = UIFont.systemFont(ofSize: 11)
        }
        
        if IS_IPHONE_5 {
            for lbl in self.arrLblGain {
                lbl.font = UIFont.systemFont(ofSize: 9)
            }
        }

        self.LoadDataFromApi()

    }
    override func setBackButton() {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "LeftArrow"), for: .normal)
        button.addTarget(self, action: #selector(doDefaultBack), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        let infoButton = UIButton(type: .custom)
        infoButton.setImage(UIImage(named: "info (1)"), for: .normal)
        infoButton.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
        infoButton.addTarget(self, action: #selector(doDefaultInfo), for: .touchUpInside)
        let infoBarButton = UIBarButtonItem(customView: infoButton)
        
        
        let addWeightButton = UIButton(type: .custom)
        addWeightButton.setImage(UIImage(named: "add"), for: .normal)
        addWeightButton.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
        addWeightButton.addTarget(self, action: #selector(doAddWeight), for: .touchUpInside)
        let addWeightBarButton = UIBarButtonItem(customView: addWeightButton)
        return [addWeightBarButton, infoBarButton]
    }
    @objc func doAddWeight(_ sender : Any?) -> Void {
        performSegue(withIdentifier: "weight_to_addWeight", sender: self)
    }
    
    //Bo tròn hiển thị chart dưới
    private func setupWeightGainView() {
        self.month1View.layer.cornerRadius = 10
        self.month2View.layer.cornerRadius = 10
        self.month3View.layer.cornerRadius = 10
        self.month4View.layer.cornerRadius = 10
        self.month5View.layer.cornerRadius = 10
        self.month6View.layer.cornerRadius = 10
        self.month7View.layer.cornerRadius = 10
        self.month8View.layer.cornerRadius = 10
        self.month9View.layer.cornerRadius = 10

    }
    
    // SetUP hiển thị Chart
    private func setupLineChart() {
        lineChartView.highlightPerTapEnabled = false
        lineChartView.highlightPerDragEnabled = false
        lineChartView.dragEnabled = false
        lineChartView.setScaleEnabled(false)
        lineChartView.pinchZoomEnabled = false
        lineChartView.xAxis.enabled = false
        lineChartView.xAxis.axisMaximum = 8 // Date from First Date
        lineChartView.xAxis.axisMinimum = 0
        lineChartView.leftAxis.enabled = false
        lineChartView.rightAxis.enabled = false
        lineChartView.legend.enabled = false
    }

    // Set dữ liệu cho Chart - đầu ra là mảng các giá trị (x,y)
    func setDataMePush(dataWei: [PregMyWeight]) -> [ChartDataEntry] {

        var startDate = Date()
        
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTampla, isSuccess, rawData) in
            if(isSuccess) {
                startDate = (pregnancy?.start_date)!
            } else {
                if dataWei.first?.start_date != nil {
                    startDate = (dataWei.first?.start_date)!
                }
            }
        }
        

        let dataSort = dataWei.sorted { (item1, item2) -> Bool in
            item1.current_date < item2.current_date
        }
        
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        
        let dateStart = calendar?.date(byAdding: .day, value: -1, to: startDate, options: .init(rawValue: 0))
        
        let endMonth1 = calendar?.date(byAdding: .day, value: 30, to: dateStart!, options: .init(rawValue: 0))
        let endMonth2 = calendar?.date(byAdding: .day, value: 30, to: endMonth1!, options: .init(rawValue: 0))
        let endMonth3 = calendar?.date(byAdding: .day, value: 30, to: endMonth2!, options: .init(rawValue: 0))
        let endMonth4 = calendar?.date(byAdding: .day, value: 30, to: endMonth3!, options: .init(rawValue: 0))
        let endMonth5 = calendar?.date(byAdding: .day, value: 30, to: endMonth4!, options: .init(rawValue: 0))
        let endMonth6 = calendar?.date(byAdding: .day, value: 30, to: endMonth5!, options: .init(rawValue: 0))
        let endMonth7 = calendar?.date(byAdding: .day, value: 30, to: endMonth6!, options: .init(rawValue: 0))
        let endMonth8 = calendar?.date(byAdding: .day, value: 30, to: endMonth7!, options: .init(rawValue: 0))
        let endMonth9 = calendar?.date(byAdding: .day, value: 42, to: endMonth8!, options: .init(rawValue: 0))
        var wM1 : [Float] = []
        var wM2 : [Float] = []
        var wM3 : [Float] = []
        var wM4 : [Float] = []
        var wM5 : [Float] = []
        var wM6 : [Float] = []
        var wM7 : [Float] = []
        var wM8 : [Float] = []
        var wM9 : [Float] = []
        
        for i in 0..<dataSort.count {
            if (dateStart! <= dataSort[i].current_date) && (dataSort[i].current_date < endMonth1!) {
                wM1.append(dataSort[i].current_weight)
            } else {
                if (endMonth1! <= dataSort[i].current_date) && (dataSort[i].current_date < endMonth2!) {
                    wM2.append(dataSort[i].current_weight)
                } else {
                    if (endMonth2! <= dataSort[i].current_date) && (dataSort[i].current_date < endMonth3!) {
                        wM3.append(dataSort[i].current_weight)
                    } else {
                        if (endMonth3! <= dataSort[i].current_date) && (dataSort[i].current_date < endMonth4!) {
                            wM4.append(dataSort[i].current_weight)
                        } else {
                            if (endMonth4! <= dataSort[i].current_date) && (dataSort[i].current_date < endMonth5!) {
                                wM5.append(dataSort[i].current_weight)
                            } else {
                                if (endMonth5! <= dataSort[i].current_date) && (dataSort[i].current_date < endMonth6!) {
                                    wM6.append(dataSort[i].current_weight)
                                } else {
                                    if (endMonth6! <= dataSort[i].current_date) && (dataSort[i].current_date < endMonth7!) {
                                        wM7.append(dataSort[i].current_weight)
                                    } else {
                                        if (endMonth7! <= dataSort[i].current_date) && (dataSort[i].current_date < endMonth8!) {
                                            wM8.append(dataSort[i].current_weight)
                                        } else {
                                            if (endMonth8! <= dataSort[i].current_date) && (dataSort[i].current_date <= endMonth9!) {
                                                wM9.append(dataSort[i].current_weight)
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if wM1.count != 0 {
            dataWeightHard[0] = wM1.last!
        } else {
            wM1.append(0)
            dataWeightHard[0] = 0 //Check
        }
        
        dataWeightHard[1] = 0.0
        if wM2.count != 0 {
            dataWeightHard[1] = wM2.last!
        }
        
        dataWeightHard[2] = 0.0
        if wM3.count != 0 {
            dataWeightHard[2] = wM3.last!
        }
        
        dataWeightHard[3] = 0.0
        if wM4.count != 0 {
            dataWeightHard[3] = wM4.last!
        }
        
        dataWeightHard[4] = 0.0
        if wM5.count != 0 {
            dataWeightHard[4] = wM5.last!
        }
        
        dataWeightHard[5] = 0.0
        if wM6.count != 0 {
            dataWeightHard[5] = wM6.last!
        }
        
        dataWeightHard[6] = 0.0
        if wM7.count != 0 {
            dataWeightHard[6] = wM7.last!
        }
        
        dataWeightHard[7] = 0.0
        if wM8.count != 0 {
            dataWeightHard[7] = wM8.last!
        }
        
        dataWeightHard[8] = 0.0
        if wM9.count != 0 {
            dataWeightHard[8] = wM9.last!
        }
//        print(dataWeightHard)
        
        
        
        
        var arrayWeight : Array<Array<Float>> = []
        arrayWeight.append(contentsOf: [wM1, wM2, wM3, wM4, wM5, wM6, wM7, wM8, wM9])
        
//        print(arrayWeight)
        
        // Caculate Weight Gain Per Month
        if arrayWeight[0].count == 0 {
            arrayWeight[0][0] = 0
        }
    
        var check : Float = 0
        
        if arrayWeight[0].count > 1  {
            weightGainPerMonth[0] = arrayWeight[0].last! - arrayWeight[0].first!
            check = arrayWeight[0].last!
        } else {
            
            if arrayWeight[0][0] != 0 {
                weightGainPerMonth[0] = 0
                check = arrayWeight[0][0]
            } else {
                weightGainPerMonth[0] = 0
                for i in 1..<arrayWeight.count {
                    if arrayWeight[i].isEmpty == false {
                        check = arrayWeight[i].last!
                        break
                    }
                }
            }
        }
        
        for i in 1..<arrayWeight.count {
            if arrayWeight[i].isEmpty == false {
                weightGainPerMonth[i] = arrayWeight[i].last! - check
                check = arrayWeight[i].last!
            }
        }

        var chartData : [ChartDataEntry] = []
        for i in 0..<(dataWeightHard.count) {
            if dataWeightHard[i] != 0 {
                chartData.append(ChartDataEntry(x: Double(i), y: Double(dataWeightHard[i])))
            }
        }
        return chartData
    }
    
    // Dựng view Chart từ Dữ liệu ChartDataEntry được tạo
    func DrawChart(dataEntry : [ChartDataEntry]) {
        
        for i in 0..<self.arrLblGain.count {
            if self.weightGainPerMonth[i] > 0 {
                self.arrLblGain[i].text = String(format: "%.1f", self.weightGainPerMonth[i])
                self.arrLblGain[i].textColor = .white
            } else if self.weightGainPerMonth[i] < 0 {
                self.arrLblGain[i].text = String(format: "%.1f", self.weightGainPerMonth[i])
                self.arrLblGain[i].textColor = .red
            } else {
                self.arrLblGain[i].textColor = .white
                self.arrLblGain[i].text = "T\(i+1)"
            }
        }
        // Set Ratio Weight Gain per mont month1View ung voi m1Gainlbl
            var ratioView : Array<Float> = Array(repeating: 0, count: 9)
        for i in 0..<self.weightGainPerMonth.count {
            if weightGainPerMonth[i] != 0 {
            let ratio = Float(weightGainPerMonth[i]) / Float(weightGainPerMonth.max()!)
            ratioView[i] = ratio
            }
        }
        
        for i in 0..<self.monthViews.count {
            if ratioView[i] > 0 {
                DispatchQueue.main.async {
                    self.subMonthViews[i].translatesAutoresizingMaskIntoConstraints = false
                    self.subMonthViews[i].heightAnchor.constraint(equalTo: self.monthViews[i].heightAnchor, multiplier: CGFloat(ratioView[i])).isActive = true
                }
            } else {
                DispatchQueue.main.async {
                    self.subMonthViews[i].translatesAutoresizingMaskIntoConstraints = false
                    self.subMonthViews[i].heightAnchor.constraint(equalToConstant: CGFloat(0))
                }
            }
        }
        
        let set1 = LineChartDataSet(values: dataEntry, label: "Cân Nặng")
        set1.axisDependency = .left
        set1.setColor(UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1))
        set1.setCircleColor(.white)
        set1.lineWidth = 3
        set1.circleRadius = 3
        set1.fillAlpha = 0.6
        set1.fillColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        set1.highlightColor = UIColor(red: 244/255, green: 117/255, blue: 117/255, alpha: 1)
        set1.drawCircleHoleEnabled = false
        
        let data = LineChartData(dataSet: set1)
        lineChartView.data = nil
        lineChartView.data = data
        if let data = lineChartView.data {
            for set in data.dataSets {
                if let set = set as? LineChartDataSet {
                    set.drawValuesEnabled = !set.drawValuesEnabled
                    set.drawFilledEnabled = !set.drawFilledEnabled
                    set.drawCirclesEnabled = !set.drawCirclesEnabled
                }
                
            }
        }
    }
    
    
    private func LoadDataFromApi(){
        
        self.showHud()
        DispatchQueue.delay(.milliseconds(700)) {
            NetworkManager.shareInstance.apiGetAllMyWeights { (data, message, isSuccess) in
                if(isSuccess) {
                    DatabaseManager.sharedManager.getPregnancys(callBack: { (pregnancy, isTempla, isSuccess, rawData) in
                        if (isSuccess) {
                            if let pregnancy = pregnancy {
                                let resutl = JSON(data)
                                self.dataMyWeight.removeAll()
                                
                                for items in resutl.arrayValue {
                                    let item = PregMyWeight()
                                    item.my_weight_type_id = items["my_weight_type_id"].intValue
                                    item.current_weight = items["current_weight"].floatValue
                                    item.week_id = items["week_id"].intValue
                                    item.id = items["id"].intValue
                                    
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
                                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                                    
                                    item.start_date = pregnancy.start_date
                                    
                                    let crrDate = items["current_date"].stringValue
                                    if crrDate.isEmpty == false {
                                        if let date = dateFormatter.date(from: crrDate) {
                                            if(date.compare(pregnancy.due_date.addingTimeInterval(86399)) == .orderedDescending ||
                                                date.compare(pregnancy.start_date.addingTimeInterval(-86399)) == .orderedAscending){
                                                continue
                                            }
                                            item.current_date  = date
                                        }
                                    }
                                    self.dataMyWeight.append(item)
                                }
                                
                                self.dataMyWeight.sort(by: { (weight, nextWeight) -> Bool in
                                    return weight.current_date.compare(nextWeight.current_date) != .orderedDescending
                                })
                                if (self.dataMyWeight.count > 0) {
                                    // Setup View
                                    self.mlblStartWei.text = self.dateToString(date: (self.dataMyWeight.first?.current_date)!)
                                    
                                    //Lay ra can nang ngay hien tai
                                    
                                    self.dataMyWeight = self.dataMyWeight.sorted(by: { (weight, nextWeight) -> Bool in
                                        return weight.current_date.compare(nextWeight.current_date) != .orderedAscending
                                    })
                                    
                                    self.lblCurWeight.text = self.dateToString(date: (self.dataMyWeight.last?.current_date)!)
                                    self.firstView.isHidden = true
                                    self.secondView.isHidden = false
                                    
                                    if self.dataMyWeight.last?.my_weight_type_id != nil {
                                        self.checkType = (self.dataMyWeight.last?.my_weight_type_id)!
                                    } else {
                                        self.checkType = 1
                                    }
                                    // Check xem ngày nhỏ nhất với start date, nếu ngày nhỏ nhất bằng ngày hiện tại thì check xem cân nặng có bằng nhau ko. Nếu ko bằng thì bắn lên
                                    if let data = self.dataMyWeight.last {
                                        let dayCheck = Calendar.current.dateComponents([.day], from: pregnancy.start_date, to: data.current_date)
                                        
                                        if dayCheck.day == 0 {
                                            if pregnancy.weight_before_pregnant != data.current_weight {
                                                DatabaseManager.sharedManager.putPregnancy(showWeek: nil, babyGender: nil, dueDate: nil, startDate: nil, dateOfBirth: nil, babyAlreadyBorn: nil, weightBeforePregnant: data.current_weight) { (pregUser, isTemp, isSuccess) in
                                                }
                                            }
                                        }
                                    }
                                    
                                    let startWeight = self.dataMyWeight.last?.current_weight
                                    let curentWeight = self.dataMyWeight.first?.current_weight
                                    
                                    self.mlblStartWei.text = self.dateToString(date: (self.dataMyWeight.last?.current_date)!)
                                    
                                    self.lblCurWeight.text = self.dateToString(date: (self.dataMyWeight.first?.current_date)!)
                                    
                                    let cur_weight = self.dataMyWeight.first?.current_weight ?? 0
                                    if cur_weight == 50.0 {
                                        self.mCurrentweight.text = "50 kg"
                                    } else {
                                        self.mCurrentweight.text = String(format: "%.1f kg", self.dataMyWeight.first?.current_weight ?? 0)
                                    }
                                    
                                    
                                    let weightChange = curentWeight! - startWeight!
                                    
                                    switch self.checkType {
                                    case 2:
                                        let startWeiLbs = startWeight! *  2.2046
                                        
                                        self.btnStartWeight.setTitle(String(format: "%.1f", startWeiLbs) + " lbs", for: .normal)
                                        
                                        let endWeightlbs = curentWeight! * 2.2046
                                        self.btnCurWeight.setTitle(String(format: "%.1f", endWeightlbs) + " lbs", for: .normal)
                                        
                                        let weightChangeLbs = weightChange * 2.2046
                                        self.lblChange.text = String(format: "%.1f", weightChangeLbs) + " lbs"
                                        
                                        if weightChange > 0.0 {
                                            self.imgWeiChange.image = UIImage(named: "arrow-top-right")
                                        } else if weightChange < 0.0 {
                                            self.imgWeiChange.image = UIImage(named: "arrow-down-right")
                                        }
                                        
                                        break
                                    case 3:
                                        let startWeiSt = startWeight! *  0.15747
                                        
                                        self.btnStartWeight.setTitle(String(format: "%.1f", startWeiSt) + " st", for: .normal)
                                        
                                        let endWeightSt = curentWeight! * 0.15747
                                        self.btnCurWeight.setTitle(String(format: "%.1f", endWeightSt) + " st", for: .normal)
                                        
                                        let weightChangeLbs = weightChange * 0.15747
                                        self.lblChange.text = String(format: "%.1f", weightChangeLbs) + " st"
                                        if weightChange > 0.0 {
                                            self.imgWeiChange.image = UIImage(named: "arrow-top-right")
                                        } else if weightChange < 0.0 {
                                            self.imgWeiChange.image = UIImage(named: "arrow-down-right")
                                        }
                                        break
                                    default:
                                        
                                        if let startWeight = startWeight {
                                            self.btnStartWeight.setTitle(String(format: "%.1f", startWeight) + " Kg", for: .normal)
                                        }
                                        if let curentWeight = curentWeight {
                                            self.btnCurWeight.setTitle(String(format: "%.1f", curentWeight) + " Kg", for: .normal)
                                            
                                        }
                                        self.imgWeiChange.image = nil
                                        
                                        self.lblChange.text = String(format: "%.1f", weightChange) + " Kg"
                                        if weightChange > 0.0 {
                                            self.imgWeiChange.image = UIImage(named: "arrow-top-right")
                                        } else if weightChange < 0.0 {
                                            self.imgWeiChange.image = UIImage(named: "arrow-down-right")
                                        }
                                        break
                                    }
                                    
                                    // Setup Chart
                                    DispatchQueue.main.async {
                                        
                                        let dataCharEntry = self.setDataMePush(dataWei: self.dataMyWeight)
                                        self.DrawChart(dataEntry: dataCharEntry)
                                    }
                                }
                                self.secondView.isHidden = self.dataMyWeight.count == 0
                                self.firstView.isHidden = !self.secondView.isHidden
                                
                            }
                        } else {
                            self.alert("Không lấy được dữ liệu người dùng. Xin vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    })
                    
                } else {
                    DispatchQueue.main.async {
                        self.secondView.isHidden = true
                        self.firstView.isHidden = false
                    }
                }
                self.hideHud()
            }
        }
    }
    private func convertDate(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "vi_VI")
        let dateString = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "d-MMM-yyyy"
        return dateFormatter.string(from: dateString!)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        

        if segue.identifier == "weight_to_addWeight" {
            let vc = segue.destination as? AddWeightViewController
            vc?.parrentAddVC = self
        }

        if segue.identifier == "myweight_to_allweight" {
            let vc = segue.destination as? WeightEntryViewController
            vc?.parrentEntryVC = self
        }
    }
    
    @IBAction func showAddWeight(_ sender: Any) {
        performSegue(withIdentifier: "weight_to_addWeight", sender: self)
    }

}

extension MyWeightViewController {
    func stringToDate(dateString : String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: dateString)!
    }
    
    func dateToString(date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: date)
    }
}

extension DispatchQueue {
    static func delay(_ delay: DispatchTimeInterval, closure: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: closure)
    }
}

