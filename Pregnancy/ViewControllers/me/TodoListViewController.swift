//
//  TodoListViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 06/12/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

struct TodoList {
    var id: Int
    var content: String
    var status: Bool
    var week: Int
    var day: Int
    
    init(id: Int, content: String, status: Bool, week: Int, day: Int) {
        self.id = id
        self.content = content
        self.status = status
        self.week = week
        self.day = day
    }
}

class TodoListViewCell: UITableViewCell {
    @IBOutlet weak var mTodoName: UILabel!
    @IBOutlet weak var mStatusImage: UIImageView!
}

var arrListTodoAdded:[TodoList] = []
var dataTodo = TodoList(id: -1,content: "",status: false,week: 0,day: 0)

class TodoListViewController: BaseViewController {

    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet var mViewTodoList: UIView!
    @IBOutlet weak var mtblTodo: UITableView!
    
    var dataUserTodos: [JSON] = []
    var dataTodos: [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addBackgroundImage("me-background-blur.png")
        let shareIcon = UIImage(named: "share")
        addIconToButton(btn: shareBtn , img: shareIcon!)
        shareBtn.setTitle("Xuất sang email", for: .normal)
        shareBtn.contentHorizontalAlignment = .left
        shareBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 60, bottom: 0, right: 0)
//        mtblTodo.addBackground(imageName: "me-background-blur")
        mtblTodo.tableFooterView = UIView()
        mtblTodo.estimatedRowHeight = 60
        mtblTodo.rowHeight = UITableView.automaticDimension
        self.mIsShowRightButton = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if self.dataTodos.isEmpty == false {
            self.dataTodos.removeAll()
        }
        self.loadDataTodos()
    }
    
    @IBAction func backTodoListViewConTroller(segue:UIStoryboardSegue){
        print("press back")
    }
    
    @IBAction func saveTodoList(segue:UIStoryboardSegue){
        if segue.identifier == "saveBabyPhoneSegue" {
//            let AddPhoneViewController = segue.source as! AddPhoneViewController
//            let NewName = AddPhoneViewController.lblBabyAddName.text
//            let NewPhone = AddPhoneViewController.lblBabyAddPhone.text
            
            // Validate name and phone field
           
        }
    }
    
    @IBAction func exportEmail(_ sender: Any) {
        var shareText = "Danh sách của tôi \n"
        var check = 0
        
        for item in dataTodos {
            for tduser in dataUserTodos {
                if item["id"].intValue == tduser["todo_id"].intValue {
                    if tduser["status"].intValue == 0 {
                        shareText += "\n-\(item["title"].stringValue)"
                        check = 1
                    }
                }
            }
        }
        
        if check == 0 {
            shareText += "\n Không có dữ liệu"
        }
        
        let email = EmailProvider(emailSubject: "Danh sách việc cần làm", emailContent: shareText)
        
        let vc = UIActivityViewController(activityItems: [email], applicationActivities: [])
        vc.setValue("Danh sách việc cần làm", forKey: "Subject")
        present(vc, animated: true, completion: nil)
    }
    
    func addIconToButton(btn: UIButton, img : UIImage){
        let imgQuestionView = UIImageView(frame: CGRect(x: 15, y: 15, width: 30, height: 30))
        imgQuestionView.image = img
        btn.addSubview(imgQuestionView)
    }
    
    private func loadDataTodos() {
        NetworkManager.shareInstance.apiGetAllToDos() {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    self.dataTodos.append(item)
                }
            }
            
            DispatchQueue.main.async {
                self.loadUserTodos()
            }
        }
    }
    
    private func loadUserTodos() {
        self.showHud()
        NetworkManager.shareInstance.apiGetUserToDos() {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                var arr: Array<JSON> = []
                for item in data.arrayValue {
                    arr.append(item)
                }
                
                self.dataUserTodos = arr
                DispatchQueue.main.async {
                    self.mtblTodo.reloadData()
                }
                
            } else {
                self.dataUserTodos = []
                DispatchQueue.main.async {
                    self.mtblTodo.reloadData()
                }
            }
            
            self.hideHud()
        }
    }
    
    private func updateDataUserTodos(_ todoId: Int,_ status: Int) {
        NetworkManager.shareInstance.apiPutUserToDos(todoId: todoId, status: status) {
            (data, message, isSuccess) in
            self.loadUserTodos()
        }
    }
    
    private func deleteDataTodos(_ todoId: Int) {
        NetworkManager.shareInstance.apiDeleteToDos(todoId: todoId) {
            (data, message, isSuccess) in
            DispatchQueue.main.async {
                self.loadUserTodos()
            }
        }
    }
    
    private func deleteDataUserTodos(_ todoId: Int) {
        NetworkManager.shareInstance.apiDeleteUserToDos(todoId: todoId) {
            (data, message, isSuccess) in
            
            DispatchQueue.main.async {
                self.loadUserTodos()
            }
        }
    }
}

extension TodoListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataUserTodos.count != 0 {
            return dataUserTodos.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoListViewCell", for: indexPath) as! TodoListViewCell

        for item in dataTodos {
            if item["id"].intValue == dataUserTodos[indexPath.row]["todo_id"].intValue {
                cell.mTodoName.text = item["title"].stringValue
//                cell.mTodoName.sizeToFit()
                cell.mStatusImage.image = UIImage(named: "untick")
                if dataUserTodos[indexPath.row]["status"].intValue == 1 {
                    cell.mStatusImage.image = UIImage(named: "tick-3")
                }
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < dataUserTodos.count {
            if dataUserTodos[indexPath.row]["status"].intValue == 0 {
                self.updateDataUserTodos(dataUserTodos[indexPath.row]["todo_id"].intValue, 1)
            }
            else {
                self.updateDataUserTodos(dataUserTodos[indexPath.row]["todo_id"].intValue, 0)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let todoId = dataUserTodos[indexPath.row]["todo_id"].intValue
            for item in dataTodos {
                if item["id"].intValue == todoId && item["custom_task_by_user_id"].intValue != 0 {
                    self.deleteDataTodos(todoId)
                }
            }
            self.deleteDataUserTodos(todoId)
        }
    }
}

class EmailProvider: NSObject, UIActivityItemSource {
    
    var emailSubject: String!
    var emailContent: String!
    
    init(emailSubject: String, emailContent: String) {
        self.emailSubject = emailSubject
        self.emailContent = emailContent
    }
    
//    init(emailSubject: String, emailContent: String) {
//        self.emailSubject = emailSubject
//        self.emailContent = emailContent
//        super.init(placeholderItem: "")
//    }
    
//    override var item: Any {
//        get {
//            return self.emailContent as Any
//        }
//    }
    
//    override func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
//        if activityType == UIActivity.ActivityType.mail {
//            return emailSubject
//        }
//
//        return ""
//    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return self.emailContent
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return self.emailContent
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return self.emailSubject
    }
    
}
