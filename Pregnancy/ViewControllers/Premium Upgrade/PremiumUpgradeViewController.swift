//
//  PremiumUpgradeViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 07/01/2019.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

struct DataPremiumUpgrade {
    var img = String()
    var lbl = String()
    var subLbl = String()
}

class PremiumUpgradeViewController: BaseViewController {

    @IBOutlet weak var mHeightTableViewPrenium: NSLayoutConstraint!
    @IBOutlet weak var mTableViewPremium: UITableView!
    
    var DataPremiumUpgradeArray : [DataPremiumUpgrade] = [
        DataPremiumUpgrade(img: "", lbl: "Michelle B.", subLbl: "This is the best pregnancy app I've tried and is well worth the upgrade. I love the pictures and this has the most useful info and tools. Love it!"),
        DataPremiumUpgrade(img: "", lbl: "Isabella C.", subLbl: "I have about 5 downloaded. This is my go to app"),
        DataPremiumUpgrade(img: "", lbl: "Susan L.", subLbl: "I curently have six Pregnancy apps and this one is my favorite :) :) The images it lets you see of your baby is incredible and so realistic! Thanks."),
        DataPremiumUpgrade(img: "", lbl: "Pinky", subLbl: "Super easy to navigate, great articles and pictures of baby's growth and development in utero. Paying for it was well worth it! Thank you!"),
        DataPremiumUpgrade(img: "", lbl: "Marsha K.", subLbl: "Having my 3rd baby and I've used this app all 3 times"),
        DataPremiumUpgrade(img: "", lbl: "Rocketgirl", subLbl: "Didn't mind paying for it. It's a really nice app."),
        DataPremiumUpgrade(img: "", lbl: "Vince M.", subLbl: "Being a male it has helped me again knowledge on different areas of my wife's pregnancy. This app does an amazing job. Keep up the great work. Guys download this!!!"),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUIs()
        // Do any additional setup after loading the view.
    }
    
    private func setupUIs() {
        setupTableView ()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
    }
    
    private func setupTableView () {
        let nib = UINib(nibName: "PremiumUpgradeTableViewCell", bundle: Bundle.main)
        mTableViewPremium.register(nib, forCellReuseIdentifier: "CellPremiumUpgrade")
        mTableViewPremium.delegate = self
        mTableViewPremium.dataSource = self
        self.mTableViewPremium.rowHeight = UITableView.automaticDimension
        self.mTableViewPremium.estimatedRowHeight = 120
    }
}

extension PremiumUpgradeViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPremiumUpgrade", for: indexPath) as! PremiumUpgradeTableViewCell
        cell.mImagePremium.image = UIImage(named: DataPremiumUpgradeArray[indexPath.row].img)
        cell.mLblPremium.text = DataPremiumUpgradeArray[indexPath.row].lbl
        cell.mSubLblPremium.text = DataPremiumUpgradeArray[indexPath.row].subLbl
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == tableView.indexPathsForVisibleRows?.last?.row) {
            mHeightTableViewPrenium.constant = tableView.contentSize.height
        }
    }
}

extension PremiumUpgradeViewController : UITableViewDelegate {
    
}
