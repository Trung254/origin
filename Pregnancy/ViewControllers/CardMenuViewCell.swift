//
//  CardMenuViewCell.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/12/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class CardMenuViewCell: UICollectionViewCell {

    @IBOutlet weak var mCollectionImage: UIImageView!
    @IBOutlet weak var mCollectionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mCollectionLabel.font = UIFont.systemFont(ofSize: 16)
        
        if IS_IPHONE_5 {
            mCollectionLabel.font = UIFont.systemFont(ofSize: 12)
        }
        if IS_IPHONE_6 {
            mCollectionLabel.font = UIFont.systemFont(ofSize: 14)
        }
    }

}
