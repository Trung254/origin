//
//  VideoViewController.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/11/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class VideoViewController: BaseViewController {

    @IBOutlet weak var mBottomInfoView: UIView!
    @IBOutlet weak var mScrollView: UIScrollView!
    @IBOutlet weak var mBackgroundScrollImage: UIImageView!
    @IBOutlet weak var mControlButton: UIButton!
    @IBOutlet weak var mDistanceControl: NSLayoutConstraint!
    
    @IBOutlet weak var mContentView: UIView!
    
    @IBOutlet weak var parentContenView: UIView!
    
    @IBOutlet weak var mBackImage: WeekImageView!
    var mBackImageWidth: NSLayoutConstraint!
    var mBackImageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mMidImage: WeekImageView!
    var mMidImageWidth: NSLayoutConstraint!
    var mMidImageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mForeImage: WeekImageView!
    var mForeImageWidth: NSLayoutConstraint!
    var mForeImageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mLenghtLbl: UILabel!
    @IBOutlet weak var mWeightLbl: UILabel!
    @IBOutlet weak var mWeekLbl: UILabel!
    @IBOutlet weak var mBackgoundLoad: UIView!
    @IBOutlet weak var mSliderChoiseWeek: UISlider!
    
    var mWeeks: Array<WeekInfo> = []
    var mCurrentWeek:Int = 1
    var mWeekSave : Int = 1
    var mSlideTimer: Timer?
    var centerPoint : CGPoint!
    
    var check = false
    
    var tapUiSlider : UITapGestureRecognizer! // Thêm tap vào uislider
    
//    var mySizeGuidesItems : [PregSizeGuide] = [PregSizeGuide]()
    
    var secondsFromGMT: Int { return TimeZone.current.secondsFromGMT() }
    
    let currentTimeDate = Date.init().addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mIsShowBackButton = false
        self.setLeftMainMenu()

        let img = UIImage(color: UIColor.clear, size: CGSize(width: 4, height: 4))
        self.mSliderChoiseWeek.setThumbImage(img, for: .normal)
        
        // Do any additional setup after loading the view.
        self.check = true
        self.mSliderChoiseWeek.isHidden = true
        
        for i in 0..<42 {
            
            let fore = UIImage(named: String(format: "%dw", i+1))
            
            var foreScale: ImageScale
            if(i == 0) {
                foreScale = ImageScale(scaleWidth: 1, scaleHeight: 1)
            } else {
                foreScale = ImageScale(scaleWidth: 1, scaleHeight: 1)
            }
            let week = WeekInfo.init(background: nil,
                                     backImageScale: nil,
                                     midImage: nil,
                                     midImageScale: nil,
                                     foreImage: fore,
                                     foreImageScale: foreScale,
                                     lenghtString: String(i+1),
                                     weightString: String(i+1),
                                     weekString: String(i+1))
            self.mWeeks.append(week)
            
        }
        self.loadSizeBaby()
        self.viewWillLayoutSubviews()
//        self.setupScrollView()
        self.mSliderChoiseWeek.minimumValue = roundf(1)
        self.mSliderChoiseWeek.maximumValue = roundf(42)
        self.mSliderChoiseWeek.addTarget(self, action: #selector(changeWeek), for: .valueChanged)
        
        self.tapUiSlider = UITapGestureRecognizer(target: self, action: #selector(sliderTapped(gestureRecognizer:)))
        self.mSliderChoiseWeek.addGestureRecognizer(self.tapUiSlider)
    }
    
    override func handleLeftMenu() {
        self.showMainMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if #available(iOS 11.0, *),
            Float((UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!) > 0 {
            self.mDistanceControl.constant = 45
        }
        
        self.checkConnection()
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        
        if check == false {
            
            DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
                if let pregnancy = pregnancy {
                    
                    if pregnancy.baby_already_born == 1 {
                        self.mCurrentWeek = 1
                        self.mWeekSave = 1
                    } else {
                        let day = Calendar.current.dateComponents([ .day], from: pregnancy.start_date, to: self.currentTimeDate)
                        
                        var week = (day.day ?? 0) / 7
                        
                        if (pregnancy.show_week == 1){
                            week = (day.day ?? 0) / 7 + 1
                        }
                        
                        if week == 0 {
                            week = 1
                            self.mCurrentWeek = week
                            self.mWeekSave = week
                        } else if week > 42 {
                            week = 42
                            self.mCurrentWeek = week
                            self.mWeekSave = week
                        } else {
                            self.mCurrentWeek = week
                            self.mWeekSave = week
                        }
                    }
                    //                print(self.mCurrentWeek)
                    self.mSliderChoiseWeek.value = Float(self.mWeekSave)
                    
                    self.loadWeek(week: self.mCurrentWeek)
                    let centerOffsetX = (self.mBackgroundScrollImage.width - self.mScrollView.width) / 2
                    let centerOffsetY = (self.mBackgroundScrollImage.height - self.mScrollView.height) / 2
                    self.centerPoint = CGPoint(x: centerOffsetX, y: centerOffsetY)
                    
                    self.mScrollView.contentSize = CGSize(width: self.mBackgroundScrollImage.size.width - 80, height: self.mBackgroundScrollImage.size.height - 80)
                    self.mScrollView.setContentOffset(self.centerPoint, animated: true)
                    
                    self.parentContenView.size = self.mScrollView.contentSize
                    self.mContentView.snp.removeConstraints()
                    self.mContentView.snp.makeConstraints { (maker) in
                        maker.top.equalToSuperview().offset(-40)
                        maker.left.equalToSuperview().offset(-40)
                        maker.bottom.equalToSuperview().offset(-40)
                        maker.right.equalToSuperview().offset(-40)
                    }
                    
                    
                    
                    self.animationImage(self.mContentView, self.parentContenView)
                }
                
                
            }
        } else {
            self.check = false
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    @objc func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        
        let positionOfSlider: CGPoint = self.mSliderChoiseWeek.frame.origin
        let widthOfSlider: CGFloat = self.mSliderChoiseWeek.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(self.mSliderChoiseWeek.maximumValue) / widthOfSlider)
        
        let newValueConvert = roundf(Float(newValue))
        
        self.mSliderChoiseWeek.setValue(newValueConvert, animated: true)
        
        var valueInt = Int(newValueConvert)
        if valueInt < 1 {
            valueInt = 1
        }
        if valueInt > 42 {
            valueInt = 42
        }
        self.mCurrentWeek = valueInt
        
        self.loadWeek(week: self.mCurrentWeek)
        
    }
    
    @objc func changeWeek(_ slider : UISlider) {
        var value = roundf(slider.value)
        
        if value < 1 {
            value = 1
        }
        if value > 42 {
            value = 42
        }
        self.mCurrentWeek = Int(value)
        
        self.loadWeek(week: self.mCurrentWeek)
    }
    
    func setupScrollView() {
        
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
            
            let day = Calendar.current.dateComponents([ .day], from: pregnancy?.start_date.addingTimeInterval(86400) ?? Date(), to: Date())
            
            let week = (day.day ?? 0) / 7
            self.mCurrentWeek = week + 1
            self.mWeekSave = week + 1
            
            //                print(self.mCurrentWeek)
            self.mSliderChoiseWeek.value = Float(self.mWeekSave)
            
            self.loadWeek(week: self.mCurrentWeek)
            let centerOffsetX = (self.mBackgroundScrollImage.width - self.mScrollView.width) / 2
            let centerOffsetY = (self.mBackgroundScrollImage.height - self.mScrollView.height) / 2
            self.centerPoint = CGPoint(x: centerOffsetX, y: centerOffsetY)
            
            self.mScrollView.contentSize = CGSize(width: self.mBackgroundScrollImage.size.width - 80, height: self.mBackgroundScrollImage.size.height - 80)
            self.mScrollView.setContentOffset(self.centerPoint, animated: true)
            
            
            self.parentContenView.size = self.mScrollView.contentSize
            self.mContentView.snp.removeConstraints()
            self.mContentView.snp.makeConstraints { (maker) in
                maker.top.equalToSuperview().offset(-40)
                maker.left.equalToSuperview().offset(-40)
                maker.bottom.equalToSuperview().offset(-40)
                maker.right.equalToSuperview().offset(-40)
            }

            
            
            self.animationImage(self.mContentView, self.parentContenView)
            
            self.hideHud()
            
            
            UIView.animate(withDuration: 0.5, animations: {
                self.mBackgoundLoad.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                self.mBackgoundLoad.alpha = 0.5
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.mBackgoundLoad.removeFromSuperview()
                    self.mBackgoundLoad.isHidden = true
                    
                }
            });
                
            
        }
    }
    
    func setupScrollViewNew() {
        
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
            if let pregnancy = pregnancy {
                
                if pregnancy.baby_already_born == 1 {
                    self.mCurrentWeek = 1
                    self.mWeekSave = 1
                } else {
                    let day = Calendar.current.dateComponents([ .day], from: pregnancy.start_date, to: self.currentTimeDate)
                    
                    var week = (day.day ?? 0) / 7
                    
                    if (pregnancy.show_week == 1){
                        week = (day.day ?? 0) / 7 + 1
                    }
                    
                    if week == 0 {
                        week = 1
                        self.mCurrentWeek = week
                        self.mWeekSave = week
                    } else if week > 42 {
                        week = 42
                        self.mCurrentWeek = week
                        self.mWeekSave = week
                    } else {
                        self.mCurrentWeek = week
                        self.mWeekSave = week
                    }
                }
                //                print(self.mCurrentWeek)
                self.mSliderChoiseWeek.value = Float(self.mWeekSave)
                
                self.loadWeek(week: self.mCurrentWeek)
                let centerOffsetX = (self.mBackgroundScrollImage.width - self.mScrollView.width) / 2
                let centerOffsetY = (self.mBackgroundScrollImage.height - self.mScrollView.height) / 2
                self.centerPoint = CGPoint(x: centerOffsetX, y: centerOffsetY)
                
                self.mScrollView.contentSize = CGSize(width: self.mBackgroundScrollImage.size.width - 80, height: self.mBackgroundScrollImage.size.height - 80)
                self.mScrollView.setContentOffset(self.centerPoint, animated: true)
                
                self.parentContenView.size = self.mScrollView.contentSize
                self.mContentView.snp.removeConstraints()
                self.mContentView.snp.makeConstraints { (maker) in
                    maker.top.equalToSuperview().offset(-40)
                    maker.left.equalToSuperview().offset(-40)
                    maker.bottom.equalToSuperview().offset(-40)
                    maker.right.equalToSuperview().offset(-40)
                }
                
                
                
                self.animationImage(self.mContentView, self.parentContenView)
            }
            
            
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func isUpdateTabBarWhenScrolling() -> Bool {
        return false
    }
    
    private func checkConnection() {
        if !Connectivity.isConnectedToInternet() {
            let alert = UIAlertController(title: "", message: "Kiểm tra kết nối", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                DatabaseManager.sharedManager.logout {
                    
                }
            }))
            self.present(alert, animated: true, completion: nil)
            self.stopSlide()
        }
    }
    
    @objc func applicationDidBecomeActive(_ notification: Notification) {
        self.checkConnection()
    }

    
    private func loadSizeBaby() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllWeeks { (pregWeek, message, isSuccess) in
            
            if (isSuccess) {
                for i in 0..<pregWeek.count {
                    
                    let length = String(format: "%.1f", pregWeek[i].length)
                    let weight = String(format: "%.1f", pregWeek[i].weight)
                    if (length.suffix(2) == ".0"){
                        
                        self.mWeeks[i].mLenghtString = String(length.dropLast(2))
                    
                    } else {
                            self.mWeeks[i].mLenghtString = String(format: "%.1f", pregWeek[i].length)
                    }
                    
                    if (weight.suffix(2) == ".0"){
                        
                        self.mWeeks[i].mWeightString = String(weight.dropLast(2))
                    }
                    else {
                         self.mWeeks[i].mWeightString = String(format: "%.1f", pregWeek[i].weight)
                    }
                    
                    
                }
                self.setupScrollView()
                self.setupScrollViewNew()
            } else {
                self.setupScrollView()
                self.setupScrollViewNew()
            }
        }
    }
    
    
    func loadWeek(week:Int) -> Void {
        let week = self.mWeeks[week - 1]
        
        self.mBackImage.image = week.mBackgroundImage
        let background = self.mBackgroundScrollImage!
        
        self.slideView(slide: self.mBackImage, setImage: week.mBackgroundImage, setScale: week.mBackImageScale, background: background)
        
        self.slideView(slide: self.mMidImage, setImage: week.mMidImage, setScale: week.mMidImageScale, background: background)
        
        self.slideView(slide: self.mForeImage, setImage: week.mForeImage, setScale: week.mForeImageScale, background: background)
        
        if let mLenghtString = week.mLenghtString {
            self.mLenghtLbl.text = mLenghtString + " cm"
        }
        if let mWeightString = week.mWeightString {
           self.mWeightLbl.text = mWeightString + " g"
        }
        
        self.mWeekLbl.text = week.mWeekString
        if let week = week.mWeekString {
            self.setTitle(title: "Tuần \(week)")
        }
        
        
    }
    
    func slideView(slide:WeekImageView, setImage image:UIImage?, setScale scale:ImageScale?, background:UIImageView) -> Void {
        
        
        slide.snp.removeConstraints()
        if let scale = scale {
            slide.snp.makeConstraints { (maker) in
                maker.width.equalTo(background.snp.width).multipliedBy(scale.mScaleWidth)
                maker.height.equalTo(background.snp.height).multipliedBy(scale.mScaleHeight)
                maker.center.equalTo(background.snp.center)
            }
        }
        
        UIView.transition(with: slide,
                          duration: 0.75,
                          options: .transitionCrossDissolve,
                          animations: { slide.image = image },
                          completion: nil)
        
    }
    
    @IBAction func mControlBtnTouchUpInside(_ sender: Any) {
        let isPlaying = self.mControlButton.isSelected
        if(isPlaying) {
            self.stopSlide()
        } else {
            self.playSlide()
            
        }
        
    }
    
    func playSlide() -> Void {
        self.mSliderChoiseWeek.isHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
//        self.hideNaviBar(hidden: true, animated: true)
//        print("centerPoint khi Bấm play : \(self.centerPoint)")
        
        if let centerPoint = self.centerPoint {
            if centerPoint != self.mScrollView.contentOffset {
                UIView.animate(withDuration: 2, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.mScrollView.contentOffset.x = centerPoint.x
                    self.mScrollView.contentOffset.y = centerPoint.y
                }, completion: { (completion) in
                    print("done move")
                })
            }
        }
        
        
        self.mControlButton.isSelected = true
        self.tabBarController?.tabBar.isHidden = true
        if(self.mCurrentWeek == self.mWeeks.count) {
            self.loadWeek(week: 1)
            self.mCurrentWeek = 1
            self.mSliderChoiseWeek.value = Float(self.mCurrentWeek)
        }
        self.mSlideTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(onChangeSlider), userInfo: self.mSlideTimer, repeats: true)
        
    }
    
    @objc func onChangeSlider() -> Void {
        
        self.checkConnection()
        
        if(self.mCurrentWeek == self.mWeeks.count) {
            self.stopSlide()
        } else {
            self.mCurrentWeek = self.mCurrentWeek + 1
            self.mSliderChoiseWeek.value = Float(self.mCurrentWeek)
            self.loadWeek(week: self.mCurrentWeek)
        }
        
    }
    override func isHiddenTabBar() -> Bool {
        return self.mControlButton.isSelected
    }
    
    func stopSlide() -> Void {
        self.mSlideTimer?.invalidate()
        self.mControlButton.isSelected = false
        
        self.handleTabBar(animated: true)
        self.mSliderChoiseWeek.isHidden = true
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension VideoViewController {
    
    func animationImage(_ view : UIView,_ parentView: UIView) {
        
        let circlePath = UIBezierPath(arcCenter: parentView.center, radius: 15, startAngle: 0, endAngle: .pi*2, clockwise: true)
        let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.position))
        animation.duration = 10
        animation.repeatCount = MAXFLOAT
        animation.path = circlePath.cgPath
        
        DispatchQueue.main.async {
            view.layer.add(animation, forKey: nil)
        }
    }
//    func hideNaviBar(hidden:Bool, animated: Bool){
//        guard let naviBar = self.navigationController?.navigationBar else { return }
//        if naviBar.isHidden == hidden{ return }
//        let frame = naviBar.frame
//        var offset = hidden ? 0 : frame.size.height
//
//        print(offset)
//
//        let duration:TimeInterval = (animated ? 0.3 : 0.0)
//        naviBar.isHidden = false
////        if IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_8 {
////            offset -= 24
////        }
//
//
//        UIView.animate(withDuration: duration, animations: {
//            naviBar.frame.origin = CGPoint(x: 0, y: offset)
//            // frame.offsetBy(dx: 0, dy: offset)
//        }, completion: { (true) in
//            naviBar.isHidden = hidden
//        })
//    }
}



class WeekInfo {
    
    var mBackgroundImage:UIImage?
    var mBackImageScale: ImageScale!
    var mMidImage: UIImage?
    var mMidImageScale: ImageScale!
    var mForeImage: UIImage?
    var mForeImageScale: ImageScale!
    
    var mLenghtString: String?
    var mWeightString: String?
    var mWeekString: String?
    
    
    
    init(background: UIImage? = nil,
         backImageScale: ImageScale? = ImageScale(scaleWidth: 0, scaleHeight: 0),
         midImage: UIImage? = nil,
         midImageScale: ImageScale? = ImageScale(scaleWidth: 0, scaleHeight: 0),
         foreImage: UIImage? = nil,
         foreImageScale: ImageScale? = ImageScale(scaleWidth: 0, scaleHeight: 0),
         lenghtString: String? = "",
         weightString: String? = "",
         weekString: String? = "") {
        
        self.mBackgroundImage = background
        self.mBackImageScale = backImageScale
        
        self.mMidImage = midImage
        self.mMidImageScale = midImageScale
        
        self.mForeImage = foreImage
        self.mForeImageScale = foreImageScale
        
        self.mLenghtString = lenghtString
        self.mWeightString = weekString
        self.mWeekString = weekString
        
    }
    
}



class ImageScale {
    
    var mScaleWidth: CGFloat = 1
    var mScaleHeight: CGFloat = 1
    
    init(scaleWidth:CGFloat = 1, scaleHeight: CGFloat = 1) {
        
        self.mScaleWidth = scaleWidth
        self.mScaleHeight = scaleHeight
    }
    
}

class  WeekImageView: UIImageView {
    var mWidthConstraint: NSLayoutConstraint?
    var mHeightConstraint: NSLayoutConstraint?
}


extension VideoViewController: UIScrollViewDelegate {
    
    
    //Zoom ảnh, set lại thông số của view
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        //Set lại size cho các view
        let sizeIncrease = CGSize(width: self.mScrollView.contentSize.width + 80, height: self.mScrollView.contentSize.height + 80)
        self.mContentView.size = sizeIncrease
        self.parentContenView.size = self.mScrollView.contentSize

        self.mContentView.snp.removeConstraints()
        self.mContentView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().offset(-40)
            maker.left.equalToSuperview().offset(-40)
        }
//
//        Set lại center cho view để khi bấm play thì View về lại center
        let centerOffsetX = (self.mContentView.width - self.mScrollView.width) / 2
        let centerOffsetY = (self.mContentView.height - self.mScrollView.height) / 2
        self.centerPoint = CGPoint(x: centerOffsetX, y: centerOffsetY)
        

//        print(mScrollView.width)
        
        return self.parentContenView
    }
    
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}
