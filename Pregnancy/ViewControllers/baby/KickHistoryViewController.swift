//
//  KickHistoryViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/24/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class KickHistoryViewController: BaseViewController {
    
    @IBOutlet weak var mLbDateTimeKickHistory: UILabel!
    @IBOutlet weak var mLBDurationKickHistory: UILabel!
    @IBOutlet weak var mLbKickResultKickHistory: UILabel!
    @IBOutlet weak var mTableViewKickHistory: UITableView!
    
    var dataKickCounter: [JSON] = []
    var dataHistories: [JSON] = []
    var dataKickDetail: [JSON] = []
    var index = 0
    var kickcouterCount = 0
    var timeCountDow: Timer?
    
    
    // Data xử lý thời gian lưu
    let realm = try! Realm()
    var kickList: Results<KickCounter>!
    
    var checkActive : Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.addBackgroundImage("baby-background-blur")
        self.mTableViewKickHistory.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = true
        super.viewWillAppear(animated)
        self.getAllUserKickHistories()
        
        if let user = DatabaseManager.sharedManager.getLocalUserInfo() {
            self.kickList = self.realm.objects(KickCounter.self).filter("user_id == %@", user.id)
            
            if self.kickList.isEmpty == false {
                self.checkActive = self.kickList.first?.kick_results_id
            }
        }
    }

    private func setupUI() {
        setupTableView()
    }
    
    private func setupTableView() {
        let nib = UINib(nibName: "KickHistoryTableViewCell", bundle: Bundle.main)
        mTableViewKickHistory.register(nib, forCellReuseIdentifier: "KickHistoryCell")
        mTableViewKickHistory.delegate = self
        mTableViewKickHistory.dataSource = self
    }
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        
        var barItems = super.createRightBarButtonItems()
        
        let addButton = UIButton(type: .custom)
        addButton.setImage(UIImage(named: "share-1"), for: .normal)
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        addButton.addTarget(self, action: #selector(addButtonAction), for: .touchUpInside)
        let barButton2 = UIBarButtonItem(customView: addButton)
        
        barItems.insert(barButton2, at: 0)
        return barItems
    }
    
    @objc func addButtonAction(){
        var shareText = "Lịch sử đạp chân của bé \n"
        
        if dataHistories.isEmpty == false {
            for item in dataHistories {
                let kick_id = item["kick_result_id"].intValue
                var count = 0
                for item in dataKickDetail {
                    if kick_id == item["kick_result_id"].intValue {
                        count += 1
                    }
                }
                var success = ""
                if count == 10 {
                    success = "Yes"
                }
                else {
                    success = "No"
                }
                shareText += "Thời gian = \(item["kick_date"])\nThời lượng = \(item["duration"].intValue)\nSố lần đạp = \(count)\nThành công = \(success)\n"
            }
        }
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        vc.setValue("Lịch sử đạp chân của bé", forKey: "Subject")
        present(vc, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! KickDetailViewController
        vc.kick_id = index
    }
    
    private func getAllUserKickHistories() {
        self.showHud()
        NetworkManager.shareInstance.apiGetUserKickHistories() {
            (data,message,iSuccess) in
            self.dataHistories = []
            if iSuccess {
                let data = data as! JSON
                self.dataHistories = data.arrayValue.reversed()
                self.getAllUserKickDetail()
            }
            
            else if ( message == "Authorization has been denied for this request."    ) {
                let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    DatabaseManager.sharedManager.logout {
                    }
                }))
                alert.addAction(UIAlertAction(title: "Huỷ", style: .default, handler: { action in
                    self.hideHud()
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
            DispatchQueue.main.async {
                self.mTableViewKickHistory.reloadData()
                self.hideHud()
            }
        }
    }
    
    private func deleteUserKickHistories(_ kick_result_id: Int) {
        NetworkManager.shareInstance.apiDeleteUserKickHistories(kick_result_id: kick_result_id) {
            (data, message, isSuccess) in
            if isSuccess {
                self.getAllUserKickHistories()
            }
        }
    }
    
    private func getAllUserKickDetail() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllKickResultDetail() {
            (data,message,iSuccess) in
            if iSuccess {
                let data = data as! JSON
                self.dataKickDetail = data.arrayValue
            }
            
            DispatchQueue.main.async {
                self.mTableViewKickHistory.reloadData()
                self.hideHud()
            }
        }
    }
}

extension KickHistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataHistories.count != 0 {
            return dataHistories.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KickHistoryCell", for: indexPath) as! KickHistoryTableViewCell
        
        if dataHistories.count != 0 {
            let fomatter = DateFormatter()
            if dataHistories[indexPath.row]["kick_date"].stringValue.contains(".") {
                fomatter.dateFormat = "y-MM-dd'T'HH:mm:ss.SS"
            } else {
                fomatter.dateFormat = "y-MM-dd'T'HH:mm:ss"
            }
            
            let date = fomatter.date(from: dataHistories[indexPath.row]["kick_date"].stringValue)
//            let components = Calendar.current.dateComponents([.day], from: Date(), to: date!)
            
//            let date = fomatter.date(from: dataHistories[indexPath.row]["kick_date"].stringValue)
            let daya = Date().getDayMonthYearHourMinuteSecond().day
            let dayb = date?.getDayMonthYearHourMinuteSecond().day
            
            let DayMontha = Date().getDayMonthYearHourMinuteSecond().month
            let DayMonthb = date?.getDayMonthYearHourMinuteSecond().month
            
            let YearA = Date().getDayMonthYearHourMinuteSecond().year
            let YearB = date?.getDayMonthYearHourMinuteSecond().year

            fomatter.dateFormat = "dd MMM"
            fomatter.locale = Locale(identifier: "vi_VI")
            let curDate = fomatter.string(from: date!)
            fomatter.dateFormat = "HH:mm"
            let curTime = fomatter.string(from: date!)
            if (YearA == YearB){
                if (DayMontha == DayMonthb) {
                    if (daya != dayb ) {
                        cell.mLblDateKickHistory.text = "\(curDate)  \(curTime)"
                    } else{
                        cell.mLblDateKickHistory.text = "Hôm nay  \(curTime)"
                    }
                }else{
                    cell.mLblDateKickHistory.text = "\(curDate)  \(curTime)"
                }
            } else {
                cell.mLblDateKickHistory.text = "\(curDate)  \(curTime)"
            }
            
//            if components.day == 0 {
//                cell.mLblDateKickHistory.text = "Hôm nay  \(curTime)"
//            }
//            else {
//                cell.mLblDateKickHistory.text = "\(curDate)  \(curTime)"
//            }
            
            let time = Int(dataHistories[indexPath.row]["duration"].floatValue)
            let seconds = (Int(time / 1000) % 60)
            let minutes = (Int(time / (1000*60)) % 60)
            let hours = (Int(time / (1000*60*60)) % 60)
            
            if (self.checkActive != nil) && (self.checkActive == dataHistories[indexPath.row]["kick_result_id"].intValue) {
//                if self.kickcouterCount <= 0 {
//                    cell.mLblDurationKickHistory.text = "00:00"
//                }else{
                    cell.mLblDurationKickHistory.text = "Active"
//                }
            } else {
                if hours > 0 {
                    cell.mLblDurationKickHistory.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
                }
                else {
                    cell.mLblDurationKickHistory.text = String(format: "%02d:%02d", minutes, seconds)
                }
            }
            
            
            let kick_id = dataHistories[indexPath.row]["kick_result_id"].intValue
            var count = 0
            for item in dataKickDetail {
                if kick_id == item["kick_result_id"].intValue {
                    count += 1
                }
            }
            
            cell.mLblKickResultKickHistory.text = "\(count)"
        }
        
        cell.selectionStyle = .none
        return cell
    }
}

extension KickHistoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataHistories.count != 0 {
            self.index = dataHistories[indexPath.row]["kick_result_id"].intValue
        }
        performSegue(withIdentifier: "kick_history_to_kick_detail", sender: self)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            self.deleteUserKickHistories(dataHistories[indexPath.row]["kick_result_id"].intValue)
        }
    }
}
