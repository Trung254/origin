//
//  BabyImagesViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/14/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON
import RealmSwift
import MessageUI

class BabyImagesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var mSendMail: UIButton!
    @IBOutlet weak var mBabyImages: UIImageView!
    @IBOutlet weak var mScrollView: UIScrollView!
}

class BabyImagesViewController: BaseViewController , MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var m_Segment: UISegmentedControl!
    @IBOutlet weak var mSLD_BabyImages: UISlider!
    @IBOutlet weak var mCollectionViewBabyImg: UICollectionView!
    
    var checkRangeImage = 0
    
    var weekSlide = 1
    var dataImages : [String] = [String]()
    var numberWeek : Int = 1
    var data: Results<PregImage>?
    
    var indexReset : Int? // row cần reset khi scroll
    
    
    var pointWeekSlider : CGFloat = 0 //point của slider ban đầu
    var checkCreateShapre = false
    let shapeLayer = CAShapeLayer()
    
    let minThumbColor = #colorLiteral(red: 0.1468058228, green: 0.704572618, blue: 0.9956740737, alpha: 1)
    let maxThumbColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    var tapUiSlider : UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadBabyimg()
        self.m_Segment.selectedSegmentIndex = 1
        
        
//        self.setThumbImageUISlider(slider: self.mSLD_BabyImages, text: String(weekSlide), color: #colorLiteral(red: 0.1468058228, green: 0.704572618, blue: 0.9956740737, alpha: 1))
        //        self.sldBabyWeekly.minimumValue = 1
        self.mSLD_BabyImages.value = 1
        self.mSLD_BabyImages.minimumTrackTintColor = #colorLiteral(red: 0.8513443845, green: 0.8513443845, blue: 0.8513443845, alpha: 1)
        self.mSLD_BabyImages.maximumTrackTintColor = #colorLiteral(red: 0.8513443845, green: 0.8513443845, blue: 0.8513443845, alpha: 1)
        self.mSLD_BabyImages.thumbTintColor = self.minThumbColor
        // before load View we need checking and setting again all data for scress
        loadDataFromAPI()
        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "info"), style: .done, target: self, action: #selector(call_Method))
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        mCollectionViewBabyImg.delegate = self
        mCollectionViewBabyImg.dataSource = self
        
        self.navigationController?.navigationBar.tintColor = .white
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupSlider()
        
        
    }
    private func loadBabyimg() {
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
            if let pregnancy = pregnancy {
                var components = Calendar.current.dateComponents([ .day], from: Date(), to:pregnancy.due_date.addingTimeInterval(86400))
                
                if var day = components.day, pregnancy.baby_already_born != 1 {
                    if (day < 0 ) {
                        day = 0
                    } else {
                        day = 280 - day
                    }
                    
                    self.weeks = day / 7
                    if pregnancy.show_week == 1 {
                        self.weeks = day / 7 + 1
                    }
                    self.days = day - (self.weeks! * 7 )
                } else {
                    self.days = 0
                    self.weeks = 0
                }
                self.mCollectionViewBabyImg.reloadData()
                self.loadSelectedWeek()
            }
        }
    }
    
    private func setupSlider(){
        self.mSLD_BabyImages.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        
        // Thêm tab vào slider -> update lại value
        self.tapUiSlider = UITapGestureRecognizer(target: self, action: #selector(sliderTapped(gestureRecognizer:)))
        self.mSLD_BabyImages.addGestureRecognizer(self.tapUiSlider)
    }
    
    // Set tab slider và di chuyển Thumb, update lại giá trị
    @objc func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
 
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        
        let positionOfSlider: CGPoint = self.mSLD_BabyImages.frame.origin
        let widthOfSlider: CGFloat = self.mSLD_BabyImages.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(self.mSLD_BabyImages.maximumValue) / widthOfSlider)
        
        self.mSLD_BabyImages.setValue(roundf(Float(newValue)), animated: true)
        
        
        let trackRect: CGRect = self.mSLD_BabyImages.trackRect(forBounds: self.mSLD_BabyImages.bounds) // Lấy giá trị của uislider
        let thumbRect: CGRect = self.mSLD_BabyImages.thumbRect(forBounds: self.mSLD_BabyImages.bounds, trackRect: trackRect, value: self.mSLD_BabyImages.value) // Lấy giá trị của thumb uislider
//        print(thumbRect)
        if self.pointWeekSlider < thumbRect.origin.x {
            self.mSLD_BabyImages.thumbTintColor = self.maxThumbColor
        } else {
            self.mSLD_BabyImages.thumbTintColor = self.minThumbColor
        }
        
        
        self.mCollectionViewBabyImg.scrollToItem(at: IndexPath(item: Int(self.mSLD_BabyImages.value - 1), section: 0), at: [.centeredVertically, .centeredHorizontally], animated: false)
        
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            var sliderValue = slider.value
            if sliderValue < 1 {
                sliderValue  = 1
            }
            switch touchEvent.phase {
            case .began:
                print("Began")
                
                self.mSLD_BabyImages.removeGestureRecognizer(self.tapUiSlider)
                break
            case .moved:
                print("moved")
                DispatchQueue.main.async {
                    let roundedValue = round(sliderValue / 1) * 1
                    slider.value = roundedValue
                        
                    // Lấy giá trị của thumb image để đổi màu
                    let trackRect: CGRect = self.mSLD_BabyImages.trackRect(forBounds: self.mSLD_BabyImages.bounds) // Lấy giá trị của uislider
                    let thumbRect: CGRect = self.mSLD_BabyImages.thumbRect(forBounds: self.mSLD_BabyImages.bounds, trackRect: trackRect, value: self.mSLD_BabyImages.value) // Lấy giá trị của thumb uislider
//                    print(thumbRect)
                    if self.pointWeekSlider < thumbRect.origin.x {
                        self.mSLD_BabyImages.thumbTintColor = self.maxThumbColor
                    } else {
                        self.mSLD_BabyImages.thumbTintColor = self.minThumbColor
                    }
                    
                    
                }
                break
            case .ended:
                print("ended")
                let page = Int(sliderValue)
                self.mCollectionViewBabyImg.scrollToItem(at: IndexPath(item: page - 1, section: 0), at: [.centeredVertically, .centeredHorizontally], animated: false)
                
                // Lấy giá trị của thumb image để đổi màu
                let trackRect: CGRect = self.mSLD_BabyImages.trackRect(forBounds: self.mSLD_BabyImages.bounds)
                let thumbRect: CGRect = self.mSLD_BabyImages.thumbRect(forBounds: self.mSLD_BabyImages.bounds, trackRect: trackRect, value: self.mSLD_BabyImages.value)
//                print(thumbRect)
                if self.pointWeekSlider < thumbRect.origin.x {
                    self.mSLD_BabyImages.thumbTintColor = self.maxThumbColor
                } else {
                    self.mSLD_BabyImages.thumbTintColor = self.minThumbColor
                }
                
                self.mSLD_BabyImages.addGestureRecognizer(self.tapUiSlider)
                
                break
            default:
                break
            }
        }
    }
    
    @objc func call_Method(sender: AnyObject) {
        self.performSegue(withIdentifier: "BabyImagesToInfo", sender: self)
    }
    
    // Check scroll thì kiểm tra index của item hiển thị để gán giá trị
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.mCollectionViewBabyImg.contentOffset, size: self.mCollectionViewBabyImg.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let indexPath = self.mCollectionViewBabyImg.indexPathForItem(at: visiblePoint)
        if let indexPath = indexPath {
            self.mSLD_BabyImages.value = Float(indexPath.item + 1)
            
            let trackRect: CGRect = self.mSLD_BabyImages.trackRect(forBounds: self.mSLD_BabyImages.bounds)
            let thumbRect: CGRect = self.mSLD_BabyImages.thumbRect(forBounds: self.mSLD_BabyImages.bounds, trackRect: trackRect, value: self.mSLD_BabyImages.value)
            if self.pointWeekSlider < thumbRect.origin.x {
                self.mSLD_BabyImages.thumbTintColor = self.maxThumbColor
            } else {
                self.mSLD_BabyImages.thumbTintColor = self.minThumbColor
            }
            setTitle(title: "Tuần \(indexPath.item + 1)")
            if let indexReset = self.indexReset {
                if indexReset != indexPath.item {
                    self.mCollectionViewBabyImg.reloadItems(at: [IndexPath(item: indexReset, section: 0)])
                    self.indexReset = indexPath.item
                }
            }
            
        }
    }
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.mCollectionViewBabyImg.contentOffset, size: self.mCollectionViewBabyImg.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        self.indexReset = self.mCollectionViewBabyImg.indexPathForItem(at: visiblePoint)?.item
    }
    
    func imageType() -> Int {
        switch self.m_Segment.selectedSegmentIndex {
        case 1:
            return 2
            
        case 2:
            return 3
            
        default:
            return 1
            
        }
    }
    
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        let imnageView = scrollView.viewWithTag(10) as! UIImageView
        return imnageView
    }
    
    @IBAction func mSegment(_ sender: Any) {
        
        mCollectionViewBabyImg.reloadData()
    }
    
    func loadDataFromAPI() {
        // declare new thread
        self.showHud()
        // call API
        DatabaseManager.sharedManager.getImages(type: nil) { (result, isTemp, isSuccess) in
            self.hideHud()
            self.data = result
            self.mCollectionViewBabyImg.reloadData()
            self.loadSelectedWeek()
        }
    }
    
    func loadSelectedWeek() {
        if var week = self.weeks, DatabaseManager.sharedManager.getLocalImages(type: self.imageType()).count > week {
            if week == 0 {
                week = 1
            }
            let indexPath = IndexPath(item: week-1 , section: 0)
            self.mSLD_BabyImages.setValue(Float(week), animated: false)
            
            let trackRect: CGRect = self.mSLD_BabyImages.trackRect(forBounds: self.mSLD_BabyImages.bounds)
            let thumbRect: CGRect = self.mSLD_BabyImages.thumbRect(forBounds: self.mSLD_BabyImages.bounds, trackRect: trackRect, value: self.mSLD_BabyImages.value)
//            print(thumbRect)
            
            self.pointWeekSlider = thumbRect.origin.x // Lưu vị trí x ban đầu
            
            self.createShape(self.mSLD_BabyImages, startPoint: self.pointWeekSlider) // Vẽ lần đầu
            
            self.mSLD_BabyImages.thumbTintColor = self.minThumbColor
            setTitle(title: "Tuần \(week)")
            
            self.mCollectionViewBabyImg.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: false)
        }
    }
    
    @objc func requestBabyImage(_ sender: UIButton) {
        let mailComposeViewController = configuredMailComposeViewController(sender.tag + 1)
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController(_ week: Int) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
//        mailComposerVC.setToRecipients(["motour.medlatec@gmail.com"])
        mailComposerVC.setSubject("Ảnh tuần \(week)")
//        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        mailComposerVC.setCcRecipients(["motour.medlatec@gmail.com"])
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Không thể gửi mail", message: "Thiết bị của bạn không thể gửi được mail. Cài đặt và thử lại lần nữa", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension BabyImagesViewController : UICollectionViewDelegate {
    
}
extension BabyImagesViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! BabyImagesCollectionViewCell
        
        let sectionData = data?.filter("image_type_id == \(self.imageType())")
        
        cell.mBabyImages.image = nil
        if let imageData = sectionData?[indexPath.row] {
            let url = URL(string: imageData.image)
            if imageData.image.contains("default.jpg") {
                cell.mSendMail.isUserInteractionEnabled = true
                cell.mSendMail.addTarget(self, action: #selector(requestBabyImage), for: .touchUpInside)
                cell.mSendMail.tag = indexPath.row
            } else {
                cell.mSendMail.isUserInteractionEnabled = false
            }
//            print(imageData.image)
            cell.mBabyImages.contentMode = .scaleAspectFill
            cell.mBabyImages.sd_setImage(with: url, completed: nil)
        }
        
        cell.mScrollView.setZoomScale(0.0, animated: true)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = DatabaseManager.sharedManager.getLocalImages(type: self.imageType()).count
        self.mSLD_BabyImages.maximumValue = Float(count)
        return count
    }

//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if indexPath.row > 0 {
//            let indexPath = IndexPath(item: indexPath.row - 1, section: 0)
//
//            DispatchQueue.main.async(execute: {() -> Void in
//                collectionView.reloadItems(at: [indexPath])
//            })
//        }
//    }
}

extension BabyImagesViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

extension BabyImagesViewController {
    private func createShape(_ slider : UISlider, startPoint: CGFloat) {
        
        let trackRect: CGRect = slider.trackRect(forBounds: slider.bounds)
        
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: trackRect.origin.x + 0.5, y: trackRect.origin.y + trackRect.height / 2))
        path.addLine(to: CGPoint(x: startPoint + 2, y: trackRect.origin.y + trackRect.height / 2))
        
        //        let shapeLayer = CAShapeLayer()
        self.shapeLayer.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        self.shapeLayer.strokeColor = minThumbColor.cgColor
        self.shapeLayer.lineWidth = 4
        self.shapeLayer.path = path.cgPath
        
        slider.layer.addSublayer(self.shapeLayer)
        self.shapeLayer.bringToFront()
        
    }
    private func removeShapre() {
        self.shapeLayer.removeFromSuperlayer()
    }
}
