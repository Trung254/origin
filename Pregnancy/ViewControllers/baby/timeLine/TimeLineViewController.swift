//
//  TimeLineViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/19/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

//class circle {
//    var width:Int = 0
//    var name:String = ""
//    var isShow:Bool = true
//}

class TimeLineViewController: UIViewController {
    
    
    @IBOutlet weak var mScrollView: UIScrollView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        mScrollView.contentSize.width = mScrollView.frame.width * CGFloat(5.25)
    }
    
}
