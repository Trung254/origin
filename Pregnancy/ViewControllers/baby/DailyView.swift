//
//  DailyView.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/19/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SnapKit
import WebKit
import SwiftyJSON



class DailyView: UIView {
    
    @IBOutlet weak var mContentView: UIView!
    
    @IBOutlet weak var mbtnLike: UIButton!
    @IBOutlet weak var mbtnDisLike: UIButton!
    @IBOutlet weak var mbtnAddto: UIButton!
    @IBOutlet weak var webViewBabyDailyView: UIView!
    @IBOutlet weak var mWebViewHeight: NSLayoutConstraint!
    var webViewBabyDaily: WKWebView!
    @IBOutlet weak var mlblTodos: UILabel!
    @IBOutlet weak var mlblAddText: UILabel!
    @IBOutlet weak var mOneLine: UIView!
    @IBOutlet weak var mHeightTodos: NSLayoutConstraint!
    @IBOutlet weak var mHeightViewTodo: NSLayoutConstraint!
    @IBOutlet weak var mViewGroupTodo: UIView!
    @IBOutlet weak var mLineTop: UIView!
    @IBOutlet weak var mSpaceToBottom: NSLayoutConstraint!
    
    var curStatus = false
    var curStatusDisLike = false
    var curStatusAdd = false
    var dataUserTodos: [JSON] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
 
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("DailyView", owner: self, options: nil)
        addSubview(mContentView)
        mContentView.frame = self.bounds
        mWebViewHeight.constant = 1000
        
        mContentView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.bottom.equalToSuperview()
            maker.left.equalToSuperview()
            maker.right.equalToSuperview()
        }
        self.setupWebviewIfNeed()
    }
    
    func setupWebviewIfNeed() -> Void {
        if(self.webViewBabyDaily == nil) {
            let webConfiguration = WKWebViewConfiguration()
            webViewBabyDaily = WKWebView(frame: .zero, configuration: webConfiguration)
            webViewBabyDaily.uiDelegate = self
            webViewBabyDaily.navigationDelegate = self
            
            webViewBabyDaily.isUserInteractionEnabled = false
            
//            webViewBabyDaily.scrollView.bounces = true
        
            
            self.webViewBabyDailyView.addSubview(self.webViewBabyDaily)
            self.webViewBabyDailyView.translatesAutoresizingMaskIntoConstraints = false
            self.webViewBabyDaily.snp.makeConstraints { (maker) in
                maker.top.equalToSuperview()
                maker.leading.equalToSuperview()
                maker.bottom.equalToSuperview()
                maker.trailing.equalToSuperview()
            }
        }
    }
    
    func setData(htmlContent:URL) -> Void {
        self.setupWebviewIfNeed()
        self.mWebViewHeight.constant = 0
        self.webViewBabyDaily.load(URLRequest(url: URL(string:"about:blank")!))
        self.webViewBabyDaily.load(URLRequest(url: htmlContent))
//        self.webViewBabyDaily.loadHTMLString(htmlContent, baseURL: nil)
        
    }
    
    @IBAction func btnLikeArticle(_ sender: Any) {
        mbtnDisLike.setImage(UIImage(named: "hand_dislike"), for: .normal)
        curStatusDisLike = false

        if curStatus == false {
           curStatus = true
            mbtnLike.setImage(UIImage(named: "hand_like_active"), for: .normal)
        }
        else {
            curStatus = false
            mbtnLike.setImage(UIImage(named: "hand_like"), for: .normal)
        }
    }
    
    @IBAction func btnDislikeArticle(_ sender: Any) {
        mbtnLike.setImage(UIImage(named: "hand_like"), for: .normal)
        curStatus = false

        if curStatusDisLike == false {
            curStatusDisLike = true
            mbtnDisLike.setImage(UIImage(named: "hand_dislike_active"), for: .normal)
        }
        else {
            curStatusDisLike = false
            mbtnDisLike.setImage(UIImage(named: "hand_dislike"), for: .normal)
        }
    }
    
    func checkReaction(check: Int) {
        switch check {
        case 1:
            curStatus = false
            curStatusDisLike = false
            mbtnLike.setImage(UIImage(named: "hand_like_active"), for: .normal)
            mbtnDisLike.setImage(UIImage(named: "hand_dislike"), for: .normal)
            break
        case 2:
            curStatus = true
            curStatusDisLike = false
            mbtnLike.setImage(UIImage(named: "hand_like"), for: .normal)
            mbtnDisLike.setImage(UIImage(named: "hand_dislike_active"), for: .normal)
            break
        case 3:
            curStatus = false
            curStatusDisLike = true
            mbtnLike.setImage(UIImage(named: "hand_like"), for: .normal)
            mbtnDisLike.setImage(UIImage(named: "hand_dislike"), for: .normal)
            break
        default: // = 0 thi tren server chua co du lieu -> push
            curStatus = false
            curStatusDisLike = false
            mbtnLike.setImage(UIImage(named: "hand_like"), for: .normal)
            mbtnDisLike.setImage(UIImage(named: "hand_dislike"), for: .normal)
            break
        }
    }
    
    @IBAction func activeTodoListIcon(_ sender: AnyObject) {
        self.loadUserTodos(sender.tag!)
        
    }
    
    func checkStatus(_ id: Int) {
        if dataUserTodos.isEmpty == false{
            for item in dataUserTodos{
                if item["todo_id"].intValue == id + 1 {
                    curStatusAdd = true
                }
            }
        }
        if curStatusAdd == false {
            curStatusAdd = true
            mbtnAddto.setImage(UIImage(named: "tick-3"), for: .normal)
            self.pushDataUserTodos(id + 1)
        }
        else {
            curStatusAdd = false
            mbtnAddto.setImage(UIImage(named: "untick"), for: .normal)
            self.deleteDataUserTodos(id + 1)
        }
    }
    
    private func pushDataUserTodos(_ todoId: Int) {
        NetworkManager.shareInstance.apiPostUserToDos(todoId: todoId, status: 0) {
            (data, message, isSuccess) in
        }
    }
    
    private func deleteDataUserTodos(_ todoId: Int) {
        NetworkManager.shareInstance.apiDeleteUserToDos(todoId: todoId) {
            (data, message, isSuccess) in
        }
    }
     
    private func loadUserTodos(_ id: Int) {
        self.dataUserTodos = []
        NetworkManager.shareInstance.apiGetUserToDos() {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                self.dataUserTodos = data.arrayValue
            }
            
            
            self.checkStatus(id)
        }
    }
}

extension DailyView: WKUIDelegate {
    
}

extension DailyView: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.webViewBabyDaily.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {

                self.webViewBabyDaily.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
//                    print(height)
//                    print(self.webViewBabyDailyView.height)
//                    self.webViewBabyDailyView.height = height as! CGFloat
                    self.mWebViewHeight.constant = height as! CGFloat
//                    print(self.mWebViewHeight.constant)
                })
            }
            
        })
    }
    
}

//extension DailyView: UIWebViewDelegate {
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//
//        var frame = webView.frame
//        frame.size.height = 1
//        webView.frame = frame
//        let fittingSize = webView.sizeThatFits(CGSize.init(width: 0, height: 0))
//        mWebViewHeight.constant = fittingSize.height
//
//    }
//}
