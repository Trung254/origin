//
//  KickDetailViewController.swift
//  Pregnancy
//
//  Created by dady on 2/11/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class KickDetailViewController: BaseViewController {
    var kick_id = 0
    var dataKickDetail: [JSON] = []
    
    @IBOutlet weak var mTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "KickDetailTableViewCell", bundle: Bundle.main)
        mTableView.register(nib, forCellReuseIdentifier: "KickDetailTableViewCell")
        self.addBackgroundImage("baby-background-blur")
        self.mTableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if kick_id != 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.getKickDetail(self.kick_id)
            }
        }
    }

    private func getKickDetail(_ kick_result_id: Int) {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllKickResultDetail() {
            (data, message, iSuccess) in
            
            if iSuccess {
                let data = data as! JSON
                self.dataKickDetail = []
                for item in data.arrayValue {
                    if item["kick_result_id"].intValue == kick_result_id {
                        self.dataKickDetail.append(item)
                    }
                }
                
                self.dataKickDetail = self.dataKickDetail.reversed()
                self.mTableView.reloadData()
            }
            
            self.hideHud()
        }
    }
    
    private func deleteKickResultDetail(_ kick_id: Int,_ kick_order: Int) {
        NetworkManager.shareInstance.apiDeleteKickResultDetail(kick_result_id: kick_id, kick_order: kick_order) {
            (data, message, iSuccess) in
            if iSuccess {
                self.getKickDetail(kick_id)
            }
        }
    }
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        
        var barItems = super.createRightBarButtonItems()
        
        let addButton = UIButton(type: .custom)
        addButton.setImage(UIImage(named: "share-1"), for: .normal)
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        addButton.addTarget(self, action: #selector(addButtonAction), for: .touchUpInside)
        let barButton2 = UIBarButtonItem(customView: addButton)
        
        barItems.insert(barButton2, at: 0)
        return barItems
    }

    
    @objc func addButtonAction(){
        
        print(dataKickDetail)
        if dataKickDetail.isEmpty == false {
            // Share trong này
            let section = dataKickDetail.first!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
            
            let sectionDate = dateFormatter.date(from: section["kick_time"].stringValue)
            
            dateFormatter.dateFormat = "'ngày 'dd' tháng 'MM' năm 'yyyy"
            
            var subject = "Phiên đạp "
            var textShare = "Dưới đây là kết quả"
            
            if let sectionDate = sectionDate {
                subject = subject + "vào \(dateFormatter.string(from: sectionDate))"
                textShare = textShare + " của phiên đạp vào \(dateFormatter.string(from: sectionDate))\n\n"
            }
       
            for i in stride(from: dataKickDetail.count - 1, to: 0, by: -1) {
                let sectionText = "Phiên đạp lần thứ: \(dataKickDetail[i]["kick_order"].stringValue)\n"
                
                let kickTime = dataKickDetail[i]["kick_time"].stringValue
                let start = kickTime.index(kickTime.startIndex, offsetBy: 11)
                let end = kickTime.index(kickTime.endIndex, offsetBy: -3)
                
                let timeKick = "Thời điểm đạp: \(kickTime[start..<end])\n"
                
                var elapsed_time = "Thời gian trôi qua: "
                let time = Int(dataKickDetail[i]["elapsed_time"].intValue)
                let seconds = (Int(time / 1000) % 60)
                let minutes = (Int(time / (1000*60)) % 60)
                let hours = (Int(time / (1000*60*60)) % 60)
                if hours > 0 {
                    elapsed_time = elapsed_time + String(format: "%02d:%02d:%02d", hours, minutes, seconds) + "\n\n"
                }
                else {
                    elapsed_time = elapsed_time + String(format: "%02d:%02d", minutes, seconds) + "\n\n"
                }
                
                textShare = textShare + sectionText + timeKick + elapsed_time
            }
            
            textShare = textShare + "Lịch sử đạp chân em bé được lưu bởi ứng dụng MomTour"
            
            let vc = UIActivityViewController(activityItems: [textShare], applicationActivities: [])
            vc.setValue(subject, forKey: "Subject")
            present(vc, animated: true, completion: nil)
        } else {
            self.alert("Không có lịch sử đạp chân em bé. Vui lòng thử lại sau")
        }
    }
}

extension KickDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataKickDetail.count != 0 {
            return dataKickDetail.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KickDetailTableViewCell", for: indexPath) as! KickDetailTableViewCell
        if dataKickDetail.count != 0 {
            cell.mElapsedTime.text = "\(dataKickDetail.count - indexPath.row)"
            
            let fomatter = DateFormatter()
//            fomatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SS"
            if dataKickDetail[indexPath.row]["kick_time"].stringValue.contains(".") {
                fomatter.dateFormat = "y-MM-dd'T'HH:mm:ss.SS"
            } else {
                fomatter.dateFormat = "y-MM-dd'T'HH:mm:ss"
            }
            let date = fomatter.date(from: dataKickDetail[indexPath.row]["kick_time"].stringValue)
            fomatter.dateFormat = "HH:mm"
            let curTime = fomatter.string(from: date!)
            cell.mTimeKick.text = curTime
            
            let time = Int(dataKickDetail[indexPath.row]["elapsed_time"].intValue)
            let seconds = (Int(time / 1000) % 60)
            let minutes = (Int(time / (1000*60)) % 60)
            let hours = (Int(time / (1000*60*60)) % 60)
            if hours > 0 {
                cell.mKick.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
            }
            else {
                cell.mKick.text = String(format: "%02d:%02d", minutes, seconds)
            }
        }
        
         cell.selectionStyle = .none
        return cell
    }
}

extension KickDetailViewController: UITabBarDelegate {
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            self.deleteKickResultDetail(dataKickDetail[indexPath.row]["kick_result_id"].intValue, dataKickDetail[indexPath.row]["kick_order"].intValue)
        }
    }
}
