//
//  KickHistoryTableViewCell.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/24/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit



class KickHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var mLblDateKickHistory: UILabel!
    @IBOutlet weak var mLblDurationKickHistory: UILabel!
    @IBOutlet weak var mLblKickResultKickHistory: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //customFontLabels()
    }

    //MARK: Private methods
//    fileprivate func customFontLabels() {
//        // Initialization code
//        var fontSize: CGFloat = 16
//        if UIDevice.IS_IPHONE_4_OR_LESS || UIDevice.IS_IPHONE_5 {
//            fontSize = 12
//        }
//        mLblDateKickHistory.font = UIFont.systemFont(ofSize: fontSize)
//        mLblDurationKickHistory.font = UIFont.systemFont(ofSize: fontSize)
//        mLblKickResultKickHistory.font = UIFont.systemFont(ofSize: fontSize)
//    }
}
