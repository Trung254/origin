//
//  MenuCardViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/8/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SnapKit

class MenuItem {
    var name : String?
    var imageName : String?
    
    init(name: String, imageName: String) {
        self.name = name
        self.imageName = imageName
    }
}

class MenuCardViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet var mMenuCardCollection: UICollectionView!
    
    var cellWidth:CGFloat = 137
    var cellHeight:CGFloat = 150
    var spacing:CGFloat = 15
    var numberOfColumn:CGFloat = 2
    var numberOfRow:CGFloat = 3
    var spaceHeight:CGFloat = 0
    var spaceWidth:CGFloat = 0
    var minimumSpacing : CGFloat = 20
    var minimumInterSpacing : CGFloat = 20
    var top : CGFloat = 20
    var left : CGFloat = 20
    var menuItems : Array<MenuItem> = []
    var mBackgroundImage : UIImageView = UIImageView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLeftMainMenu()
        self.addPadLockButton()
        self.setupMenuItems()
        self.setCollectionViewDelegateAndDataSource()
        self.setupBackgroundImage()
        self.changeStyleNavigationLabel()
    }
    
    func getViewTitle() -> String {
        fatalError("Must Override to return View Title here")
    }
    
    func setupBackgroundImage() {
        self.view.addSubview(mBackgroundImage)
        self.view.sendSubviewToBack(mBackgroundImage)
        mBackgroundImage.snp.makeConstraints { (make) in
            make.top.equalTo(mMenuCardCollection)
            make.left.equalTo(mMenuCardCollection)
            make.bottom.equalTo(mMenuCardCollection)
            make.right.equalTo(mMenuCardCollection)
        }
        mBackgroundImage.contentMode = .scaleAspectFill
        mBackgroundImage.image = UIImage(named: self.backgroundImageName())
        
    }
    
    func backgroundImageName() -> String {
        fatalError("Must Override to return background View Name here")
    }
    
    func setCollectionViewDelegateAndDataSource() {
        if(mMenuCardCollection != nil) {
            mMenuCardCollection.delegate = self
            mMenuCardCollection.dataSource = self
            mMenuCardCollection.register(UINib.init(nibName: "CardMenuViewCell", bundle: nil), forCellWithReuseIdentifier: "CardMenuViewCell")
            mMenuCardCollection.backgroundColor = UIColor.clear
        } else {
            fatalError("CollectionView Must not nil.")
        }
    }
    
    func setupMenuItems () {
        fatalError("Must Override to create Menu Item data here")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardMenuViewCell", for: indexPath) as! CardMenuViewCell
        let menuData = menuItems[indexPath.item]
        
        cell.mCollectionImage.image = UIImage(named: menuData.imageName!)
        cell.mCollectionLabel.text = menuData.name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let segue_id : String = self.segueForMenuItemAtIndex(indexPath.item)
        if (!segue_id.isEmpty) {
            self.performSegue(withIdentifier: segue_id, sender: self)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
    
    func segueForMenuItemAtIndex(_ index: Int) -> String {
        fatalError("Must Override to return segue id")
    }
    
    func changeStyleNavigationLabel(){
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width * 0.8, height: 44))
        lbl.text = self.getViewTitle()
        lbl.textColor = .white
        lbl.textAlignment = .left
        lbl.font = lbl.font.withSize(25.0)
        lbl.sizeToFit()
        navigationItem.titleView = lbl
    }
    
}

extension MenuCardViewController:UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        cellHeight = (collectionView.height - minimumSpacing * 2 - top * 2) / numberOfRow
        cellWidth = (collectionView.width - minimumInterSpacing - left * 2 ) / numberOfColumn
        if UIScreen.main.nativeBounds.height < 1136 {
            cellHeight = cellWidth * 1.1
        }
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
//        let deviceType = UIScreen.main.nativeBounds.height
//        if deviceType >= 1136 {
//            minimumSpacing = (collectionView.frame.height - cellHeight * numberOfRow) / 4
//        }
        return minimumSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        minimumInterSpacing = (collectionView.frame.width - cellWidth * numberOfColumn)/3
        return minimumInterSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        let top = self.collectionView(collectionView, layout: collectionViewLayout, minimumLineSpacingForSectionAt: section)
//        let left = self.collectionView(collectionView, layout: collectionViewLayout, minimumInteritemSpacingForSectionAt: section)
        return UIEdgeInsets(top:top , left: left, bottom: top, right: left)
    }
    
}
