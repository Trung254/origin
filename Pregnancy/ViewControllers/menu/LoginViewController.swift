//
//  LoginViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 1/7/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit



class AuthViewController: UIViewController, UITextFieldDelegate {
    
   
    var receiveType:String = ""
    var isShowPass:Bool = false
    var LoginItem : Array<MenuData> = [
        MenuData(name: "Continue", image: "key",type:"no image"),
        MenuData(name: "", image: "",type:"border"),
        MenuData(name: "Create account with Google", image: "google",type:"google"),
        MenuData(name: "Create account with Facebook", image: "facebook",type:"facebook"),
        MenuData(name: "Create account with Twiter", image: "twiter",type:"twitter"),
    ]
    
    @IBOutlet weak var mPassTF: UITextField!
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var mLinkLabel: UILabel!
    @IBOutlet weak var mTextView: UITextView!
    @IBOutlet weak var heightOfTextView: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        mTableView.register(UINib.init(nibName: "GuideTableViewCell", bundle: nil), forCellReuseIdentifier: "guideCell")
        mTableView.register(UINib.init(nibName: "BorderTableViewCell", bundle: nil), forCellReuseIdentifier: "BorderTableViewCell")
        mTableView.alwaysBounceVertical = false

        self.setNavigationTitle()
        self.mTableView.endEditing(true)
        mTableView.tableFooterView = UIView()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func setNavigationTitle(){
        
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
        lbl.text = receiveType
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(25.0)
        navigationItem.titleView = lbl
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.heightOfTextView?.constant = self.mTextView.contentSize.height
    }
    
    @IBAction func pressToShowPass(_ sender: Any) {
        if isShowPass == true {
            mPassTF.isSecureTextEntry = false
        }
        else {
            mPassTF.isSecureTextEntry = true
        }
        isShowPass = !isShowPass
    }
    
}

extension AuthViewController : UITableViewDelegate {
    
}

extension AuthViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LoginItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if LoginItem[indexPath.row].type == "border"{
            let cell:BorderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BorderTableViewCell", for: indexPath) as! BorderTableViewCell
            return cell
        }
        else {
            let cell:GuideTableViewCell = tableView.dequeueReusableCell(withIdentifier: "guideCell", for: indexPath) as! GuideTableViewCell
            cell.mImageView.image = UIImage(named: LoginItem[indexPath.row].image)
            cell.LabelView.text = LoginItem[indexPath.row].name
            cell.LabelView.textAlignment = .center
            cell.LabelView.font = UIFont.systemFont(ofSize: 16)
            
            switch LoginItem[indexPath.row].type {
            case "no image":
                if receiveType == "Sign up" {
                    cell.LabelView.text = "Continue"
                }
                cell.backgroundColor = #colorLiteral(red: 0.2862745098, green: 0.7254901961, blue: 0.7803921569, alpha: 1)
                cell.mImageView.isHidden = true
                cell.LabelView.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            case "google":
                cell.LabelView.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
            case "twitter":
                cell.backgroundColor = #colorLiteral(red: 0, green: 0.6745098039, blue: 0.9333333333, alpha: 1)
                cell.LabelView.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            case "facebook":
                cell.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.3529411765, blue: 0.6, alpha: 1)
                cell.LabelView.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            default:
                print("no selected")
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let verticalPadding: CGFloat = 14
        
        let maskLayer = CALayer()
        maskLayer.backgroundColor = UIColor.black.cgColor
        maskLayer.frame = CGRect(x: cell.bounds.origin.x, y: cell.bounds.origin.y, width: cell.bounds.width, height: cell.bounds.height).insetBy(dx: 8, dy: verticalPadding/2)
        cell.layer.mask = maskLayer
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}



