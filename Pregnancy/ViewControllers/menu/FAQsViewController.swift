//
//  FAQsViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/4/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

struct cellDataFAQs {
    var opened = Bool()
    var title = String()
    var sectionData = [String]()
}

class FAQsViewController: UIViewController {

    @IBOutlet weak var mFAQsTableView: UITableView!
    
    var tableViewData : [cellDataFAQs] = [
        cellDataFAQs(opened: false, title: "Why does it say I'm 20 weeks, when I'm 19 weeks and 3 days?", sectionData: ["The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings"]),
        cellDataFAQs(opened: false, title: "I've updated the App and now it looks as if I have to buy it again?", sectionData: ["The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings"]),
        cellDataFAQs(opened: false, title: "Refunds and Payment Problems!", sectionData: ["The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings"]),
        cellDataFAQs(opened: false, title: "How do I share information from the App?", sectionData: ["The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings"]),
        cellDataFAQs(opened: false, title: "How do I set the App for a twin pregnancy?", sectionData: ["The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings"]),
        cellDataFAQs(opened: false, title: "Why should I create an account?", sectionData: ["The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings"]),
        cellDataFAQs(opened: false, title: "I recently switched from iOS to Android (or vice versa)", sectionData: ["The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings"]),
        cellDataFAQs(opened: false, title: "Help - I can't reset my password?", sectionData: ["The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings"]),
        cellDataFAQs(opened: false, title: "How is my due date calculated?", sectionData: ["The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings The week number you see d depends on your settings. In Settings"]),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        setupNavigationBar()
        setupTableView()
    }

    private func setupNavigationBar() {
        let lbl = UILabel()
        lbl.text = "FAQs"
        lbl.textColor = UIColor.white
        lbl.font = lbl.font.withSize(20.0)
        navigationItem.titleView = lbl
    }
    
    private func setupTableView() {
        let nib = UINib(nibName: "FAQsTableViewCell", bundle: Bundle.main)
        mFAQsTableView.register(nib, forCellReuseIdentifier: "FAQsCell")
        mFAQsTableView.delegate = self
        mFAQsTableView.dataSource = self
        self.mFAQsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.mFAQsTableView.rowHeight = UITableView.automaticDimension
        self.mFAQsTableView.estimatedRowHeight = 200
    }
}

extension FAQsViewController : UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewData[section].opened == true {
            return tableViewData[section].sectionData.count + 1
        } else {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 54
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQsCell", for: indexPath) as! FAQsTableViewCell
        if indexPath.row == 0 {
            cell.mLblFAQsCell.text = tableViewData[indexPath.section].title
            //configureCel(cell, indexPath: indexPath)
            if tableViewData[indexPath.section].opened == true {
                cell.mLblFAQsCell.textColor = UIColor(red: 85/255, green: 200/255, blue: 202/255, alpha: 1)
                cell.backgroundColor = UIColor(red: 93/255, green: 203/255, blue: 218/255, alpha: 0.1)
            }
            return cell
        } else {
            cell.mLblFAQsCell.text = tableViewData[indexPath.section].sectionData[indexPath.row - 1]
            cell.mLblFAQsCell.textColor = UIColor.black
            cell.backgroundColor = UIColor.white
            return cell
        }
    }
    
//    private func configureCel(_ cell: FAQsTableViewCell, indexPath: IndexPath) {
//        if tableViewData[indexPath.section].opened == true {
//            cell.mLblFAQsCell.textColor = UIColor(red: 0/255, green: 156/255, blue: 177/255, alpha: 1)
//            cell.backgroundColor = UIColor(red: 93/255, green: 203/255, blue: 218/255, alpha: 0.1)
//        } else {
//            cell.mLblFAQsCell.textColor = UIColor.black
//            cell.backgroundColor = UIColor.white
//        }
//    }
}

extension FAQsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableViewData[indexPath.section].opened == true {
            tableViewData[indexPath.section].opened = false
            let section = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(section, with: .none)
        } else {
            tableViewData[indexPath.section].opened = true
            let section = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(section, with: .none)
        }
    }
}
