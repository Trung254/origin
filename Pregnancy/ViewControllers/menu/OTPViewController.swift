//
//  OTPViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/21/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import FirebaseAuth
import SwiftyJSON



class OTPViewController: BaseViewController {
    
    @IBOutlet weak var mCountDownLbl: UILabel!
    @IBOutlet weak var mInputTF: UITextField!
    @IBOutlet weak var mcontinueBtn: UIButton!
    
    var mIsCounting: Bool = false
    var mTimer: Timer?
    let dateFormatter: DateFormatter = DateFormatter()
    var durationCountDown: TimeInterval = 300
    var mIsSignup: Bool = false
    var mIsForgotPassWord : Bool = false
    var mPhoneNumber: String?
    var mCount:Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.startCountDown()
        UIDevice.vibrate()
        self.mCountDownLbl.isHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.setTitle(title: "Nhập mã xác thực")
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
        self.mcontinueBtn.isEnabled = true
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    func startCountDown() -> Void {
//        if(!self.mIsCounting) {
//            setCountDowntime(durationCountDown)
//            self.mIsCounting = true
//            self.mInputTF.isUserInteractionEnabled = true
//            self.mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkCounter), userInfo:nil, repeats: true)
//        }
    }
    
    @objc func checkCounter() -> Void {
        durationCountDown -= 1
        if(durationCountDown < 0) {
            self.stopCounting()
            durationCountDown = 300
            self.popUpStopTimer()
        }
        self.setCountDowntime(durationCountDown)
    }
    
    private func popUpStopTimer() {
        self.mInputTF.isUserInteractionEnabled = false
//        let alert = UIAlertController(title: "Thông báo", message: "Phiên làm việc của bạn đã hết hạn", preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
    }
    
    func stopCounting() {
        self.mTimer?.invalidate()
        self.mIsCounting = false
        durationCountDown = 300
        self.setCountDowntime(durationCountDown)
    }
    
    private func setCountDowntime(_ timeInterval:TimeInterval) -> Void {
        let timeString = self.stringFromTimeInterval(interval: timeInterval)
        self.mCountDownLbl.text = timeString
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    @IBAction func Back_To_Forgot_PassWord(_ sender: Any) {
        self.doDefaultBack(sender)
    }
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        if segue.identifier == "OTPViewControllerToSettingCreatAccount" {
    //            if let vc = segue.destination as? NewPasswordViewController {
    //                vc.mIsSignup = self.mIsSignup
    //                vc.mPhoneNumber = self.mPhoneNumber
    //            }
    //        }
    //    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        super.prepare(for: segue, sender: sender)
//        if(segue.identifier == "OTPViewcontrollerToNewPassword") {
//            if let vc = segue.destination as? NewPasswordViewController {
//                vc.mIsForgotPassWord = true
//            }
//        }
//    }
    
    @IBAction func mContinueDidSelect(_ sender: UIButton) {
        self.showHud()
        sender.isEnabled = false
        DispatchQueue.global(qos: .background).asyncAfter(deadline: DispatchTime.now() + 0.1) {
            DispatchQueue.main.async {
                self.doVerifyCode(sender)
            }
        }
        
    }
    
    @objc func doVerifyCode(_ sender: UIButton) {
        
        if(mIsSignup) {
            
            if let  verificationCode = mInputTF.text, verificationCode.isEmpty == false {
                if let verificationID = UserDefaults.standard.string(forKey: "authVerificationID") {
                    Auth.auth().useAppLanguage()
                    let credential = PhoneAuthProvider.provider().credential(
                        withVerificationID: verificationID,
                        verificationCode: verificationCode)
                    Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                        if error != nil {
                            if (self.mCount < 2) {
                                self.alert("Bạn còn \(2 - self.mCount) lần nhập")
                                self.mCount = self.mCount + 1
                            } else {
                                self.mCount = 0
                                self.doDefaultBack(nil)
//                                self.alert(error.localizedDescription, title: "", handler: { (action) in
//
//                                })
                            }
                            
                            
                            self.hideHud()
                            sender.isEnabled = true
                            
                            return
                        }
                        self.mCount = 0
                        // User is signed in
                        // ...
                        self.hideHud()
                        AuthManager.shared.authPassword = self.mInputTF.text
                        
                        // alert mã xác nhận gửi thành công
                        
//                        let actionSheetController = UIAlertController (title: "Thông báo", message: "Mã xác nhận đã gửi thành công", preferredStyle: .alert)
                        
//                        Add Save-Action
//                        actionSheetController.addAction(UIAlertAction(title: "Xác nhận", style: UIAlertAction.Style.default, handler: { (actionSheetController) -> Void in
//                            self.performSegue(withIdentifier: "OTPViewControllerToSettingCreatAccount", sender: sender)
//                            sender.isEnabled = false
//                        }))
//
                        self.performSegue(withIdentifier: "OTPViewControllerToSettingCreatAccount", sender: sender)
                        sender.isEnabled = false
                        

//                        self.present(actionSheetController, animated: true, completion: nil)

                    }
                    
                }
            } else {
                sender.isEnabled = true
                self.alert("Vui lòng nhập mã xác nhận")
            }
            self.hideHud()
            
        }
        
        if(mIsForgotPassWord) {
            
            if let  verificationCode = mInputTF.text, verificationCode.isEmpty == false {
                if let verificationID = UserDefaults.standard.string(forKey: "authVerificationID") {
                    Auth.auth().useAppLanguage()
                    let credential = PhoneAuthProvider.provider().credential(
                        withVerificationID: verificationID,
                        verificationCode: verificationCode)
                    Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                        if error != nil {
                            if (self.mCount < 2) {
                                self.alert("Bạn còn \(2 - self.mCount) lần nhập")
                                self.mCount = self.mCount + 1
                            } else {
                                self.mCount = 0
                                self.doDefaultBack(nil)
                                //                                self.alert(error.localizedDescription, title: "", handler: { (action) in
                                //
                                //                                })
                            }
                            
                            
                            self.hideHud()
                            sender.isEnabled = true
                            
                            return
                        }
                        self.mCount = 0
                        // User is signed in
                        // ...
                        self.hideHud()
                        AuthManager.shared.authPassword = self.mInputTF.text
                        self.performSegue(withIdentifier: "OTPViewcontrollerToNewPassword", sender: sender)
                        
                        sender.isEnabled = false
                    }
                    
                }
            } else {
                sender.isEnabled = true
                self.alert("Vui lòng nhập mã xác nhận")
            }
            self.hideHud()
            
        }
        
    }
    
    @IBAction func resendCodeDidSelected(_ sender: Any) {
        Auth.auth().useAppLanguage()
        PhoneAuthProvider.provider().verifyPhoneNumber(dataUser.phone.convertVietnamPhoneNumer(true)!, uiDelegate: self) { (verificationID, error) in
            if let error = error {
                self.alert(error.localizedDescription)
                self.hideHud()
                return
            }
            self.stopCounting()
            self.startCountDown()
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            UserDefaults.standard.synchronize()
            // the alert view
            let alert = UIAlertController(title: "Thông báo", message: "Đã gửi SMS thành công", preferredStyle: .alert)
            self.present(alert, animated: true, completion: nil)
            
            // change to desired number of seconds (in this case 5 seconds)
            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when){
                // your code with delay
                alert.dismiss(animated: true, completion: nil)
            }
            self.hideHud()
            
        }
    }
}


extension OTPViewController: GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension OTPViewController: GIDSignInDelegate {
    // Present a view that prompts the user to sign in with Google
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("\(error.localizedDescription)")
        }
        else {
            //            let userId = user.userID
            //            let idToken = user.authentication.idToken
            //            let fullName = user.profile.name
            //            let givenName = user.profile.givenName
            //            let familyName = user.profile.familyName
            //            let email = user.profile.email
            //            let imageUrl = user.profile.imageURL(withDimension: UInt(400))
            
        }
    }
    
}

extension OTPViewController: AuthUIDelegate {
    
}
