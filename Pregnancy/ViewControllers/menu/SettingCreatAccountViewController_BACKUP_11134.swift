//
//  SettingCreatAccountViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/22/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class SettingDueDateViewController: BaseViewController {

    var dateGetFromDueDate : String?
    
    var periodSaveFomCalDueDate = ""
    
    @IBOutlet weak var btnDueDate: UIButton!
    @IBOutlet weak var btnDueDateLbl: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let textAttr : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : UIFont.italicSystemFont(ofSize: 17),
            NSAttributedString.Key.foregroundColor : UIColor.black,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        btnDueDateLbl.setAttributedTitle(NSMutableAttributedString(string: "phân tích",
                                                                 attributes: textAttr), for: .normal)
        
        let image = UIImage(named:"Acc5")
        btnDueDate.setImage(image, for: .normal)
        btnDueDate.imageView?.contentMode = .scaleAspectFit
        btnDueDate.contentHorizontalAlignment = .left
        btnDueDate.imageEdgeInsets = UIEdgeInsets(top:0, left:btnDueDate.frame.width - 40, bottom:0, right:0)
        btnDueDate.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
        self.btnDueDate.setTitle(dateGetFromDueDate, for: .normal)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    @IBAction func mBtnDueDateTouchUpInside(_ sender: Any) {
        let pickerDate = PickerDateView.initView()
        pickerDate.delegate = self
        self.navigationController?.view.addSubview(pickerDate)
    }
<<<<<<< Updated upstream
    @IBAction func showDueDateCal(_ sender: Any) {
        let dueDateSB = UIStoryboard.init(name: "menu", bundle: nil)
        let dueDateVC = dueDateSB.instantiateViewController(withIdentifier: "dueDateStoryboard") as! CalculatorDueDateViewController
        dueDateVC.datePushFromSetting = periodSaveFomCalDueDate
        dueDateVC.mSettingCreateAccount = self
        self.navigationController?.pushViewController(dueDateVC, animated: true)

    }
=======
    
>>>>>>> Stashed changes
    @IBAction func mBtnAgreeTouchUpInside(_ sender: Any) {
        if let currentViewController = ViewManager.getCurrentViewController() {
            if let navigationController = currentViewController.navigationController {
                navigationController.popToRootViewController(animated: true)
            }
        }
    }
}

extension SettingDueDateViewController: PickerDateViewDelegate {
    func doneTapped(_ sender: PickerDateView) {
        print("tapped ben setting")
    }
    
    func didSelectedDateString(dateString: String) {
        self.btnDueDate.setTitle(dateString, for: .normal)
    }
}
