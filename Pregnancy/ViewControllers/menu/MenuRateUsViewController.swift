//
//  MenuRateUsViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/28/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SnapKit
import StoreKit
import MessageUI


class MenuRateUsViewController: UIViewController, UITextViewDelegate, UIScrollViewDelegate, SKStoreProductViewControllerDelegate, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var mRateUsView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mTextViewRateUs: UITextView!
    
    let aboveView:UIView = UIView(frame: UIScreen.main.bounds)
    
    override func viewDidLoad() {
        super.viewDidLoad()
   

        self.view.removeSubsView()
        self.view.alpha = 0
        
        let mutableAttributedString = NSMutableAttributedString(string: "Ngoài ra, nếu bạn muốn nói chuyện với nhóm Dịch vụ khách hàng của chúng tôi, vui lòng liên hệ với chúng tôi tại đây")
        mutableAttributedString.setAsLink(textToFind: "đây", linkName: "day")
        
        mTextViewRateUs.attributedText = mutableAttributedString
        mTextViewRateUs.delegate = self
        mTextViewRateUs.isScrollEnabled = false
        mTextViewRateUs.isEditable = false
        //mTextViewRateUs.textContainerInset = .zero
        //mTextViewRateUs.textContainer.lineFragmentPadding = 0
        mTextViewRateUs.font = UIFont.systemFont(ofSize: 19)

//        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.showAnimate()
        scrollView.delegate = self
        
        self.setupViewAbove()
        
        
    }
    
    
    // setup View above navigation and tabbar
    func setupViewAbove() {
        if let applicationDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate? {
            if let window:UIWindow = applicationDelegate.window {
                
                self.aboveView.backgroundColor = UIColor.clear.withAlphaComponent(0)
                
                self.aboveView.addSubview(mRateUsView)
                self.aboveView.sendSubviewToBack(mRateUsView)
                mRateUsView.snp.makeConstraints { (maker) in
                    maker.top.equalToSuperview().offset(100)
                    maker.bottom.equalToSuperview().offset(-100)
                    maker.leading.equalToSuperview().offset(16)
                    maker.trailing.equalToSuperview().offset(-16)
                }

                window.addSubview(aboveView)
            }
        }
    }
//    @IBAction func sendEmailButtonTapped(_ sender: Any) {
//        let mailComposeViewController = configuredMailComposeViewController()
//        if MFMailComposeViewController.canSendMail() {
//            self.present(mailComposeViewController, animated: true, completion: nil)
//        } else {
//            self.showSendMailErrorAlert()
//        }
//    }
//
//    func configuredMailComposeViewController() -> MFMailComposeViewController {
//        let mailComposerVC = MFMailComposeViewController()
//        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
//
//        mailComposerVC.setToRecipients(["info@medlatec.com"])
//        mailComposerVC.setSubject("Đánh giá về ứng đụng của chúng tôi")
//        mailComposerVC.setMessageBody("", isHTML: false)
//
//        return mailComposerVC
//    }
//
//
//    func showSendMailErrorAlert() {
//        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
//        sendMailErrorAlert.show()
//    }
//
//    // MARK: MFMailComposeViewControllerDelegate Method
//    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
//        controller.dismiss(animated: true, completion: nil)
//    }
    
    @IBAction func RateUs(_ sender: Any) {
        
//        let storeViewController = SKStoreProductViewController()
//        storeViewController.delegate = self
//        let parameters = [SKStoreProductParameterITunesItemIdentifier :
//            NSNumber(value: 505864483)]
//        storeViewController.loadProduct(withParameters: parameters,
//                                        completionBlock: {success, error in
//                                            if (success == true) {
//                                                self.present(storeViewController,
//                                                             animated: true, completion: nil)
//                                            }
//                                            else {
//                                                print(error?.localizedDescription as Any)
//                                            }
//        })
        
        //id1453622066
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1453622066"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
        self.removeAnimate()
         
    }
    
//    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
//        viewController.dismiss(animated: true, completion: nil)
//    }
    
    
    @IBAction func Dissmiss(_ sender: Any) {
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    @IBAction func RateUsCancel(_ sender: Any) {
        
//        dismiss(animated: false)
//        print("Click cancel rate Us")
        
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    func showAnimate()
    {
        self.aboveView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        self.aboveView.alpha = 0.5
        UIView.animate(withDuration: 0.3, animations: {
            self.aboveView.alpha = 1.0
            self.aboveView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.3, animations: {
            self.aboveView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            self.aboveView.alpha = 0.5
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.aboveView.removeFromSuperview()
                
            }
        });
    }
    
    func textView(_ mTextViewRateUs: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL.absoluteString == "day" {
//            dismiss(animated: true)
            self.removeAnimate()
            //self.showAnimate()
            // Handle action tap on AppleStoreLink
            print("Go to mail")
//            let contactUs = self.storyboard?.instantiateViewController(withIdentifier: "MenuContactUsViewController") as! MenuContactUsViewController
//            self.presentingViewController?.presentMenu(contactUs)
            let activityController = UIActivityViewController(activityItems: ["Rất tiếc vì đã làm phiền bạn. Phản ảnh với chúng tôi về vấn đề của bạn. Chúng tôi sẽ cố gắng giải quyết vấn đề đó. Xin cảm ơn "], applicationActivities: nil)
//            activityController.setValue("support@medlatec.com", forKey: "mailto://")
            activityController.setValue("Phản ảnh lại với chúng tôi", forKey: "subject")
            UIApplication.shared.keyWindow?.rootViewController?.present(activityController, animated: true, completion: nil)
            
            return true
        }
        return false
    }
    
    private func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
    
}

extension NSMutableAttributedString {
    func setAsLink(textToFind:String, linkName:String) {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSAttributedString.Key.link, value: linkName, range: foundRange)
        }
    }
}

//extension UITextView: UITextViewDelegate {
//    public func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
//        if URL.absoluteString == "here" {
//            print("Tap vao here")
//            return true
//        }
//        return false
//    }
//
//}
