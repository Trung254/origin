//
//  AppSettingViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 1/3/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class AppSettingViewController: BaseViewController {

    @IBOutlet weak var mTableView: UITableView!
    
    var settingData : Array<MenuData> = [
        MenuData(name: "Nhắc nhở", image: "speech-bubble",type: "switch"),
        MenuData(name: "", image: "", type: "border"),
        MenuData(name: "Reset dữ liệu", image: "redo-circular-arrow", type: "not button")
    ]
    var dataSettings: [JSON] = []
    var dataPicker = ["kg","lbs","st"]
    @IBOutlet weak var mPickerSettings: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLeftMainMenu()
        self.setTitle(title: "Cài đặt")
        mTableView.register(UINib.init(nibName: "SwitchSettingTableViewCell", bundle: nil), forCellReuseIdentifier: "SwitchSettingTableViewCell")
        mTableView.register(UINib.init(nibName: "GuideTableViewCell", bundle: nil), forCellReuseIdentifier: "guideCell")
        mTableView.register(UINib.init(nibName: "BorderBottomTableViewCell", bundle: nil), forCellReuseIdentifier: "BorderBottomTableViewCell")
        mTableView.alwaysBounceVertical = false;
        self.loadAppSetting()
        self.mPickerSettings.isHidden = true
    }
    
    override func handleLeftMenu() {
        self.showMainMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = true
        self.mIsShowBackButton = false
        super.viewWillAppear(animated)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    func setNavigationTitle(){
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
        lbl.text = "Cài đặt"
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(25.0)
        navigationItem.titleView = lbl
    }
    
    @IBAction func pressDone(_ sender: Any) {
        if dataSettings.isEmpty == false {
            let reminders = dataSettings[0]["reminders"].boolValue
            self.updateSetting(reminders)
            if let currentViewController = ViewManager.getCurrentViewController() {
                if let navigationController = currentViewController.navigationController {
                    navigationController.popToRootViewController(animated: true)
                }
            }
        }
    }

    private func loadAppSetting() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAppSetting() {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as? JSON
                self.dataSettings = data!.arrayValue
            }
            DispatchQueue.main.async {
                self.mTableView.reloadData()
                self.hideHud()
            }
        }
    }
    
    private func updateSetting(_ reminders: Bool) {
        self.showHud()
        NetworkManager.shareInstance.apiPutAppSetting(reminders: reminders) {
            (data, message, isSuccess) in
            DispatchQueue.main.async {
                self.hideHud()
            }
        }
    }
    
//    @objc func btnShowPicker(_ sender : UIButton){
//         self.mPickerSettings.isHidden = false
//    }
    
    @objc func changeReminder(_ sender : UISwitch){
        if sender.isOn == true {
            dataSettings[0]["reminders"].boolValue = true
        }
        else {
           dataSettings[0]["reminders"].boolValue = false
        }
        
        mTableView.reloadData()
    }
    
//    @objc func chanegLegthUnit(_ sender : UISwitch){
//        if sender.isOn == true {
//            dataSettings[0]["length_units"].boolValue = true
//        }
//        else {
//            dataSettings[0]["length_units"].boolValue = false
//        }
//
//        mTableView.reloadData()
//    }
 }

extension AppSettingViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            let alert = UIAlertController(title: "Message", message: "Bằng cách nhấn nút đặt lại, bạn sẽ xóa tất cả dữ liệu đã nhập trước và ứng dụng sẽ trở về trạng thái ban đầu khi bạn mua. Bạn có chắc chắn muốn đặt lại ứng dụng", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Quay lai", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Đặt lại ứng dụng", style: .default){
                UIAlertAction in
                
                NetworkManager.shareInstance.apiResetdataUser ()  {
                    (data, message, isSuccess) in
                    if (isSuccess){
                    DatabaseManager.sharedManager.logout {
                        
                        }
                        
                    }
                    
                }
                
                
            })
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension AppSettingViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       if settingData[indexPath.row].type == "border" {
            let cell:BorderBottomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BorderBottomTableViewCell", for: indexPath) as! BorderBottomTableViewCell
            cell.height = 1
            return cell
        }
        else {
            let cell:SwitchSettingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SwitchSettingTableViewCell", for: indexPath) as! SwitchSettingTableViewCell
            cell.mImage.image = UIImage(named: settingData[indexPath.row].image)
            cell.mLabel.text = settingData[indexPath.row].name
//            cell.mWeightButton.removeTarget(nil, action: nil, for: .allEvents)
////            cell.mWeightButton.addTarget(self, action: #selector(self.btnShowPicker(_:)), for: .touchUpInside)
            if settingData[indexPath.row].type == "switch" {
                cell.mView2.isHidden = true
                if dataSettings.isEmpty == false {
                    if indexPath.row == 0 {
                        if dataSettings[0]["reminders"].boolValue == true {
                            cell.mSwitch.isOn = true
                            cell.mSwitchLabel.text = "Bật"
                        }
                        else {
                            cell.mSwitch.isOn = false
                            cell.mSwitchLabel.text = "Tắt"
                        }
                        
                        cell.mSwitch.removeTarget(nil, action: nil, for: .allEvents)
                        cell.mSwitch.addTarget(self, action: #selector(self.changeReminder(_:)), for: .valueChanged)
                    }
                    
//                    if indexPath.row == 2 {
//                        if dataSettings[0]["length_units"].boolValue == true {
//                            cell.mSwitch.isOn = true
//                            cell.mSwitchLabel.text = "in"
//                        }
//                        else {
//                            cell.mSwitch.isOn = false
//                            cell.mSwitchLabel.text = "cm"
//                        }
//
//                        cell.mSwitch.addTarget(self, action: #selector(self.chanegLegthUnit(_:)), for: .valueChanged)
//                    }
                }
            }
//            else if settingData[indexPath.row].type == "button" {
//                cell.mView1.isHidden = true
//                if dataSettings != [] {
//                    if dataSettings[0]["weight_unit"].intValue == 1 {
//                        cell.mWeightButton.setTitle("kg", for: .normal)
//                    }
//                    else if dataSettings[0]["weight_unit"].intValue == 2 {
//                        cell.mWeightButton.setTitle("lbs", for: .normal)
//                    }
//                    else {
//                        cell.mWeightButton.setTitle("st", for: .normal)
//                    }
//                }
//            }
            else{
                cell.mView1.isHidden = true
                cell.mView2.isHidden = true
            }
        
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if settingData[indexPath.row].type == "border"{
            return 1
        }
        else {
            return UITableView.automaticDimension
        }
    }
}

extension AppSettingViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataPicker.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataPicker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        let indexPath = IndexPath(row: 3, section: 0)
//        let cell:SwitchSettingTableViewCell = mTableView.dequeueReusableCell(withIdentifier: "SwitchSettingTableViewCell", for: indexPath) as! SwitchSettingTableViewCell

        dataSettings[0]["weight_unit"].intValue = row + 1
        self.mTableView.reloadData()
    }
}
