//
//  MyAccountViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit


class MyAccountViewController: BaseViewController {
   
    @IBOutlet weak var heightOfTableView: NSLayoutConstraint!
    
    @IBOutlet weak var mTableView: UITableView!
    
    var accountStyle : Array<MenuData> = [
        MenuData(name: "Đăng nhập (tài khoản hiện tại)", image: "key",type:"Log in"),
        MenuData(name: "Đăng ký (tài khoản mới)", image: "plus-1",type: "Sign up"),
        MenuData(name: "", image: "",type: "border"),
        MenuData(name: "Tên đầu tiên:", image: "woman",type: "button"),
        MenuData(name: "Họ:", image: "woman",type: "button"),
        MenuData(name: "Bạn là", image: "woman",type: "button"),
        MenuData(name: "Vị trí", image: "placeholder",type: "button")
    ]
    
    var passType:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.tintColor = UIColor.white
        mTableView.register(UINib.init(nibName: "SwitchSettingTableViewCell", bundle: nil), forCellReuseIdentifier: "SwitchSettingTableViewCell")
        mTableView.register(UINib.init(nibName: "BorderBottomTableViewCell", bundle: nil), forCellReuseIdentifier: "BorderBottomTableViewCell")
        mTableView.alwaysBounceVertical = false;

        self.setNavigationTitle()
        self.setNavigationIcon()
    }
    
    func setNavigationIcon(){
        
        let rightButton = UIButton(type: .custom)
        rightButton.setImage(UIImage(named: "info (1)"), for: .normal)
        rightButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        rightButton.addTarget(self, action: #selector(handleNavigationIcon), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: rightButton)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func handleNavigationIcon(){
        
    }
    
    override func doDefaultBack(_ sender: Any?) {
        super.doDefaultBack(sender)
    }
    
    func setNavigationTitle(){
        
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
        lbl.text = "Tài khoản của tôi"
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(25.0)
        navigationItem.titleView = lbl
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.heightOfTableView?.constant = self.mTableView.contentSize.height
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "myAccount_to_Login" {
            let viewController = segue.destination as! AuthViewController
            viewController.authType = (passType == "Log in") ? .Login : .SignUp
        }
    }
}

extension MyAccountViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if accountStyle[indexPath.row].type == "Log in" || accountStyle[indexPath.row].type == "Sign up" {
            passType = accountStyle[indexPath.row].type
            performSegue(withIdentifier: "myAccount_to_Login", sender: self)
            
        }
    }
}

extension MyAccountViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountStyle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if accountStyle[indexPath.row].type == "border" {
            let cell:BorderBottomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BorderBottomTableViewCell", for: indexPath) as! BorderBottomTableViewCell
            cell.height = 1
            return cell
        }
        else {
            let cell:SwitchSettingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SwitchSettingTableViewCell", for: indexPath) as! SwitchSettingTableViewCell
            cell.mImage.image = UIImage(named: accountStyle[indexPath.row].image)
            cell.mLabel.text = accountStyle[indexPath.row].name
            cell.mLabel.sizeToFit()
            if accountStyle[indexPath.row].type == "Log in" || accountStyle[indexPath.row].type == "Sign up" {
                cell.mView1.isHidden = true
                cell.mView2.isHidden = true
            }
            else {
                cell.selectionStyle = .none
                cell.mView1.isHidden = true
            }
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if accountStyle[indexPath.row].type == "border"{
            return 1
        }
        else {
            return UITableView.automaticDimension
        }
        
    }
}
