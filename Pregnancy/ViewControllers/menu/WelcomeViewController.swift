//
//  WelcomeViewController.swift
//  Pregnancy
//
//  Created by dady on 1/19/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

struct accountUser {
    var isSignup: Bool
    var phone: String
    var socialTypeId: Int
    var firstName: String
    var lastName: String
    var youAreThe: String
    var location: String
    var startDate: String
    var dueDate: String
    var dataGender: Int
    var weekPregnancy: String
    var weightUnit: Int
    var lengthUnit: Bool
    
    init(isSignup: Bool, phone: String, socialTypeId: Int, firstName: String, lastName: String, youAreThe: String, location: String, startDate: String, dueDate: String, dataGender: Int, weekPregnancy: String, weightUnit: Int,lengthUnit: Bool) {
        self.isSignup = isSignup
        self.phone = phone
        self.socialTypeId = socialTypeId
        self.firstName = firstName
        self.lastName = lastName
        self.youAreThe = youAreThe
        self.location = location
        self.startDate = startDate
        self.dataGender = dataGender
        self.dueDate = dueDate
        self.weekPregnancy = weekPregnancy
        self.weightUnit = weightUnit
        self.lengthUnit = lengthUnit
    }
}

var dataUser: accountUser = accountUser(isSignup: true,phone: "",socialTypeId: 0,firstName: "",lastName: "",youAreThe: "",location: "", startDate: "",dueDate: "", dataGender: 1, weekPregnancy: "",weightUnit: 1, lengthUnit: false)


class WelcomeViewController: BaseViewController {

    static let username = "user"
    static let password = "123"
    static let grant_type = "password"
    
    @IBOutlet var mViewWelcome: UIView!
    @IBOutlet weak var mbtnLogin: UIButton!
    @IBOutlet weak var mbtnPrivacy: UIButton!
    @IBOutlet weak var mGuestBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        ViewManager.setWelcomeViewController(self)
        self.mViewWelcome.addBackground(imageName: "login-bg")
        mbtnLogin.layer.borderColor = UIColor.white.cgColor
        mbtnLogin.layer.borderWidth = 1

        let textAttr : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : UIFont.italicSystemFont(ofSize: 17),
            NSAttributedString.Key.foregroundColor : UIColor.white,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        mbtnPrivacy.setAttributedTitle(NSMutableAttributedString(string: "riêng tư",
                                                              attributes: textAttr), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mGuestBtn.isEnabled = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "welcomeViewController_to_Login" {
            let viewController = segue.destination as! AuthViewController
            viewController.authType = .Login
        } else if (segue.identifier == "welcomeViewController_to_Signup") {
            let viewController = segue.destination as! AuthViewController
            viewController.authType = .SignUp
        } else if (segue.identifier == "welcome_view_controller_to_setting_create_account_view_controller") {
            let viewController = segue.destination as! SettingCreatAccountViewController
            viewController.isGuest = true
        }
    }
    
    
    @IBAction func guestUserSelected(_ sender: UIButton) {
        sender.isEnabled = false
        NetworkManager.shareInstance.apiGuestRegister { (data, message, isSuccess) in
            if (isSuccess) {
                if let data = data {
                    DatabaseManager.sharedManager.login(in: self, user: data["phone"].stringValue , password: data["password"].stringValue, token: nil, provider: nil) { (user, isTempData, isGetInfoSuccess, isLoginSuccess) in
                        //login failed
                        if(!isLoginSuccess){
                            self.hideHud()
                            self.alert("Sai số điện thoại hoặc mật khẩu.")
                            sender.isEnabled = true
                            return
                        }
                        // login succeed but failed to get info
                        if(!isGetInfoSuccess) {
                            self.hideHud()
                            self.alert("Vui lòng thử lại sau!")
                            sender.isEnabled = true
                            return
                        }
                        
                        // check if data on cloud is empty ( name ) then let's user put their info if need
                        if(!isTempData) {
                            DatabaseManager.sharedManager.getPregnancys(callBack: { (pregnancy, isTemp, isSuccess, rawData) in
                                self.hideHud()
                                if let user = user, let rawData = rawData {
                                    let firstData = rawData.arrayValue.first
                                    if user.first_name.isEmpty || firstData == nil || (firstData != nil && firstData?["due_date"].stringValue.count == 0 )  {
                                        if let social_type = Int(user.social_type_id) {
                                            dataUser.socialTypeId = social_type
                                        }
                                        dataUser.isSignup = false
                                        self.performSegue(withIdentifier: "welcome_view_controller_to_setting_create_account_view_controller", sender: nil)
                                        sender.isEnabled = true
                                        return
                                    }
                                    
                                }
                                
                                // show main view
                                self.showMainView()
                                sender.isEnabled = true
                            })
                            
                        }
                    }
                } else {
                    sender.isEnabled = true
                }
                
            } else {
                self.alert("Vui lòng thử lại sau")
                sender.isEnabled = true
            }
        }
        
    }
    
    @objc func showMainView() {
        if(Thread.isMainThread != true) {
            self.perform(#selector(showMainView), on: Thread.main, with: nil, waitUntilDone: true)
            return
        }
        
        if(ViewManager.getRootTabbarController() == nil) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            let rootVC = ViewManager.getRootViewController()
            rootVC.createRootTabbarControllerIfNeed()
            
            ViewManager.showTabbarController()
        } else {
            self.doDefaultBack(nil)
        }
    }
    
}
