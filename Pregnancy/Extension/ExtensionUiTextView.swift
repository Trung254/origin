//
//  ExtensionUiTextView.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 3/5/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

private var maxLengths = [UITextView: Int]()

extension UITextView : UITextViewDelegate {
    
    
    // Giới hạn ký tự của uitextview (edit trong storyboard)
    @IBInspectable var maxLength: Int {
        
        get {
            
            guard let length = maxLengths[self]
                else {
                    return Int.max
            }
            return length
        }
        set {
            maxLengths[self] = newValue
            self.delegate = self
        }
    }
    
    @objc func limitLength(textView: UITextView) {
        guard let prospectiveText = textView.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = String(prospectiveText[..<maxCharIndex])
        selectedTextRange = selection
        
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        limitLength(textView:textView)
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        scrollToBottom()
        return true
    }
    
    public func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        scrollToBottom()
        return true
    }
    
    func scrollToBottom() {
        let location = text.count - 1
        let bottom = NSMakeRange(location, 1)
        self.scrollRangeToVisible(bottom)
    }
    
}
