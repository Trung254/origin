//
//  CALayerExtension.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 3/21/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

extension CALayer {
    
    func bringToFront() {
        guard let sLayer = superlayer else {
            return
        }
        removeFromSuperlayer()
        sLayer.insertSublayer(self, at: UInt32(sLayer.sublayers?.count ?? 0))
    }
    
    func sendToBack() {
        guard let sLayer = superlayer else {
            return
        }
        removeFromSuperlayer()
        sLayer.insertSublayer(self, at: 0)
    }
}
