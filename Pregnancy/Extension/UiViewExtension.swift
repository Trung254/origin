//
//  UiViewExtension.swift
//  Thuat Toan
//
//  Created by Nguyễn Thanh on 3/6/19.
//  Copyright © 2019 TunBeo. All rights reserved.
//

import UIKit

extension UIView {
    
    // Lấy ra fisr Responder (subview nào đang được dùng) và trỏ đến nó
    var firstResponder: UIView? {
        guard !isFirstResponder else { return self }
        
        for subview in subviews {
            if let firstResponder = subview.firstResponder {
                return firstResponder
            }
        }
        
        return nil
    }
}
