//
//  AccountImageTableViewCell.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class AccountImageTableViewCell: UITableViewCell {
    

    @IBOutlet weak var mViewAccImage: UIView!
    @IBOutlet weak var mImageAcc: UIImageView!
    override func awakeFromNib() {
        
        super.awakeFromNib()
        mViewAccImage.layer.borderWidth = 3
        mViewAccImage.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        mViewAccImage.layer.cornerRadius = mViewAccImage.frame.width / 2
        mViewAccImage.layer.masksToBounds = true
        
        mImageAcc.layer.cornerRadius = mImageAcc.frame.width / 2
        mImageAcc.layer.masksToBounds = true
        
        self.loadAvatarFromDisk()
      
    }
    
    func loadAvatarFromDisk() -> Void {
        
        DatabaseManager.sharedManager.getUserInfo(callBack: { (user, isTemp, isSuccess) in
            if let user = user {
                var avatar = NetworkManager.rootDomain
                if user.avatar.hasPrefix("http") {
                    avatar = user.avatar
                } else {
                    avatar.append(user.avatar)
                }
                self.mImageAcc.sd_setImage(with: URL(string:avatar), completed: {(image, error, cacheType, url) in
                    if let image = image {
                        self.mImageAcc.image = image
                    } else {
                        self.mImageAcc.image = UIImage(named: "user_placeholder")
                    }
                })
            } else {
                self.mImageAcc.image = UIImage(named: "user_placeholder")
            }
        })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
