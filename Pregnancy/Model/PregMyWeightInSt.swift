//
//  PregMyWeightInSt.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/8/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift

class PregMyWeightInSt: Object {
    @objc dynamic var id = 0
    @objc dynamic var position = 0
    @objc dynamic var value = 0
}
