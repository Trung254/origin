//
//  DatabaseManager.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 1/25/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import MBProgressHUD
import SwiftyJSON

class DatabaseManager {
    
    static let guest_id: Int = 9
    
    var driver: Realm!
    var testDriver: Realm!
    
    //Tạo mặc định date
    var dateFormatter : DateFormatter {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormat.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormat
    }
    let currentDate = Date.init().addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
    
    class var sharedManager : DatabaseManager {
        struct Static {
            static let instance : DatabaseManager = DatabaseManager()
        }
        return Static.instance
    }
    
    init() {
        DispatchQueue.main.async {
            try! self.driver = Realm()
            try? self.testDriver = Realm()
        }
        
    }
    
    func getLocalToken() -> String? {
        if let user = self.getLocalUserInfo() {
            return user.authToken
        }
        return nil
    }
    
    func createUser() -> Void {
        
    }
    
    func isGuest() ->Bool {
        if let data = self.getLocalUserInfo() {
            return (data.phone.isEmpty && data.social_type_id.isEmpty)
        }
        
        return true
    }
    
    func isLogin() -> Bool {
        let data = self.getLocalUserInfo()
        return (data != nil)
    }
    
    func logout(_ loginType: Int = 0, _ callBack:@escaping ()->() ) {
        DispatchQueue.main.async {
            
            
            let kickListCheck = self.driver.objects(KickCounter.self)
            // Phần này để lưu lại phiên của kickcounter trước đó (mỗi user ID chỉ có 1 phiên)
            var kickSave = [KickCounter]()
            
            if kickListCheck.count > 0 {
                for i in 0..<kickListCheck.count {
                    let kick = KickCounter()
                    kick.dataKickCounter = kickListCheck[i].dataKickCounter
                    kick.kick_time = kickListCheck[i].kick_time
                    kick.status = kickListCheck[i].status
                    kick.user_id = kickListCheck[i].user_id
                    kick.eslapedTime = kickListCheck[i].eslapedTime
                    kick.kick_results_id = kickListCheck[i].kick_results_id
                    kick.start_date = kickListCheck[i].start_date
                    kick.end_date = kickListCheck[i].end_date
                    
                    kickSave.append(kick)
                }
            }
            
            // Xóa hết tất cả trong Realm
            try? self.driver.write {
                self.driver.deleteAll()
            }
            //Ghi lại kickCounter vào realm lần nữa
            if kickSave.count > 0 {
                try? self.driver.write {
                    for i in 0..<kickSave.count {
                        self.driver.add(kickSave[i])
                    }
                }
            }
            
            // done kickcounter
            
            // Xóa image sếp add vào khi login
            AttachmentHandler.shared.clearAllFile()
            //Done xóa
            
            
            AuthManager.shared.stopTimer()
            AuthManager.shared.authToken = nil
            AuthManager.shared.guestAccount = nil
            AuthManager.shared.guestPassword = nil
            AuthManager.shared.authPhoneNumber = nil
            AuthManager.shared.authPassword = nil
            
            // new loginType == 0 nghia la login binh thuong, chay vao ViewManager.Logout() nhu thuong
            // neu loginType == 1 thi khong chay vao ham logout de back ve wellcome viewcontroller
            if loginType == 0 {
                ViewManager.logout()
            }
            NotificationManager().logoutUser()
            callBack()
        }
        
    }
    
    func login(in viewController: BaseViewController, user: String?, password: String?, token: String?, provider: SocialProvider? , loginType: Int = 0, callBack: @escaping (_ user: PregUser?, _ isTempData: Bool, _ isGetInfoSuccess: Bool, _ isLoginSuccess: Bool) -> () ) {
        
        viewController.showHud()
        NetworkManager.shareInstance.apiToken(username: user, password: password, provider: provider, accessToken: token) { (isSuccess) in
            
            if isSuccess == true {
                if let systemToken = AuthManager.shared.authToken {
                    
                    // truyen loginType vao ham logout de check cac truong hop login
                    self.logout(loginType, {
                        AuthManager.shared.authToken = systemToken
                        AuthManager.shared.guestPassword = password
                        AuthManager.shared.guestAccount = user
                        AuthManager.shared.startTimer()
                        DatabaseManager.sharedManager.getUserInfo(password, systemToken, callBack: { (user, isTempData, isSuccess) in
                            if(!isTempData) {
                                viewController.hideHud()
                                callBack(user, isTempData, isSuccess, true)
                                
                            }
                        })
                    })
                }
            } else {
                callBack(nil, false, false, false)
            }
        }
    }
    
    func updateUserInformations(in viewController: BaseViewController, userId: Int,password: String? = nil,
                                phone: String? = nil, socialTypeId: String? = nil, firstName: String? = nil,
                                lastname: String? = nil, youAreThe: String? = nil, location: String? = nil,
                                status: String? = nil, avatar: String? = nil, email: String? = nil ,
                                callBack: @escaping (_ user: PregUser?, _ isTempData: Bool, _ isSucess: Bool) -> ()) {
        viewController.showHud()
        NetworkManager.shareInstance.apiPutUser(userId: userId, password: password, phone: phone, socialTypeId: socialTypeId, firstName: firstName, lastName: lastname, youAreThe: youAreThe, location: location, status: status, avatar: avatar, email: email) {
            (data, message, isSuccess) in
            DatabaseManager.sharedManager.getUserInfo(callBack: { (user, isTempData, isSuccess) in
                viewController.hideHud()
                if(!isTempData) {
                    callBack(user, isTempData, isSuccess)
                }
            })
        }
    }
    
    func forgotPassword(phone: String, password: String,
                        callBack: @escaping (_ user: PregUser?, _ isTempData: Bool, _ isSucess: Bool) -> ()) {
        NetworkManager.shareInstance.apiForgotPassword(phone: phone, password: password) { (data, mesaage, isSuccess) in
            DatabaseManager.sharedManager.getUserInfo(callBack: { (user, isTempData, isSuccess) in
                if(!isTempData) {
                    callBack(user, isTempData, isSuccess)
                }
            })
        }
        
    }
    
    func getLocalUserInfo() -> PregUser? {
        do {
            let realm = try Realm()
            let data = realm.objects(PregUser.self)
            return data.last
        } catch let error as NSError {
            // handle error
            print(error.description)
            return nil
        }
        
        //        let data = testDriver.objects(PregUser.self)
        //        return data.last
    }
    
    func getLocalPregnancys() -> PregPregnancy? {
        do {
            let realm = try Realm()
            let data = realm.objects(PregPregnancy.self)
            return data.last
        } catch let error as NSError {
            // handle error
            print(error.description)
            return nil
        }
        
        //        let data = testDriver.objects(PregUser.self)
        //        return data.last
    }
    
    ///offline-first ( ưu tiên làm việc offline )
    // luôn cần cập nhật data trong offline database và sử dụng data trong này nếu có - trả về để render View
    // trong trường hợp ko có dữ liệu thì call network và update vào db, sau đó trả về dữ liệu mới
    
    /// lấy user data từ Realm DB nếu có và gọi api để update rồi trả về lần nữa
    ///
    /// - Parameters:
    ///   - password: mật khẩu nếu có
    ///   - systemToken: token của user
    ///   - callBack: callBack để view cập nhật dữ liệu
    ///   @params : data : du lieu cua user sau khi lay tu Realm hoac API
    ///   @params : isTempData : neu = true thi la data duoc lay ve tu Realm, false thi la tu Cloud
    ///   @params : isSuccess : neu = true thi la call API thanh cong, false thi la loi
    func getUserInfo(_ password: String? = nil, _ systemToken: String? = nil, callBack: @escaping (_ data:PregUser?, _ isTempData: Bool, _ isSuccess: Bool  ) -> () ) -> Void {
        
        /// Kiem tra neu function nay co phai duoc goi tu Main Thread hay khong?
        if(Thread.isMainThread == false ) {
            /// neu khong phai thi goi lai trong main thread roi return
            DispatchQueue.main.async {
                self.getUserInfo(password, systemToken, callBack: callBack)
            }
            return
        }
        
        /// co gang lay user data trong database
        var user: PregUser? = self.getLocalUserInfo()
        
        /// neu user khac nil thi tra ve luon cho View Render len:
        if (user != nil ) {
            /// callBack nay co y nghia la :
            ///callBack(user data , du lieu tu realm , goi thanh cong )
            callBack(user, true, true)
        } else {
            /// neu khong ton tai User trong DB thi tao moi :D
            user = PregUser()
        }
        
        /// goi network lay user info
        NetworkManager.shareInstance.apiGetProfile { (data, message, isSuccess) in
            
            
            /// Neu Thanh cong
            if(isSuccess) {
                
                /// kiem tra xem co phai dang JSON khong ?
                if let userInfo = data as? JSON {
                    
                    /// lay data theo dang Array
                    let userArray = userInfo.arrayValue
                    
                    /// lay thang dau tien thoi
                    if let userData = userArray.first {
                        /// bắt đầu update / create record để lưu vào offline database
                        try! self.driver.write {
                            /// nếu set password lúc call cái function này thì update password vào dB
                            
                            
                            // id là primary key nên ko được update tuỳ tiện ( chỉ khi create mới assign cho field này nếu k
                            // muốn crash app :D
                            if(user?.id != userData["id"].intValue){
                                if user?.id != 0 {
                                    self.driver.delete(user!)
                                    user = PregUser()
                                    
                                }
                                user?.id = userData["id"].intValue
                                
                            }
                            
                            
                            if let passwd = password {
                                user?.password = passwd
                            }
                            
                            if let token = systemToken {
                                user?.authToken = token
                            }
                            
                            /// vì đang fix guest id === 9 nên check, nếu ko phải guest mới save info vào DB ( guest ko dùng info chung)
                            if(user?.id != DatabaseManager.guest_id) {
                                if userData["phone"].stringValue.count == 10 {
                                    user?.phone = userData["phone"].stringValue
                                }
                                user?.first_name = userData["first_name"].stringValue
                                user?.last_name = userData["last_name"].stringValue
                                user?.you_are_the = userData["you_are_the"].stringValue
                                user?.location = userData["location"].stringValue
                                user?.status = userData["status"].stringValue
                                user?.avatar = userData["avatar"].stringValue
                                user?.social_type_id = userData["social_type_id"].stringValue
                                user?.time_last_login = userData["time_last_login"].stringValue
                            }
                            
                            /// lưu thông tin người dùng vào DB
                            self.driver.add(user!, update: true)
                        }
                    }
                }
            }
            /// update lại View 1 lần nữa, để view reload lại theo thông tin từ Cloud nếu cần thiết
            /// nếu failed thì user là 1 object ko có thông tin, isSuccess = false
            callBack(user, false, isSuccess)
        }
    }
    
    func getPregnancys(callBack: @escaping (_ data: PregPregnancy?, _ isTempData: Bool, _ isSuccess: Bool, _ rawData: JSON?) -> () ) -> Void {
        if(Thread.isMainThread == false ) {
            //  
            DispatchQueue.main.async {
                self.getPregnancys(callBack: callBack)
            }
            return
        }
        var pregnancy : PregPregnancy? = self.getLocalPregnancys()
        if(pregnancy != nil) {
            callBack(pregnancy, true, true, nil)
        } else {
            pregnancy = PregPregnancy()
        }
        
        NetworkManager.shareInstance.apiGetPregnancys { (data, message, isSuccess) in
            //var dataPregnancy : [PregPregnancy] = []
            if (isSuccess) {
                if let userInfo = data as? JSON {
                    let userArray = userInfo.arrayValue
                    if let userData = userArray.first {
                        var user = self.getLocalUserInfo()
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        try? self.driver.write {
                            
                            let id = userData["id"].intValue
                            if pregnancy?.id != id, pregnancy?.id == 0 {
                                pregnancy?.id = id
                            }
                            pregnancy?.baby_gender = userData["baby_gender"].intValue
                            pregnancy?.weeks_pregnant = userData["weeks_pregnant"].stringValue
                            pregnancy?.weight_before_pregnant = userData["weight_before_pregnant"].floatValue
                            
                            let dateStr = userData["due_date"].stringValue
                            if dateStr.isEmpty == false {
                                if let date = dateFormatter.date(from: dateStr) {
                                    pregnancy?.due_date = date
                                }
                            }
                            
                            let dob = userData["date_of_birth"].stringValue
                            if dob.isEmpty == false {
                                if let date = dateFormatter.date(from: dob) {
                                    pregnancy?.date_of_birth = date
                                }
                            }
                            
                            
                            let startDateStr = userData["start_date"].stringValue
                            print(startDateStr)
                            if startDateStr.isEmpty == false {
                                if let date = dateFormatter.date(from: startDateStr) {
                                    pregnancy?.start_date = date
                                }
                            }
                            
                            
                            pregnancy?.show_week = userData["show_week"].intValue
                            pregnancy?.baby_already_born = userData["baby_already_born"].intValue
                            user?.pregPregnancy.append(pregnancy!)
                            //                            self.driver.add(pregnancy!, update: true)
                            
                            if (user != nil ) {
                                self.driver.add(user!, update: true)
                            }
                            
                        }
                        
                    }
                    
                }
            }
            callBack(pregnancy, false, isSuccess, JSON(data) )
        }
    }
//    func updateUserInformations(in viewController: BaseViewController, userId: Int, firstName: String?, lastname: String?, youAreThe: String?, location: String?, callBack: @escaping (_ user: PregUser?, _ isTempData: Bool, _ isSucess: Bool) -> ()) {
//        viewController.showHud()
//        NetworkManager.shareInstance.apiPutUser(userId: userId, firstName: firstName, lastName: lastname, youAreThe: youAreThe, location: location) { (data, message, isSuccess) in
//            DatabaseManager.sharedManager.getUserInfo(callBack: { (user, isTempData, isSuccess) in
//                viewController.hideHud()
//                if(!isTempData) {
//                    callBack(user, isTempData, isSuccess)
//                }
//            })
//        }
//    }
    
    
    func putPregnancy(showWeek:Int?, babyGender: Int?, dueDate: String?, startDate: String?, dateOfBirth: String?, babyAlreadyBorn: Int?, weightBeforePregnant: Float?, callBack: @escaping (_ user: PregUser?, _ isTempData: Bool, _ isSuccess: Bool) -> ()) {
        NetworkManager.shareInstance.apiPutPregnancy(babyGender: babyGender, dueDate: dueDate, startDate: startDate, showWeek: showWeek, babyAlreadyBorn: babyAlreadyBorn, dateOfBirth: dateOfBirth, weightBeforePregnant: weightBeforePregnant) { (data, message, isSuccess) in
            if( isSuccess ) {
                var user = self.getLocalUserInfo()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                
                var pregnancy: PregPregnancy
                if let preg = user?.pregPregnancy, preg.count > 0 {
                    pregnancy = preg.first!
                    callBack(user, true, true)
                } else {
                    pregnancy = PregPregnancy()
                }
                
                try? self.driver.write {
                    
                    let id = user?.id
                    if pregnancy.id != id, pregnancy.id == 0 {
                        pregnancy.id = id!
                    }
                    if let babyGender = babyGender {
                        pregnancy.baby_gender = babyGender
                    }
//                    pregnancy?.weeks_pregnant = userData["weeks_pregnant"].stringValue
                    
                    if let dateStr = dueDate {
                        if dateStr.isEmpty == false {
                            if let date = dateFormatter.date(from: dateStr) {
                                pregnancy.due_date = date
                            }
                        }
                    }
                    if let startDateStr = startDate {
                        if startDateStr.isEmpty == false {
                            if let date = dateFormatter.date(from: startDateStr) {
                                pregnancy.start_date = date
                            }
                        }
                    }
                    
                    if let dob = dateOfBirth {
                        if dob.isEmpty == false {
                            if let date = dateFormatter.date(from: dob) {
                                pregnancy.date_of_birth = date
                            }
                        }
                    }
                    if let showWeek = showWeek {
                        pregnancy.show_week = showWeek
                    }
                    if let babyAlreadyBorn = babyAlreadyBorn {
                        pregnancy.baby_already_born = babyAlreadyBorn
                    }
                    user?.pregPregnancy.append(pregnancy)
                    self.driver.add(user!, update: true)
                }
                
            }
            callBack(self.getLocalUserInfo(), false, isSuccess)
            
        }
    }
    
    func getLocalImages(type:Int? ) -> Results<PregImage> {
        
        var images: Results<PregImage>
        images = self.driver.objects(PregImage.self)
        if let type = type {
            if images.count > 0 {
                images = images.filter("image_type_id == \(type)")
            }
            
        }
        return images
    }
    
    func getImages(type:Int?, callBack: @escaping (_ data:Results<PregImage>?, _ isTempData: Bool, _ isSuccess: Bool  ) -> () ) -> Void {
        
        let images = self.getLocalImages(type: type)
        if(images.count > 0) {
            callBack(images, true, true)
        }
        
        NetworkManager.shareInstance.apiGetImages { (data, message, isSuccess) in
            if(isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    try! self.driver.write {
                        let image: PregImage = PregImage()
                        image.id = item["id"].intValue
                        let orignalUrlString = item["image"].stringValue
                        let urlString = NetworkManager.rootDomain + orignalUrlString
                        image.image = urlString
                        image.image_type_id = item["image_type_id"].intValue
                        image.week_id = item["week_id"].intValue
                        self.driver.add(image, update: true)
                    }
                    
                }
            }
            callBack(self.getLocalImages(type: type), false, isSuccess)
            
        }
        
        
    }
    
    func getLocalIllustrateBellyBy(month:Int? ) -> Results<PregMyBelly> {
        
        var bellies: Results<PregMyBelly>
        if let month = month {
            bellies = self.driver.objects(PregMyBelly.self).filter("month == \(month)")
        } else {
            bellies = self.driver.objects(PregMyBelly.self)
        }
        return bellies
    }
    
    func getIllustrateBellyBy(month:Int?, callBack: @escaping (_ data:Results<PregMyBelly>?, _ isTempData: Bool, _ isSuccess: Bool  ) -> () ) -> Void {
        
        let bellies = self.getLocalIllustrateBellyBy(month: month)
        if(bellies.count > 0) {
            callBack(bellies, true, true)
        }
        NetworkManager.shareInstance.apiGetIllustrateBellyBy(month: month) { (data, message, isSuccess) in
            if(isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    try! self.driver.write {
                        let myBelly : PregMyBelly = PregMyBelly()
                        myBelly.id = item["id"].intValue
                        let orignalUrlString = item["image"].stringValue
                        let urlString = NetworkManager.rootDomain + orignalUrlString
                        //print(urlString)
                        myBelly.image = urlString
                        myBelly.month = item["month"].intValue
                        self.driver.add(myBelly, update: true)
                    }
                }
            }
            
            callBack(self.getLocalIllustrateBellyBy(month: month), false, isSuccess)
        }
        //        NetworkManager.shareInstance.apiGetMyBelliesBy(month: month) { (data, message, isSuccess) in
        //
        //        }
        //        NetworkManager.shareInstance.apiGetMyBelliesBy { (data, message, isSuccess) in
        //            if(isSuccess) {
        //                let result = JSON(data)
        //                for item in result.arrayValue {
        //                    try! self.driver.write {
        //                        let myBelly : PregMyBelly = PregMyBelly()
        //                        myBelly.id = item["id"].intValue
        //                        let orignalUrlString = item["image"].stringValue
        //                        let urlString = NetworkManager.mainDomain + orignalUrlString
        //                        //print(urlString)
        //                        myBelly.image = urlString
        //                        self.driver.add(myBelly, update: true)
        //                    }
        //                }
        //            }
        //
        //            callBack(self.getLocalIllustrateBellyBy(), false, isSuccess)
        //
        //        }
    }
    
    
    // Get My Weight
    func getLocalMyWeight() -> Results<PregMyWeight>? {
        do {
            let realm = try Realm()
            let data = realm.objects(PregMyWeight.self)
            return data
        } catch let error as NSError {
            // handle error
            print(error.description)
            return nil
        }
    }
    
    func getMyWeights(callBack: @escaping (_ data:Results<PregMyWeight>?, _ isTempData: Bool, _ isSuccess: Bool, _ rawData: JSON?) -> () ) -> Void {
        
        if Thread.isMainThread == false {
            DispatchQueue.main.async {
                self.getMyWeights(callBack: callBack)
            }
            return
        }
        
        
        let myWeights = self.getLocalMyWeight()
        if (myWeights != nil && myWeights!.count > 0)
        {
            callBack(myWeights, true, true, nil)
        }
        
        NetworkManager.shareInstance.apiGetAllMyWeights { (data, message, isSuccess) in
            if(isSuccess) {
                DatabaseManager.sharedManager.getPregnancys(callBack: { (prenancy, isTempData, isSuccessPreg, rawData) in
                    if prenancy != nil {
                        let result = JSON(data)
                        for item in result.arrayValue {
                            try! self.driver.write {
                                let myWeight = PregMyWeight()
                                myWeight.id = item["id"].intValue
                                myWeight.current_weight = Float(item["current_weight"].intValue)
                                myWeight.my_weight_type_id = item["my_weight_type_id"].intValue
                                myWeight.week_id = item["week_id"].intValue
                                myWeight.id = item["id"].intValue
                                myWeight.start_date = prenancy!.start_date
                                
                                let crrDate = item["current_date"].stringValue
                                if crrDate.isEmpty == false {
                                    if let date = self.dateFormatter.date(from: crrDate) {
                                        myWeight.current_date = date
                                    }
                                }
                                
                                self.driver.add(myWeight, update: true)
                            }
                        }
                    }
                })
                
            }
            callBack(self.getLocalMyWeight(), false, isSuccess, JSON(data))
        }
    }
    func putMyWeights(myWeightId : Int, myWeightTypeId: Int?, currentDate : String?, curWeight: Float?,  callBack: @escaping (_ data:Results<PregMyWeight>?, _ isTempData: Bool, _ isSuccess: Bool  ) -> () ) -> Void {
        NetworkManager.shareInstance.apiPutMyWeights(myWeightId: myWeightId, myWeightTypeId: myWeightTypeId, currentDate: currentDate, curWeight: curWeight) { (data, message, isSuccess) in
            
            // Vì server chỉ trả về có mỗi message -> bắt buộc get toàn bộ về để update
            DatabaseManager.sharedManager.getMyWeights(callBack: { (pregnancy, isTempData, isSuccess, rawData) in
                if(!isTempData) {
                    callBack(pregnancy, isTempData, isSuccess)
                }
            })
        }
    }
    
    func postMyWeights(myWeightId : Int, myWeightTypeId: Int?, currentDate : String?, curWeight: Float?,  callBack: @escaping (_ data:Results<PregMyWeight>?, _ isTempData: Bool, _ isSuccess: Bool  ) -> () ) -> Void {
        // Post lên cũng chẳng khác gì, vẫn phải get cả cục để lấy dữ liệu về
        DatabaseManager.sharedManager.getMyWeights(callBack: { (pregnancy, isTempData, isSuccess, rawData) in
            if(!isTempData) {
                callBack(pregnancy, isTempData, isSuccess)
            }
        })
    }
    
    
    // Get all size guide
    func getLocalAllSizeguides() -> Results<PregSizeGuide>? {
        do {
            let realm = try Realm()
            let data = realm.objects(PregSizeGuide.self)
            return data
        } catch let error as NSError {
            // handle error
            print(error.description)
            return nil
        }
    }
    
    func getAllSizeguides(callBack: @escaping (_ data:Results<PregSizeGuide>?, _ isTempData: Bool, _ isSuccess: Bool  ) -> () ) -> Void {
        
        if Thread.isMainThread == false {
            DispatchQueue.main.async {
                self.getAllSizeguides(callBack: callBack)
            }
            return
        }
        
        var sizeGuides = self.getLocalAllSizeguides()
        
        if (sizeGuides != nil) && (sizeGuides!.count > 0) {
            callBack(sizeGuides, true, true)
        }
        
        NetworkManager.shareInstance.apiGetAllSizeguides { (pregnancy, message, isSuccess) in
            if (isSuccess) {
                for i in 0..<pregnancy.count {
                    try! self.driver.write {
                        self.driver.add(pregnancy[i], update: true)
                    }
                }
            }
        }
    }
    
}
