//
//  InformationViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 05/12/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class InformationViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableViewInfor: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "GuideTableViewCell", bundle: Bundle.main)
        tableViewInfor.register(nib, forCellReuseIdentifier: "guideCell")
        tableViewInfor.delegate = self
        tableViewInfor.dataSource = self
        addBackButton()
        
        self.title = "Thông tin"
        let navigationFont = UIFont.systemFont(ofSize: 20)
        let navigationColor =  UIColor.black
        var attrs = [NSAttributedString.Key: Any]();
        attrs[.font] = navigationFont
        attrs[.foregroundColor] = navigationColor
        self.navigationController?.navigationBar.titleTextAttributes = attrs
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        self.hidesBottomBarWhenPushed = true
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        self.navigationController?.isNavigationBarHidden = false
        super.viewWillAppear(animated)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }

    func addBackButton() {
        self.navigationItem.leftItemsSupplementBackButton = false
        let backBtn = UIButton()
        backBtn.setImage(UIImage(named: "Back"), for: UIControl.State.normal)
        backBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backBtn.addTarget(self, action: #selector(self.doBack(_:)), for: .touchUpInside)
        //backBtn.addTarget(self, action: #selector(self.addObserver(_:forKeyPath:options:context:)), for: UIControl.Event.touchUpInside)
        let backView = UIBarButtonItem()
        backView.customView = backBtn
        
        self.navigationItem.leftBarButtonItem = backView
    }
    
    @IBAction func doBack(_ : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "guideCell", for: indexPath) as! GuideTableViewCell
        return cell
    }

}
