//
//  RootViewController.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/11/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class RootViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        ViewManager.setRootViewController(self)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(DatabaseManager.sharedManager.isLogin()) {
            
            let rootTab = self.storyboard?.instantiateViewController(withIdentifier: "RootTabBarController") as! RootTabBarController
            ViewManager.setRootTabbarController(rootTab)
            self.navigationController?.pushViewController(rootTab, animated: true)
            
        } else {
            
            let welcomeView = ViewManager.instantiateWelcomeViewController()
            self.navigationController?.pushViewController(welcomeView, animated: true)
            
        }
        

        
//        self.present(rootTab, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func createRootTabbarControllerIfNeed() {
        if ViewManager.getRootTabbarController() == nil {
            let rootTabbar: RootTabBarController = (self.storyboard?.instantiateViewController(withIdentifier: "RootTabBarController") as! RootTabBarController)
            ViewManager.setRootTabbarController(rootTabbar)
        }
        
    }

}
