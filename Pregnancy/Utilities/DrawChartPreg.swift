//
//  ChartData.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 1/31/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import Charts

class DrawChartPreg {
    
    class var shareInstance : DrawChartPreg {
        struct Static {
            static let instance : DrawChartPreg = DrawChartPreg()
        }
        return Static.instance
    }
    
    
    func CreateDataChart( callBack: @escaping ( _ data: [PregMyWeight] ) -> () ) -> Void {
        
        var dataMyWeight : [PregMyWeight] = []
        
        NetworkManager.shareInstance.apiGetAllMyWeights { (data, message, isSuccess) in
//            print(data)
//            print(message)
            if(isSuccess) {
                
                if let pregnancy = DatabaseManager.sharedManager.getLocalPregnancys() {
                    let resutl = JSON(data)
                    dataMyWeight.removeAll()
                    
                    for items in resutl.arrayValue {
                        let item = PregMyWeight()
                        item.my_weight_type_id = items["my_weight_type_id"].intValue
                        item.current_weight = items["current_weight"].floatValue
                        item.week_id = items["week_id"].intValue
                        item.id = items["id"].intValue
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        
//                        let startDate = items["start_date"].stringValue
//                        if startDate.isEmpty == false {
//                            if let date = dateFormatter.date(from: startDate) {
//                                if(Calendar.current.dateComponents([.day], from: date, to: pregnancy.start_date).day != 0){
//                                    continue
//                                }
//                                item.start_date  = date
//                            }
//                        }
                        item.start_date = pregnancy.start_date
                        
                        let crrDate = items["current_date"].stringValue
                        if crrDate.isEmpty == false {
                            if let date = dateFormatter.date(from: crrDate) {
                                if(date.compare(pregnancy.due_date.addingTimeInterval(86399)) == .orderedDescending ||
                                    date.compare(pregnancy.start_date.addingTimeInterval(-86399)) == .orderedAscending){
                                    continue
                                }
                                item.current_date  = date
                            }
                        }
                        dataMyWeight.append(item)
                    }
                }
                callBack(dataMyWeight)
            } else {
                callBack(dataMyWeight)
            }
            
        }
        
    }
    
    func setDataMePush(dataWei: [PregMyWeight], callBack: @escaping ( _ dataEntry: [ChartDataEntry]) -> () ) -> Void{
        
//        var dataCaculated = dataWei
        var startDate = Date()
        
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTampla, isSuccess, rawData) in
            if(isSuccess) {
                startDate = (pregnancy?.start_date)!
            } else {
                if dataWei.first?.start_date != nil {
                    startDate = (dataWei.first?.start_date)!
                }
            }
        }
        
        
        let dataSort = dataWei.sorted { (item1, item2) -> Bool in
            item1.current_date < item2.current_date
        }
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        
//        let dateStart = calendar?.date(byAdding: .day, value: -1, to: (dataWei.first?.start_date)!, options: .init(rawValue: 0))
        
        let dateStart = calendar?.date(byAdding: .day, value: -1, to: startDate, options: .init(rawValue: 0))
        
        let endMonth1 = calendar?.date(byAdding: .day, value: 30, to: dateStart!, options: .init(rawValue: 0))
        let endMonth2 = calendar?.date(byAdding: .day, value: 30, to: endMonth1!, options: .init(rawValue: 0))
        let endMonth3 = calendar?.date(byAdding: .day, value: 30, to: endMonth2!, options: .init(rawValue: 0))
        let endMonth4 = calendar?.date(byAdding: .day, value: 30, to: endMonth3!, options: .init(rawValue: 0))
        let endMonth5 = calendar?.date(byAdding: .day, value: 30, to: endMonth4!, options: .init(rawValue: 0))
        let endMonth6 = calendar?.date(byAdding: .day, value: 30, to: endMonth5!, options: .init(rawValue: 0))
        let endMonth7 = calendar?.date(byAdding: .day, value: 30, to: endMonth6!, options: .init(rawValue: 0))
        let endMonth8 = calendar?.date(byAdding: .day, value: 30, to: endMonth7!, options: .init(rawValue: 0))
        let endMonth9 = calendar?.date(byAdding: .day, value: 42, to: endMonth8!, options: .init(rawValue: 0))
        var wM1 : [Float] = []
        var wM2 : [Float] = []
        var wM3 : [Float] = []
        var wM4 : [Float] = []
        var wM5 : [Float] = []
        var wM6 : [Float] = []
        var wM7 : [Float] = []
        var wM8 : [Float] = []
        var wM9 : [Float] = []
        
        for dayCheck in dataSort {
            if (dateStart! <= dayCheck.current_date) && (dayCheck.current_date < endMonth1!) {

                wM1.append(dayCheck.current_weight)
            } else {
                if (endMonth1! <= dayCheck.current_date) && (dayCheck.current_date < endMonth2!) {
                    wM2.append(dayCheck.current_weight)
                } else {
                    if (endMonth2! <= dayCheck.current_date) && (dayCheck.current_date < endMonth3!) {
                        wM3.append(dayCheck.current_weight)
                    } else {
                        if (endMonth3! <= dayCheck.current_date) && (dayCheck.current_date < endMonth4!) {
                            wM4.append(dayCheck.current_weight)
                        } else {
                            if (endMonth4! <= dayCheck.current_date) && (dayCheck.current_date < endMonth5!) {
                                wM5.append(dayCheck.current_weight)
                            } else {
                                if (endMonth5! <= dayCheck.current_date) && (dayCheck.current_date < endMonth6!) {
                                    wM6.append(dayCheck.current_weight)
                                } else {
                                    if (endMonth6! <= dayCheck.current_date) && (dayCheck.current_date < endMonth7!) {
                                        wM7.append(dayCheck.current_weight)
                                    } else {
                                        if (endMonth7! <= dayCheck.current_date) && (dayCheck.current_date < endMonth8!) {
                                            wM8.append(dayCheck.current_weight)
                                        } else {
                                            if (endMonth8! <= dayCheck.current_date) && (dayCheck.current_date <= endMonth9!) {
                                                wM9.append(dayCheck.current_weight)
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        var dataWeightHard : Array<Float> = Array(repeating: 0, count: 9)
        var weightGainPerMonth : Array<Float> = Array(repeating: 0, count: 9)
        
        if wM1.count != 0 {
            dataWeightHard[0] = wM1.last!
        } else {
            wM1.append(0)
            dataWeightHard[0] = 0 //Check
        }
        if wM2.count != 0 {
            dataWeightHard[1] = wM2.last!
        }
        if wM3.count != 0 {
            dataWeightHard[2] = wM3.last!
        }
        if wM4.count != 0 {
            dataWeightHard[3] = wM4.last!
        }
        if wM5.count != 0 {
            dataWeightHard[4] = wM5.last!
        }
        if wM6.count != 0 {
            dataWeightHard[5] = wM6.last!
        }
        if wM7.count != 0 {
            dataWeightHard[6] = wM7.last!
        }
        if wM8.count != 0 {
            dataWeightHard[7] = wM8.last!
        }
        if wM9.count != 0 {
            dataWeightHard[8] = wM9.last!
        }

        var arrayWeight : Array<Array<Float>> = []
        arrayWeight.append(contentsOf: [wM1, wM2, wM3, wM4, wM5, wM6, wM7, wM8, wM9])

        if arrayWeight[0].count == 0 {
            arrayWeight[0][0] = 0
        }
        
        var check = arrayWeight[0][0]
        for i in 0..<arrayWeight.count {
            if arrayWeight[i].count >= 2 {
                weightGainPerMonth[i] = arrayWeight[i].last! - arrayWeight[i].first!
                check = arrayWeight[i].last!

            } else {
                if arrayWeight[i].count == 1 {
                    weightGainPerMonth[i] = arrayWeight[i][0] - check
                    check = arrayWeight[i][0]
                }
            }
        }
        
        
        var chartData : [ChartDataEntry] = []
        for i in 0..<(dataWeightHard.count) {
            if dataWeightHard[i] != 0 {
                chartData.append(ChartDataEntry(x: Double(i), y: Double(dataWeightHard[i])))
            }
        }
//        return chartData
        callBack(chartData)
    }
    
    func drawChart(lineChartView: LineChartView,dataEntry : [ChartDataEntry]) -> Void {
        
        
        lineChartView.highlightPerTapEnabled = false
        lineChartView.highlightPerDragEnabled = false
        lineChartView.dragEnabled = false
        lineChartView.setScaleEnabled(false)
        lineChartView.pinchZoomEnabled = false
        lineChartView.xAxis.enabled = false
        lineChartView.xAxis.axisMaximum = 8 // Date from First Date
        lineChartView.xAxis.axisMinimum = 0
        lineChartView.leftAxis.enabled = false
        lineChartView.rightAxis.enabled = false
        lineChartView.legend.enabled = false

 
        let set1 = LineChartDataSet(values: dataEntry, label: "Cân Nặng")
        set1.axisDependency = .left
        set1.setColor(UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1))
        set1.setCircleColor(.white)
        set1.lineWidth = 3
        set1.circleRadius = 3
        set1.fillAlpha = 0.6
        set1.fillColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        set1.highlightColor = UIColor(red: 244/255, green: 117/255, blue: 117/255, alpha: 1)
        set1.drawCircleHoleEnabled = false
        
        let data = LineChartData(dataSet: set1)
        lineChartView.data = data
//        lineChartView.animate(xAxisDuration: 1, yAxisDuration: 1)
        
        for set in lineChartView.data!.dataSets as! [LineChartDataSet] {
            set.drawValuesEnabled = !set.drawValuesEnabled
            set.drawFilledEnabled = !set.drawFilledEnabled
            set.drawCirclesEnabled = !set.drawCirclesEnabled
        }
    }
}
