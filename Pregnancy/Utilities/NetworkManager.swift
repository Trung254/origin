import UIKit
import Alamofire
import SwiftyJSON

enum SocialProvider: String {
    case facebook
    case google
}

class NetworkManager {
    static let rootDomain = "http://www.momtour.vn/"
//    static let rootDomain = "http://1.55.17.233:6868/"
    static let apiDomain = "\(NetworkManager.rootDomain)api/"
    static let rootService = "http://admin.momtour.vn/"
    static let apiService = "\(NetworkManager.rootService)api/"
    static let vnpayURL = "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html"
    static let vnpayCallBack = "http://vnp.momtour.vn/test.php"
    static let ApiSecretKey = "m117z119u100l110e122w115p122i120"
    static let StoreId = "1"
    static let LanguageId = "1"
    
    static let apiProductsToken = "bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE1NTE3NzM1OTcsImV4cCI6MTg2NzEzMzU5NywiaXNzIjoiaHR0cDovL2FkbWluLm1vbXRvdXIudm4iLCJhdWQiOlsiaHR0cDovL2FkbWluLm1vbXRvdXIudm4vcmVzb3VyY2VzIiwibm9wX2FwaSJdLCJjbGllbnRfaWQiOiI3ODFhYzY1Ni1hYTJlLTRlMTUtODI3Ny1jODk5NGRiMjIwYmIiLCJzdWIiOiI3ODFhYzY1Ni1hYTJlLTRlMTUtODI3Ny1jODk5NGRiMjIwYmIiLCJhdXRoX3RpbWUiOjE1NTE3NzM1OTYsImlkcCI6ImxvY2FsIiwic2NvcGUiOlsibm9wX2FwaSIsIm9mZmxpbmVfYWNjZXNzIl0sImFtciI6WyJwd2QiXX0.Uwz983V3GfEilEo1EGQjB1BcTLGpar5FwORoF5FTChOimMY0ghMeCR1BgBzLEcqXRiMAqLJJ7oos2PuYLMUZXH8PQa8fjU314WGQj24AgP3ruKTTiXqNs9cuZ_mX0A3Ag-Jdx5A5JsTNalhzmb5jSJcLd_Ph_q6xHSjS1UzGm0A8KSUrB2l0Wb8JX_KHI1Ir0pAvZrMq5WcUm0ZVMTHRudKUGd6_rqpY-ZOYQCEno1qAFmAbH8rkzxlN0id5ji0L-kzbNEL-Udkpj413Zpag33HrWrsuzHLODnNs1HXVLxxcNBNTlmg6uvgnyUFUeyz81BXBex23e-j-syzzjaCfpg"
    static let apiClientProductToken = "bearer vj3q47Y5faApDFTAy-FP4zom6L-JbsQMkKFHXa_pXUxJi4uu9eni4y5lP0UAxyGRDZKs7oHObuPxFVk-5AMSbwCdj-JYA76GdMh0hv8LHGiDohYq9dB8IdSsmPesZCgmLxfDFIj67F2LGDIXHazHq1lMvWMorqiAcjXwkBloGfTGjS6E-ugcv_43BffZsiWL_uCleK3_cZm0HLfRCRmC-Z9UVc-qHAdOXmXTnuIgoK4JPy6lDt7NkwcJfGSOAbTncMZIxEZ9rdP0p5bg_EhjsS5HcjC5ry4an1Q7pKjDjOA"
    
    
    
    let dateFormatter : NetworkDateFormatter = NetworkDateFormatter.init("yyyy-MM-dd HH:mm:ss")
    class var shareInstance : NetworkManager {
        struct Static {
            static let instance : NetworkManager = NetworkManager()
        }
        return Static.instance
    }
    
    func executeRequest(_ url: String,
                        _ method: HTTPMethod = .get,
                        _ params: Parameters? = nil,
                        _ encoding: ParameterEncoding = URLEncoding.queryString,
                        _ header: HTTPHeaders? = nil,
                        callBack: @escaping (Any, String, Bool) -> ()) {
        
        DispatchQueue.global(qos:.background).async {
            request(url,
                    method: method,
                    parameters: params,
                    encoding: encoding,
                    headers: header).responseJSON { (response) in
                        
                        self.handleDataResponse(response, callBack: callBack)
            }
        }
        
    }
    
    func handleDataResponse(_ response:DataResponse<Any>, callBack:@escaping (Any, String, Bool) -> ()) {
        let result = response.result
        var statusCode = -1
        if let responseCode = response.response {
            statusCode = responseCode.statusCode
        }
        switch result {
        case .success(let aData):
            if statusCode == 200 ||
                statusCode == 201 ||
                statusCode == 202 {

                callBack(JSON(aData) as Any,"", true)
//                String(data: response.request?.httpBody, encoding: String.Encoding.utf8)
            } else {
                let jsonResult = JSON(aData)
                let mess = jsonResult["message"].stringValue
                
                //Check statusCode 404: vẫn trả về JSON và ko có data
                if statusCode == 404 {
                    callBack(jsonResult as Any, mess, true)
                } else {
                    callBack(jsonResult as Any, mess, false)
                }
                
                
//                callBack(jsonResult as Any, mess, false)
                //                NSString.init(data: response.request?.httpBody, encoding: .utf8)
                //                String(data: (response.request?.httpBody)!, encoding: .utf8)
            }
            break
        case .failure( _):
            callBack(result as Any,"", false)
            break
        }
    }
    
    
    func apiGuestRegister(callBack: @escaping (JSON?, String, Bool) -> () ) -> Void {
        self.executeRequest(NetworkManager.apiDomain + "users/createguestaccount", HTTPMethod.post, nil, URLEncoding.default) { (data, message, isSuccess) in
            callBack(isSuccess ? JSON(data) : nil, message, isSuccess)
        }
    }
    
    // /api/token
    func apiToken(username: String?, password: String?,provider: SocialProvider?, accessToken: String?, callBack: @escaping (Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        var params : Dictionary = [ "grant_type": "password"] as Dictionary
        if let username = username , let password = password {
            params["username"] = username
            params["password"] = password
            
        } else if let provider = provider, let accessToken = accessToken {
            headers["Provider"] = provider.rawValue
            headers["access_token"] = accessToken
            
        } else {
            fatalError("Username && password OR accessToken && Provider must be provided")
        }
        
        self.executeRequest(NetworkManager.apiDomain + "token", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                let jsonData = JSON(data)
                let token = jsonData["access_token"].stringValue
                AuthManager.shared.authToken = token
                if let username = username, let password = password {
                    AuthManager.shared.guestAccount = username
                    AuthManager.shared.guestPassword = password
                }
            } else {
                AuthManager.shared.authToken = nil
            }
            DispatchQueue.global(qos: .background).async {
                callBack(isSuccess)
                
            }
            
        }
    }
    
    // /api/users/checkphone
    func apiCheckPhoneNumber(phone: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["phone": phone] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "users/checkphone", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            
            callBack(data, message, isSuccess)
        }
        
    }
    
    
    
    // /api/user
    func apiPostUser(password: String?, phone: String?, socialTypeId: Int?, firstName: String?, lastName: String?, youAreThe: String?, location: String?, status: String?, avarta: String?, email: String?, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        //        if let token = AuthManager.shared.authToken {
        //            headers["Authorization"] = "bearer " + token
        //        }
        
        //        var paramss : Dictionary = ["password": password as Any, "phone": phone as Any, "social_type_id": socialTypeId as Any, "first_name": firstName as Any, "last_name": lastName as Any, "you_are_the": youAreThe as Any, "location": location as Any, "status": status as Any, "avarta": avarta as Any, "email": email as Any] as Dictionary<String, Any>
        
        var params: Dictionary<String, Any> = [:]
        
        if let password = password  {
            params["password"] = password
        }
        
        if let phone = phone {
            params["phone"] = phone
        }
        
        if let socialTypeId = socialTypeId {
            params["socialTypeId"] = socialTypeId
        }
        
        if let firstName = firstName {
            params["first_name"] = firstName
        }
        
        if let lastName = lastName {
            params["last_name"] = lastName
        }
        
        if let youAreThe = youAreThe {
            params["you_are_the"] = youAreThe
        }
        
        if let location = location {
            params["location"] = location
        }
        
        if let status = status {
            params["status"] = status
        }
        
        if let avarta = avarta {
            params["avarta"] = avarta
        }
        
        if let email = email {
            params["email"] = email
        }
        
        self.executeRequest(NetworkManager.apiDomain + "users", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetUser(authorization: String, id: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["id": id] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "users/", .get, params, URLEncoding.default, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutUser(userId: Int, password: String? = nil,
                    phone: String? = nil, socialTypeId: String? = nil,
                    firstName: String? = nil, lastName: String? = nil,
                    youAreThe: String? = nil, location: String? = nil ,
                    status: String? = nil, avatar: String? = nil, email: String? = nil,
                    callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        var params: Dictionary<String, Any> = ["user_id": userId]
        if let password = password {
            params["password"] = password
        }
        
        if let phone = phone {
            params["phone"] = phone
        }
        
        if let socialTypeId = socialTypeId {
            params["social_type_id"] = socialTypeId
        }
        
        if let firstName = firstName {
            params["first_name"] = firstName
        }
        
        if let lastName = lastName {
            params["last_name"] = lastName
        }
        
        if let youAreThe = youAreThe {
            params["you_are_the"] = youAreThe
        }
        
        if let location = location {
            params["location"] = location
        }
        
        if let status = status {
            params["status"] = status
        }
        
        if let avatar = avatar {
            params["avatar"] = avatar
        }
        
        if let email = email {
            params["email"] = email
        }
        
        let url = "users/" + String(userId)

        self.executeRequest(NetworkManager.apiDomain + url, .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                print("done")
            }
            callBack(data, message, isSuccess)
            print("failed")
        }
    }
    
    func apiUploadAvatar(image: UIImage, updateProgress:@escaping (_ progress: Float) -> Void?, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        if let user = DatabaseManager.sharedManager.getLocalUserInfo() {
            let id = user.id
            var header: Dictionary<String, String> = ["Content-Type":"multipart/form-data"]
            
            if let token = AuthManager.shared.authToken {
                header["Authorization"] = "bearer " + token
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                var imageData: Data
                var fileType: String
                if(image.isPNG()) {
                    imageData = image.pngData()!
                    fileType = "png"
                } else {
                    imageData = image.jpegData(compressionQuality: 1.0)!
                    fileType = "jpg"
                }
                let imageName = String("\(id)_\(Date().timeIntervalSince1970).\(fileType)")
                multipartFormData.append(imageData, withName:"Upload_file", fileName:imageName, mimeType: "image/*")
            }, usingThreshold: 10*1024*1024,
               to: NetworkManager.apiDomain + "userprofile/upload",
               method: .post,
               headers: header
            ) { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        // success block
                        self.handleDataResponse(response, callBack: callBack)
                    }
                    
                    upload.uploadProgress(closure: { (progress) in
                        updateProgress(Float(progress.completedUnitCount) /  Float(progress.totalUnitCount))
                        
                    })
                    
                case .failure(let encodingError):
                    print("Failed to encode upload \(encodingError)")
                }
            }
        }
        
    }
    
    func apiForgotPassword( phone: String, password : String,
                            callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        let params : Dictionary = ["phone": phone, "password": password] as Dictionary
        
        let url = "users/forgotpassword/" + phone
        
        self.executeRequest(NetworkManager.apiDomain + url, .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                print("done")
            }
            callBack(data, message, isSuccess)
            print("failed")
        }
    }
    
    func apiResetdataUser(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers: Dictionary<String, String> = [:]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "userprofile/reset", .delete, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
               
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteUser(userId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["user_id": userId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "users/", .delete, params, URLEncoding.default, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // /api/pregnancys
    
    func apiPostPregnacys(babyGender: Int, dueDate: String, startDate : String, showWeek: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["baby_gender": babyGender, "due_date": dueDate, "start_date" : startDate , "show_week" : showWeek] as Dictionary
        
        self.executeRequest(NetworkManager.apiDomain + "pregnancys", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetPregnancys(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "pregnancys/", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in

            if(isSuccess) {
                callBack(JSON(data), message, isSuccess)
            } else {
                callBack(data, message, isSuccess)
            }
            
        }
    }
    
    func apiPutPregnancy(babyGender: Int?, dueDate: String?, startDate: String?, showWeek: Int?, babyAlreadyBorn: Int? , dateOfBirth : String?, weightBeforePregnant: Float? , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        var params : Dictionary<String, Any> = [:] as Dictionary
        
        if let babyGender = babyGender {
            params["baby_gender"] = babyGender
        }
        
        if let dueDate = dueDate {
            params["due_date"] = dueDate
        }
        
        if let showWeek = showWeek {
            params["show_week"] = showWeek
        }
        if let babyAlreadyBorn = babyAlreadyBorn {
            params["baby_already_born"] = babyAlreadyBorn
        }
        
        if let dateOfBirth = dateOfBirth {
            params["date_of_birth"] = dateOfBirth
        }
        
        if let startDate = startDate {
            params["start_date"] = startDate
        }
        if let weightBeforePregnant = weightBeforePregnant {
            params["weight_before_pregnant"] = weightBeforePregnant
        }
        
        
        self.executeRequest(NetworkManager.apiDomain + "pregnancys/update", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                print("done")
            }
            callBack(data, message, isSuccess)
            print("failed")
        }
    }
    
    // /api/profile
    func apiGetProfile(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "userprofile/", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
//            var dataUser = [PregUser]()
//            if(isSuccess) {
//                let result = JSON(data)
//                for items in result.arrayValue {
//                    let item = PregUser()
//                    item.id = Int(items["id"].stringValue)!
//                    dataUser.append(item)
//                }
//            }
            if(isSuccess) {
                callBack(JSON(data), message, isSuccess)
            } else {
                callBack(data, message, isSuccess)
            }
            
        }
    }
    
    func apiPutProfile(socialTypeId: Int?, firstName: String?, lastName: String?, youAreThe: String?, location: String?, status: String?, avarta: String?, email: String?, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
         var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["social_type_id": socialTypeId, "first_name": firstName, "last_name": lastName, "you_are_the": youAreThe, "location": location, "status": status, "avarta": avarta, "email": email] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "userprofile/", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // /api/weeks
    func apiPostWeeks(authorization: String, length: Float, weight: Float, title: String, shortDescription: String, description: String, dailyRelation: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["length": length, "weight": weight, "title": title, "short_description": shortDescription, "description": description, "daily_relation": description] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "weeks", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllWeeks(callBack: @escaping ([PregWeek], String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "weeks", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            var weekArray : [PregWeek] = []
//            let domain1 = "http://1.55.17.233:8888/api"
            if(isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    let week : PregWeek = PregWeek()
                    week.title = item["title"].stringValue
                    week.week_description = NetworkManager.rootService + item["description"].stringValue
                    week.id = item["id"].intValue
//                    week.short_description = item["short_description"].stringValue
                    week.comment = item["comment"].stringValue
                    if item["photo"].stringValue.isEmpty == false {
                        week.photo = NetworkManager.rootDomain + item["photo"].stringValue
                    }
                    week.length = item["length"].floatValue
                    week.weight = item["weight"].floatValue
                    weekArray.append(week)
                }
            }
            callBack(weekArray, message, isSuccess)
        }
    }
    
    func apiGetWeeks(weekId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "weeks/\(weekId)", .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api read daily
    func apiReadDaily(dailyId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "daillies/read/\(dailyId)", .put, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api read weekly
    func apiReadWeeks(weekId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "weeks/read/\(weekId)", .put, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutWeeks(weekId: Int, length: Float?, weight: Float?, title: String?, shortDescription: String?, description: String?, dailyRelation: String?, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        var params : Dictionary<String, Any> = [:] as Dictionary
        
//        if let weekId = weekId {
//            params["id"] = weekId
//        }
        if let length = length {
            params["length"] = weekId
        }
        if let weight = weight {
            params["weight"] = weekId
        }
        if let title = title {
            params["title"] = weekId
        }
        if let shortDescription = shortDescription {
            params["short_description"] = shortDescription
        }
        if let description = description {
            params["description"] = description
        }
        if let dailyRelation = dailyRelation {
            params["daily_relation"] = dailyRelation
        }
        
        self.executeRequest(NetworkManager.apiDomain + "weeks/\(weekId)", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteWeeks(weekId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["week _id": weekId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "weeks/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // /api/daillies/
    func apiPostDaillies(dailyTypeId: Int, title: String,highlineImage: String, shortDescription: String, description: String, dailyRelation: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["daily_type_id": dailyTypeId, "title": title, "highline_image": highlineImage, "short_description": shortDescription, "description": description, "daily_relation": description] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "daillies", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllDaillies(callBack: @escaping ([PregDaily], String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "daillies", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            var allDailyArray : [PregDaily] = []
            if(isSuccess) {
                let jsonData = JSON(data)
                for json in jsonData.arrayValue {
                    let daily = PregDaily()
                    daily.id = Int(json["id"].stringValue)!
                    daily.title = json["title"].stringValue
                    daily.hingline_image = json["highline_image"].stringValue
                    daily.daily_description = json["description"].stringValue
                    daily.like = json["like"].intValue
                    daily.todo_id = json["todo_id"].intValue
                    allDailyArray.append(daily)
                }
            }
            callBack(allDailyArray, message, isSuccess)
        }
    }
    
    func apiGetDaillies(dailyId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["daily_id" : dailyId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "daillies/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutDaillies(dailyId: Int, dailyTypeId: Int, title: String,highlineImage: String, shortDescription: String, description: String, dailyRelation: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["daily_id": dailyId, "daily_type_id": dailyTypeId, "title": title, "highline_image": highlineImage, "short_description": shortDescription, "description": description, "daily_relation": description] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "daillies/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteDaillies(dailyId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["daily_id": dailyId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "daillies/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    // /api/dailyinteract/
    func apiPostDailyInteract(dailyID : Int, like : Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["daily_id": dailyID, "like": like] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailyinteract/", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutDailyInteract(dailyID : Int, like : Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["daily_id" : dailyID, "like": like] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailyinteract/\(dailyID)", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                print(message)
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteDailyInteract(dailyID : Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
//        let params : Dictionary = ["daily_id" : dailyID, "like": like] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailyinteract/\(dailyID)", .delete, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                print(message)
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // /api/Weeklyinteract/
    func apiPostWeeklyInteract(weekID : Int, comment : String?, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        var params : Dictionary<String, Any> = ["week_id" : weekID] as Dictionary
        
        if let cm = comment {
            params["comment"] = cm
        }
        
        self.executeRequest(NetworkManager.apiDomain + "weeklyinteract/", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                callBack(data, message, isSuccess)
            }
                callBack(data, message, isSuccess)
        }
    }
    
    func apiPutWeeklyInteract(weekID : Int, comment : String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["week_id": weekID, "comment": comment] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "weeklyinteract/\(weekID)", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                print(message)
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiUploadWeeklyImageInteract(weekID: Int, image: UIImage, updateProgress:@escaping (_ progress: Float) -> Void?, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        var header: Dictionary<String, String> = ["Content-Type":"application/x-www-form-urlencoded"]
        
        if let token = AuthManager.shared.authToken {
            header["Authorization"] = "bearer " + token
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            var imageData: Data
            var fileType: String
            if(image.isPNG()) {
                imageData = image.pngData()!
                fileType = "png"
            } else {
                imageData = image.jpegData(compressionQuality: 1.0)!
                fileType = "jpg"
            }
            let imageName = String("\(weekID)_\(Date().timeIntervalSince1970).\(fileType)")
            multipartFormData.append(imageData, withName:"Upload_file", fileName:imageName, mimeType: "image/*")
        }, usingThreshold: 10*1024*1024,
           to: NetworkManager.apiDomain + "weeklyinteract/\(weekID)/upload",
           method: .post,
           headers: header
        ) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    // success block
                    self.handleDataResponse(response, callBack: callBack)
                }
                
                upload.uploadProgress(closure: { (progress) in
                    updateProgress(Float(progress.completedUnitCount) /  Float(progress.totalUnitCount))
                    
                })
                
            case .failure(let encodingError):
                print("Failed to encode upload \(encodingError)")
            }
        }
    }
    
    // api/sizeguides/
    func apiPostSizeguides(image: Int, title: String, description: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["image": image, "title": title, "description": description] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "sizeguides", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllSizeguides(callBack: @escaping ([PregSizeGuide], String, Bool) -> ()) -> Void {
        var headers = [
            
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "sizeguides", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            var mySizeGuidesArray : [PregSizeGuide] = []
            if(isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    if (item["id"].intValue != 3 ){
                    let mySizeGuides : PregSizeGuide = PregSizeGuide()
                    mySizeGuides.image = item["image"].stringValue
                    mySizeGuides.title = item["title"].stringValue
                    mySizeGuides.length = item["length"].stringValue
                    mySizeGuides.weight = item["weight"].stringValue
                    mySizeGuides.id = item["id"].intValue
                    mySizeGuides.week_id = item["week_id"].intValue
                    mySizeGuidesArray.append(mySizeGuides)
                    }
                }
            }
            callBack(mySizeGuidesArray, message, isSuccess)
        }
    }
    
    /**
     Function get all Baby Images file
     Respon : Dictionary with all data 2d tab and image late 3d screen
     image_type_id=1 : this is nomal image
     image_type_id=2 : this is 2d image
     image_type_id=3 : this is 3d image
     **/
    func apiGetImages(callBack: @escaping (Any, String, Bool) -> ()) -> Void  {
        // header data Request API
        var headers = [
            "Content-Type": "application/json"
        ]
        // add more access token to header Request API
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        // call API
        self.executeRequest(NetworkManager.apiDomain + "images", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
            return
//            var dataRespon : Dictionary<String,Array<String>> = ["0":[],"1":[],"2":[]]
//            if(isSuccess) {
//                let result = JSON(data)
//                for item in result.arrayValue {
//                    var keyRespon:String = "0"
//                    switch item["image_type_id"] {
//                    case 1:
//                        keyRespon = "1"
//                    case 2:
//                        keyRespon = "0"
//                    case 3:
//                        keyRespon = "2"
//                    default:
//                        keyRespon = "1"
//                    }
//                    // append data respon API json into Dictionary
//                    dataRespon[item["image_type_id"].stringValue]?.append("http://1.55.17.233:8888" + item["image"].stringValue)
//                }
//            }
//            callBack(dataRespon, message, isSuccess)
        }
    }
    
    
    func apiGetSizeguides(sizeGuideId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["size_guide_id": sizeGuideId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "sizeguides/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutSizeguides(sizeGuideId: Int, image: Int, title: String, description: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["size_guide_id": sizeGuideId, "image": image, "title": title, "description": description] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "sizeguides/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteSizeguides(sizeGuideId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["size_guide_id": sizeGuideId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "sizeguides/", .delete, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/images/
    func apiPostImages(imageTypeId: Int, image: String, weekId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["image_type_id": imageTypeId, "image": image, "week_id": weekId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "images", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetImages(id: Int, imageTypeId: Int, image: String, weekId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id" : id, "image_type_id": imageTypeId, "image": image, "week_id": weekId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "images", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutImages(imageId: Int, imageTypeId: Int, image: String, weekId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["image_id" : imageId, "image_type_id": imageTypeId, "image": image, "week_id": weekId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "images/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteImages(imageId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["image_id" : imageId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "images/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/guides/
    func apiPostGuides(pageID: Int, guideTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["page_id": pageID, "guides_type_id": guideTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "guides", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllGuides(callBack: @escaping ([PregGuidesType], String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "guidestypes", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            var guidesArray : [PregGuidesType] = []
            if (isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    let guidesType : PregGuidesType = PregGuidesType()
                    guidesType.type = item["type"].stringValue
                    guidesType.id = item["id"].intValue
                    guidesArray.append(guidesType)
                }
            }
            callBack(guidesArray, message, isSuccess)
        }
    }
    
    func apiGetGuides(guideTypeId : Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["guides_type_id" : guideTypeId] as Dictionary
        
        self.executeRequest(NetworkManager.apiDomain + "guides", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
//            var guidesArray : [PregGuides] = []
//            if (isSuccess) {
//                let result = JSON(data)
//                for item in result.arrayValue {
//                    let guidesType : PregGuides = PregGuides()
//                    guidesType.guides_type_id = item["guides_type_id"].intValue
//                    guidesType.guides_type = item["guides_type"].stringValue
//                    guidesType.title = item["title"].stringValue
//                    guidesType.content = item["content"].stringValue
//                    guidesType.page_image = item["page_image"].stringValue
//                    guidesArray.append(guidesType)
//                    print(guidesArray)
//                }
//            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutGuides(guideId: Int, pageID: Int, guideTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["guide_id" : guideId, "page_id": pageID, "guides_type_id": guideTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "guides/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteGuides(guideId: Int, pageID: Int, guideTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["guide_id" : guideId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "guides/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/myweights/
    func apiPostMyWeights(myWeightTypeId: Int, startDate: String, currentDate: String,currentWeight : Float,weekID: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["current_date": currentDate, "my_weight_type_id": myWeightTypeId, "start_date": startDate, "current_weight": currentWeight, "week_id": weekID] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweights", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllMyWeights(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
//        let params : Dictionary = ["user_id": userID] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweights", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
//                print(data)
                callBack(JSON(data), message, isSuccess)
            } else {
                callBack(data, message, isSuccess)
            }
        }
    }
    
    func apiGetMyWeights(myWeightId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["my_weight_id" : myWeightId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweights/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutMyWeights(myWeightId: Int, myWeightTypeId: Int?, currentDate: String?, curWeight : Float?, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        var params: Dictionary<String, Any> = [:]
        
        if let myWeightTypeId = myWeightTypeId  {
            params["my_weight_type_id"] = myWeightTypeId
        }
        if let currentDate = currentDate  {
            params["current_date"] = currentDate
        }
        if let curWeight = curWeight  {
            params["current_weight"] = curWeight
        }
 
        self.executeRequest(NetworkManager.apiDomain + "myweights/\(myWeightId)", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteMyWeights(myWeightId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        //        let params : Dictionary = ["id" : myWeightId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweights/\(myWeightId)", .delete, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            print(message)
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteAllMyWeights( callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers: Dictionary<String, String> = [:]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        //        let params : Dictionary = ["id" : myWeightId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweights/all", .delete, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            print(message)
            callBack(data, message, isSuccess)
        }
    }
    
    // api/mybellies/
    func apiPostMyBellies(image: String, myBellyTypeId: Int, month: Int, userId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["image": image, "my_belly_type_id": myBellyTypeId, "month": month, "user_id": userId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybellies", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiUploadImageMyBellies(month: Int, image: UIImage, updateProgress:@escaping (_ progress: Float) -> Void?, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        if let user = DatabaseManager.sharedManager.getLocalUserInfo() {
            let id = user.id
            var header: Dictionary<String, String> = ["Content-Type":"multipart/form-data"]
            
            if let token = AuthManager.shared.authToken {
                header["Authorization"] = "bearer " + token
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                var imageData: Data
                var fileType: String
                if(image.isPNG()) {
                    imageData = image.pngData()!
                    fileType = "png"
                } else {
                    imageData = image.jpegData(compressionQuality: 1.0)!
                    fileType = "jpg"
                }
//                multipartFormData.append(imageData, withName: "Upload_file", mimeType: "image/*")
                //let imageName = String("\(id)_\(Date().timeIntervalSince1970).\(fileType)")
                let imageName = String("\(id)\(month)_\(Date().timeIntervalSince1970).\(fileType)")
                multipartFormData.append(imageData, withName:"Upload_file", fileName: imageName, mimeType: "image/*")
            }, usingThreshold: 10*1024*1024,
               to: NetworkManager.apiDomain + "mybellies/\(month)",
               method: .post,
               headers: header
            ) { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        // success block
                        self.handleDataResponse(response, callBack: callBack)
                    }
                    
                    upload.uploadProgress(closure: { (progress) in
                        updateProgress(Float(progress.completedUnitCount) /  Float(progress.totalUnitCount))
                        
                    })
                    
                case .failure(let encodingError):
                    print("Failed to encode upload \(encodingError)")
                }
            }
        }
    }
    
    func apiGetAllMyBellies(callBack: @escaping ([PregMyBelly], String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "mybellies", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            var myBelliesArray : [PregMyBelly] = []
            if(isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    if item["user_id"].intValue == 9 {
                        let myBelly : PregMyBelly = PregMyBelly()
                        let orignalUrlString = item["image"].stringValue
                        let urlString = orignalUrlString.replacingOccurrences(of: "~", with: NetworkManager.rootDomain)
                        myBelly.image = urlString
                        myBelliesArray.append(myBelly)
                    }
                }
            }
            callBack(myBelliesArray, message, isSuccess)
        }
    }
    
    func apiGetMyBelliesBy(callBack: @escaping ([PregMyBelly], String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json",
            ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "mybellies", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            var myBelliesArray : [PregMyBelly] = []
            if(isSuccess) {
                let result = JSON(data)
                //print(data)
                for item in result.arrayValue {
                    let myBelly : PregMyBelly = PregMyBelly()
                    let orignalUrlString = item["image"].stringValue
                    let monthUrl = item["month"].intValue
                    let urlString = NetworkManager.rootDomain + orignalUrlString
                    //print(urlString)
                    myBelly.image = urlString
                    myBelly.month = monthUrl
                    myBelliesArray.append(myBelly)
                    print(myBelliesArray)
                }
            }
            callBack(myBelliesArray, message, isSuccess)
        }
    }
    
    func apiGetIllustrateBellyBy(month:Int?, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        var api = "mybellies/template/"
        if let month = month {
            api = api + "\(month)"
        }
        self.executeRequest(NetworkManager.apiDomain + api, .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            
            callBack(data, message, isSuccess)
        }
    }
    
    
    func apiPutMyBellies(myBellyId: Int, image: String, myBellyTypeId: Int, month: Int, userId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["my_belly_id" : myBellyTypeId, "image": image, "my_belly_type_id": myBellyTypeId, "month": month, "user_id": userId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybellies/", .get, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteMyBellies(myBellyId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["my_belly_id" : myBellyId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybellies/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/mybirthplans/
    func apiPostMyBirthType(userId: Int, myBirthPlanItemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["user_id": userId, "my_birth_plan_item_id": myBirthPlanItemId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplans", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    
    func apiGetAllMyBirthPlansType(callBack: @escaping ([PregMyBirthPlanType], String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "mybirthplantypes/", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
//            print(data)
            var myBirthPlanArray : [PregMyBirthPlanType] = []
            if(isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    let myBirthPlan : PregMyBirthPlanType = PregMyBirthPlanType()
                    myBirthPlan.id = Int(item["id"].stringValue)!
                    myBirthPlan.type = item["type"].stringValue
                    myBirthPlan.type_icon = item["type_icon"].stringValue
                    myBirthPlanArray.append(myBirthPlan)
                }
            }
            callBack(myBirthPlanArray, message, isSuccess)
        }
    }
    
    func apiPostMyBirthPlansItems(userID: Int, my_birth_plan_type_id: Int, item_content: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["my_birth_plan_type_id": my_birth_plan_type_id, "item_content": item_content, "custom_item_by_user_id" : userID] as Dictionary
        
        self.executeRequest(NetworkManager.apiDomain + "mybirthplanitems/", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllMyBirthPlansItems(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
//        let params : Dictionary = ["custom_item_by_user_id" : userID] as Dictionary
        
        self.executeRequest(NetworkManager.apiDomain + "mybirthplanitems/", .get, nil, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    
    func apiGetMyBirthPlansItem(birthPlanTypeID: Int, callBack: @escaping ([PregMyBirthPlanItem], String, Bool) -> ()) -> Void {
        
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        let params : Dictionary = ["my_birth_plan_type_id": birthPlanTypeID] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplanitems/", .get, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            var birthPlanItemArray : [PregMyBirthPlanItem] = [PregMyBirthPlanItem]()
            if(isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    let birthPlanItemData : PregMyBirthPlanItem = PregMyBirthPlanItem()
                    birthPlanItemData.item_content = item["item_content"].stringValue
                    birthPlanItemData.id = Int(item["id"].stringValue)!
                    birthPlanItemArray.append(birthPlanItemData)
                }
            }
            callBack(birthPlanItemArray, message, isSuccess)
        }
    }
    
    func apiPostMyBirthPlans(my_birth_plan_item_id : Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["my_birth_plan_item_id": my_birth_plan_item_id] as Dictionary
        
        self.executeRequest(NetworkManager.apiDomain + "mybirthplans/", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetMyBirthPlans(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "mybirthplans/", .get, nil, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutMyBirthPlans(myBirthPlanId: Int, userId: Int, myBirthPlanItemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["my_birth_plan_id": myBirthPlanId, "user_id": userId, "my_birth_plan_item_id": myBirthPlanItemId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplans/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteMyBirthPlans(itemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "mybirthplans/\(itemId)", .delete, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    
    
    // api/todos/
    func apiPostUserToDos(todoId: Int, status: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        let params : Dictionary = ["todo_id": todoId, "status": status] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "usertodo", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetUserToDos(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "usertodo", .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutUserToDos(todoId: Int, status: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        let params : Dictionary = ["todo_id": todoId, "status": status] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "usertodo/\(todoId)", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteUserToDos(todoId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }

        let params : Dictionary = ["todo_id": todoId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "usertodo/\(todoId)", .delete, params, JSONEncoding.default, headers) {
            (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/todos/
    func apiPostToDos(title: String, customTaskByUserId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["title": title, "custom_task_by_user_id": customTaskByUserId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "todos", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllToDos(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
    
        self.executeRequest(NetworkManager.apiDomain + "todos", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetToDos(todoId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["todo_id" : todoId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "todos/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutToDos(todoId: Int, weekId: Int, title: String, customTaskByUserId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["todo_id": todoId, "week_id": weekId, "title": title, "custom_task_by_user_id": customTaskByUserId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "todos/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteToDos(todoId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let request = NSMutableURLRequest(url: NSURL(string: NetworkManager.apiDomain + "todos/\(todoId)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "DELETE"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error == nil) {
                let httpResponse = response as? HTTPURLResponse
            } 
        })
        
        dataTask.resume()
    }
    
    // api/professiontype/
    
    func apiGetAllProfessionType(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "professiontypes", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/professions/
    
    func apiPostProfession(userId: Int, professionId: Int, status: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["user_id": userId, "profession_id": professionId, "status": status] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "professions", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/phones/
    func apiPostPhones(professionId: Int, phoneNumber: String, userId: Int ,name: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["profession_id": professionId, "phone_number": phoneNumber, "user_id": userId, "name": name] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "phones", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllPhones(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "phones", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetPhones(phoneId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["phone_id" : phoneId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "phones/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutPhones(phoneId: Int, professionId: Int, phoneNumber: String,name: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }

        let params : Dictionary = ["phone_id": phoneId,"profession_id": professionId, "phone_number": phoneNumber, "name": name] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "phones/\(phoneId)", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeletePhones(phoneId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
//        let params : Dictionary = ["phone_id": phoneId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "phones/\(phoneId)", .delete, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/babynames//Users/dady/Desktop/workspace/pregnacy/Pregnancy/NetworkManager/NetworkManager.swift
    func apiPostBabyName(countryId: Int, genderId: String, name: String, customBabyNameByUserId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        let params : Dictionary = ["country_id": countryId, "gender_id": genderId, "name": name, "custom_baby_name_by_user_id": customBabyNameByUserId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "babynames", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllBabyName(countryId: Int,
                           callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "babynames", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetBabyName(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }

        self.executeRequest(NetworkManager.apiDomain + "babynames/", .get, nil, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                callBack(JSON(data), message, isSuccess)
            } else {
                callBack(data, message, isSuccess)
            }
        }
    }
    
    func apiPutBabyName(id: Int, customBabyNameByUserId: String , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["id": id, "custom_baby_name_by_user_id": customBabyNameByUserId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "babynames/\(id)", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteBabyNamne(babyNameId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        let params : Dictionary = ["baby_name_id": babyNameId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "babynames/\(babyNameId)", .delete, params, JSONEncoding.default, headers) {
            (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    // api/userbabynames
    
    func apiPostUserBabyName(userId: Int, babyNameId: Int,
                             callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        let params : Dictionary = ["user_id": userId, "baby_name_id": babyNameId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "userbabyname", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllUserBabyName(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "userbabyname", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetUserBabyName(userId: Int, babyNameId: Int,
                        callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["baby_name_id": babyNameId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "userbabyname/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    //    func apiPutBabyName(userId: Int, babyNameId: Int,
    //        callBack: @escaping (Any, String, Bool) -> ()) -> Void {
    //        let headers = [
    //            "Content-Type": "application/json"
    //        ]
    //        let params : Dictionary = ["baby_name_id": babyNameId,"id": id,"country_id": countryId, "gender_id": genderId, "name": name, "custom_baby_name_by_user_id": customBabyNameByUserId] as Dictionary
    //        self.executeRequest(NetworkManager.domain + "babynames/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
    //            if(isSuccess) {
    //                
    //            }
    //            callBack(data, message, isSuccess)
    //        }
    //    }
    //
    func apiDeleteUserBabyNamne(babyNameId: Int,
                                callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        let params : Dictionary = ["baby_name_id": babyNameId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "userbabyname/\(babyNameId)", .delete, params, JSONEncoding.default, headers) {
            (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    // api/shoppingcategorys/
    func apiPostShoppingCategorys(title: Int, status: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["title": title, "status": status] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "shoppingcategorys", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllShoppingCategorys(callBack: @escaping ([PregShoppingCategorys], String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "Bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "shoppingcategorys", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            
            var shoppingData = [PregShoppingCategorys]()
            
            if(isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    let shoppingItem = PregShoppingCategorys()
                    shoppingItem.id = Int(item["id"].stringValue)!
                    shoppingItem.title = item["title"].stringValue
                    shoppingItem.status = Int(item["status"].stringValue)!
                    //                    shoppingItem.icon = item["title"].stringValue
                    shoppingData.append(shoppingItem)
                }
            }
            callBack(shoppingData, message, isSuccess)
        }
    }
    
    func apiGetShoppingCategorys(shoppingCategoryId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["shopping_category_id": shoppingCategoryId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "shoppingcategorys/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutShoppingCategorys(shoppingCategoryId: Int, title: String, status: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["shopping_category_id": shoppingCategoryId, "title": title, "status": status] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "shoppingcategorys/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteShoppingCategorys(shoppingCategoryId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["shopping_category_id": shoppingCategoryId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "shoppingcategorys/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/hospitalbagitems/
    func apiPostHospitalBagItems(name: String, type: Int, customItemByUserId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "Bearer " + token
        }
        let params : Dictionary = ["name": name, "type": type, "custom_item_by_user_id": customItemByUserId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "hospitalbagitems", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllHospitalBagItems(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "Bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "hospitalbagitems", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetHospitalBagItems(hospitalBagItemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["hospital_bag_item_id" : hospitalBagItemId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "hospitalbagitems/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutHospitalBagItems(hospitalBagItemId: Int, name: String, type: Int, customItemByUserId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["hospital_bag_item_id" : hospitalBagItemId, "name": name, "type": type, "custom_item_by_user_id": customItemByUserId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "hospitalbagitems/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteHospitalBagItems(hospitalBagItemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["hospital_bag_item_id" : hospitalBagItemId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "hospitalbagitems/\(hospitalBagItemId)", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    // api/timelines/
    func apiPostTimeLines(weekId: Int, title: String, image: String, position: String, timeFrameId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["week_id": weekId, "title": title, "image": image, "position": position, "time_frame_id":timeFrameId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "timelines", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllTimeLines(id: Int, weekId: Int, title: String, image: String, position: String, timeFrameId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id": id, "week_id": weekId, "title": title, "image": image, "position": position, "time_frame_id":timeFrameId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "timelines", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetTimeLines(timeLineId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["time_line_id": timeLineId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "timelines/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutTimeLines(timeLineId: Int, weekId: Int, title: String, image: String, position: String, timeFrameId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["time_line_id": timeLineId, "week_id": weekId, "title": title, "image": image, "position": position, "time_frame_id":timeFrameId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "timelines/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteTimeLines(timeLineId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["time_line_id": timeLineId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "timelines/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/guidestypes/
    func apiPostGuidesTypes(type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "guidestypes", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllGuidesTypes(id: Int, type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id": id, "type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "guidestypes", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetGuidesTypes(guidestypeId: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["guidestype_id": guidestypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "guidestypes/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutGuidesTypes(guidestypeId: Int, type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["guidestype_id": guidestypeId, "type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "guidestypes", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteGuidesTypes(guidestypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["guidestype_id": guidestypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "guidestypes", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/myweighttypes/
    func apiPostMyWeightTypes(type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweighttypes", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllMyWeightTypes(id: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["id": id] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweightunits", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetMyWeightTypes(myWeightTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["id": myWeightTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweightunits/\(myWeightTypeId)", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutMyWeightTypes(myWeightTypeId: Int, type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["my_weight_type_id": myWeightTypeId, "type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweighttypes/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteMyWeightTypes(myWeightTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["my_weight_type_id": myWeightTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweighttypes/", .delete, params, JSONEncoding.default, nil) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    // api/myweightinsts/
    func apiPostMyWeightInSts(position: Int, value: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["position": position, "value" : value] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweightinsts", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllMyWeightInSts(id: Int, position: Int, value: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id": id, "position": position, "value" : value] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweightinsts", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetMyWeightInSts(myWeightInStId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["my_weight_in_st_id": myWeightInStId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweightinsts/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutMyWeightInSts(myWeightInStId: Int, position: Int, value: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["my_weight_in_st_id": myWeightInStId, "position": position, "value" : value] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweightinsts/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteMyWeightInSts(myWeightInStId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["my_weight_in_st_id": myWeightInStId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "myweightinsts/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/mybellytypes/
    func apiPostMyBellyTypes(type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybellytypes", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllMyBellyTypes(id: Int, type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id" : id, "type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybellytypes", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetMyBellyTypes(myBellyTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["my_belly_type_id" : myBellyTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybellytypes/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutMyBellyTypes(myBellyTypeId: Int , type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["my_belly_type_id" : myBellyTypeId, "type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybellytypes", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteMyBellyTypes(myBellyTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["my_belly_type_id" : myBellyTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybellytypes/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/mybirthplantype/
    func apiPostMybirthPlanType(type: String, typeIcon: String , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["type": type, "type_icon": typeIcon] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplantype", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllMybirthPlanType(id: Int, type: String, typeIcon: String , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id" : id, "type": type, "type_icon": typeIcon] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplantype", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetMybirthPlanType(myBirthPlanTypeId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["my_birthplan_type_id": myBirthPlanTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplantype/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutMybirthPlanType(myBirthPlanTypeId: Int, type: String, typeIcon: String , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["my_birthplan_type_id": myBirthPlanTypeId, "type": type, "type_icon": typeIcon] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplantype/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteMybirthPlanType(myBirthPlanTypeId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["my_birthplan_type_id": myBirthPlanTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplantype/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/mybirthplanitems/
    func apiPostMybirthPlanItems(myBirthPlanTypeId: Int, itemContent: String, customItemByUserId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["my_birth_plan_type_id": myBirthPlanTypeId, "item_content": itemContent, "custom_item_by_user_id": customItemByUserId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplanitems", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllMybirthPlanItems(id: Int, myBirthPlanTypeId: Int, itemContent: String, customItemByUserId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id": id, "my_birth_plan_type_id": myBirthPlanTypeId, "item_content": itemContent, "custom_item_by_user_id": customItemByUserId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplanitems", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetMybirthPlanItems(myBirthPlanItemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["my_birth_plan_item_id": myBirthPlanItemId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplanitems/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutMybirthPlanItems(myBirthPlanItemId: Int, myBirthPlanTypeId: Int, itemContent: String, customItemByUserId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["my_birth_plan_item_id": myBirthPlanItemId, "my_birth_plan_type_id": myBirthPlanTypeId, "item_content": itemContent, "custom_item_by_user_id": customItemByUserId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplanitems/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteMybirthPlanItems(myBirthPlanItemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
//        let params : Dictionary = ["my_birth_plan_item_id": myBirthPlanItemId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "mybirthplanitems/\(myBirthPlanItemId)", .delete, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    // api/dailytypes/
    func apiPostDailyTypes(type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailytypes", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllDailyTypes(id: Int, type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id": id, "type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailytypes", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetDailyTypes(dailyTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["daily_type_id": dailyTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailytypes/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutDailyTypes(dailyTypeId: Int, type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["daily_type_id": dailyTypeId, "type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailytypes/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteDailyTypes(dailyTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["daily_type_id": dailyTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailytypes/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/dailylikes/
    func apiPostDailyLikes(userId: Int, likeTypeId: Int, dailyId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["user_id": userId, "like_type_id": likeTypeId, "daily_id": dailyId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailylikes", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllDailyLikes(id: Int, userId: Int, likeTypeId: Int, dailyId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id": id, "user_id": userId, "like_type_id": likeTypeId, "daily_id": dailyId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailylikes", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetDailyLikes(dailyLikeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["daily_like_id": dailyLikeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailylikes/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutDailyLikes(dailyLikeId: Int, userId: Int, likeTypeId: Int, dailyId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["daily_like_id": dailyLikeId, "user_id": userId, "like_type_id": likeTypeId, "daily_id": dailyId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailylikes/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteDailyLikes(dailyLikeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["daily_like_id": dailyLikeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "dailylikes/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/weeklynotes/
    func apiPostWeeklyNotes(weekId: Int, userId: Int, photo: String, note: String, like: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["week_id": weekId, "user_id": userId, "photo": photo, "note": note, "like": like] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "weeklynotes", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllWeeklyNotes(id: Int, weekId: Int, userId: Int, photo: String, note: String, like: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id": id, "week_id": weekId, "user_id": userId, "photo": photo, "note": note, "like": like] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "weeklynotes/", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetWeeklyNotes(weeklyNoteId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["weekly_note_id": weeklyNoteId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "weeklynotes/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutWeeklyNotes(weeklyNoteId: Int, weekId: Int, userId: Int, photo: String, note: String, like: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["week_id": weekId, "user_id": userId, "photo": photo, "note": note, "like": like] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "weeklynotes/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteWeeklyNotes(weeklyNoteId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["weekly_note_id": weeklyNoteId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "weeklynotes/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/imagetypes/
    func apiPostImageTypes(type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "imagetypes", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllImageTypes(id: String, type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id": id, "type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "imagetypes", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetImageTypes(imageTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["image_type_id": imageTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "imagetypes/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutImageTypes(imageTypeId: Int, type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["image_type_id": imageTypeId, "type": type] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "imagetypes", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteImageTypes(imageTypeId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["image_type_id": imageTypeId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "imagetypes/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/helps/
    func apiPostHelps(helpCategoryId: Int,image: String, description: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["help_category_id": helpCategoryId, "image": image, "description" : description] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "helps", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllHelps(id: Int, helpCategoryId: Int, image: String, description: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id": id, "help_category_id": helpCategoryId, "image": image, "description" : description] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "helps", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetHelps(helpId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["help_id": helpId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "helps/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutHelps(helpId: Int, helpCategoryId: Int,image: String, description: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["help_id": helpId, "help_category_id": helpCategoryId, "image": image, "description" : description] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "helps/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteHelps(helpId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["help_id": helpId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "helps/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/helpcategories/
    func apiPostHelpCategories(name: String, highLineImage: String, order: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["name": name, "highline_image": highLineImage, "order" : order] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "helhelpcategoriesps", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apGetAllHelpCategories(id: Int, name: String, highLineImage: String, order: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["id": id, "name": name, "highline_image": highLineImage, "order" : order] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "helhelpcategoriesps", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apGetHelpCategories(helpCategoryId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["help_category_id" : helpCategoryId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "helhelpcategoriesps/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutHelpCategories(helpCategoryId: Int, name: String, highLineImage: String, order: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["help_category_id": helpCategoryId, "name": name, "highline_image": highLineImage, "order" : order] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "helhelpcategoriesps/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apGetDeleteCategories(helpCategoryId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["help_category_id" : helpCategoryId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "helhelpcategoriesps/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/shoppingitems/
    func apiPostShoppingItems(itemName: String, customItemByUserId: Int, categoryId: Int, status: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["item_name": itemName, "custom_item_by_user_id": customItemByUserId, "category_id" : categoryId, "status": status] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "shoppingitems", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetShoppingItemsByID(categoryId: Int, callBack: @escaping ([PregShoppingItemByID], String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        //        NetworkManager.shareInstance.apiGetUser
        let params : Dictionary = ["category_id": categoryId] as Dictionary
        
        self.executeRequest(NetworkManager.apiDomain + "shoppingitems/", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            var arrDataShoppingItem = [PregShoppingItemByID]()
            if(isSuccess) {
                //                print(data)
                let result = JSON(data)
                for item in result.arrayValue {
                    let dataShoppingItem = PregShoppingItemByID()
                    dataShoppingItem.id_item = Int(item["id"].stringValue)!
                    dataShoppingItem.item_name = item["item_name"].stringValue
                    dataShoppingItem.status = Int(item["status"].stringValue)!
                    arrDataShoppingItem.append(dataShoppingItem)
                }
            }
            callBack(arrDataShoppingItem, message, isSuccess)
        }
    }
    
    func apiGetShoppingItems(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "shoppingitems/", .get, nil, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutShoppingItems(shoppingItemId: Int, status: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["status": status] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "shoppingitems/\(shoppingItemId)", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteShoppingItems(shoppingItemId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["shopping_item_id": shoppingItemId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "shoppingitems/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    // api/User Shopping Cart
    func apiPostShoppingCart(shoppingItemId: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        let params : Dictionary = ["shopping_item_id": shoppingItemId, "status": 3] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "usershoppingcart/", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetShoppingCart(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "usershoppingcart/", .get, nil, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutShoppingCart(shoppingItemId: Int, status: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["status": status] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "usershoppingcart/\(shoppingItemId)", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteShoppingCart(shoppingItemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "usershoppingcart/\(shoppingItemId)", .delete, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    // api/userhospitalbagitems/
    func apiPostUserHospitalBagItems(hospitalBagItemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["hospital_bag_item_id": hospitalBagItemId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "userhospitalbagitems", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllUserHospitalBagItems(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "userhospitalbagitems", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetUserHospitalBagItems(userId: Int, hospitalBagItemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["user_id": userId, "hospital_bag_item_id": hospitalBagItemId] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "userhospitalbagitems/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteUserHospitalBagItems(hospitalBagItemId: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
    
        self.executeRequest(NetworkManager.apiDomain + "userhospitalbagitems/\(hospitalBagItemId)", .delete, nil, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    //    api/pregnancys/
    
    func apiPostPregnancyDueDate(userId: Int, baby_gender: Int, due_date: Date, show_week: Int, pregnancy_loss: Int, baby_already_born:Int, date_of_birth: Date, weeks_pregnant: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["user_id": userId, "baby_gender": baby_gender, "due_date": due_date, "show_week": show_week, "pregnancy_loss": pregnancy_loss, "baby_already_born" : baby_already_born, "date_of_birth": date_of_birth, "weeks_pregnant": weeks_pregnant ] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "pregnancys/", .post, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllPregnancyDueDate(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "pregnancys/", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetPregnancyDueDate(pregnancy_id: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["pregnancy_id": pregnancy_id] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "pregnancys/", .get, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutPregnancyDueDate(pregnancy_id: Int, userId: Int, baby_gender: Int, due_date: Date, show_week: Int, pregnancy_loss: Int, baby_already_born:Int, date_of_birth: Date, weeks_pregnant: Int , callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        let headers = [
            "Content-Type": "application/json"
        ]
        let params : Dictionary = ["pregnancy_id":pregnancy_id, "user_id": userId, "baby_gender": baby_gender, "due_date": due_date, "show_week": show_week, "pregnancy_loss": pregnancy_loss, "baby_already_born" : baby_already_born, "date_of_birth": date_of_birth, "weeks_pregnant": weeks_pregnant ] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "pregnancys/", .put, params, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeletePregnancyDueDate(pregnancy_id: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["pregnancy_id": pregnancy_id] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "pregnancys/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    
    //api/settings/
    func apiPostAppSetting(reminders:Bool, length_units:Bool, weight_unit:Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["reminders": reminders, "length_units": length_units, "weight_unit": weight_unit] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "settings/", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllAppSetting(user_id:String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["user_id": user_id] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "settings/", .get, params, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAppSetting(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "settings/", .get, nil, URLEncoding.queryString, headers) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutAppSetting(reminders:Bool, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["reminders": reminders] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "settings/update", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteAppSetting(setting_id: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let params : Dictionary = ["setting_id": setting_id] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "settings/", .delete, params, URLEncoding.queryString, nil) { (data, message, isSuccess) in
            if(isSuccess) {
                
            }
            callBack(data, message, isSuccess)
        }
    }
    
    //api/kickresults/
    func apiPostKickResults(kick_date: String, duration: Float, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["kick_date": kick_date, "duration": duration] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "kickresults/", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllKickResults(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "kickresults/", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPutKickResults(kick_result_id: Int,kick_date: String, duration: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["kick_date": kick_date, "duration": duration] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "kickresults/\(kick_result_id)", .put, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    //api/userkickhistories/
    func apiPostUserKickHistories(kick_result_id: Int, kick_date: String, duration: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["kick_result_id": kick_result_id, "kick_date": kick_date, "duration": duration] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "userkickhistories/", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetUserKickHistories(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
        "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
        headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "userkickhistories/", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteUserKickHistories(kick_result_id: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "userkickhistories/\(kick_result_id)", .delete, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    //api/kickresultsdetail/
    func apiPostKickResultsDetail(kick_result_id: Int, kick_order: Int, kick_time: String, elapsed_time: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        let params : Dictionary = ["kick_result_id": kick_result_id,"kick_order": kick_order, "kick_time": kick_time, "elapsed_time": elapsed_time] as Dictionary
        self.executeRequest(NetworkManager.apiDomain + "kickresultdetail/", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAllKickResultDetail(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        self.executeRequest(NetworkManager.apiDomain + "kickresultdetail/", .get, nil, URLEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiDeleteKickResultDetail(kick_result_id: Int, kick_order: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        var headers = [
            "Content-Type": "application/json"
        ]
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest(NetworkManager.apiDomain + "kickresultdetail/\(kick_result_id)/\(kick_order)", .delete, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    
    //////////////producs
    
    func apiGetAppointmentProductsByID(productID : Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        self.executeRequest(NetworkManager.apiService + "products/\(productID)", .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    // Get Customers
    func apiGetAppointmentCustomers(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        self.executeRequest(NetworkManager.apiService + "customers", .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    func apiGetAppointmentCustomersById(nopcustomer_id: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        self.executeRequest(NetworkManager.apiService + "customers/\(nopcustomer_id)", .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    //Get order
    
    func apiGetOrderIdByCustommerId(apiSecretKey : String, customerGUID : String, storeId : String, languageId : String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        let params : Dictionary = ["ApiSecretKey": NetworkManager.ApiSecretKey,
                                   "CustomerGUID": customerGUID,
                                   "StoreId": NetworkManager.StoreId,
                                   "LanguageId": NetworkManager.LanguageId] as Dictionary
        
        self.executeRequest(NetworkManager.apiService + "client/GetOrder", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    func apiGetOrderByID(orderId : Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        self.executeRequest(NetworkManager.apiService + "orders/\(orderId)", .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    // Get Product
    func apiGetAppointmentProduct(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        self.executeRequest(NetworkManager.apiService + "products", .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    //Get categories
    func apiGetCategories(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        
        self.executeRequest(NetworkManager.apiService + "categories", .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    
    // Get Manufacturer : thông tin phòng khám
    
    func apiGetAppointmentManufacturer(customerGUID : String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        let params : Dictionary = ["ApiSecretKey": NetworkManager.ApiSecretKey,
                                   "CustomerGUID": customerGUID,
                                   "StoreId": NetworkManager.StoreId,
                                   "LanguageId": NetworkManager.LanguageId] as Dictionary
        self.executeRequest(NetworkManager.apiService + "Client/Manufacturer", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    // Get Product By Category (kiểm tra xem lịch đặt là gói khám hay dịch vụ hay là bác sĩ)
    func apiGetProductByCategory(categoryId : String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
 
        let params : Dictionary = ["ApiSecretKey": NetworkManager.ApiSecretKey,
                                   "CustomerGUID": "60fea809-9bdf-44e4-aa0f-197c3e3f3e99",
                                   "StoreId": NetworkManager.StoreId,
                                   "LanguageId": NetworkManager.LanguageId,
                                   "CurrencyId": "1",
                                   "CategoryId": categoryId,
                                   "CatalogPagingResponse":
                                    [
                                        "PageSize": "8",
                                        "PageNumber": "1",
                                        "OrderBy": "0"
                                    ]] as Dictionary
        
        self.executeRequest(NetworkManager.apiService + "client/GetProductByCategory", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiGetAppointmentProducts(id: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiClientProductToken,
            "Content-Type": "application/json"
        ]
        
        let parameters: Dictionary<String,Any> = [
            "ApiSecretKey": NetworkManager.ApiSecretKey,
            "CustomerGUID": "60fea809-9bdf-44e4-aa0f-197c3e3f3e99",
            "StoreId": NetworkManager.StoreId,
            "LanguageId": NetworkManager.LanguageId,
            "CurrencyId": "1",
            "CategoryId": id,
            "CatalogPagingResponse": [
                "PageSize": "999999999",
                "PageNumber": "1",
                "OrderBy": "0"
            ]
        ]
        
        self.executeRequest(NetworkManager.apiService + "client/GetProductByCategory", .post, parameters, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    func apiGetProductByManufacturerId(ManufacturerId: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        let CatalogPagingResponse: Dictionary = ["PageSize": "100000",
                                                 "PageNumber": "1",
                                                 "OrderBy": "0"] as Dictionary
        
        let params : Dictionary = ["ApiSecretKey": NetworkManager.ApiSecretKey,
                                    "CustomerGUID": "60fea809-9bdf-44e4-aa0f-197c3e3f3e99",
                                    "StoreId": NetworkManager.StoreId,
                                    "LanguageId": NetworkManager.LanguageId,
                                    "CurrencyId": "1",
                                    "ManufacturerId": ManufacturerId,
                                    "CatalogPagingResponse": CatalogPagingResponse] as Dictionary
        self.executeRequest("\(NetworkManager.rootService)Api/" + "Client/GetProductByManufacturerId", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    func apiGetAllProducts(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        self.executeRequest(NetworkManager.apiService + "products", .get, nil, JSONEncoding.default, headers) {
            (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    func apiGetProducts(id_product: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        self.executeRequest(NetworkManager.apiService + "products/\(id_product)", .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    func apiGetUserProfile( callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        var headers = [
            "Content-Type": "application/json"
        ]
        
        if let token = AuthManager.shared.authToken {
            headers["Authorization"] = "bearer " + token
        }
        
        self.executeRequest("http://www.momtour.vn/api/userprofile", .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
        
    }
    
    
    func apiPostOrders(city: String, email: String, ship_address: String, first_name: String, last_name: String, phone_number: String, zip_postal_code: String, bill_address: String, created_on_utc: String, customer_id: Int, customer_gender: String, customer_birthday: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        let billing_address: Dictionary = [
            "city": city,
            "email": email,
            "address1": bill_address,
            "first_name": first_name,
            "last_name": last_name,
            "country_id":1,
            "phone_number": phone_number,
            "zip_postal_code": zip_postal_code,
            "address2": customer_gender,
            "Fax": customer_birthday] as Dictionary
        
        let shipping_address: Dictionary = [
            "city": city,
            "email": email,
            "address1": ship_address,
            "first_name": first_name,
            "last_name": last_name,
            "country_id":1,
            "phone_number": phone_number,
            "zip_postal_code": zip_postal_code,
            "address2": customer_gender,
            "Fax": customer_birthday] as Dictionary

        let order : Dictionary = ["store_id":1,
                                   "pick_up_in_store":true,
                                   "payment_method_system_name":"Payments.CheckMoneyOrder",
                                   "currency_rate":23000,
                                   "customer_tax_display_type_id":1,
                                   "order_subtotal_incl_tax":2,
                                   "order_subtotal_excl_tax":2,
                                   "order_sub_total_discount_incl_tax":2,
                                   "order_sub_total_discount_excl_tax":2,
                                   "order_shipping_incl_tax":2,
                                   "order_shipping_excl_tax":2,
                                   "payment_method_additional_fee_incl_tax":2,
                                   "payment_method_additional_fee_excl_tax":2,
                                   "order_tax":2,
                                   "order_discount":2,
                                   "order_total":2,
                                   "refunded_amount":10,
                                   "customer_language_id":1,
                                   "affiliate_id":0,
                                   "deleted":false,
                                   "created_on_utc": created_on_utc,
                                   "customer_id": customer_id,
                                   "billing_address": billing_address,
                                   "shipping_address": shipping_address] as Dictionary
        let params: Dictionary = ["order": order]
        self.executeRequest(NetworkManager.apiService + "orders", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    func apiPostOrdersNote(OrderId: String, tenBV: String, anh: String, idPro: Int, ngayDat: String, ngayKham: String, hoTen: String, sdt: String, datLich: String, diaChi: String, gioiTinh: String, namSinh: String, trieuChung: String, khungGio: String, email: String, noiKham: String, kham: String, type: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        let message: Dictionary = [
            "tenBV": tenBV,
            "anh": anh,
            "idPro": idPro,
            "ngayDat": ngayDat,
            "ngayKham": ngayKham,
            "hoTen":hoTen,
            "sdt": sdt,
            "datLich": datLich,
            "diaChi": diaChi,
            "gioiTinh": gioiTinh,
            "namSinh": namSinh,
            "trieuChung": trieuChung,
            "khungGio": khungGio,
            "email": email,
            "noiKham": noiKham,
            "kham": kham,
            "type": type] as Dictionary

        
        let ordernote : Dictionary = [
                                  "OrderId":OrderId,
                                  "apiSecretKey": "d114l98j99h103a114u114k101k114",
                                  "restAPIadminAccessKey": "76306b1e-6a2a-42b3-95b8-9d290ef8cdf9",
                                  "VendorId":"0",
                                  "LanguageId":"1",
                                  "DownloadId": "0",
                                  "DisplayToCustomer": "true",
                                  "Message": message] as Dictionary
        
        let params: Dictionary = ["ordernote": ordernote]
        self.executeRequest(NetworkManager.apiService + "Admin/AddOrderNote", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }
    
    
    
    func apiPostShoppingCartItemProduct(arrDic: Array<Dictionary<String, Any>>, product_id: Int, user_id: Int, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        let shopping_cart_item : Dictionary = ["product_attributes": arrDic,
//                                   "customer_entered_price": customer_entered_price,
                                   "quantity": 1,
                                   "created_on_utc": "\(Date())",
                                   "shopping_cart_type": 1,
                                   "product_id": product_id,
                                   "customer_id": user_id] as Dictionary
        let params: Dictionary = ["shopping_cart_item": shopping_cart_item]
        self.executeRequest(NetworkManager.apiService + "shopping_cart_items", .post, params, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
        }
    }

    func apiGetProductById(productId: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]

        
        self.executeRequest(NetworkManager.apiService + "products/\(productId)" , .get, nil, JSONEncoding.default, headers) { (data, message, isSuccess) in
            callBack(data, message, isSuccess)
            
        }
        
    }
    
    func apiGetManufacture(callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        let parameters: Dictionary<String,Any> = [
            "ApiSecretKey": NetworkManager.ApiSecretKey,
            "CustomerGUID": "60fea809-9bdf-44e4-aa0f-197c3e3f3e99",
            "StoreId": NetworkManager.StoreId,
            "LanguageId": NetworkManager.LanguageId
        ]
        
        
        self.executeRequest("\(NetworkManager.rootService)Api/" + "Client/Manufacturer" , .post, parameters, JSONEncoding.default, headers) { (data, message, isSuccess) in

            callBack(data, message, isSuccess)
            
        }
        
    }
    
    
    func apiGetProductReviews(product_id: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        let parameters: Dictionary<String,Any> = [
            "ApiSecretKey": NetworkManager.ApiSecretKey,
            "CustomerGUID": "60fea809-9bdf-44e4-aa0f-197c3e3f3e99",
            "StoreId": NetworkManager.StoreId,
            "ProductId": product_id,
            "LanguageId": NetworkManager.LanguageId
        ]
        
        
        self.executeRequest(NetworkManager.apiService + "client/ProductReviews" , .post, parameters, JSONEncoding.default, headers) { (data, message, isSuccess) in
            
            callBack(data, message, isSuccess)
            
        }
    }
    
    func apiPostProductReviews(product_id: String, ReviewText: String, Rating: String, CustomerGUID: String, callBack: @escaping (Any, String, Bool) -> ()) -> Void {
        
        let headers = [
            "Authorization" : NetworkManager.apiProductsToken,
            "Content-Type": "application/json"
        ]
        
        let ProductReviewRequest: Dictionary<String,Any> = [
            "Title": "Dịch vụ tốt",
            "ReviewText": ReviewText,
            "Rating": Rating,
            "DisplayCaptcha": "true",
            "CanCurrentCustomerLeaveReview": "true",
            "SuccessfullyAdded": "false",
            "Result": "Abc"
        ]
        
        let parameters: Dictionary<String,Any> = [
            "ApiSecretKey": NetworkManager.ApiSecretKey,
            "CustomerGUID": CustomerGUID,
            "StoreId": NetworkManager.StoreId,
            "ProductId": product_id,
            "LanguageId": NetworkManager.LanguageId,
            "CaptchaValid": "true",
            "ProductReviewRequest": ProductReviewRequest
        ]
        
        
        self.executeRequest(NetworkManager.apiService + "client/AddProductReview" , .post, parameters, JSONEncoding.default, headers) { (data, message, isSuccess) in
            
            callBack(data, message, isSuccess)
            
        }
    }
}

class NetworkDateFormatter: DateFormatter {
    
    init(_ dateFormat:String) {
        super.init()
        self.dateFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

