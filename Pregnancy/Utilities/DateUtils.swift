//
//  DateUtils.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/28/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class DateUtils: NSObject {
    
    let dateFormatter: DateFormatter = DateFormatter()
    
    class var shareInstance : DateUtils {
        struct Static {
            static let instance : DateUtils = DateUtils()
        }
        return Static.instance
    }
    
    func dateFromStringOrToday(string dataDate: String, format typeFormat: String) -> Date {
        var date = self.dateFromString(string: dataDate, defaultDate: nil, format: typeFormat)
        if ( date == nil) {
            date = Date()
        }
        return date!
    }
    
    func dateFromString(string dataDate: String, defaultDate date: Date?, format typeFormat: String) -> Date? {
        //        let date = Date()
        
        dateFormatter.dateFormat = typeFormat
        if let date = dateFormatter.date(from: dataDate) {
            return date
        }
        
        return date
    }
    

}
