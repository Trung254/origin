//
//  Utils.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 2/23/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import Foundation
import UIKit


func formatAttributeString(value:String)->NSMutableAttributedString{
//    let formatter = NumberFormatter()
//    formatter.numberStyle = .decimal
//    formatter.groupingSeparator = "."
//    formatter.decimalSeparator = "."
//    formatter.usesGroupingSeparator = true
//    formatter.minimumFractionDigits = 2
//    let number: NSNumber = NSNumber(value:value)
//    let formattedString = formatter.string(for: number)
//
//    if let result = formattedString{
//        let format = String(format: "%@0đ", result)
//
//    }
    var attributeString: NSMutableAttributedString? = nil
    attributeString =  NSMutableAttributedString(string: value)
    
    attributeString?.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, (attributeString?.length)!))
    return attributeString!
}

func formatString(value:Float)->String{
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.groupingSeparator = "."
    formatter.decimalSeparator = ","
    formatter.usesGroupingSeparator = true
    formatter.minimumFractionDigits = 0
    let number: NSNumber = NSNumber(value:value)
    let formattedString = formatter.string(for: number)
    if let result = formattedString{
        let format = String(format: "%@ đ", result)
        return format
    }else{
        return ""
    }
}

extension UIColor {
    
    convenience init(hexColor: Int, alpha:Float) {
        let components = (
            R: CGFloat((hexColor >> 16) & 0xff) / 255,
            G: CGFloat((hexColor >> 08) & 0xff) / 255,
            B: CGFloat((hexColor >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: CGFloat(alpha))
    }
}


