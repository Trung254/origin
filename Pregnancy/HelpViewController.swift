//
//  HelpViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 05/12/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import WebKit
class HelpViewController: BaseViewController {

    @IBOutlet weak var btnHelpView: UIButton!
    @IBOutlet weak var webVHelpView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       let htmlPath = Bundle.main.path(forResource: "test", ofType: "html")
        let url = URL(fileURLWithPath: htmlPath!)
        let req = URLRequest(url: url)
        webVHelpView.loadRequest(req)
        
        addBackButton()
        
         let questionIcon = UIImage(named: "hoi")
        addIconToButton(btn: btnHelpView, img: questionIcon!)
        
        btnHelpView.setTitle("Xem tất cả các trợ giúp", for: UIControl.State.normal)
        btnHelpView.contentHorizontalAlignment = .left
        btnHelpView.titleEdgeInsets = UIEdgeInsets(top: 0, left: 60, bottom: 0, right: 0)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        self.hidesBottomBarWhenPushed = true
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setTitle(title: "Thông tin")
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    func addBackButton() {
        self.navigationItem.leftItemsSupplementBackButton = false
        let backBtn = UIButton()
        backBtn.setImage(UIImage(named: "Back"), for: UIControl.State.normal)
        backBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backBtn.addTarget(self, action: #selector(self.doBack(_:)), for: .touchUpInside)
        //backBtn.addTarget(self, action: #selector(self.addObserver(_:forKeyPath:options:context:)), for: UIControl.Event.touchUpInside)
        let backView = UIBarButtonItem()
        backView.customView = backBtn
        
        self.navigationItem.leftBarButtonItem = backView
    }
    
    @IBAction func doBack(_ : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func addIconToButton(btn: UIButton, img : UIImage){
        let imgQuestionView = UIImageView(frame: CGRect(x: 20, y: 15, width: 30, height: 30))
        imgQuestionView.image = img
        btn.addSubview(imgQuestionView)
    }

}
